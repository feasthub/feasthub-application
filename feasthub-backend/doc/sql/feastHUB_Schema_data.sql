-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: feasthub
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB
drop schema feasthub;
create schema feasthub;
use feasthub;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apps_secrets`
--

DROP TABLE IF EXISTS `apps_secrets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_secrets` (
  `APPS_SECRET_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `APPS_NAME` varchar(100) DEFAULT NULL,
  `CLIENT_ID` varchar(100) DEFAULT NULL,
  `CLIENT_SECRET` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`APPS_SECRET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_secrets`
--

LOCK TABLES `apps_secrets` WRITE;
/*!40000 ALTER TABLE `apps_secrets` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_secrets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_types_to_services_mapping`
--

DROP TABLE IF EXISTS `business_types_to_services_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_types_to_services_mapping` (
  `BUSINESS_TYPES_TO_SERVICE_MAPPING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BUSINESS_TYPES_TAGS` varchar(100) DEFAULT NULL,
  `SERVICES_OFFERING_TYPES` varchar(500) DEFAULT NULL,
  `ADDITIONAL_SERVICE_OFFERING` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`BUSINESS_TYPES_TO_SERVICE_MAPPING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_types_to_services_mapping`
--

LOCK TABLES `business_types_to_services_mapping` WRITE;
/*!40000 ALTER TABLE `business_types_to_services_mapping` DISABLE KEYS */;
INSERT INTO `business_types_to_services_mapping` VALUES (1,'FH','IN_PREMISES_COOKED_FOOD','IN_PREMISES_COOKED_FOOD_DELIVERY'),(2,'FH','IN_PREMISES_COOKED_FOOD','IN_PREMISES_COOKED_FOOD_TAKEAWAY'),(3,'FH','BULK_COOKING','IN_PREMISES_COOKED_FOOD_DELIVERY'),(4,'FH','BULK_COOKING','IN_PREMISES_COOKED_FOOD_TAKEAWAY'),(5,'FH','SPECIALITY_FOOD','IN_PREMISES_COOKED_FOOD_DELIVERY'),(6,'FH','SPECIALITY_FOOD','IN_PREMISES_COOKED_FOOD_TAKEAWAY'),(7,'FH','SPECIALITY_FOOD','FINE_DINING_EXPERIENCE_AT_CUSTOMER_PREMISES'),(8,'FH_FRANCHISE','IN_PREMISES_COOKED_FOOD','IN_PREMISES_COOKED_FOOD_DELIVERY'),(9,'FH_FRANCHISE','IN_PREMISES_COOKED_FOOD','IN_PREMISES_COOKED_FOOD_TAKEAWAY'),(10,'FH_FRANCHISE','BULK_COOKING','IN_PREMISES_COOKED_FOOD_DELIVERY'),(11,'FH_FRANCHISE','BULK_COOKING','IN_PREMISES_COOKED_FOOD_TAKEAWAY');
/*!40000 ALTER TABLE `business_types_to_services_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current_featured_items`
--

DROP TABLE IF EXISTS `current_featured_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current_featured_items` (
  `CURRENT_FEATURED_ITEM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVERY_LOCATION_ID` bigint(20) DEFAULT NULL,
  `EVENT_ID` bigint(20) DEFAULT NULL,
  `START_DATE` bigint(20) DEFAULT NULL,
  `END_DATE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CURRENT_FEATURED_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current_featured_items`
--

LOCK TABLES `current_featured_items` WRITE;
/*!40000 ALTER TABLE `current_featured_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `current_featured_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current_promotions`
--

DROP TABLE IF EXISTS `current_promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current_promotions` (
  `CURRENT_PROMOTION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVERY_LOCATION_ID` bigint(20) DEFAULT NULL,
  `EVENT_ID` bigint(20) DEFAULT NULL,
  `PROMO_CODE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CURRENT_PROMOTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current_promotions`
--

LOCK TABLES `current_promotions` WRITE;
/*!40000 ALTER TABLE `current_promotions` DISABLE KEYS */;
/*!40000 ALTER TABLE `current_promotions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_charge`
--

DROP TABLE IF EXISTS `delivery_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_charge` (
  `DELIVERY_CHARGE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BUSINESS_TYPES` BigInt(20) DEFAULT NULL,
  `SERVICES_OFFERING_TYPES` BigInt(20) DEFAULT NULL,
  `COUNTRY_CODE` varchar(10) DEFAULT NULL,
  `STATE_CODE` varchar(10) DEFAULT NULL,
  `CITY_CODE` varchar(10) DEFAULT NULL,
  `DISTANCE_LOWER_RANGE` int(11) DEFAULT NULL,
  `DISTANCE_UPPER_RANGE` int(11) DEFAULT NULL,
  `PRICE_IN_FP` int(11) DEFAULT NULL,
  `PRICE_IN_CASH` int(11) DEFAULT NULL,
  `FEES_APPLCABILITY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DELIVERY_CHARGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_charge`
--

LOCK TABLES `delivery_charge` WRITE;
/*!40000 ALTER TABLE `delivery_charge` DISABLE KEYS */;
INSERT INTO `delivery_charge` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `delivery_charge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_location`
--

DROP TABLE IF EXISTS `delivery_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_location` (
  `DELIVERY_LOCATION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COUNTRY_CODE` varchar(10) DEFAULT NULL,
  `STATE_CODE` varchar(10) DEFAULT NULL,
  `CITY_CODE` varchar(5) DEFAULT NULL,
  `POSTAL_CODE` varchar(10) DEFAULT NULL,
  `AREA_NAME` varchar(20) DEFAULT NULL,
  `LATTITUDE` double DEFAULT NULL,
  `LONGITUDE` double DEFAULT NULL,
  `DISTANCE_UPPER_RANGE_OF_COVERAGE_AREA` int(2) DEFAULT NULL,
  PRIMARY KEY (`DELIVERY_LOCATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_location`
--

LOCK TABLES `delivery_location` WRITE;
/*!40000 ALTER TABLE `delivery_location` DISABLE KEYS */;
INSERT INTO `delivery_location` VALUES (1,'IN','TS','HYD','500084','KONDAPUR',17.4694857,78.3587598,5),(2,'IN','TS','HYD','500048','ATTAPUR',17.365785,78.4211386,5);
/*!40000 ALTER TABLE `delivery_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_cuisine_listing`
--

DROP TABLE IF EXISTS `event_cuisine_listing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_cuisine_listing` (
  `EVENT_CUISINE_LISTING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OFFERED_QUANTITY` int(11) DEFAULT NULL,
  `MAX_OFFERED_QUANTITY_QUICK_SERVICE` int(11) DEFAULT NULL,
  `NOTE` varchar(500) DEFAULT NULL,
  `FOOD_MENU_ID` bigint(20) NOT NULL,
  `EVENT_ID` bigint(20) NOT NULL,
  `FOOD_SERVING_SIZE_CATEGORY_TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`EVENT_CUISINE_LISTING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='max order to be for daily order as available under offered quantity, otherwise MAX_OFFERED_QUANTITY_SHORT_TERM_DELIVERY\r\n\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_cuisine_listing`
--

LOCK TABLES `event_cuisine_listing` WRITE;
/*!40000 ALTER TABLE `event_cuisine_listing` DISABLE KEYS */;
INSERT INTO `event_cuisine_listing` VALUES (1,100,100,NULL,1,1,1),(2,100,100,NULL,2,2,1),(3,100,100,NULL,3,3,1),(4,100,100,NULL,4,4,1),(5,100,100,NULL,5,5,1),(6,100,100,NULL,6,6,1),(7,100,100,NULL,7,7,1),(8,100,100,NULL,8,8,1),(9,100,100,NULL,9,9,1),(10,100,100,NULL,10,10,1),(11,100,100,NULL,11,11,1),(12,100,100,NULL,12,12,1),(13,100,100,NULL,13,13,1),(14,100,100,NULL,14,14,1),(15,100,100,NULL,15,15,1),(16,100,100,NULL,16,16,1),(17,100,100,NULL,24,17,1),(18,100,100,NULL,25,18,1),(19,0,0,NULL,1,19,3),(20,0,0,NULL,3,20,3),(21,0,0,NULL,19,21,3),(22,0,0,NULL,10,22,3);
/*!40000 ALTER TABLE `event_cuisine_listing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_delivery_availbility_part_of_day`
--

DROP TABLE IF EXISTS `event_delivery_availbility_part_of_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_delivery_availbility_part_of_day` (
  `EVENT_DELIVERY_AVAILBILITY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PART_OF_DAY_NAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`EVENT_DELIVERY_AVAILBILITY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_delivery_availbility_part_of_day`
--

LOCK TABLES `event_delivery_availbility_part_of_day` WRITE;
/*!40000 ALTER TABLE `event_delivery_availbility_part_of_day` DISABLE KEYS */;
INSERT INTO `event_delivery_availbility_part_of_day` VALUES (1,'ALL_DAY'),(2,'MORNING'),(3,'DAY_TIME'),(4,'PRIME_TIME'),(5,'NIGHT_TIME');
/*!40000 ALTER TABLE `event_delivery_availbility_part_of_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_delivery_part_of_day_to_slot_mapping`
--

DROP TABLE IF EXISTS `event_delivery_part_of_day_to_slot_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_delivery_part_of_day_to_slot_mapping` (
  `EVENT_DELIVERY_PART_TO_SLOT_MAPPING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENT_DELIVERY_AVAILBILITY_ID` bigint(20) DEFAULT NULL,
  `DELIVERY_SLOTS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`EVENT_DELIVERY_PART_TO_SLOT_MAPPING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='event_delivery_part_of_day_to_slot_mapping';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_delivery_part_of_day_to_slot_mapping`
--

LOCK TABLES `event_delivery_part_of_day_to_slot_mapping` WRITE;
/*!40000 ALTER TABLE `event_delivery_part_of_day_to_slot_mapping` DISABLE KEYS */;
INSERT INTO `event_delivery_part_of_day_to_slot_mapping` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,2),(5,3,3),(6,4,4);
/*!40000 ALTER TABLE `event_delivery_part_of_day_to_slot_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_delivery_slot`
--

DROP TABLE IF EXISTS `event_delivery_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_delivery_slot` (
  `DELIVERY_SLOTS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVERY_SLOT_NAME` varchar(50) NOT NULL,
  `PART_OF_DAY` varchar(50) DEFAULT NULL,
  `BUSINESS_TYPES` varchar(20) DEFAULT NULL,
  `SERVICES_OFFERING_TYPES` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`DELIVERY_SLOTS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_delivery_slot`
--

LOCK TABLES `event_delivery_slot` WRITE;
/*!40000 ALTER TABLE `event_delivery_slot` DISABLE KEYS */;
INSERT INTO `event_delivery_slot` VALUES (1,'MRNG_SLOT','MORNING','FH','FOOD_DELIVERY'),(2,'DAY_TIME_SLOT','DAYTIME','FH','FOOD_DELIVERY'),(3,'PRIME_TIME_SLOT','EVENING','FH','FOOD_DELIVERY'),(4,'LATE_NIGHT_SLOT','LATE_NIGHT','FH','FOOD_DELIVERY');
/*!40000 ALTER TABLE `event_delivery_slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_delivery_slots_time`
--

DROP TABLE IF EXISTS `event_delivery_slots_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_delivery_slots_time` (
  `DELIVERY_SLOTS_TIME_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVERY_SLOTS_ID` int(10) DEFAULT NULL,
  `SLOTS_TIMING_STARTS_AT` time DEFAULT NULL,
  `SLOTS_TIMING_ENDS_AT` time DEFAULT NULL,
  PRIMARY KEY (`DELIVERY_SLOTS_TIME_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='morning  -- \r\nLunch Time -> 1-2, 2-3,3-4\r\nEvening --> 5-6,6-7,7-8\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_delivery_slots_time`
--

LOCK TABLES `event_delivery_slots_time` WRITE;
/*!40000 ALTER TABLE `event_delivery_slots_time` DISABLE KEYS */;
INSERT INTO `event_delivery_slots_time` VALUES (1,1,'07:00:00','08:00:00'),(2,1,'08:00:00','09:00:00'),(3,1,'09:00:00','10:00:00'),(4,2,'12:00:00','13:00:00'),(5,2,'13:00:00','14:00:00'),(6,2,'14:00:00','15:00:00'),(7,3,'19:00:00','20:00:00'),(8,3,'20:00:00','21:00:00'),(9,3,'21:00:00','22:00:00'),(10,3,'22:00:00','23:00:00'),(11,4,'23:00:00','23:59:00'),(12,4,'01:00:00','02:00:00');
/*!40000 ALTER TABLE `event_delivery_slots_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_display_category`
--

DROP TABLE IF EXISTS `event_display_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_display_category` (
  `EVENT_DISPLAY_CATEGORY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DISPLAY_CATEGORY_NAME` varchar(50) NOT NULL DEFAULT '0',
  `DISPLAY_ORDER` int(2) NOT NULL DEFAULT '0',
  `DISPLAY_FLAG` int(1) NOT NULL DEFAULT '0',
  `PROMOTIONAL_COLLECTION_FLAG` int(1) NOT NULL DEFAULT '0',
  `SHOWCASE_IMAGE_URL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`EVENT_DISPLAY_CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_display_category`
--

LOCK TABLES `event_display_category` WRITE;
/*!40000 ALTER TABLE `event_display_category` DISABLE KEYS */;
INSERT INTO `event_display_category` VALUES (1,'FEATURED_ITEMS',0,1,1,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(2,'MOST_POPULAR',1,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(3,'NEW_ARRIVAL',2,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(4,'RICE_BIRYANI',3,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(5,'MEALS COMBO',4,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(6,'MAIN_COURSES',5,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(7,'BEVERAGES',6,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(8,'HEALTHY',7,1,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(9,'CROSS_SALES_ITEMS',0,0,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg'),(10,'BULK_ITEMS',0,0,0,'http://focussnapeat.com/wp-content/uploads/2016/01/howden-market-oakland01.jpg');
/*!40000 ALTER TABLE `event_display_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_display_category_to_event_mapping`
--

DROP TABLE IF EXISTS `event_display_category_to_event_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_display_category_to_event_mapping` (
  `EVENT_DISPLAY_CATEGORY_TO_EVENT_MAPPING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` bigint(20) NOT NULL DEFAULT '0',
  `EVENT_DISPLAY_CATEGORY_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`EVENT_DISPLAY_CATEGORY_TO_EVENT_MAPPING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_display_category_to_event_mapping`
--

LOCK TABLES `event_display_category_to_event_mapping` WRITE;
/*!40000 ALTER TABLE `event_display_category_to_event_mapping` DISABLE KEYS */;
INSERT INTO `event_display_category_to_event_mapping` VALUES (1,1,1),(2,3,1),(3,10,1),(4,5,1),(5,1,2),(6,9,2),(7,6,2),(8,10,2),(9,12,2),(10,13,2),(11,5,3),(12,8,3),(13,15,3),(14,11,3),(15,1,4),(16,9,4),(17,12,5),(18,13,5),(19,1,6),(20,2,6),(21,3,6),(22,4,6),(23,6,6),(24,7,6),(25,8,6),(26,9,6),(27,10,6),(28,11,6),(29,14,6),(30,15,6),(31,5,8),(32,9,8),(33,16,9),(34,17,0),(35,19,10),(36,20,10),(37,21,10),(38,22,10);
/*!40000 ALTER TABLE `event_display_category_to_event_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_items_availability`
--

DROP TABLE IF EXISTS `event_items_availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_items_availability` (
  `EVENT_ITEMS_AVAILABILITY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` bigint(20) DEFAULT NULL,
  `LISTING_DATE` date DEFAULT NULL,
  `OFFERED_QUANTITY` int(4) DEFAULT NULL,
  `MAX_OFFERED_QUANTITY_FOR_QUICK_SERVICE` int(4) DEFAULT NULL,
  `QUICK_SERVICE_SOLD_QUANTITY` int(4) DEFAULT NULL,
  `TOTAL_SOLD_QUANTITY` int(4) DEFAULT NULL,
  `QUICK_SERVICE_AVAILABLE_QUANTITY` int(4) DEFAULT NULL,
  `CURRENT_AVAILABLE_QUANTITY` int(4) DEFAULT NULL,
  `BLOCKED_QUANTITY` int(4) DEFAULT NULL,
  `SCHEDULED_SOLD_QUANTITY` int(4) DEFAULT NULL,
  PRIMARY KEY (`EVENT_ITEMS_AVAILABILITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='the way it works is the first entry to be made on first order for the event and subsequent entry should be updated into this table.\r\n\r\n                                                                               	1	2\r\n		\r\nOFFERED_QUANTITY                                      	100	100\r\nMAX_OFFERED_QUANTITY_QUICK_SERVICE	50	50\r\nQUICK_SERVICE_SOLD_QUANTITY                                 40	40\r\nSERVICE_OFFERING_SOLD_QUANTITY         	100	0\r\nTOTAL_SOLD_QUANTITY                               	140	40\r\nQUICK_SERVICE_AVAILABLE_QUANTITY      	10	10\r\nCURRENT_AVAILABLE_QUANTITY                	10	60\r\n\r\n\r\ncurrent available quantity is for display at apps, if the total dold quantity is more than the offered quantity, than the available quantity will be as per quick_service_sold_quantity. This is required to make available of the item for normal delivery, however currently no restricitions as of now for Scheduled order and bulk ordering. Service offering sold type would be scheduled ordering or bulk ordering. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_items_availability`
--

LOCK TABLES `event_items_availability` WRITE;
/*!40000 ALTER TABLE `event_items_availability` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_items_availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_cross_sales_event_items`
--

DROP TABLE IF EXISTS `events_cross_sales_event_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_cross_sales_event_items` (
  `EVENT_CROSS_SALES_EVENT_ITEM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MAIN_EVENT_ID` bigint(20) DEFAULT NULL,
  `FOOD_MENU_ID` bigint(20) DEFAULT NULL,
  `CROSS_SALES_EVENT_ID` bigint(20) DEFAULT NULL,
  `CROSS_SALES_FOOD_MENU_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`EVENT_CROSS_SALES_EVENT_ITEM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_cross_sales_event_items`
--

LOCK TABLES `events_cross_sales_event_items` WRITE;
/*!40000 ALTER TABLE `events_cross_sales_event_items` DISABLE KEYS */;
INSERT INTO `events_cross_sales_event_items` VALUES (1,1,1,16,16),(2,2,2,14,14),(3,2,2,15,15),(4,2,2,16,16),(5,3,3,14,14),(6,3,3,15,15),(7,3,3,16,16),(8,4,4,14,14),(9,4,4,15,15),(10,4,4,16,16),(11,5,5,16,16),(12,6,6,14,14),(13,6,6,15,15),(14,6,6,16,16),(15,7,7,14,14),(16,7,7,15,15),(17,7,7,16,16),(18,8,8,14,14),(19,8,8,15,15),(20,8,8,16,16),(21,9,9,16,16),(22,10,10,14,14),(23,10,10,15,15),(24,10,10,16,16),(25,11,11,14,14),(26,11,11,15,15),(27,11,11,16,16),(28,12,12,16,16),(29,13,13,16,16);
/*!40000 ALTER TABLE `events_cross_sales_event_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `external_resource_endpoints`
--

DROP TABLE IF EXISTS `external_resource_endpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_resource_endpoints` (
  `EXTERNAL_RESOURCE_ENDPOINT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXTERNAL_SERVER_NAME` varchar(100) DEFAULT NULL,
  `RESOURCE_NAME` varchar(100) DEFAULT NULL,
  `RESOURCE_ENDPOINT_URL` varchar(500) DEFAULT NULL,
  `SCOPES` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`EXTERNAL_RESOURCE_ENDPOINT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `external_resource_endpoints`
--

LOCK TABLES `external_resource_endpoints` WRITE;
/*!40000 ALTER TABLE `external_resource_endpoints` DISABLE KEYS */;
/*!40000 ALTER TABLE `external_resource_endpoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feast_complaints`
--

DROP TABLE IF EXISTS `feast_complaints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feast_complaints` (
  `FEAST_COMPLAINT_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `OS_COMPLAINT_NUMBER` varchar(50) DEFAULT NULL,
  `EVENT_ID` bigint(20) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `STATUS` varchar(500) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`FEAST_COMPLAINT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feast_complaints`
--

LOCK TABLES `feast_complaints` WRITE;
/*!40000 ALTER TABLE `feast_complaints` DISABLE KEYS */;
/*!40000 ALTER TABLE `feast_complaints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feast_event_review`
--

DROP TABLE IF EXISTS `feast_event_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feast_event_review` (
  `FEAST_EVENT_REVIEW_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `FOOD_MENU_ID` bigint(20) NOT NULL,
  `EVENT_ID` bigint(20) NOT NULL,
  `RATING` varchar(100) NOT NULL,
  `REVIEW_COMMENT` varchar(500) NOT NULL,
  `REVIEW_DATE` date NOT NULL,
  `MARKED_AS_OFFENSIVE` varchar(1) NOT NULL,
  `DELETED_FLAG` varchar(1) NOT NULL,
  PRIMARY KEY (`FEAST_EVENT_REVIEW_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feast_event_review`
--

LOCK TABLES `feast_event_review` WRITE;
/*!40000 ALTER TABLE `feast_event_review` DISABLE KEYS */;
INSERT INTO `feast_event_review` VALUES (1,1,1,1,'5','Awesome biryani--','2016-04-22','0','0'),(2,1,2,2,'3','Average biryani','2016-04-22','0','0'),(3,1,1,1,'4','average biryani','2016-04-22','0','0'),(4,1,1,1,'5','best biryani I ever had','2016-04-22','0','0');
/*!40000 ALTER TABLE `feast_event_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feast_point_balance`
--

DROP TABLE IF EXISTS `feast_point_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feast_point_balance` (
  `FEAST_POINT_BALANCE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `FP_TYPE` varchar(100) DEFAULT NULL,
  `FP_BALANCE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`FEAST_POINT_BALANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feast_point_balance`
--

LOCK TABLES `feast_point_balance` WRITE;
/*!40000 ALTER TABLE `feast_point_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `feast_point_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feast_point_general_rule`
--

DROP TABLE IF EXISTS `feast_point_general_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feast_point_general_rule` (
  `FEAST_POINT_GENERAL_RULE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FEAST_POINT_BALANCE_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `FP_POINTS` int(11) DEFAULT NULL,
  `FP_EXPIRY_DATES` date DEFAULT NULL,
  `PROMOTIONS_START_DATE` date DEFAULT NULL,
  `PROMOTIONS_START_TIME` time DEFAULT NULL,
  `PROMOTION_EXPIRY_DATE` date DEFAULT NULL,
  `PROMOTION_EXPIRY_TIME` time DEFAULT NULL,
  `PROMOTION_EXPIRY_FLAG` varchar(1) DEFAULT NULL,
  `COUPON_CODE` varchar(100) DEFAULT NULL,
  `COUPON_CODE_EXPIRY_DATE` date DEFAULT NULL,
  `COUPNE_CODE_EXPIRED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`FEAST_POINT_GENERAL_RULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feast_point_general_rule`
--

LOCK TABLES `feast_point_general_rule` WRITE;
/*!40000 ALTER TABLE `feast_point_general_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `feast_point_general_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fh_events`
--

DROP TABLE IF EXISTS `fh_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_events` (
  `EVENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BUSINESS_TYPE` bigint(20) DEFAULT NULL,
  `SERVICE_OFFERING_TYPE` bigint(20) DEFAULT NULL,
  `ADDITIONAL_SERVICE_OFFERING_TYPE` bigint(20) DEFAULT NULL,
  `EVENT_OFFERED_BY` bigint(20) DEFAULT NULL,
  `EVENT_NAME` varchar(100) DEFAULT NULL,
  `EVENT_LISTING_START_DATE` date DEFAULT NULL,
  `EVENT_LISTING_START_TIME` time DEFAULT NULL,
  `EVENT_LISTING_END_DATE` date DEFAULT NULL,
  `EVENT_LISTING_END_TIME` time DEFAULT NULL,
  `EVENT_LISTING_NO_OF_DAYS_LIVE` int(11) DEFAULT NULL,
  `EVENT_LISTING_SHOWTIME_START_TIME` time DEFAULT NULL,
  `EVENT_LISTING_SHOWTIME_END_TIME` time DEFAULT NULL,
  `DELIVERY_LOCATION_ID` bigint(20) DEFAULT NULL,
  `EVENT_IMAGE_UPLOADED_FLAG` tinyint(1) DEFAULT NULL,
  `USER_CAN_BLOCK_ONLINE_FLAG` tinyint(1) DEFAULT NULL,
  `USER_CAN_PURCHASE_ONLINE_FLAG` tinyint(1) DEFAULT NULL,
  `ADDITIONAL_COMMENTS` varchar(500) DEFAULT NULL,
  `ITEM_AVAILABLE_FOR_SCHEDULED_ORDERING` tinyint(1) DEFAULT NULL,
  `ITEM_AVAILABLE_FOR_PROMOTION` tinyint(1) DEFAULT NULL,
  `TAX_STRUCTURE_ID` bigint(20) DEFAULT NULL,
  `DELIVERY_CHARGE_ID` bigint(20) DEFAULT NULL,
  `EVENT_DELIVERY_AVAILBILITY_ID` bigint(20) DEFAULT NULL,
  `OFFERED_PRICE` double DEFAULT NULL,
  `ADD_COMMISION` double DEFAULT NULL,
  `ADD_TAXES` double DEFAULT NULL,
  `ADD_DELIVERY` double DEFAULT NULL,
  `ADD_OTHER_CHARGE` double DEFAULT NULL,
  `FINAL_OFFERED_PRICE` double DEFAULT NULL,
  `FINAL_OFFERED_PRICE_FP` double DEFAULT NULL,
  `EVENT_AVERAGE_REVIEW` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`EVENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='ITEM_AVAILABLE_FOR_SCHEDULED_ORDERING  -- IF SET then the item will be available for scheduled ordering and if not then the expiry date needs to set, if the item needs to be converted for not available as scheduled ordering then the future expire date needs tobe set. \r\n\r\nEVENT_DELIVERY_AVAILBILITY_ID -- > event_delivery_availbility_part_of_day.EVENT_DELIVERY_AVAILBILITY_ID\r\n\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_events`
--

LOCK TABLES `fh_events` WRITE;
/*!40000 ALTER TABLE `fh_events` DISABLE KEYS */;
INSERT INTO `fh_events` VALUES 
(1,30,43,95,0,'Kachchi Yakhnni Mutton Biryani','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,320,0,25,0,0,345,345,0),
(2,30,43,95,0,'Dal Tadka','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,150,0,5,0,0,160,160,0),
(3,30,43,95,0,'Mutton Rogan Josh','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,350,0,25,0,0,375,375,0),
(4,30,43,95,0,'Palak Paneer','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,175,0,20,0,0,195,195,0),
(5,30,43,95,0,'Veg Daliya','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,90,0,10,0,0,100,100,0),
(6,30,43,95,0,'Dal Makhani','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,150,0,15,0,0,165,165,0),
(7,30,43,95,0,'Paneer Butter Masala','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,150,0,15,0,0,165,165,0),
(8,30,43,95,0,'Healthy Chicken Biryani','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,250,0,20,0,0,270,270,0),
(9,30,43,95,0,'Pancharatni Daal','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,140,0,10,0,0,150,150,0),
(10,30,43,95,0,'Mixed Veg','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,150,0,20,0,0,170,170,0),
(11,30,43,95,0,'Non Veg Meals','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,320,0,30,0,0,350,350,0),
(12,30,43,95,0,'Veg Meals','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,250,0,20,0,0,270,270,0),
(13,30,43,95,0,'Roti','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,25,0,10,0,0,35,35,0),
(14,30,43,95,0,'Jowar Roti','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,40,0,10,0,0,50,50,0),
(15,30,43,95,0,'Packaged Water','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,10,0,5,0,0,15,15,0),
(16,30,43,95,0,'FH Ghee','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,20,0,10,0,0,30,30,0),
(17,30,43,95,0,'Veg Thali ( Make your Thali)','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,315,0,50,0,0,365,365,0),
(18,30,43,95,0,'Mutton Rogan Josh With Roti','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,350,0,50,0,0,400,400,0),
(19,30,43,100,0,'Mutton Rogan Josh With Roti - BULK','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,0,1,NULL,1,1500,0,200,0,0,1700,1700,0),
(20,30,43,100,0,'Kachchi Yakhnni Mutton Biryani - BULK','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,1200,0,200,0,0,1400,1400,0),
(21,30,43,100,0,'Plain Rice - BULK','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,200,0,50,0,0,250,250,0),
(22,30,43,100,0,'Daal - BULK','2016-04-15','01:01:00','2017-03-31','01:01:00',365,'00:00:00','00:00:00',1,1,0,1,NULL,1,1,1,NULL,1,450,0,100,0,0,550,550,0);
/*!40000 ALTER TABLE `fh_events` ENABLE KEYS */;
UNLOCK TABLES;

update fh_events set SERVICE_OFFERING_TYPE = 11 where SERVICE_OFFERING_TYPE = 43;
update fh_events set BUSINESS_TYPE = 5 where BUSINESS_TYPE = 30;
update fh_events set ADDITIONAL_SERVICE_OFFERING_TYPE = 0 where ADDITIONAL_SERVICE_OFFERING_TYPE = 95;
update fh_events set ADDITIONAL_SERVICE_OFFERING_TYPE = 5 where ADDITIONAL_SERVICE_OFFERING_TYPE = 100;
update fh_events set EVENT_OFFERED_BY=2;


--
-- Table structure for table `fh_seller_commision_fees`
--

DROP TABLE IF EXISTS `fh_seller_commision_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_seller_commision_fees` (
  `FH_SELLER_COMMISION_FEES_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BUSINESS_TYPES` varchar(20) DEFAULT NULL,
  `SERVICES_OFFERING_TYPES` varchar(100) DEFAULT NULL,
  `MEMBERSHIP_TYPE_ID` varchar(50) DEFAULT NULL,
  `FEES_TYPE` varchar(50) DEFAULT NULL,
  `FEES_APPLCABILITY` varchar(50) DEFAULT NULL,
  `PERCENTAGE_CHARGE` double DEFAULT NULL,
  PRIMARY KEY (`FH_SELLER_COMMISION_FEES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_seller_commision_fees`
--

LOCK TABLES `fh_seller_commision_fees` WRITE;
/*!40000 ALTER TABLE `fh_seller_commision_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fh_seller_commision_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fh_seller_memberships`
--

DROP TABLE IF EXISTS `fh_seller_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_seller_memberships` (
  `SELLER_MEMBERSHIP_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `BUSINESS_TYPES_KEY` bigint(20) DEFAULT NULL,
  `SERVICE_ID_KEY` bigint(20) DEFAULT NULL,
  `MEMBERSHIP_TYPE_ID_KEY` bigint(20) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `EXPIRY_DATE` date DEFAULT NULL,
  `APPLICABLE_TAX_STRUCTURE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`SELLER_MEMBERSHIP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_seller_memberships`
--

LOCK TABLES `fh_seller_memberships` WRITE;
/*!40000 ALTER TABLE `fh_seller_memberships` DISABLE KEYS */;
/*!40000 ALTER TABLE `fh_seller_memberships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fh_user`
--

DROP TABLE IF EXISTS `fh_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fh_user` (
	`USER_ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`SALUTATION` VARCHAR(10) NULL DEFAULT NULL,
	`FIRST_NAME` VARCHAR(100) NULL DEFAULT NULL,
	`MIDDLE_NAME` VARCHAR(100) NULL DEFAULT NULL,
	`LAST_NAME` VARCHAR(100) NULL DEFAULT NULL,
	`USER_NAME` VARCHAR(100) NULL DEFAULT NULL,
	`COMPANY_NAME` VARCHAR(100) NULL DEFAULT NULL,
	`PASSWORD` VARCHAR(500) NULL DEFAULT NULL,
	`DEFAULT_EMAIL_ID` VARCHAR(100) NULL DEFAULT NULL,
	`DEFAULT_MOBILE_NUMBER` VARCHAR(100) NOT NULL,
	`DEFAULT_LANDLINE_NUMBER` VARCHAR(100) NULL DEFAULT NULL,
	`DATE_OF_BIRTH_INCORPORTAION` DATE NULL DEFAULT NULL,
	`GENDER` VARCHAR(1) NULL DEFAULT NULL,
	`USER_TYPE` VARCHAR(10) NULL DEFAULT NULL,
	`PRIMARY_ROLE` VARCHAR(10) NULL DEFAULT NULL,
	`USER_AUTH_METHOD` VARCHAR(10) NULL DEFAULT NULL,
	`EMAIL_VERIFIED_FLAG` TINYINT(4) NULL DEFAULT NULL,
	`MOBILE_VERIFIED_FLAG` TINYINT(4) NULL DEFAULT NULL,
	`REFERRAL_ID` BIGINT(20) NULL DEFAULT NULL,
	`PROFILE_PHOTO` VARCHAR(10) NULL DEFAULT NULL,
	`SUBSCRIPTION_FLAG` VARCHAR(1) NULL DEFAULT NULL,
	`REGISTERED_COUNTRY_CODE` VARCHAR(4) NULL DEFAULT NULL,
	`LANGUAGE_CODE` VARCHAR(4) NULL DEFAULT NULL,
	PRIMARY KEY (`USER_ID`),
	UNIQUE INDEX `DEFAULT_MOBILE_NUMBER` (`DEFAULT_MOBILE_NUMBER`),
	UNIQUE INDEX `DEFAULT_EMAIL_ID` (`DEFAULT_EMAIL_ID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=COMPACT
AUTO_INCREMENT=4
;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fh_user`
--

LOCK TABLES `fh_user` WRITE;
/*!40000 ALTER TABLE `fh_user` DISABLE KEYS */;
INSERT INTO `fh_user` (`USER_ID`, `SALUTATION`, `FIRST_NAME`, `MIDDLE_NAME`, `LAST_NAME`, `USER_NAME`, `COMPANY_NAME`, `PASSWORD`, `DEFAULT_EMAIL_ID`, `DEFAULT_MOBILE_NUMBER`, `DEFAULT_LANDLINE_NUMBER`, `DATE_OF_BIRTH_INCORPORTAION`, `GENDER`, `USER_TYPE`, `PRIMARY_ROLE`, `USER_AUTH_METHOD`, `EMAIL_VERIFIED_FLAG`, `MOBILE_VERIFIED_FLAG`, `REFERRAL_ID`, `PROFILE_PHOTO`, `SUBSCRIPTION_FLAG`, `REGISTERED_COUNTRY_CODE`, `LANGUAGE_CODE`) VALUES (1, 'mr', 'user1', NULL, 'user1', 'user1', NULL, 'user1', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fh_user` (`USER_ID`, `SALUTATION`, `FIRST_NAME`, `MIDDLE_NAME`, `LAST_NAME`, `USER_NAME`, `COMPANY_NAME`, `PASSWORD`, `DEFAULT_EMAIL_ID`, `DEFAULT_MOBILE_NUMBER`, `DEFAULT_LANDLINE_NUMBER`, `DATE_OF_BIRTH_INCORPORTAION`, `GENDER`, `USER_TYPE`, `PRIMARY_ROLE`, `USER_AUTH_METHOD`, `EMAIL_VERIFIED_FLAG`, `MOBILE_VERIFIED_FLAG`, `REFERRAL_ID`, `PROFILE_PHOTO`, `SUBSCRIPTION_FLAG`, `REGISTERED_COUNTRY_CODE`, `LANGUAGE_CODE`) VALUES (2, NULL, 'feasthub', NULL, NULL, 'feasthub', 'Feasthub', 'feasthub', 'info.feasthub@gmail.com', '9988771234', NULL, '2016-03-31', NULL, 'BUSINESS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `fh_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_category`
--

DROP TABLE IF EXISTS `food_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_category` (
  `FOOD_CATEGORY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `KEY_ID` varchar(100) DEFAULT NULL,
  `FOOD_CATEGORY_CODE` varchar(200) NOT NULL,
  `FOOD_CATEGORY_NAME` varchar(200) DEFAULT NULL,
  `FOOD_CATEGORY_DESC` varchar(200) DEFAULT NULL,
  `PARENT_ID` varchar(100) DEFAULT NULL,
  `FLAT_TAGS_FLAG` int(1) DEFAULT NULL,
  `PARENT_NODE_FLAG` int(1) DEFAULT NULL,
  PRIMARY KEY (`FOOD_CATEGORY_ID`),
  UNIQUE KEY `KEY_ID` (`KEY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_category`
--

LOCK TABLES `food_category` WRITE;
/*!40000 ALTER TABLE `food_category` DISABLE KEYS */;
INSERT INTO `food_category` VALUES (1,'P_1','TYPE','TYPE','FOOD TYPE',NULL,0,1),(2,'C_11','VEG','VEG','Veg','P_1',0,0),(3,'C_12','NVEG','NVEG','Non Veg','P_1',0,0),(4,'P_2','COURSES','COURSES','Courses',NULL,0,1),(5,'C_21','BEVERAGES','BEVERAGES','Beverages','P_2',0,0),(6,'C_22','SWEETS','SWEETS','Sweets','P_2',0,0),(7,'C_23','MAIN_COURSE','MAIN_COURSE','Main Course','P_2',0,0),(8,'C_24','STARTER','STARTER','Starter','P_2',0,0),(9,'C_25','SOUP','SOUP','Soup','P_2',0,0),(10,'C_26','SALADS','SALADS','Salads','P_2',0,0),(11,'C_27','SNACK','SNACK','Snack','P_2',0,0),(12,'P_3','CATEGORY','CATEGORY','FOOD CATEGORY',NULL,0,1),(13,'C_31','APPETIZERS','APPETIZERS','Appetizers','P_3',0,0),(14,'C_32','BEVERAGES','BEVERAGES','Beverages','P_3',0,0),(15,'C_33','CONDIMETS','CONDIMETS','Condiments','P_3',0,0),(16,'C_34','GRAVIES','GRAVIES','Gravies','P_3',0,0),(17,'C_35','SIDE_DISH','SIDE_DISH','Side Dishes','P_3',0,0),(18,'C_36','RICE_VARIETIES','RICE_VARIETIES','Entree - Rice Varieties','P_3',0,0),(19,'C_37','ROTIES','ROTIES','Roties','P_3',0,0),(20,'C_38','SNACKS','SNACKS','Snacks','P_3',0,0),(21,'C_39','INDIAN_SWEETS','INDIAN_SWEETS','Indian Sweets','P_3',0,0),(22,'C_311','SOUPS','SOUPS','Soups','P_3',0,0),(23,'C_312','PACKAGED_MEALS','PACKAGED_MEALS','Packaged Meals','P_3',0,0),(24,'C_313','HEALTH_DRINKS','HEALTH_DRINKS','Healthy Drinks','P_3',0,0),(25,'P_4','CUISINE','CUISINE','Cuisine',NULL,0,1),(26,'C_41','ANDHRA','ANDHRA','Andhra','P_4',0,0),(27,'C_42','BENGALI','BENGALI','Bengali','P_4',0,0),(28,'C_43','GUJARATI','GUJARATI','Gujarati','P_4',0,0),(29,'C_44','HYDERABADI','HYDERABADI','Hyderabadi','P_4',0,0),(30,'C_45','KARNATKA','KARNATKA','Karnataka','P_4',0,0),(31,'C_46','KASHMIRI','KASHMIRI','Kashmiri','P_4',0,0),(32,'C_47','MAHARASTRIAN','MAHARASTRIAN','Maharashtrian','P_4',0,0),(33,'C_48','MALVANI','MALVANI','Malvani','P_4',0,0),(34,'C_49','MARWARI','MARWARI','Marwari','P_4',0,0),(35,'C_411','MOGLAI','MOGLAI','Moghlai','P_4',0,0),(36,'C_412','ORIA','ORIA','Orissa','P_4',0,0),(37,'C_413','PARSI','PARSI','Parsi','P_4',0,0),(38,'C_414','PUNJABI','PUNJABI','Punjabi','P_4',0,0),(39,'C_415','RAJASTHANI','RAJASTHANI','Rajasthani','P_4',0,0),(40,'C_416','SATVIC','SATVIC','Satvic','P_4',0,0),(41,'C_417','SINDHI','SINDHI','Sindhi','P_4',0,0),(42,'C_418','CHETTINADU','CHETTINADU','Cheetinadu','P_4',0,0),(43,'C_419','VEG_FESTIVE_FOOD','VEG_FESTIVE_FOOD','Vegetarian Festive Food','P_4',0,0),(44,'C_4111','INDIAN','INDIAN','Indian','P_4',0,0),(45,'C_4112','FUSION','FUSION','Fusion','P_4',0,0),(46,'C_4113','KONKANI','KONKANI','Konkani','P_4',0,0),(47,'C_4114','ASAMEES','ASAMEES','Asamiya','P_4',0,0),(48,'C_4115','AVADH','AVADH','Avadhi','P_4',0,0),(49,'C_4116','KATIWADI','KATIWADI','Katiwadi','P_4',0,0),(50,'C_4117','TELANGANA','TELANGANA','Telangana','P_4',0,0),(51,'C_4118','PRASADAM','PRASADAM','Prasad','P_4',0,0),(52,'C_4119','HINDU_FASTING_FOOD','HINDU_FASTING_FOOD','Hindu Fasting food','P_4',0,0),(53,'P_5','TASTE','TASTE','Taste',NULL,0,1),(54,'P_51','SPICY','SPICY','Spicy','P_5',0,0),(55,'P_52','MILD','MILD','Mild','P_5',0,0),(56,'P_53','SWEET','SWEET','Sweet','P_5',0,0),(57,'P_54','SOUR','SOUR','Sour','P_5',0,0),(58,'P_55','TANGY','TANGY','Tangy','P_5',0,0),(59,'P_56','SWEET_SOUR','SWEET_SOUR','Sweet & Sour','P_5',0,0),(60,'P_57','LIGHT','LIGHT','Light','P_5',0,0),(61,'P_58','BALANCED','BALANCED','Balanced Taste','P_5',0,0),(62,'ADDT_P_1','HEALTH','HEALTH','Health',NULL,0,1),(63,'ADDT_C_11','DIABETIC','DIABETIC','Diabetic','ADDT_P_1',0,0),(64,'ADDT_C_12','ENERGY_RICH','ENERGY_RICH','Energy Rich','ADDT_P_1',0,0),(65,'ADDT_C_13','BODY_BLDNG','BODY_BLDNG','Body Building','ADDT_P_1',0,0),(66,'ADDT_C_14','PROTECTIVE_FOOD','PROTECTIVE_FOOD','Protective Food','ADDT_P_1',0,0),(67,'ADDT_C_15','WEIGHT_LOSS','WEIGHT_LOSS','Weight Loss','ADDT_P_1',0,0),(68,'ADDT_C_16','HEART','HEART','Heart','ADDT_P_1',0,0),(69,'ADDT_C_17','AYURVEDA','AYURVEDA','Ayurveda','ADDT_P_1',0,0),(70,'ADDT_C_18','ORGANIC','ORGANIC','Organic','ADDT_P_1',0,0),(71,'ADDT_C_19','LOW_CAL','LOW_CAL','Low Calorie','ADDT_P_1',0,0),(72,'ADDT_P_2','RELIGIOUS','RELIGIOUS','Religious',NULL,0,1),(73,'ADDT_C_21','HINDU_FASTING_FOOD','HINDU_FASTING_FOOD','Hindu - Fasting Food','ADDT_P_2',0,0),(74,'ADDT_C_22','JAIN','JAIN','Jain','ADDT_P_2',0,0),(75,'ADDT_C_23','KOSHER','KOSHER','Kosher','ADDT_P_2',0,0),(76,'ADDT_C_24','HALAL','HALAL','Halal','ADDT_P_2',0,0),(77,'ADDT_C_25','NON_HALAL','NON_HALAL','Non Halal','ADDT_P_2',0,0),(78,'ADDT_P_3','GOURMET','GOURMET','Gourmet',NULL,0,1),(79,'ADDT_C_31','FINE_DINING','FINE_DINING','Fine Dining','ADDT_P_3',0,0),(80,'ADDT_C_32','ETHNIC_CUISINE_FINE_DINING','ETHNIC_CUISINE_FINE_DINING','Ethnic Cuisine','ADDT_P_3',0,0),(81,'AFFP_P_1','AFFILIATE_PARTNER_PRODUCT','AFFILIATE_PARTNER_PRODUCT','Packaged product from our partners',NULL,0,1),(82,'AFFP_P_1_1','PACKAGED_BEVERAGES','PACKAGED_BEVERAGES','Packaged Beverage','AFFP_P_1',0,1),(83,'AFFP_C_1_11','PACKAGED_DRINKING_WATER','PACKAGED_DRINKING_WATER','Packaged Water','AFFP_P_1_1',0,0),(84,'AFFP_C_1_12','PACKAGED_HEALTHY_JUICE','PACKAGED_HEALTHY_JUICE','Packaged Health Juice','AFFP_P_1_1',0,0),(85,'AFFP_P_1_2','PACKAGED_DAIRY','PACKAGED_DAIRY','Packaged Dairy item','AFFP_P_1',0,1),(86,'AFFP_C_1_21','PACKAGED_DAIRY_LONG_LIFE','PACKAGED_DAIRY_LONG_LIFE','Packaged Dairy Long Self life','AFFP_P_1_2',0,0),(87,NULL,'',NULL,NULL,NULL,NULL,NULL),(88,NULL,'',NULL,NULL,NULL,NULL,NULL),(89,NULL,'',NULL,NULL,NULL,NULL,NULL),(90,NULL,'',NULL,NULL,NULL,NULL,NULL),(91,NULL,'',NULL,NULL,NULL,NULL,NULL),(92,NULL,'',NULL,NULL,NULL,NULL,NULL),(93,NULL,'',NULL,NULL,NULL,NULL,NULL),(94,NULL,'',NULL,NULL,NULL,NULL,NULL),(95,NULL,'',NULL,NULL,NULL,NULL,NULL),(96,NULL,'',NULL,NULL,NULL,NULL,NULL),(97,NULL,'',NULL,NULL,NULL,NULL,NULL),(98,NULL,'',NULL,NULL,NULL,NULL,NULL),(99,NULL,'',NULL,NULL,NULL,NULL,NULL),(100,NULL,'',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `food_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_menu_category_mapping`
--

DROP TABLE IF EXISTS `food_menu_category_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_menu_category_mapping` (
  `FOOD_MENU_CATEGORY_MAPPING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_MENU_ID` bigint(20) NOT NULL,
  `FOOD_CATEGORY_PARENT_ID` bigint(20) NOT NULL COMMENT 'PARENT ID OF THE FOOD CATEGORY ID',
  `FOOD_CATEGORY_ID` bigint(20) NOT NULL,
  `ADDITIONAL_TAG_FLAG` int(1) NOT NULL,
  PRIMARY KEY (`FOOD_MENU_CATEGORY_MAPPING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='FOOD_CATEGORY_PARENT_ID - PARENT ID OF THE FOOD CATEGORY ID';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_menu_category_mapping`
--

LOCK TABLES `food_menu_category_mapping` WRITE;
/*!40000 ALTER TABLE `food_menu_category_mapping` DISABLE KEYS */;
INSERT INTO `food_menu_category_mapping` VALUES (1,1,1,3,0),(2,1,4,7,0),(3,1,12,18,0),(4,1,25,29,0),(5,1,53,55,0),(6,2,1,2,0),(7,2,4,7,0),(8,2,12,16,0),(9,2,25,44,0),(10,2,53,55,0),(11,3,1,3,0),(12,3,4,7,0),(13,3,12,16,0),(14,3,25,31,0),(15,3,53,54,0),(16,4,1,2,0),(17,4,4,7,0),(18,4,12,16,0),(19,4,25,44,0),(20,4,53,55,0),(21,5,1,2,0),(22,5,4,7,0),(23,5,12,16,0),(24,5,25,44,0),(25,5,53,55,0),(26,5,62,71,1),(27,6,1,2,0),(28,6,4,7,0),(29,6,12,16,0),(30,6,25,44,0),(31,6,53,55,0),(32,7,1,2,0),(33,7,4,7,0),(34,7,12,17,0),(35,7,25,44,0),(36,7,53,55,0),(37,8,1,2,0),(38,8,4,7,0),(39,8,12,17,0),(40,8,25,44,0),(41,8,53,55,0),(42,9,1,3,0),(43,9,4,7,0),(44,9,12,18,0),(45,9,25,44,0),(46,9,53,55,0),(47,10,1,2,0),(48,10,4,7,0),(49,10,12,17,0),(50,10,25,44,0),(51,10,53,55,0),(52,10,62,69,0),(53,11,1,2,0),(54,11,4,7,0),(55,11,12,17,0),(56,11,25,44,0),(57,11,53,55,0),(58,12,1,3,0),(59,12,4,7,0),(60,12,12,23,0),(61,12,25,44,0),(62,12,53,55,0),(63,13,1,2,0),(64,13,4,7,0),(65,13,12,23,0),(66,13,25,44,0),(67,13,53,55,0),(68,14,1,2,0),(69,14,4,7,0),(70,14,12,19,0),(71,14,25,44,0),(72,14,53,61,0),(73,15,1,2,0),(74,15,4,7,0),(75,15,12,19,0),(76,15,25,44,0),(77,15,53,61,0),(78,15,62,69,0),(79,16,1,2,0),(80,16,82,83,0),(81,17,1,2,0),(82,17,85,86,0),(83,18,1,2,0),(84,18,4,7,0),(85,18,12,19,0),(86,18,25,44,0),(87,18,53,61,0),(88,19,1,2,0),(89,19,4,7,0),(90,19,12,18,0),(91,19,53,61,0),(92,20,1,2,0),(93,20,4,7,0),(94,20,12,18,0),(95,20,53,61,0),(96,21,1,2,0),(97,21,4,7,0),(98,21,12,18,0),(100,21,53,61,0),(101,22,1,2,0),(102,22,4,7,0),(103,22,12,18,0),(104,22,53,61,0),(105,23,1,2,0),(106,23,4,7,0),(107,23,12,18,0),(108,23,53,61,0),(109,24,1,2,0),(110,24,4,7,0),(111,24,12,23,0),(112,24,25,44,0),(113,24,53,55,0),(114,25,1,3,0),(115,25,4,7,0),(116,25,12,23,0),(117,25,25,31,0),(118,25,53,55,0),(119,26,1,2,0),(120,26,4,7,0),(121,26,12,17,0),(123,26,53,55,0),(124,0,0,0,0),(125,0,0,0,0);
/*!40000 ALTER TABLE `food_menu_category_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_menu_items`
--

DROP TABLE IF EXISTS `food_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_menu_items` (
  `FOOD_MENU_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_RECEIPE_ID` bigint(20) DEFAULT NULL,
  `FOOD_NAME` varchar(50) DEFAULT NULL,
  `FOOD_SHORT_DESCRIPTION` varchar(100) DEFAULT NULL,
  `FOOD_LONG_DESCRIPTION` varchar(250) DEFAULT NULL,
  `IMAGE1_URL` varchar(200) DEFAULT NULL,
  `IMAGE2_URL` varchar(200) DEFAULT NULL,
  `PACKAGED_ITEM_FLAG` int(1) DEFAULT '0',
  PRIMARY KEY (`FOOD_MENU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_menu_items`
--

LOCK TABLES `food_menu_items` WRITE;
/*!40000 ALTER TABLE `food_menu_items` DISABLE KEYS */;
INSERT INTO `food_menu_items` VALUES (1,NULL,'Kachchi Yakhnni Mutton Biryani','Slow Cooked Briyani is prepared from raw meat to preserve it\'s aroma.','Pure Basmati rice cooked in layers of marinated choicest softest meat in a wide, heavy bottomed vessel on a low flame, it is where it gets the taste from. Dine like royalty with this traditional nawabi fare.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/Mutton_biryani.jpg',NULL,0),(2,NULL,'Dal Tadka','Arhar Daal tempered with ghee fried spices and herbs in thick base.','Mixed with a combination of spices like turmeric and a few veggies, slow cooked to fineness and to preserve its plainness, yet adding flavour to your food at the same time.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/plain_Daal.JPG',NULL,0),(3,NULL,'Mutton Rogan Josh','A signature recipe of Kashmiri cuisine cooked in Red Chillies from Kashmir.','Mutton cooked with ghee,curd, spices and red chillies from Kashmir, chilli used is so bright red in colour which gives beauty and taste to this gravy. Enjoyed best with roti or plain rice.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/mutton_rogan_josh.jpg',NULL,0),(4,NULL,'Palak Paneer','Our own kitchen made soft Paneer cooked with a perfect mix of spinach and spices.','Curry made of blanched spinach and soft paneer, sautéed with finely balanced curry spices with aromatic ghee. Sour in taste and cooked on a slow flame, it radiates perfection with its lovely taste.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/palak-paneer.jpg',NULL,0),(5,NULL,'Veg Daliya','Dalia is a delicious dish made from broken wheat that also happens to be really good for you.','Dalia is considered to be one of the simplest and healthy food, low in fat & cholesterol, high in iron & fibre, good in carbs. Make it a habit to have it as everyday food.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/veg_dalia.JPG',NULL,0),(6,NULL,'Dal Makhani','A Punjabi staple, dal makhani is a hearty, spicy, tomato-based dish of lentils and rajma.','Slow-cooked with butter and some cream for a thick consistency and rich taste. It is pretty darn delicious—like a buttery bean chili, to our tastes. It tastes best when served with roti.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/dal-makhani-instant-pot.jpg',NULL,0),(7,NULL,'Aalo Gobhi','Flavorful vegetarian dish made with potatoes, cauliflower and spices which makes it mouth watering','Chunks of potato and cauliflower soak up the rich flavors of toasted cumin, turmeric and dried coriander in this traditional dish. Try this with rice or the roti for a quintessential feast','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/aalo_gobi.jpg',NULL,0),(8,NULL,'Paneer Butter Masala','Softest panner in thick gravy made up of cream, tomatoes and spices, goes best with butter roti.','Our own Kitchen made softest paneer and magic of slow cooking makes it absolute delicious. Eat this rich dish with roti.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/paneer_butter_masala.jpg',NULL,0),(9,NULL,'Healthy Chicken Biryani','Tender chicken layered with long grain brown rice and spices and slow cooked. You can eat it daily.','Spiced long grain brown rice rice layered with chicken and cooked very slowly in a sealed pot until everything is tender and aromatic. Goes best with fresh salads, bagara baigan and raita.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/chicken_Biryani.jpg',NULL,0),(10,NULL,'Pancharatni Daal','5 vaieties of lentils, marwar delicacy is rich in spices and truly unique.','Nutritious dish made of 5 varieties of lentils full of proteins. Mixed lentils cooked delicately in a spicy onion-tomato based to a creamy consistency, can be eaten as thick soup only or with roti.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/Pancharatna_Daal.jpg',NULL,0),(11,NULL,'Mixed Veg','Nutritious combo of a variety of healthy veggies that makes the dish very colorful and tempting.','Nutritious veg dish made from variety of seasonal veggies. Goes best when accompained with Rice-Daal or just plain roti.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/mixed_veg.jpg',NULL,0),(12,NULL,'Non Veg Meals','The complete action packed non veg meal for your lunch.','Soft Mutton pieces in Rigan Josh and served with Malai Kebab, Dal, Mixed Subzi, Jeera Rice, and a piping-hot Roti. Accompanied with Mixed Raita, Pickled onions, and Dessert.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/Non-VEG-combo_big.jpg',NULL,0),(13,NULL,'Veg Meals','The complete action packed veg meal for your lunch.','Delicious Paneer Makhanwala served with Veg Tikki, Dal, Mixed Subzi, Jeera Rice, and a piping-hot Roti. Accompanied with Mixed Raita, Pickled onions, and Dessert.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/Panner-Makhani-big-combo.jpg',NULL,0),(14,NULL,'Roti','Whole wheat roti without any oil, availabe as 5 roti in a packet.','Whole wheat roti, try with light coating of ghee and with any veg or non veg greavy.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/wheat_roti.jpg',NULL,0),(15,NULL,'Jowar Roti','Jowar or shorgun roti, is a highly nutritious and gluten free. Try eating daily in atleast one meal.','Jowar is a gluten-free, high-protein, cholesterol-free source of a variety nutrients, dietary fiber, iron, phosphorus and thiamine.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/Jowar_roti.jpg',NULL,0),(16,NULL,'Packaged water.','Packaged Mineral Water.','Packaged mineral water in 1 litre bottle.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/packaged_drinking+water.jpeg',NULL,0),(17,NULL,'FH Ghee','A sacred and celebrated symbol of auspiciousness, nourishment and healing, eat a spoonful daily.','Ghee as an essential part of a balanced diet, and considers it to be the best fat one can eat, eat with roti or in rice.','https://s3-eu-west-1.amazonaws.com/feasthub/offeringImages/test/ghee.jpg',NULL,0),(18,NULL,'Ragi Roti',NULL,NULL,NULL,NULL,0),(19,NULL,'Plain Rice',NULL,NULL,NULL,NULL,0),(20,NULL,'Veg Pulav',NULL,NULL,NULL,NULL,0),(21,NULL,'Plain Brown Rice',NULL,NULL,NULL,NULL,0),(22,NULL,'Plain Raw Rice',NULL,NULL,NULL,NULL,0),(23,NULL,'Red Boiled Rice',NULL,NULL,NULL,NULL,0),(24,NULL,'Veg Thali ( Make your own meals)','The complete action packed veg meal for your lunch. Now you can even customised to your need.','The complete action packed veg meal for your lunch. Now you can even customised to your need.',NULL,NULL,1),(25,NULL,'Mutton Rogan Josh with Roti','Now a mutton rogan Josh complemented with Roti','Now a mutton rogan Josh complemented with Roti',NULL,NULL,1),(26,NULL,'Raita','Raita',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `food_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_menu_receipe`
--

DROP TABLE IF EXISTS `food_menu_receipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_menu_receipe` (
  `FOOD_RECEIPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RECEIPE_DES1` varchar(2000) DEFAULT NULL,
  `RECEIPE_DES2` varchar(2000) DEFAULT NULL,
  `RECEIPE_DOC_FILE` varchar(2000) DEFAULT NULL,
  `RECEIPE_URL1` varchar(1000) DEFAULT NULL,
  `RECEIPE_URL2` varchar(100) DEFAULT NULL,
  `PRIMARY_INGREDIENTS_LIST` varchar(500) DEFAULT NULL,
  `SECONDARY_INGREDIENT_LIST` varchar(500) DEFAULT NULL,
  `SPICES_LIST` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`FOOD_RECEIPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_menu_receipe`
--

LOCK TABLES `food_menu_receipe` WRITE;
/*!40000 ALTER TABLE `food_menu_receipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_menu_receipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_menu_serving_category`
--

DROP TABLE IF EXISTS `food_menu_serving_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_menu_serving_category` (
  `FOOD_SERVING_SIZE_CATEGORY_TYPE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_SERVING_CATEGORY_NAME` varchar(50) DEFAULT NULL,
  `ONE_SERVING_UNIT_QUANTITY` int(11) DEFAULT NULL,
  `MEASURE_UNITS` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`FOOD_SERVING_SIZE_CATEGORY_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Data would be something as follows\r\n\r\nservercat1   packaged_meal 1 pac\r\nservercat1   ala_carta_generic 1 portion\r\nservercat1   packaged_bulk_in_weight 1 KG\r\nservercat1   packaged_bulk_in_liters 1 Lt\r\nservercat1   packaged_bulk_in_quantity 1 count';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_menu_serving_category`
--

LOCK TABLES `food_menu_serving_category` WRITE;
/*!40000 ALTER TABLE `food_menu_serving_category` DISABLE KEYS */;
INSERT INTO `food_menu_serving_category` VALUES (1,'TBD',500,'GRAM'),(2,'PACKAGED_ITEM',1,'UNITS'),(3,'BULK_KG',1,'KG'),(4,'BULK_Litre',1,'Litre');
/*!40000 ALTER TABLE `food_menu_serving_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_menu_tags`
--

DROP TABLE IF EXISTS `food_menu_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_menu_tags` (
  `FOOD_MENU_TAG_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_MENU_ID` bigint(20) DEFAULT NULL,
  `FOOD_CATEGORY_KEY` bigint(20) DEFAULT NULL,
  `FOOD_DISPLAY_CATEGORY_KEY_FLAG` bigint(1) DEFAULT NULL,
  `ADDITIONAL_FOOD_CATEGORY_KEY_TAGS_FLAG` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`FOOD_MENU_TAG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_menu_tags`
--

LOCK TABLES `food_menu_tags` WRITE;
/*!40000 ALTER TABLE `food_menu_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_menu_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_raw_materials`
--

DROP TABLE IF EXISTS `food_raw_materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_raw_materials` (
  `FOOD_RAW_MATERIAL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_RECEIPE_ID` bigint(20) DEFAULT NULL,
  `FOOD_INGREDIENT_ID` bigint(20) DEFAULT NULL,
  `QUANTITY` varchar(5) DEFAULT NULL,
  `MEASURE_UNITS` varchar(5) DEFAULT NULL,
  `PRIMARY_INGREDIENT_FLAG` bit(1) DEFAULT NULL,
  PRIMARY KEY (`FOOD_RAW_MATERIAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_raw_materials`
--

LOCK TABLES `food_raw_materials` WRITE;
/*!40000 ALTER TABLE `food_raw_materials` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_raw_materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functional_parameters`
--

DROP TABLE IF EXISTS `functional_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_parameters` (
  `FUNCTIONAL_PARAMETER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `FUNCTIONAL_PARAMETERS_KEY` varchar(100) DEFAULT NULL,
  `FUNCTIONAL_PARAMETERS_VALUE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FUNCTIONAL_PARAMETER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_parameters`
--

LOCK TABLES `functional_parameters` WRITE;
/*!40000 ALTER TABLE `functional_parameters` DISABLE KEYS */;
INSERT INTO `functional_parameters` VALUES 
(1,1,'FH_AUTH','FH_AUTH'),(2,1,'FB','FB'),(3,1,'GOGL','GOGL'),(4,1,'NONE','NONE'),(5,2,'FH_ADMIN','FH_ADMIN'),(6,2,'FH_SYS_ADMIN','FH_SYS_ADMIN'),(7,2,'FH_USER_SUPPORT','FH_USER_SUPPORT'),(8,2,'SELLER','SELLER'),(9,2,'CONSUMER','CONSUMER'),(10,2,'FH_OPERATION_MANAGER','FH_OPERATION_MANAGER'),(11,2,'FH_OPERATION_DELIVERY_AGENT','FH_OPERATION_DELIVERY_AGENT'),(12,3,'FH_USER','FH_USER'),(13,3,'FH_SELLER','FH_SELLER'),(14,3,'RETAIL_CONSUMER','RETAIL_CONSUMER'),(15,3,'FH_CORP_PARTNER','FH_CORP_PARTNER'),(16,4,'INDIVIDUAL','INDIVIDUAL'),(17,4,'PROPRIETORSHIP','PROPRIETORSHIP'),(18,4,'PARTNERSHIP','PARTNERSHIP'),(19,4,'PRIVATE_LIMITED','PRIVATE_LIMITED'),(20,4,'PUBLIC_COMPANY','PUBLIC_COMPANY'),(21,4,'MNC','MNC'),(22,4,'GOVT_ORGANISTAION','GOVT_ORGANISTAION'),(23,4,'NGO','NGO'),(24,4,'FH','FH'),(25,5,'CONSUMER','CONSUMER'),(26,5,'HOMEBASE','HOMEBASE'),(27,5,'CATERER_RESTAUTRER','CATERER_RESTAUTRER'),(28,5,'SERVICES','SERVICES'),(30,5,'FH','FH'),(31,5,'FH_FRANCHISE','FH_FRANCHISE'),(32,6,'HOME_COOK_TAKEAWAY','HOME_COOK_TAKEAWAY'),(33,6,'HOME_COOK_DELIVERY','HOME_COOK_DELIVERY'),(34,6,'HOME_COOK_HOST','HOME_COOK_HOST'),(35,6,'BULK_COOKING','BULK_COOKING'),(36,6,'COOKS_ON_HIRE','COOKS_ON_HIRE'),(37,6,'CHEF_ON_HIRE','CHEF_ON_HIRE'),(38,6,'BANQUET_SERVER','BANQUET_SERVER'),(39,6,'NUTRITIONIST_SERVICE','NUTRITIONIST_SERVICE'),(40,6,'EVENT_PLANNER','EVENT_PLANNER'),(41,6,'SPECIALITY_FOOD','SPECIALITY_FOOD'),(42,6,'COOKING_LESSON','COOKING_LESSON'),(43,6,'IN_PREMISES_COOKED_FOOD','IN_PREMISES_COOKED_FOOD'),(44,6,'IN_PREMISES_MANUFACTURING','IN_PREMISES_MANUFACTURING'),(45,5,'FH_EXTENSION','FH_EXTENSION'),(46,8,'REFERRED','REFERRED'),(47,8,'PURCHASED','PURCHASED'),(48,8,'PERSONAL_GIFT','PERSONAL_GIFT'),(49,8,'PARTNER_OFFERING_THRU_COUPONS','PARTNER_OFFERING_THRU_COUPONS'),(50,8,'PARTNER_PROMOTIONS','PARTNER_PROMOTIONS'),(51,8,'SELLER_EARNED','SELLER_EARNED'),(52,8,'CORPORATE_GIFT','CORPORATE_GIFT'),(53,8,'FSHB_BONUS_POINTS','FSHB_BONUS_POINTS'),(54,9,'PRIVATE','PRIVATE'),(55,9,'PUBLIC','PUBLIC'),(56,10,'MR','MR'),(57,10,'MRS','MRS'),(58,10,'DR','DR'),(59,10,'MISS','MISS'),(60,10,'MASTER','MASTER'),(61,11,'LISTING_FEES','LISTING_FEES'),(62,11,'COMMISION','COMMISION'),(63,11,'SPONSERED_LISTING','SPONSERED_LISTING'),(64,12,'LISTING','LISTING'),(65,12,'ITEM_TOTAL','ITEM_TOTAL'),(66,12,'SUB_TOTAL','SUB_TOTAL'),(67,12,'TOTAL','TOTAL'),(68,12,'ON_TAXES','ON_TAXES'),(69,12,'ON_ADDITIONAL_FEES','ON_ADDITIONAL_FEES'),(70,13,'CREATED','CREATED'),(71,13,'PAID','PAID'),(72,13,'CONFIRMED_BY_COMPANY','CONFIRMED_BY_COMPANY'),(73,13,'BEING_PREPARED','BEING_PREPARED'),(74,13,'PACKING_IN_PROGRESS','PACKING_IN_PROGRESS'),(75,13,'OUT_FOR_DELIVERY','OUT_FOR_DELIVERY'),(76,13,'DELIVERED','DELIVERED'),(77,13,'COMPLETED','COMPLETED'),(78,13,'WAITINGFOR_CUSTOMER_TAKE_AWAY','WAITINGFOR_CUSTOMER_TAKE_AWAY'),(79,13,'DELIVERED_TO_NEIGHBOUR','DELIVERED_TO_NEIGHBOUR'),(80,14,'PAID_IN','PAID_IN'),(81,14,'PAID_OUT','PAID_OUT'),(82,15,'CASH','CASH'),(83,15,'FP','FP'),(84,15,'BANK_TRANSFER','BANK_TRANSFER'),(85,15,'CHQ','CHQ'),(86,15,'MOBILE_PAYMENT','MOBILE_PAYMENT'),(87,15,'E_WALLET','E_WALLET'),(88,16,'FP','FP'),(89,16,'EVENT','EVENT'),(90,16,'PAID_TO_USER','PAID_TO_USER'),(91,17,'ZAAKPAY','ZAAKPAY'),(92,17,'MOBIKWIK','MOBIKWIK'),(93,17,'PAYTM','PAYTM'),(94,15,'SEND_PAYMENT_LINK','SEND_PAYMENT_LINK'),(95,7,'IN_PREMISES_RETAIL_COOKED_FOOD_DELIVERY','IN_PREMISES_RETAIL_COOKED_FOOD_DELIVERY'),(96,7,'IN_PREMISES_RETAIL_COOKED_FOOD_TAKEAWAY','IN_PREMISES_RETAIL_COOKED_FOOD_TAKEAWAY'),(97,7,'AT_CUSTOMER_PREMISES','AT_CUSTOMER_PREMISES'),(98,7,'FINE_DINING_EXPERIENCE_AT_CUSTOMER_PREMISES','FINE_DINING_EXPERIENCE_AT_CUSTOMER_PREMISES'),(99,7,'IN_PREMISES_INSTITUTIONAL_COOKED_FOOD_DELIVERY','IN_PREMISES_INSTITUTIONAL_COOKED_FOOD_DELIVERY'),(100,7,'IN_PREMISES_BULK_COOKED_FOOD_DELIVERY','IN_PREMISES_BULK_COOKED_FOOD_DELIVERY'),(101,7,'IN_PREMISES_BULK_COOKED_FOOD_TAKEAWAY','IN_PREMISES_BULK_COOKED_FOOD_TAKEAWAY');
/*!40000 ALTER TABLE `functional_parameters` ENABLE KEYS */;
UNLOCK TABLES;

update fh_events set SERVICE_OFFERING_TYPE = 11 where SERVICE_OFFERING_TYPE = 43;
update fh_events set BUSINESS_TYPE = 5 where BUSINESS_TYPE = 30;
update fh_events set ADDITIONAL_SERVICE_OFFERING_TYPE = 0 where ADDITIONAL_SERVICE_OFFERING_TYPE = 95;
update fh_events set ADDITIONAL_SERVICE_OFFERING_TYPE = 5 where ADDITIONAL_SERVICE_OFFERING_TYPE = 100;

--
-- Table structure for table `functional_parameters_categories`
--

DROP TABLE IF EXISTS `functional_parameters_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functional_parameters_categories` (
  `CATEGORY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CATEGORY_NAME` varchar(100) DEFAULT NULL,
 
  PRIMARY KEY (`CATEGORY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functional_parameters_categories`
--

LOCK TABLES `functional_parameters_categories` WRITE;
/*!40000 ALTER TABLE `functional_parameters_categories` DISABLE KEYS */;
INSERT INTO `functional_parameters_categories` VALUES 
(1,'AUTH_METHOD'),
(2,'USER_ROLE'),
(3,'USER_TYPE'),
(4,'BUSINESS_STRUCTURE'),
(5,'BUSINESS_TYPES'),
(6,'SERVICES_OFFERING_TYPES'),
(7,'ADDITIONAL_SERVICE_OFFERING'),
(8,'FEASTPOINT_ACCUMULATION_TYPE'),
(9,'EVENT_VISIBILITY'),
(10,'USER_SALUTATION'),
(11,'FH_ADDITIONAL_CHARGE_FEES_TYPE'),
(12,'FEES_APPLCABILITY'),
(13,'ORDER_STATUS'),
(14,'AUDIT_TRANSACTION_TYPE'),
(15,'TRANSACTION_MODE'),
(16,'TRANSACTION_PURPOSE'),
(17,'PAYMENT_GATEWAY');
/*!40000 ALTER TABLE `functional_parameters_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_grocery_ingredients_list`
--

DROP TABLE IF EXISTS `master_grocery_ingredients_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_grocery_ingredients_list` (
  `FOOD_INGREDIENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_INGREDIENTS_CATEGORY_KEY` bigint(20) DEFAULT NULL,
  `FOOD_INGREDIENT_CODE` varchar(20) DEFAULT NULL,
  `FOOD_INGREDIENT_NAME` varchar(50) DEFAULT NULL,
  `FOOD_INGREDIENT_MEASURE_TYPE` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`FOOD_INGREDIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='master list of all ingredients\r\n\r\n1 spices cloved gms\r\n2 staple rice kg';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_grocery_ingredients_list`
--

LOCK TABLES `master_grocery_ingredients_list` WRITE;
/*!40000 ALTER TABLE `master_grocery_ingredients_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_grocery_ingredients_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_ingredients_category`
--

DROP TABLE IF EXISTS `master_ingredients_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_ingredients_category` (
  `INGREDIENTS_CATEGORY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INGREDIENTS_CATEGORY_CODE` varchar(20) DEFAULT NULL,
  `INGREDIENTS_CATEGORY_NAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`INGREDIENTS_CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_ingredients_category`
--

LOCK TABLES `master_ingredients_category` WRITE;
/*!40000 ALTER TABLE `master_ingredients_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_ingredients_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_token`
--

DROP TABLE IF EXISTS `oauth2_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_token` (
  `OAUTH2_TOKEN_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `FSHB_ACCESS_TOKEN` varchar(100) DEFAULT NULL,
  `FSHB_ACCESS_TOKEN_EXPIRES_IN` varchar(20) DEFAULT NULL,
  `AUTH_SERVER_NAME` varchar(50) DEFAULT NULL,
  `EXTERNAL_ACCESS_TOKEN` varchar(100) DEFAULT NULL,
  `EXTERNAL_ACCESS_TOKEN_EXPIRES_IN` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`OAUTH2_TOKEN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_token`
--

LOCK TABLES `oauth2_token` WRITE;
/*!40000 ALTER TABLE `oauth2_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offerer_history_favourite_items`
--

DROP TABLE IF EXISTS `offerer_history_favourite_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offerer_history_favourite_items` (
  `OFFEREE_HISTORY_FAVOURITE_ITEM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `FOOD_MENU_ID` bigint(20) DEFAULT NULL,
  `FAVORITE_FLAG` varchar(1) DEFAULT NULL,
  `LAST_ORDER_DATE` date DEFAULT NULL,
  `NO_OF_TIMES_OFFERED` int(11) DEFAULT '0',
  PRIMARY KEY (`OFFEREE_HISTORY_FAVOURITE_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offerer_history_favourite_items`
--

LOCK TABLES `offerer_history_favourite_items` WRITE;
/*!40000 ALTER TABLE `offerer_history_favourite_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `offerer_history_favourite_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_customised_cusine`
--

DROP TABLE IF EXISTS `order_customised_cusine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_customised_cusine` (
  `ORDER_CUSTOMISED_CUSINE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DEFAULT_FOOD_MENU_ID` int(11) DEFAULT NULL,
  `ORDER_EVENT_ID` bigint(20) DEFAULT NULL,
  `CUSTOMISED_FOOD_MENU_ID` int(11) DEFAULT NULL,
  `ORDERED_QUANTITY` int(11) DEFAULT NULL,
  `FOOD_SERVING_SIZE_CATEGORY_TYPE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ORDER_CUSTOMISED_CUSINE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Normal food menu id to be populated in default menu id';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_customised_cusine`
--

LOCK TABLES `order_customised_cusine` WRITE;
/*!40000 ALTER TABLE `order_customised_cusine` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_customised_cusine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_delivery`
--

DROP TABLE IF EXISTS `order_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_delivery` (
  `ORDER_DELIVERY_ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` varchar(50) NOT NULL,
  `DELIVERY_DATE` date DEFAULT NULL,
  `DELIVERY_STATUS` bigint(2) DEFAULT NULL,
  `DELIVERY_BY` varchar(50) DEFAULT NULL,
  `DELIVERY_SLOTS_ID` bigint(11) DEFAULT NULL,
  `DELIVERY_SLOTS_TIME_ID` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`ORDER_DELIVERY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_delivery`
--

LOCK TABLES `order_delivery` WRITE;
/*!40000 ALTER TABLE `order_delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_events`
--

DROP TABLE IF EXISTS `order_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_events` (
  `ORDER_EVENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` varchar(50) NOT NULL,
  `EVENT_ID` bigint(11) DEFAULT NULL,
  `SELLER_USER_ID` bigint(11) DEFAULT NULL,
  `ORDERED_QUANTITY` int(11) DEFAULT NULL,
  `BUSINESS_TYPE` BigInt(20) DEFAULT NULL,
  `SERVICE_OFFERING_TYPE` BigInt(20) DEFAULT NULL,
  `ADDITIONAL_SERVICE_OFFERING_TYPE` BigInt(20) DEFAULT NULL,
  `ORDER_CUSTOMISED_CUSINE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ORDER_EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='ORDER_PURCHASED_MODE_TYPE -- CASH OR FP\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_events`
--

LOCK TABLES `order_events` WRITE;
/*!40000 ALTER TABLE `order_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_id_sequence`
--

DROP TABLE IF EXISTS `order_id_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_id_sequence` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_ID_PREFIX` varchar(30) DEFAULT NULL,
  `SEQUENCE_NUMBER` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_id_sequence`
--

LOCK TABLES `order_id_sequence` WRITE;
/*!40000 ALTER TABLE `order_id_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_id_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_special_request`
--

DROP TABLE IF EXISTS `order_special_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_special_request` (
  `ORDER_SPECIAL_REQUEST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` varchar(50) NOT NULL,
  `REQUESTED_ITEM_NAME` varchar(11) DEFAULT NULL,
  `QUANTITY` varchar(1000) DEFAULT NULL,
  `MEASURING_UNITS` varchar(5) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `ORDER_NOTE` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ORDER_SPECIAL_REQUEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='ORDER_PURCHASED_MODE_TYPE -- CASH OR FP\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_special_request`
--

LOCK TABLES `order_special_request` WRITE;
/*!40000 ALTER TABLE `order_special_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_special_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_transaction`
--

DROP TABLE IF EXISTS `order_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

CREATE TABLE  `order_transaction` (
  `ORDER_TRANSACTION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` varchar(50) NOT NULL,
  `PURCHASER_USER_ID` bigint(11) DEFAULT NULL,
  `ORDER_PURCHASED_MODE_TYPE` varchar(11) DEFAULT NULL,
  `ADDITIONAL_NOTE_BY_USER` varchar(1000) DEFAULT NULL,
  `CASH_AUDIT_ID` bigint(11) DEFAULT NULL,
  `FEAST_AUDIT_ID` int(11) DEFAULT NULL,
  `TRANSACTION_DATE` date DEFAULT NULL,
  `PAYMENT_STATUS` varchar(50) DEFAULT NULL,
  `FIRST_DELIVERY_DATE` date DEFAULT NULL,
  `DELIVERY_ADDRESS_ID` bigint(11) DEFAULT NULL,
  `ORDER_HAS_SPECIAL_REQUEST` varchar(1) DEFAULT NULL,
  `ORDER_STATUS` bigint(20) DEFAULT NULL,
  `REFUND_STATUS` bigint(20) DEFAULT NULL,
  `COLLECTION_IN_CASH` varchar(1) DEFAULT NULL,
  `COLLECTION_IN_FP` varchar(1) DEFAULT NULL,
  `TAXES` varchar(1) DEFAULT NULL,
  `TOTAL_AMOUNT` double DEFAULT NULL,
  `TOTAL_TAX` double DEFAULT NULL,
  `PAYMENT_MODE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ORDER_TRANSACTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='ORDER_PURCHASED_MODE_TYPE -- CASH OR FP\r\n';

--
-- Dumping data for table `order_transaction`
--

LOCK TABLES `order_transaction` WRITE;
/*!40000 ALTER TABLE `order_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packaged_food_item_to_alternative_option_item_list`
--

DROP TABLE IF EXISTS `packaged_food_item_to_alternative_option_item_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packaged_food_item_to_alternative_option_item_list` (
  `PACKAGED_FOOD_ITEM_TO_ALTERNATIVE_OPTION_ITEM_LIST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PACKAGED_FOOD_MENU_ITEMS_LIST_ID` bigint(20) NOT NULL,
  `ALTERNATIVE_OPTIONAL_FOOD_MENU_ID` bigint(20) NOT NULL,
  `PACKAGED_FOOD_MENU_ITEMISED_PRICING_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PACKAGED_FOOD_ITEM_TO_ALTERNATIVE_OPTION_ITEM_LIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='list of all alternative options availabe against the food menu id like for roti what options naan, ragi roti\r\n\r\n\r\nPACKAGED_FOOD_MENU_ITEMS_LIST_ID -- Default item\r\nALTERNATIVE_OPTIONAL_FOOD_MENU_ID --> alternative options   linked to packaged_food_menu_itemised_pricing --> food_menu_id\r\nPACKAGED_FOOD_MENU_ITEMISED_PRICING_ID --> price to select this top up\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packaged_food_item_to_alternative_option_item_list`
--

LOCK TABLES `packaged_food_item_to_alternative_option_item_list` WRITE;
/*!40000 ALTER TABLE `packaged_food_item_to_alternative_option_item_list` DISABLE KEYS */;
INSERT INTO `packaged_food_item_to_alternative_option_item_list` VALUES (1,1,14,0),(2,1,15,0),(3,1,18,0),(4,3,19,0),(5,2,20,0),(6,2,21,0),(7,2,22,0),(8,2,23,0),(9,2,1,0),(10,2,9,0);
/*!40000 ALTER TABLE `packaged_food_item_to_alternative_option_item_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packaged_food_menu_itemised_pricing`
--

DROP TABLE IF EXISTS `packaged_food_menu_itemised_pricing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packaged_food_menu_itemised_pricing` (
  `PACKAGED_FOOD_MENU_ITEMISED_PRICING_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOOD_MENU_ID` bigint(20) NOT NULL,
  `TOP_UP_PRICE` double NOT NULL,
  PRIMARY KEY (`PACKAGED_FOOD_MENU_ITEMISED_PRICING_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packaged_food_menu_itemised_pricing`
--

LOCK TABLES `packaged_food_menu_itemised_pricing` WRITE;
/*!40000 ALTER TABLE `packaged_food_menu_itemised_pricing` DISABLE KEYS */;
INSERT INTO `packaged_food_menu_itemised_pricing` VALUES (1,14,0),(2,15,20),(3,18,15),(4,19,0),(5,20,25),(6,21,26),(7,22,25),(8,23,25),(9,1,25);
/*!40000 ALTER TABLE `packaged_food_menu_itemised_pricing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packaged_food_menu_items_list`
--

DROP TABLE IF EXISTS `packaged_food_menu_items_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packaged_food_menu_items_list` (
  `PACKAGED_FOOD_MENU_ITEMS_LIST_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PACKAGED_FOOD_MENU_ID` bigint(20) NOT NULL,
  `ADDITIONAL_FOOD_MENU_ID` bigint(20) NOT NULL,
  `ALTERNATIVE_OPTIONS_AVAILABILITY_FLAG` int(1) NOT NULL,
  PRIMARY KEY (`PACKAGED_FOOD_MENU_ITEMS_LIST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='This table contain the list of food items available as a part of package.\r\n\r\nPACKAGED_FOOD_MENU_ID --> packaged item id\r\nADDITIONAL_FOOD_MENU_ID --> additional food menu items associeted with the packaged item\r\nALTERNATIVE_OPTIONS_AVAILABILITY_FLAG --> if the alternative option is available is not. like for roti options could be tandoori, naan. \r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packaged_food_menu_items_list`
--

LOCK TABLES `packaged_food_menu_items_list` WRITE;
/*!40000 ALTER TABLE `packaged_food_menu_items_list` DISABLE KEYS */;
INSERT INTO `packaged_food_menu_items_list` VALUES (1,23,14,1),(2,23,19,1),(3,23,2,0),(4,23,4,0),(5,23,11,0),(6,23,26,0),(7,25,14,0),(9,0,0,0),(10,0,0,0),(11,0,0,0),(13,0,0,0),(14,0,0,0),(15,0,0,0),(16,0,0,0),(17,0,0,0),(18,0,0,0),(19,0,0,0),(20,0,0,0),(21,0,0,0),(22,0,0,0),(23,0,0,0),(24,0,0,0),(25,0,0,0),(26,0,0,0),(27,0,0,0);
/*!40000 ALTER TABLE `packaged_food_menu_items_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_collection`
--

DROP TABLE IF EXISTS `tax_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_collection` (
  `TAX_COLLECTION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENT_SALES_TRANS_ID` bigint(11) DEFAULT NULL,
  `ORDER_ID` varchar(30) DEFAULT NULL,
  `EVENT_ID` bigint(11) DEFAULT NULL,
  `USER_ID` bigint(11) DEFAULT NULL,
  `TAX_COLLECTION_IN_FP` double DEFAULT NULL,
  `TAX_COLLECTION_CONVERTED_TO_CASH` double DEFAULT NULL,
  `TAX_COLLECTION_IN_CASH` double DEFAULT NULL,
  `TAX_STRUCTURE_ID` bigint(20) DEFAULT NULL,
  `TRANSACTION_DATE` date DEFAULT NULL,
  `TAX_SETTLEMENT_COMPLETED_FLAG` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`TAX_COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='TAX STRUCTURE ID DENOTES THE TAXES TYPES';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_collection`
--

LOCK TABLES `tax_collection` WRITE;
/*!40000 ALTER TABLE `tax_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_rates` (
  `TAX_RATES_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATE_TAX_TYPE` varchar(50) DEFAULT NULL,
  `STATE_TAX_RATE` double DEFAULT NULL,
  `STATE_TAX_APPLICABILITY` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TAX_RATES_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='STATE_TAX_TYPE -- Name of the tax like VAT, Service tax';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rates`
--

LOCK TABLES `tax_rates` WRITE;
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
INSERT INTO `tax_rates` VALUES (1,'SERVICE_TAX',5,'TOTAL'),(2,'VAT',10,'TOTAL'),(3,'SBT',5,'2');
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_structure`
--

DROP TABLE IF EXISTS `tax_structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_structure` (
  `TAX_STRUCTURE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COUNTRY_CODE` varchar(10) DEFAULT NULL,
  `STATE_CODE` varchar(10) DEFAULT NULL,
  `TAX_TYPE` varchar(10) DEFAULT NULL,
  `TAX_NAME` varchar(10) DEFAULT NULL,
  `BUSINESS_TYPES` varchar(20) DEFAULT NULL,
  `SERVICES_OFFERING_TYPES` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TAX_STRUCTURE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_structure`
--

LOCK TABLES `tax_structure` WRITE;
/*!40000 ALTER TABLE `tax_structure` DISABLE KEYS */;
INSERT INTO `tax_structure` VALUES (1,'IN','TS','FOOD_TAX','TS_FOOD_TA','COOKED_FOOD','FOOD_DELIVERY');
/*!40000 ALTER TABLE `tax_structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_structure_tax_rates`
--

DROP TABLE IF EXISTS `tax_structure_tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_structure_tax_rates` (
  `TAX_STRUCTURE_TAX_RATE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TAX_STRUCTURE_ID` bigint(20) DEFAULT NULL,
  `TAX_RATE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`TAX_STRUCTURE_TAX_RATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_structure_tax_rates`
--

LOCK TABLES `tax_structure_tax_rates` WRITE;
/*!40000 ALTER TABLE `tax_structure_tax_rates` DISABLE KEYS */;
INSERT INTO `tax_structure_tax_rates` VALUES (1,1,1),(2,1,2),(3,1,3);
/*!40000 ALTER TABLE `tax_structure_tax_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_cash_audit`
--

DROP TABLE IF EXISTS `transaction_cash_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE  `transaction_cash_audit` (
  `CASH_AUDIT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` bigint(20) NOT NULL,
  `AMOUNT` double DEFAULT NULL,
  `TRANSACTION_DATE` date DEFAULT NULL,
  `TRANSACTION_TIME` time DEFAULT NULL,
  `CURRENCY` varchar(11) DEFAULT NULL,
  `PAYMENT_MODE` varchar(11) DEFAULT NULL,
  `TRANSACTION_PURPOSE` varchar(11) DEFAULT NULL,
  `TRANSACTION_TYPE` varchar(11) DEFAULT NULL,
  `IS_FULL_REFUND` tinyint(1) DEFAULT NULL,
  `IS_PARTIAL_REFUND` tinyint(1) DEFAULT NULL,
  `REFUND_AGAINST_CASH_AUDIT_ID` bigint(11) DEFAULT NULL,
  `PAYMENT_GATEWAY_USED` varchar(11) DEFAULT NULL,
  `PAYMENT_STATUS` varchar(11) DEFAULT NULL,
  `CHEQUE_STATUS` int(2) DEFAULT NULL,
  `CHEQUE_NUMBER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CASH_AUDIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='All information related to monies to be made here, if the money goining in or out\r\n\r\n\r\nTRANSACTION_TYPE :  PAID_IN:PAID_OUT\r\nTRANSACTION_MODE : CASH:CHQ:RTGS:NEFT:MOBILE_PAYMENT:E_WALLET\r\nTRANSACTION_PURPOSE :  FP:EVENT:PAID_TO_USER ( FP PURCHASED OR EVENT TRANSACTION)\r\nPAYMENT_STATUS : NOT_INITIATED, INITIATED, DECLINED, FRAUD, COD_NOT_HONORED, COMPLETE\r\n\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_cash_audit`
--

LOCK TABLES `transaction_cash_audit` WRITE;
/*!40000 ALTER TABLE `transaction_cash_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_cash_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_feast_audit`
--

DROP TABLE IF EXISTS `transaction_feast_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_feast_audit` (
  `FEAST_AUDIT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` bigint(20) NOT NULL,
  `FP_CREDIT_USER_ID` bigint(11) DEFAULT NULL,
  `FP_DEBIT_USER_ID` bigint(11) DEFAULT NULL,
  `FP_POINTS` int(11) DEFAULT NULL,
  `TRANSACTION_DATE` date DEFAULT NULL,
  `TRANSACTION_TIME` time DEFAULT NULL,
  `TRANSACTION_TYPE` BigInt(20) DEFAULT NULL,
  `FP_TYPE` varchar(11) DEFAULT NULL,
  `TRANSACTION_PURPOSE` BigInt(20) DEFAULT NULL,
  PRIMARY KEY (`FEAST_AUDIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='\r\nTRANSACTION_TYPE :  PAID_IN:PAID_OUT\r\n\r\nTRANSACTION_PURPOSE :  FP:EVENT:PAID_TO_USER ( FP PURCHASED OR EVENT TRANSACTION)\r\n\r\n\r\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_feast_audit`
--

LOCK TABLES `transaction_feast_audit` WRITE;
/*!40000 ALTER TABLE `transaction_feast_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_feast_audit` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `transaction_online_audit`;

CREATE TABLE  `transaction_online_audit` (
  `ONLINE_AUDIT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` bigint(20) NOT NULL,
  `TRANSACTION_MODE` int(2) DEFAULT '0',
  `TRANSACTION_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PAYMENT_STATUS` int(2) DEFAULT '0',
  `CURRENCY` varchar(4) NOT NULL,
  `TRANSACTION_PURPOSE` int(2) DEFAULT '0',
  PRIMARY KEY (`ONLINE_AUDIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table is used to hold data regarding the online payments.';

DROP TABLE IF EXISTS `online_transaction_status`;
CREATE TABLE `online_transaction_status` (
  `ONLINE_TRANSACTION_STATUS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDER_TRANSACTION_ID` bigint(20) DEFAULT NULL,
  `PAYMENT_TRANSACTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` varchar(20) DEFAULT NULL,
  `TRANSACTION_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PAYMENT_STATUS` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ONLINE_TRANSACTION_STATUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table will be updated from the webhook call';

--
-- Table structure for table `transaction_mode_cash`
--



--
-- Table structure for table `user_additional_fees_in`
--

DROP TABLE IF EXISTS `user_additional_fees_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_additional_fees_in` (
  `ADDITIONAL_FEES_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_DEFINED_FEE_1_FLAG` varchar(1) DEFAULT NULL,
  `USER_DEFINED_FEE_1_APPLICABILITY` varchar(20) DEFAULT NULL,
  `USER_DEFINED_FEE_NAME_1` varchar(500) DEFAULT NULL,
  `USER_DEFINED_FEE_RATE_1` double DEFAULT NULL,
  `USER_DEFINED_FEE_2_FLAG` varchar(1) DEFAULT NULL,
  `USER_DEFINED_FEE_2_APPLICABILITY` varchar(20) DEFAULT NULL,
  `USER_DEFINED_FEE_NAME_2` varchar(500) DEFAULT NULL,
  `USER_DEFINED_FEE_RATE_2` double DEFAULT NULL,
  `USER_DEFINED_FEE_3_FLAG` varchar(1) DEFAULT NULL,
  `USER_DEFINED_FEE_3_APPLICABILITY` varchar(20) DEFAULT NULL,
  `USER_DEFINED_FEE_NAME_3` varchar(500) DEFAULT NULL,
  `USER_DEFINED_FEE_RATE_3` double DEFAULT NULL,
  `USER_DEFINED_FEE_4_FLAG` varchar(1) DEFAULT NULL,
  `USER_DEFINED_FEE_4_APPLICABILITY` varchar(20) DEFAULT NULL,
  `USER_DEFINED_FEE_NAME_4` varchar(500) DEFAULT NULL,
  `USER_DEFINED_FEE_RATE_4` double DEFAULT NULL,
  `USER_DEFINED_FEE_5_FLAG` varchar(1) DEFAULT NULL,
  `USER_DEFINED_FEE_5_APPLICABILITY` varchar(20) DEFAULT NULL,
  `USER_DEFINED_FEE_NAME_5` varchar(500) DEFAULT NULL,
  `USER_DEFINED_FEE_RATE_5` double DEFAULT NULL,
  PRIMARY KEY (`ADDITIONAL_FEES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_additional_fees_in`
--

LOCK TABLES `user_additional_fees_in` WRITE;
/*!40000 ALTER TABLE `user_additional_fees_in` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_additional_fees_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_address` (
  `ADDRESS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `PINCODE` varchar(15) DEFAULT NULL,
  `STATE_CODE` varchar(200) DEFAULT NULL,
  `ADDRESS` varchar(300) DEFAULT NULL,
  `COUNTRY_CODE` varchar(5) DEFAULT NULL,
  `CITY` varchar(100) DEFAULT NULL,
  `LANDMARK` varchar(200) DEFAULT NULL,
  `LATITUDE` varchar(50) DEFAULT NULL,
  `LONGITUDE` varchar(50) DEFAULT NULL,
  `ZIPPR_CODE` varchar(10) DEFAULT NULL,
  `DEFAULT_DELIVERY_ADDRESS_FLAG` tinyint(1) DEFAULT NULL,
  `ADDRESS_TYPE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ADDRESS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cash_balance`
--

DROP TABLE IF EXISTS `user_cash_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_cash_balance` (
  `CASH_BALANCE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `TOTAL_CASH_AMOUNT` decimal(10,0) DEFAULT NULL,
  `CASH_CURRENCY` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`CASH_BALANCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cash_balance`
--

LOCK TABLES `user_cash_balance` WRITE;
/*!40000 ALTER TABLE `user_cash_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_cash_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_feast_point_detail`
--

DROP TABLE IF EXISTS `user_feast_point_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_feast_point_detail` (
  `USER_FEAST_POINT_DETAIL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FP_ID` bigint(20) NOT NULL DEFAULT '0',
  `FEAST_POINT_BALANCE_ID` bigint(20) NOT NULL DEFAULT '0',
  `USER_ID` bigint(20) NOT NULL DEFAULT '0',
  `FP_POINTS` int(11) DEFAULT NULL,
  `FP_EXPIRY_DATES` date DEFAULT NULL,
  `PROMOTIONS_START_DATE` date DEFAULT NULL,
  `PROMOTIONS_START_TIME` time DEFAULT NULL,
  `PROMOTION_EXPIRY_DATE` date DEFAULT NULL,
  `PROMOTION_EXPIRY_TIME` time DEFAULT NULL,
  `PROMOTION_EXPIRY_FLAG` varchar(1) DEFAULT NULL,
  `COUPON_CODE` varchar(100) DEFAULT NULL,
  `COUPON_CODE_EXPIRY_DATE` date DEFAULT NULL,
  `COUPNE_CODE_EXPIRED` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`USER_FEAST_POINT_DETAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_feast_point_detail`
--

LOCK TABLES `user_feast_point_detail` WRITE;
/*!40000 ALTER TABLE `user_feast_point_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_feast_point_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_financial_details_in`
--

DROP TABLE IF EXISTS `user_financial_details_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_financial_details_in` (
  `FINANCIAL_DETAIL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) DEFAULT NULL,
  `ENTERPRISE_NAME` varchar(100) DEFAULT NULL,
  `PAN` varchar(100) DEFAULT NULL,
  `TIN` varchar(100) DEFAULT NULL,
  `VAT` varchar(100) DEFAULT NULL,
  `TIN_OPERATION_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `BANK_ACCOUNT_HOLDER_NAME_1` varchar(100) DEFAULT NULL,
  `BANK_ACCOUNT_NUMBER_1` varchar(100) DEFAULT NULL,
  `BANK_IFSC_CODE_1` varchar(100) DEFAULT NULL,
  `BANK_NAME_1` varchar(100) DEFAULT NULL,
  `BANK_CITY_1` varchar(100) DEFAULT NULL,
  `BANK_BRANCH_1` varchar(100) DEFAULT NULL,
  `BANK_VERIFIED_1` varchar(1) DEFAULT NULL,
  `BANK_IS_DEFAULT_ACCOUNT_1_FLAG` varchar(1) DEFAULT NULL,
  `BANK_ACCOUNT_HOLDER_NAME_2` varchar(100) DEFAULT NULL,
  `BANK_ACCOUNT_NUMBER_2` varchar(100) DEFAULT NULL,
  `BANK_IFSC_CODE_2` varchar(100) DEFAULT NULL,
  `BANK_NAME_2` varchar(100) DEFAULT NULL,
  `BANK_CITY_2` varchar(100) DEFAULT NULL,
  `BANK_BRANCH_2` varchar(100) DEFAULT NULL,
  `BANK_VERIFIED_2` varchar(1) DEFAULT NULL,
  `BANK_IS_DEFAULT_ACCOUNT_2_FLAG` varchar(1) DEFAULT NULL,
  `BANK_AUTO_TRANSFER_ACTIVE_FLAG` varchar(1) DEFAULT NULL,
  `DEFAULT_OPERATIONAL_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `OTHER_ASSOCIATED_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `KYC_COMPLIANT_FLAG` varchar(1) DEFAULT NULL,
  `CURRENCY_CODE` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`FINANCIAL_DETAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_financial_details_in`
--

LOCK TABLES `user_financial_details_in` WRITE;
/*!40000 ALTER TABLE `user_financial_details_in` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_financial_details_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_history_favourite_items`
--

DROP TABLE IF EXISTS `user_history_favourite_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_history_favourite_items` (
  `USER_ID` bigint(20) NOT NULL,
  `FOOD_MENU_ID` bigint(20) DEFAULT NULL,
  `FAVORITE_FLAG` tinyint(1) DEFAULT NULL,
  `USER_HISTORY_FAV_ITEM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LAST_ORDER_DATE` date DEFAULT NULL,
  `NO_OF_TIMES_ORDERED` int(11) DEFAULT '0',
  PRIMARY KEY (`USER_HISTORY_FAV_ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_history_favourite_items`
--

LOCK TABLES `user_history_favourite_items` WRITE;
/*!40000 ALTER TABLE `user_history_favourite_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_history_favourite_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-29 23:37:22