package com.monsor.feasthub.endpoints.mobile;

import static org.mockito.Mockito.when;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.jpa.repository.UserRepository;
import com.monsor.feasthub.jpa.service.UserRegistrationService;

@Ignore
public class UserProfileTest {
	
	@InjectMocks
	private UserProfileEndPoint userProfileEndPoints;
	
	@Mock
	private UserProfileAppService userProfileAppService;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private UserRegistrationService userRegistrationService;
	
	@Mock
	private FhUser user ;

	@Before
	public void before(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUpdateUser() {
		final BigInteger ID = new BigInteger("1");
		
		when(user.getId()).thenReturn(ID);
		userProfileEndPoints.updateProfile(ID, user);
	}
	
	@Test
	public void testChangePassword() {
		final String USER_NAME = "amit";
		final String PASSWORD = "amit";
		final String NEW_PASSWORD = "amit1";
		
		when(user.getUserName()).thenReturn(USER_NAME);
		when(userRepository.findByUserNameAndPassword(USER_NAME, PASSWORD)).thenReturn(user);
		
		//userProfileEndPoints.changePassword(USER_NAME, PASSWORD, NEW_PASSWORD);
	}

	@Test(expected=IllegalStateException.class)
	public void testNewPasswordSameAsOld() {
		final String USER_NAME = "amit";
		final String PASSWORD = "amit";
		final String NEW_PASSWORD = "amit";
		
		when(user.getUserName()).thenReturn(USER_NAME);
		when(userRepository.findByUserNameAndPassword(USER_NAME, PASSWORD)).thenReturn(user);
		
		//userProfileEndPoints.changePassword(USER_NAME, PASSWORD, NEW_PASSWORD);
	}
}
