package com.monsor.feasthub.endpoints.mobile;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.dto.RegistrationDTO;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.jpa.repository.UserRepository;
import com.monsor.feasthub.jpa.service.UserRegistrationService;
import com.monsor.feasthub.response.FeastHubResponse;

@Ignore
public class UserRegistrationTest {

	@InjectMocks
	private UserRegistrationEndPoint userRegistration;
	
	@Mock
	private UserProfileAppService userProfileAppService;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private UserRegistrationService userRegistrationService;
	
	@Before
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
	}
	
	
//	@Test
	public void testRegistrationSuccess(){
		
		RegistrationDTO userDTO = mock(RegistrationDTO.class);
		
		when(userDTO.getUserName()).thenReturn("Amit");
		when(userDTO.getMobileNumber()).thenReturn("9960274393");
		when(userDTO.getPassword()).thenReturn("Amit");
		when(userDTO.getEmailId()).thenReturn("amit@gmail.com");
	 
		when(userProfileAppService.isUserExists(userDTO.getUserName(), userDTO.getEmailId(), userDTO.getMobileNumber())).thenReturn(null);
		userRegistration.registerUser(userDTO);
	}
	
//	@Test(expected = IllegalStateException.class)
	public void testRegistrationUsernameNull(){
		
		RegistrationDTO userDTO = mock(RegistrationDTO.class);
		
		when(userDTO.getUserName()).thenReturn(null);
		when(userDTO.getMobileNumber()).thenReturn("9960274393");
		when(userDTO.getPassword()).thenReturn("Amit");
		when(userDTO.getEmailId()).thenReturn("amit@gmail.com");
	
		
		when(userProfileAppService.isUserExists(userDTO.getUserName(), userDTO.getEmailId(), userDTO.getMobileNumber())).thenReturn(null);
		userRegistration.registerUser(userDTO);
	}
	
	//@Test(expected = IllegalStateException.class)
	public void testRegistrationPasswordEmpty(){
		
		RegistrationDTO userDTO = mock(RegistrationDTO.class);
		
		when(userDTO.getUserName()).thenReturn("Amit");
		when(userDTO.getMobileNumber()).thenReturn("9960274393");
		when(userDTO.getPassword()).thenReturn("");
		when(userDTO.getEmailId()).thenReturn("amit@gmail.com");
	
		
		when(userProfileAppService.isUserExists(userDTO.getUserName(), userDTO.getEmailId(), userDTO.getMobileNumber())).thenReturn(null);
		userRegistration.registerUser(userDTO);
	}
	
//	@Test(expected = IllegalStateException.class)
	public void testRegistrationDuplicateUserName(){
		
		RegistrationDTO userDTO = mock(RegistrationDTO.class);
		FhUser user = mock(FhUser.class);
		
		when(userDTO.getUserName()).thenReturn("Amit");
		when(userDTO.getMobileNumber()).thenReturn("9960274393");
		when(userDTO.getPassword()).thenReturn("");
		when(userDTO.getEmailId()).thenReturn("amit@gmail.com");
	
		
		//when(userProfileAppService.isUserExists(userDTO.getUserName(), userDTO.getEmailId(),userDTO.getMobileNumber())).thenReturn(user);
		userRegistration.registerUser(userDTO);
	}
	
//	@Test
	public void testIsUserExists(){
		
		FhUser user = mock(FhUser.class);
		
		when(user.getUserName()).thenReturn("Amit");
		when(user.getDefaultMobileNumber()).thenReturn("9960274393");
		when(user.getPassword()).thenReturn("");
		when(user.getDefaultEmailId()).thenReturn("amit@gmail.com");
	
	//	when(userProfileAppService.isUserExists(user.getUserName(), user.getDefaultEmailId(), user.getDefaultMobileNumber())).thenReturn(user);
	//	final FeastHubResponse newuser = userRegistration.isUserExists(user.getUserName(), user.getDefaultEmailId(), user.getDefaultMobileNumber());
		
		
	}

}
