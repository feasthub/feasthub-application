package com.monsor.feasthub.common;

import javax.annotation.PostConstruct;
import javax.ws.rs.BadRequestException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AuthorizedBaseEndPoint extends BaseEndPoint {

	@PostConstruct
	public void validateToken() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			String name = auth.getName();
			if (name == null || "".equals(name) || "client1".equals(name)) {
				throw new BadRequestException("User is not allowed to perform this action");
			}
			System.out.println("Username ::" + name);
		}
	}
}
