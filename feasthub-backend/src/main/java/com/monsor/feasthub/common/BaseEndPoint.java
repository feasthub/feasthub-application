package com.monsor.feasthub.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.monsor.feasthub.jpa.model.FeasthubError;
import com.monsor.feasthub.jpa.repository.FeasthubErrorRepository;

public abstract class BaseEndPoint {

	@Context
	private UriInfo uriInfo;
	@Autowired
	private FeasthubErrorRepository feasthubErrorRepository;

	public BaseEndPoint() {
		super();
	}

	public void logEror(Throwable e, Object... requests) {
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder jsonInString = new StringBuilder("");
		String queryParam = "";

		try {
			for (Object obj : requests) {
				jsonInString.append(mapper.writeValueAsString(obj));
			}
			queryParam = prepareParameters(uriInfo.getQueryParameters());
			feasthubErrorRepository.save(new FeasthubError(e.getMessage(), ExceptionUtils.getStackTrace(e),
					jsonInString.toString(), uriInfo.getAbsolutePath().toString() + queryParam));

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	
	public void logMessage(Object... requests) {
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder jsonInString = new StringBuilder("");
		String queryParam = "";

		try {
			for (Object obj : requests) {
				jsonInString.append(mapper.writeValueAsString(obj));
			}
			queryParam = prepareParameters(uriInfo.getQueryParameters());
			feasthubErrorRepository.save(new FeasthubError("Message", "",
					jsonInString.toString(), uriInfo.getAbsolutePath().toString() + queryParam));

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private String prepareParameters(MultivaluedMap<String, String> queryParameters) {
		StringBuilder stringBuilder = new StringBuilder("?");
		Map<String, String> parameters = new HashMap<String, String>();

		Iterator<String> it = queryParameters.keySet().iterator();

		while (it.hasNext()) {
			String theKey = (String) it.next();
			parameters.put(theKey, queryParameters.getFirst(theKey));
			stringBuilder.append(theKey + "=" + queryParameters.getFirst(theKey));
			stringBuilder.append("&");
		}

		return stringBuilder.toString();

	}

}