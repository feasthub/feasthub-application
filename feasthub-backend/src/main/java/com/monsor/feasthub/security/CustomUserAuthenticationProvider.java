package com.monsor.feasthub.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.jpa.model.user.FhUser;

public class CustomUserAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private UserProfileAppService userProfileAppService;

	private String baseurl;

	 
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		CustomUserPasswordAuthenticationToken auth = null;
		System.out.println("Base URL : " + baseurl);
		if (authentication != null && authentication.getPrincipal() != null) {
			String username = (String) authentication.getPrincipal();
			final FhUser fhUser = userProfileAppService.findByUsername(username);
			
			if (null != fhUser) {
				if(!fhUser.getMobileVerifiedFlag()){
					throw new BadCredentialsException("Mobile Verification is not completed for user please verify mobile number");
				}
				if ((fhUser.getUserName().equals(authentication.getPrincipal()) ||
						fhUser.getDefaultEmailId().equals(authentication.getPrincipal())||
								fhUser.getDefaultMobileNumber().equals(authentication.getPrincipal()))
						&& fhUser.getPassword().equals(authentication.getCredentials())) {

					List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
					auth = new CustomUserPasswordAuthenticationToken(authentication.getPrincipal(),
							authentication.getCredentials(), grantedAuthorities);
					auth.setLoginId(123);
				} else if (authentication.getPrincipal().equals("admin")
						&& authentication.getCredentials().equals("admin")) {
					List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
					auth = new CustomUserPasswordAuthenticationToken(authentication.getPrincipal(),
							authentication.getCredentials(), grantedAuthorities);
				} else if (authentication.getPrincipal().equals("user1")
						&& authentication.getCredentials().equals("user1")) {
					List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
					auth = new CustomUserPasswordAuthenticationToken(authentication.getPrincipal(),
							authentication.getCredentials(), grantedAuthorities);
				} else {
					throw new BadCredentialsException("Bad User Credentials.");
				}
			} else {
				throw new BadCredentialsException("Account does not exists, please signup");
			}

		}
		return auth;

	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {

		return true;
	}

	public String getBaseurl() {
		return baseurl;
	}

	public void setBaseurl(String baseurl) {
		this.baseurl = baseurl;
	}

}
