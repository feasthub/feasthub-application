package com.monsor.feasthub.endpoints.otp;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nonnull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.dto.FeasthubOTPDTO;
import com.monsor.feasthub.jpa.model.otp.FeasthubOtp;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.otp.GenerateAndVerifyOTP;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.response.SuccessResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/otp")
@Group(name = "/otp", title = "otp")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FeasthubOtpEndPoint extends BaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(FeasthubOtpEndPoint.class);
	@Autowired
	private UserProfileAppService userProfileAppService;

	// Base URL
	private static String baseUrl = "https://sendotp.msg91.com/api/";
	// Your application key
	private static String applicationKey = "KTKwyLuDxG6md1ln0PiF0rvQvLCw8PpXFrqShTrP_T0eDNK2MojbvG0avMbyvkIrkPGYYKPNlAuh3cIviZvWbvT6dZISQ4Hy_9YqXPHKxvc0U_wBnz1AjOcjMay1VbI7XWgMsb3UHJKjK25CWthydA==";

	@POST
	@Path("/generateOTP")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse registerUser(@RequestBody @Nonnull final FeasthubOTPDTO otpDTO) {
		GenerateAndVerifyOTP genOTP = new GenerateAndVerifyOTP(baseUrl, applicationKey);
		try {
			FeasthubOtp otp = null;
			checkNotNull(otpDTO, "Invalid otpDTO must not be null");
			checkNotNull(otpDTO.getCountryCode(), "countryCode must not be null");
			checkNotNull(otpDTO.getMobileNumber(), "mobileNumber cannot be null");
			checkNotNull(otpDTO.getGetGeneratedOTP(), "getGeneratedOTP must not be null");

			SuccessResponse response = genOTP.generateOTP(otpDTO.getCountryCode(), otpDTO.getMobileNumber(),
					otpDTO.getGetGeneratedOTP());
			if("200".equals(response.getResponseCode())){
				response = new SuccessResponse("200", "OTP Successfully sent to registered mobile number. Please verify mobile number");
			}else{
				throw new Exception("Failed to verify mobile number.");
			}
			return response;
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, otpDTO);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, otpDTO);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@POST
	@Path("/verifyOTP")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse verifyOTP(@RequestBody @Nonnull final FeasthubOTPDTO otpDTO) {
		GenerateAndVerifyOTP genOTP = new GenerateAndVerifyOTP(baseUrl, applicationKey);
		try {
			checkNotNull(otpDTO, "Invalid otpDTO must not be null");
			checkNotNull(otpDTO.getCountryCode(), "countryCode must not be null");
			checkNotNull(otpDTO.getMobileNumber(), "mobileNumber cannot be null");
			checkNotNull(otpDTO.getOneTimePassword(), "OneTimePassword cannot be null");

			FhUser fhUser = userProfileAppService.findByDefaultMobileNumber(otpDTO.getMobileNumber());
			checkNotNull(fhUser, "There exists no user with this phone number.");
			SuccessResponse response = genOTP.verifyOTP(otpDTO.getCountryCode(), otpDTO.getMobileNumber(),
					otpDTO.getOneTimePassword());
			if("200".equals(response.getResponseCode())){
				fhUser.setMobileVerifiedFlag(Boolean.TRUE);
				userProfileAppService.update(fhUser);
				response = new SuccessResponse("200", "Mobile verified successfully");
			}else{
				throw new Exception("Failed to verify mobile number.");
			}
			return response;

		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, otpDTO);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, otpDTO);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}

	}

}
