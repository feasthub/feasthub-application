/**
 * 
 */
package com.monsor.feasthub.endpoints.mobile;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.dto.ChangePasswordDTO;
import com.monsor.feasthub.dto.RegistrationDTO;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/registrations")
@Group(name = "/registrations", title = "registrations")
@HttpCode("500>Internal Server Error,200>Success Response")
public class UserRegistrationEndPoint extends BaseEndPoint {

    private static final Logger LOG = LoggerFactory.getLogger(UserRegistrationEndPoint.class);

    @Autowired
    private UserProfileAppService userProfileAppService;

    @POST
    @Path("/registeruser")
    @RequestMapping(
	consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })

    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse registerUser(@RequestBody @Nonnull final RegistrationDTO userDTO) {
	try {
	    checkNotNull(userDTO, "Invalid userDTO must not be null");
	    checkNotNull(userDTO.getPassword(), "Password cannot be null");
	    checkNotNull(userDTO.getMobileNumber(), "Mobile number must not be null");

	    checkState(
		    isUserExisting(userDTO.getUserName(), userDTO.getEmailId(),
			    userDTO.getMobileNumber()).size() ==0,
		    "User already exist, Please choose different combination of username, email id and mobile number");

	    final FhUser fhUser = new FhUser(userDTO.getUserName(), userDTO.getEmailId(),
		    userDTO.getMobileNumber(), userDTO.getPassword(), false);
	    fhUser.setMobileVerifiedFlag(Boolean.FALSE);
	    userProfileAppService.create(fhUser);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", fhUser);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, userDTO);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, userDTO);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    

    @PUT
    @Path("/resetPassword/{username}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse resetPassword(@PathParam(value = "username") final String username,
	    @RequestBody final ChangePasswordDTO resetPassword) {

	try {
	    checkNotNull(resetPassword, "This resetPassword must not be null");
	    checkState(StringUtils.isNotEmpty(username), "This userid must not be null");
	    checkState(StringUtils.isNotEmpty(resetPassword.getNewPassword()),
		    "This newPassword must not be null");
	    checkState(StringUtils.isNotEmpty(resetPassword.getConfirmPassword()),
		    "This confirm password must not be null");
	    checkState(resetPassword.getNewPassword().equals(resetPassword.getConfirmPassword()),
		    "Passwords does not match.");
	    userProfileAppService.resetPassword(username, resetPassword.getNewPassword());
	    final FhUser user = userProfileAppService.findByUsername(username);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    private List<FhUser> isUserExisting(String username, String emailId, String mobileNumber) {
	List<FhUser> fhUser = userProfileAppService.isUserExists(username, emailId, mobileNumber);
	return fhUser;
    }
}
