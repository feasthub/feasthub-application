package com.monsor.feasthub.endpoints.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.delivery.DeliveryLocationAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.delivery.DeliveryLocation;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/deliverylocations")
@Group(name = "/deliverylocations", title = "deliverylocations")
@HttpCode("500>Internal Server Error,200>Success Response")
public class DeliveryLocationEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryLocationEndPoint.class);
	@Autowired
	private DeliveryLocationAppService deliveryLocationAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse createDeliveryLocation(@RequestBody @Nonnull DeliveryLocation deliverylocation) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverylocation :: " + deliverylocation);
			}

			deliverylocation = deliveryLocationAppService.create(deliverylocation);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverylocation);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverylocation);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverylocation);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse updateDeliveryLocation(@PathParam(value = "id") final BigInteger id,
			@RequestBody @Nonnull DeliveryLocation deliverylocation) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverylocation :: " + deliverylocation);
			}
			checkNotNull(id, "Please provide the resource id ");
			checkState(id != null || !id.equals(deliverylocation.getId()), "No resource found with matching id");
			deliverylocation = deliveryLocationAppService.update(deliverylocation);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverylocation);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverylocation);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverylocation);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse deleteDeliveryLocation(@RequestBody @Nonnull final BigInteger deliverylocationId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverylocation :: " + deliverylocationId);
			}
			checkNotNull(deliverylocationId, "This deliverylocationId must not be null");
			deliveryLocationAppService.delete(deliverylocationId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverylocation :: ");
			}
			List<DeliveryLocation> items = deliveryLocationAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger deliverylocationId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverylocation :: " + deliverylocationId);
			}
			checkNotNull(deliverylocationId, "This deliverylocationId must not be null");
			final DeliveryLocation deliverylocation = deliveryLocationAppService.findById(deliverylocationId);

			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverylocation);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validatedeliverylocations/{deliverylocationname}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isDeliveryLocationExists(
			@PathParam(value = "deliverylocationname") @Nullable final BigInteger deliverylocationname) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isDeliveryLocationExists");
			}
			if (deliverylocationname == null) {
				throw new BadRequestException("Please provide either deliverylocationname");
			}

			final DeliveryLocation deliveryLocation = deliveryLocationAppService
					.isDeliveryLocationExists(deliverylocationname);

			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryLocation);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/findByLatLng/{latitude}/{longitude}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findByLattitudeAndLongitude(@PathParam(value = "latitude") @Nullable final Double latitude,
			@PathParam(value = "longitude") @Nullable final Double longitude) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside findByLatLng");
			}
			if ((latitude == null) || (longitude == null)) {
				throw new BadRequestException("Please provide either deliverylocationname");
			}

			final DeliveryLocation deliveryLocation = deliveryLocationAppService.findByLattitudeAndLongitude(latitude,
					longitude);

			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryLocation);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
