package com.monsor.feasthub.endpoints.app;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.EventDisplayCategoryAppService;
import com.monsor.feasthub.appservice.event.EventDisplayCategoryToEventMappingAppService;
import com.monsor.feasthub.appservice.mobile.delivery.DeliverySlotAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.app.StartUpConfig;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlot;
import com.monsor.feasthub.jpa.model.events.EventDisplayCategory;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/startup/config/params")
@Group(name = "/startup/config/params", title = "params")
@HttpCode("500>Internal Server Error,200>Success Response")
public class StartUpParamsEndPoint extends BaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(StartUpParamsEndPoint.class);

	@Autowired
	private DeliverySlotAppService deliveryslotAppService;

	@Autowired
	private EventDisplayCategoryAppService eventDisplayCategoryAppService;

	@Autowired
	private EventDisplayCategoryToEventMappingAppService eventDisplayCategoryToEventMappingAppService;

	@GET
	@Path("/")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse getConfigParams() {
		try {

			List<DeliverySlot> eventDeliverySlots = deliveryslotAppService.findAll();
			List<EventDisplayCategory> eventDisplayCategories = eventDisplayCategoryAppService.findAll();
			StartUpConfig configParams = new StartUpConfig();
			configParams.setId(getRandomBigInteger());
			configParams.setEventDisplayCategories(eventDisplayCategories);
			configParams.setEventDeliverySlots(eventDeliverySlots);
			return ResponseUtil.getResponse("200", "Operation completed successfully", configParams);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	public static BigInteger getRandomBigInteger() {
		Random rand = new Random();
		BigInteger upperLimit = new BigInteger("13");
		BigInteger result;
		do {
			result = new BigInteger(upperLimit.bitLength(), rand); // (2^4-1) =
																	// 15 is the
																	// maximum
																	// value
		} while (result.compareTo(upperLimit) >= 0); // exclusive of 13
		return result;
	}
}
