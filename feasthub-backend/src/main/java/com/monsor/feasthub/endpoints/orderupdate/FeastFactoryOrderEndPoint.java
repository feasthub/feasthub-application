package com.monsor.feasthub.endpoints.orderupdate;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.annotation.Nonnull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.dto.FeastFactoryDTO;
import com.monsor.feasthub.response.FeastHubResponse;

@Path("/v1.0/feasthub/factory/orderupdate")
@Group(name = "/factoryorderupdate", title = "feast factory order update")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FeastFactoryOrderEndPoint extends AuthorizedBaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(FeastFactoryOrderEndPoint.class);

	@POST
	@Path("/checkout")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFactoryOrder(@RequestBody @Nonnull final FeastFactoryDTO[] feastFactoryDTO) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside  updateFactoryOrder :: " + feastFactoryDTO);
		}
		for (FeastFactoryDTO order : feastFactoryDTO) {
			checkNotNull(order, "This orderUpdateDTO must not be null");
			checkNotNull(order.getEventId(), "This eventid must not be null");
			checkNotNull(order.getPurchaserUserId(), "This userId must not be null");
			checkNotNull(order.getUnit(), "This unit must not be null");
			checkNotNull(order.getDeliveryDate(), "This delivery date must not be null");
			checkNotNull(order.getDeliveryTime(), "This delivery time  must not be null");
			checkNotNull(order.getDeliveryAddressId(), "This delivery address  must not be null");
			checkNotNull(order.getFoodMenuId(), "This food menu id  must not be null");
			checkNotNull(order.getOrderedQuantity(), "This ordered quantity  must not be null");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				formatter.parse(order.getDeliveryDate());
				new BigInteger(order.getEventId());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				LOG.error(e.getMessage(), e);
				super.logEror(e, order);
			}

		}
		return null;

	}
}
