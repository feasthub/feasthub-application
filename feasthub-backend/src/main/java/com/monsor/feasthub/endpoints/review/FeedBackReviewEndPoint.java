package com.monsor.feasthub.endpoints.review;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.FhEventAppService;
import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.appservice.review.FeedBackReviewAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.dto.FeedbackAndReviewDTO;
import com.monsor.feasthub.jpa.model.events.FhEvent;
import com.monsor.feasthub.jpa.model.review.FeedbackAndReview;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.jpa.model.view.FhUserViewForReview;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/reviews")
@Group(name = "/useraddress", title = "useraddress")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FeedBackReviewEndPoint extends BaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(FeedBackReviewEndPoint.class);
	@Autowired
	private FeedBackReviewAppService feedBackReviewAppService;

	@Autowired
	private UserProfileAppService userProfileAppService;

	@Autowired
	private FhEventAppService fheventAppService;

	@POST
	@Path("/{userId}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFeedBackReview(@PathParam("userId") BigInteger userId,
			@RequestBody @Nonnull FeedbackAndReviewDTO feedbackAndReviewDTO) {
		Date date = null;
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside create FeedbackAndReview :: " + feedbackAndReviewDTO);
			}
			FeedbackAndReview feedbackAndReview = null;
			if (feedbackAndReviewDTO.getUserId().compareTo(userId) == 0) {
				checkNotNull(feedbackAndReviewDTO.getUserId(), "This userid must not be null");
				checkNotNull(feedbackAndReviewDTO.getFoodMenuId(), "This foodmenuid must not be null");
				checkNotNull(feedbackAndReviewDTO.getEventId(), "This eventid type must not be null");
				checkNotNull(feedbackAndReviewDTO.getRating(), "This rating type must not be null");
				checkNotNull(feedbackAndReviewDTO.getReviewComment(), "This review comment type must not be null");
				checkNotNull(feedbackAndReviewDTO.getReviewDate(), "This review date type must not be null");
				FhUser user = userProfileAppService.findById(userId);
				FhUserViewForReview user1 = new FhUserViewForReview(user.getId(), user.getUserName(),
						user.getFirstName(), user.getLastName(), user.getMiddleName());

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				try {
					date = formatter.parse(feedbackAndReviewDTO.getReviewDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					LOG.error(e.getMessage(), e);
					super.logEror(e);
				}
				feedbackAndReview = new FeedbackAndReview(user1, feedbackAndReviewDTO.getFoodMenuId(),
						feedbackAndReviewDTO.getEventId(), feedbackAndReviewDTO.getRating(),
						feedbackAndReviewDTO.getReviewComment(), feedbackAndReviewDTO.getMarkedAsOffensive(),
						feedbackAndReviewDTO.getDeletedFlag(), date);
				feedbackAndReview = feedBackReviewAppService.create(feedbackAndReview);
				Long countByEventId = feedBackReviewAppService.countByEventId(feedbackAndReviewDTO.getEventId());
				Long ratingSum = feedBackReviewAppService.getSumOfRatings(feedbackAndReviewDTO.getEventId());
				Double avgRating = (double) ((double) ratingSum / (double) countByEventId);
				System.out.println("avgRating is: " + avgRating);
				FhEvent fhevent = fheventAppService.findById(feedbackAndReviewDTO.getEventId());
				fhevent.setEventAverageReview(avgRating);
				fhevent = fheventAppService.update(fhevent);
				return ResponseUtil.getResponse("200", "Operation completed successfully", feedbackAndReview);
			} else {
				return ResponseUtil.getErrorResponse("300", "Conflict in User id's");
			}

		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, feedbackAndReviewDTO);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, feedbackAndReviewDTO);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/findreviews/{eventId}/page/{page}/itemperpage/{itemperpage}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findPagedDate(@PathParam("eventId") BigInteger eventId, @PathParam("page") int page,
			@PathParam("itemperpage") int itemperpage) {
		Page<FeedbackAndReview> items = null;
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside  findPagedDate :: ");
			}
			items = feedBackReviewAppService.getPageableReview(eventId,
					new PageRequest(page, itemperpage, Sort.Direction.DESC, "reviewDate"));
			return ResponseUtil.getResponse("200", "Operation Completed Successfully", items.getContent());
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register fhevent :: ");
			}
			List<FeedbackAndReview> items = feedBackReviewAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	/*
	 * @GET
	 * 
	 * @Path("/firstFive")
	 * 
	 *  
	 * 
	 * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
	 * MediaType.APPLICATION_XML })
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
	 * MediaType.TEXT_PLAIN }) public FeastHubResponse findFirst5Reviews() { try
	 * { if (LOG.isDebugEnabled()) { LOG.debug("Inside register fhevent :: "); }
	 * List<FeedbackAndReview> items =
	 * feedBackReviewAppService.findTop5Review(); return
	 * ResponseUtil.getResponse("200", "Operation completed successfully",
	 * items); } catch (IllegalStateException e) { LOG.error(e.getMessage(), e);
	 * super.logEror(e); return ResponseUtil.getErrorResponse("412",
	 * e.getMessage()); } catch (Exception e) { LOG.error(e.getMessage(), e);
	 * super.logEror(e); return ResponseUtil.getErrorResponse("500",
	 * e.getMessage()); } }
	 * 
	 */
	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger reviewId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside findById FeedbackAndReview :: ");
			}
			checkNotNull(reviewId, "This reviewId must not be null");
			FeedbackAndReview item = feedBackReviewAppService.findById(reviewId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", item);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
