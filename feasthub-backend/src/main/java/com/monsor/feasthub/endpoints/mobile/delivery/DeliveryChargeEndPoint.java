package com.monsor.feasthub.endpoints.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.delivery.DeliveryChargeAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.delivery.DeliveryCharge;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/deliverycharges")
@Group(name = "/deliverycharges", title = "deliverycharges")
@HttpCode("500>Internal Server Error,200>Success Response")
public class DeliveryChargeEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryChargeEndPoint.class);
	@Autowired
	private DeliveryChargeAppService deliveryChargeAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse createDeliveryCharge(@RequestBody @Nonnull DeliveryCharge deliverycharge) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverycharge :: " + deliverycharge);
			}
			checkNotNull(deliverycharge, "deliverycharge can not be null");
			deliverycharge = deliveryChargeAppService.create(deliverycharge);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverycharge);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverycharge);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverycharge);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse updateDeliveryCharge(@PathParam(value = "id") final BigInteger id,
			@RequestBody @Nonnull DeliveryCharge deliverycharge) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverycharge :: " + deliverycharge);
			}
			checkNotNull(deliverycharge, "deliverycharge can not be null");
			checkNotNull(id, "Invalid resource requested to update");
			checkState(id.equals(deliverycharge.getId()),
					"Invalid Delivery Charges, Please provide valid deliver charges");

			deliverycharge = deliveryChargeAppService.update(deliverycharge);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverycharge);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverycharge);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliverycharge);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse deleteDeliveryCharge(@PathParam("id") @Nonnull final BigInteger deliverychargeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverycharge :: " + deliverychargeId);
			}
			checkNotNull(deliverychargeId, "Invalid resource requested to delete");
			deliveryChargeAppService.delete(deliverychargeId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverycharge :: ");
			}

			final List<DeliveryCharge> items = deliveryChargeAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger deliverychargeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliverycharge :: " + deliverychargeId);
			}
			checkNotNull(deliverychargeId, "This deliverychargeId must not be null");

			final DeliveryCharge deliverycharge = deliveryChargeAppService.findById(deliverychargeId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliverycharge);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validatedeliverycharges/{deliverychargename}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isDeliveryChargeExists(
			@PathParam(value = "deliverychargename") @Nullable final BigInteger deliverychargename) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isDeliveryChargeExists");
			}
			if (deliverychargename == null) {
				throw new BadRequestException("Please provide either deliverychargename");
			}

			final DeliveryCharge deliveryCharge = deliveryChargeAppService.isDeliveryChargeExists(deliverychargename);

			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryCharge);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
