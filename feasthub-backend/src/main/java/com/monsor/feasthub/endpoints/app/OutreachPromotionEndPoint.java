package com.monsor.feasthub.endpoints.app;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.app.OutreachPromotionAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.app.OutreachPromotion;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/outreachpromotions")
@Group(name = "/outreachpromotions", title = "outreachpromotions")
@HttpCode("500>Internal Server Error,200>Success Response")
public class OutreachPromotionEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(OutreachPromotionEndPoint.class);
	@Autowired
	private OutreachPromotionAppService currentpromotionAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createOutreachPromotion(@RequestBody @Nonnull OutreachPromotion currentpromotion) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: " + currentpromotion);
			}
			checkNotNull(currentpromotion.getPromoCode(), "This promoCode must not be null");
			checkState(isResourceExists(currentpromotion.getPromoCode()) == null,
					"This record is already exits, Please provide different combination of data");
			currentpromotion = currentpromotionAppService.create(currentpromotion);
			return ResponseUtil.getResponse("200", "Operation completed successfully", currentpromotion);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, currentpromotion);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, currentpromotion);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateOutreachPromotion(@RequestBody @Nonnull OutreachPromotion currentpromotion) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: " + currentpromotion);
			}
			checkNotNull(currentpromotion.getPromoCode(), "This promoCode must not be null");
			currentpromotion = currentpromotionAppService.update(currentpromotion);
			return ResponseUtil.getResponse("200", "Operation completed successfully", currentpromotion);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, currentpromotion);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, currentpromotion);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteCurrentPromotion(@RequestBody @Nonnull final BigInteger currentpromotionId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: " + currentpromotionId);
			}
			checkNotNull(currentpromotionId, "This currentpromotionId must not be null");
			currentpromotionAppService.delete(currentpromotionId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: ");
			}
			List<OutreachPromotion> items = currentpromotionAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger currentpromotionId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: " + currentpromotionId);
			}
			checkNotNull(currentpromotionId, "This currentpromotionId must not be null");
			final OutreachPromotion currentpromotion = currentpromotionAppService.findById(currentpromotionId);

			return ResponseUtil.getResponse("200", "Operation completed successfully", currentpromotion);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchCurrentPromotions(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register currentpromotion :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<OutreachPromotion> items = currentpromotionAppService.searchCurrentPromotions(search);
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	// 0
	@GET
	@Path("/validatecurrentpromotions/{promocode}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isResourceExists(
			@PathParam(value = "promocode") @Nullable final java.lang.String promoCode) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isCurrentPromotionExists");
			}
			checkNotNull(promoCode, "This promoCode must not be null");

			final OutreachPromotion CurrentPromotion = currentpromotionAppService.isResourceExists(promoCode);

			return ResponseUtil.getResponse("200", "Operation completed successfully", CurrentPromotion);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
