package com.monsor.feasthub.endpoints.event;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.FhEventAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.events.EventsCrossSalesEventItem;
import com.monsor.feasthub.jpa.model.events.FhEvent;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/search")
@Group(name = "/search", title = "feast search")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FeastSearchEndPoint extends BaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(FeastSearchEndPoint.class);

	@Autowired
	private FhEventAppService fheventAppService;

	@GET
	@Path("/event")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse getEvents(@QueryParam(value = "lat") String lat, @QueryParam(value = "lng") String lng, @QueryParam(value="foodPreference")final String foodPreference) {
		try {
			checkNotNull(lat, "latitude can not be null");
			checkNotNull(lng, "langitute can not be null");
			System.out.println("latitude is: " + lat);
			System.out.println("langitute is: " + lng);
			List<FhEvent> eventList = fheventAppService.getEvents(lat, lng,foodPreference);
			return ResponseUtil.getResponse("200", "Operation completed successfully", eventList);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, lat, lng);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, lat, lng);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/event/feastfactory")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchfeastFactory(@QueryParam(value = "lat") String lat,
			@QueryParam(value = "lng") String lng) {
		try {
			checkNotNull(lat, "latitude can not be null");
			checkNotNull(lng, "langitute can not be null");
			List<FhEvent> eventList = fheventAppService.searchfeastFactory(lat, lng);
			return ResponseUtil.getResponse("200", "Operation completed successfully", eventList);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/event/{eventId}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam(value = "eventId") BigInteger eventId) {
		try {
			checkNotNull(eventId, "eventId can not be null");
			FhEvent event = fheventAppService.findById(eventId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", event);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/event/crossSales/{eventId}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findCrossSalesEvent(@PathParam(value = "eventId") BigInteger eventId) {
		try {
			checkNotNull(eventId, "eventId can not be null");
			List<EventsCrossSalesEventItem> crossSalesEvent = fheventAppService.findCrossSalesEvent(eventId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", crossSalesEvent);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
