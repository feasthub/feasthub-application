package com.monsor.feasthub.endpoints.event;

import javax.ws.rs.Path;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;

@Path("/v1.0/feasthub/foodmenuitems/{foodmenuitem}/relateditems")
@Group(name = "/relatedfooditems", title = "relatedfooditems")
@HttpCode("500>Internal Server Error,200>Success Response")
public class EventsCrossSalesEventItemEndPoint extends AuthorizedBaseEndPoint {
	/*
	 * private static final Logger LOG =
	 * LoggerFactory.getLogger(RelatedFoodItemEndPoint.class);
	 * 
	 * @Autowired private RelatedFoodItemAppService relatedfooditemAppService;
	 * 
	 * @Autowired private FoodMenuItemAppService foodMenuItemAppService;
	 * 
	 * @POST
	 * 
	 * @Path("/{id}")
	 * 
	 *  
	 * 
	 * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
	 * MediaType.APPLICATION_XML })
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
	 * MediaType.TEXT_PLAIN }) public FeastHubResponse
	 * createRelatedFoodItem(@PathParam("foodmenuitem") BigInteger
	 * foodMenuItemId,
	 * 
	 * @PathParam("id") BigInteger relatedFoodMenuItemId) { try {
	 * RelatedFoodItem relatedFoodMenuItem = new RelatedFoodItem();
	 * relatedFoodMenuItem.setFoodMenuId(foodMenuItemId); FoodMenuItem
	 * foodMenuItem = null; FoodMenuItemView foodMenuItemView =
	 * foodMenuItemAppService.findFoodMenuItemViewById(relatedFoodMenuItemId);
	 * relatedFoodMenuItem.setRelatedFoodMenuItem(foodMenuItemView);
	 * relatedfooditemAppService.create(relatedFoodMenuItem); foodMenuItem =
	 * foodMenuItemAppService.findById(foodMenuItemId); return
	 * ResponseUtil.getResponse("200", "Operation completed Successfully",
	 * foodMenuItem); } catch (IllegalStateException e) {
	 * LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("412", e.getMessage()); } catch (Exception
	 * e) { LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("500", e.getMessage()); } }
	 * 
	 * @DELETE
	 * 
	 * @Path("/{id}")
	 * 
	 *  
	 * 
	 * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
	 * MediaType.APPLICATION_XML })
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
	 * MediaType.TEXT_PLAIN }) public FeastHubResponse
	 * deleteRelatedFoodItem(@PathParam("foodmenuitem") BigInteger
	 * foodMenuItemId,
	 * 
	 * @PathParam("id") BigInteger relatedFoodMenuItemId) { try { if
	 * (LOG.isDebugEnabled()) { LOG.debug("Inside register relatedfooditem :: "
	 * + relatedFoodMenuItemId); } checkNotNull(relatedFoodMenuItemId,
	 * "This relatedfooditemId must not be null");
	 * relatedfooditemAppService.delete(relatedFoodMenuItemId); FoodMenuItem
	 * foodMenuItem = foodMenuItemAppService.findById(foodMenuItemId); return
	 * ResponseUtil.getResponse("200", "Operation completed Successfully",
	 * foodMenuItem); } catch (IllegalStateException e) {
	 * LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("412", e.getMessage()); } catch (Exception
	 * e) { LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("500", e.getMessage()); } }s
	 */
}
