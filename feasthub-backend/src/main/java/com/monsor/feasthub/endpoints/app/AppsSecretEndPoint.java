package com.monsor.feasthub.endpoints.app;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.app.AppsSecretAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.app.AppsSecret;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/appssecrets")
@Group(name = "/appssecrets", title = "appssecrets")
@HttpCode("500>Internal Server Error,200>Success Response")
public class AppsSecretEndPoint extends BaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(AppsSecretEndPoint.class);
	@Autowired
	private AppsSecretAppService appSecretAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createAppsSecret(@RequestBody @Nonnull AppsSecret appssecret) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register appssecret :: " + appssecret);
			}
			checkNotNull(appssecret.getAppsName(), "This appsName must not be null");
			checkNotNull(appssecret.getClientId(), "This clientId must not be null");
			checkState(isResourceExists(appssecret.getAppsName(), appssecret.getClientId()) == null,
					"This record is already exits, Please provide different combination of data");
			appssecret = appSecretAppService.create(appssecret);
			return ResponseUtil.getResponse("200", "Operation completed successfully", appssecret);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecret);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecret);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateAppsSecret(@RequestBody @Nonnull AppsSecret appssecret) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register appssecret :: " + appssecret);
			}
			checkNotNull(appssecret.getAppsName(), "This appsName must not be null");
			checkNotNull(appssecret.getClientId(), "This clientId must not be null");
			appssecret = appSecretAppService.update(appssecret);
			return ResponseUtil.getResponse("200", "Operation completed successfully", appssecret);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecret);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecret);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteAppsSecret(@RequestBody @Nonnull final BigInteger appssecretId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register appssecret :: " + appssecretId);
			}
			checkNotNull(appssecretId, "This appssecretId must not be null");
			appSecretAppService.delete(appssecretId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecretId);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, appssecretId);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register appssecret :: ");
			}

			List<AppsSecret> items = appSecretAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger appssecretId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register appssecret :: " + appssecretId);
			}
			checkNotNull(appssecretId, "This appssecretId must not be null");
			final AppsSecret appssecret = appSecretAppService.findById(appssecretId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", appssecret);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	// 1
	@GET
	@Path("/validateappssecrets/{appsname}/{clientid}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isResourceExists(@PathParam(value = "appsname") @Nullable final java.lang.String appsName,
			@PathParam(value = "clientid") @Nullable final java.lang.String clientId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isAppsSecretExists");
			}
			checkNotNull(appsName, "This appsName must not be null");
			checkNotNull(clientId, "This clientId must not be null");
			final AppsSecret appsSecret = appSecretAppService.isResourceExists(appsName, clientId);

			return ResponseUtil.getResponse("200", "Operation completed successfully", appsSecret);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
