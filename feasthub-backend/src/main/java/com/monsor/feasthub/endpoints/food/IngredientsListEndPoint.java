package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.IngredientsListAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.MasterGroceryIngredientsList;
import com.monsor.feasthub.response.FeastHubResponse;
/**
 * @author Amit
 *
 */
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/ingredientslists")
@Group(name = "/ingredientslists", title = "ingredientslists")
@HttpCode("500>Internal Server Error,200>Success Response")
public class IngredientsListEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(IngredientsListEndPoint.class);
	@Autowired
	private IngredientsListAppService ingredientslistAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createMasterGroceryIngredientsList(
			@RequestBody @Nonnull MasterGroceryIngredientsList ingredientslist) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register ingredientslist :: " + ingredientslist);
			}
			ingredientslist = ingredientslistAppService.create(ingredientslist);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", ingredientslist);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, ingredientslist);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, ingredientslist);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateMasterGroceryIngredientsList(
			@PathParam("id") @Nonnull final BigInteger ingredientslistId,
			@RequestBody @Nonnull MasterGroceryIngredientsList ingredientslist) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register ingredientslist :: " + ingredientslist);
			}
			checkState(ingredientslistId.equals(ingredientslist.getId()), "Provided respource not matching with data");

			ingredientslist = ingredientslistAppService.update(ingredientslist);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", ingredientslist);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteIngredientsList(@PathParam("id") @Nonnull final BigInteger ingredientslistId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register ingredientslist :: " + ingredientslistId);
			}
			checkNotNull(ingredientslistId, "This ingredientslistId must not be null");
			ingredientslistAppService.delete(ingredientslistId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register ingredientslist :: ");
			}
			List<MasterGroceryIngredientsList> items = ingredientslistAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger ingredientslistId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register ingredientslist :: " + ingredientslistId);
			}
			checkNotNull(ingredientslistId, "This ingredientslistId must not be null");
			final MasterGroceryIngredientsList ingredientslist = ingredientslistAppService.findById(ingredientslistId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", ingredientslist);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
