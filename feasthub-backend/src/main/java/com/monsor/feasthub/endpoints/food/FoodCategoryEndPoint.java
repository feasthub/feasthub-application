package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodCategoryAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodCategory;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/foodcategories")
@Group(name = "/foodcategorys", title = "foodcategorys")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodCategoryEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodCategoryEndPoint.class);
	@Autowired
	private FoodCategoryAppService foodcategoryAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodCategory(@RequestBody @Nonnull FoodCategory foodcategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: " + foodcategory);
			}

			checkState(StringUtils.isEmpty(foodcategory.getKeyId()) || StringUtils.isEmpty(foodcategory.getParentId()),
					"Please provide either key id or parent id");

			/*
			 * if (StringUtils.isEmpty(foodcategory.getKeyId())) { String newKey
			 * = foodCategoryKeyGeneratorService.getNextKey(foodcategory.
			 * getParentId()); foodcategory.setKeyId(newKey); }
			 */

			foodcategory = foodcategoryAppService.create(foodcategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodcategory);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodcategory);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodCategory(@PathParam("id") @Nonnull final BigInteger foodcategoryId,
			@RequestBody @Nonnull FoodCategory foodcategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: " + foodcategory);
			}

			checkState(foodcategoryId.equals(foodcategory.getId()), "Provided respource not matching with data");

			checkNotNull(foodcategory.getKeyId(), "This key must not be null");

			foodcategory = foodcategoryAppService.update(foodcategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodcategory);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodcategory);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodCategory(@PathParam("id") @Nonnull final BigInteger foodcategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: " + foodcategoryId);
			}
			checkNotNull(foodcategoryId, "This foodcategoryId must not be null");
			foodcategoryAppService.delete(foodcategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: ");
			}
			List<FoodCategory> items = foodcategoryAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/sub-categories/{parentId}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll(@PathParam("parentId") String parentId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: ");
			}
			List<FoodCategory> items = foodcategoryAppService.findAllByParentId(parentId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodcategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: " + foodcategoryId);
			}
			checkNotNull(foodcategoryId, "This foodcategoryId must not be null");
			final FoodCategory foodcategory = foodcategoryAppService.findById(foodcategoryId);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchFoodCategorys(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodcategory :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<FoodCategory> items = foodcategoryAppService.searchFoodCategorys(search);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	// 1
	@GET
	@Path("/validatefoodcategorys/{foodcategoryname}/key/{keyid}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isResourceExists(
			@PathParam(value = "foodcategoryname") @Nullable final java.lang.String foodcategoryname,
			@PathParam(value = "keyid") @Nullable final String keyId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isFoodCategoryExists");
			}
			checkNotNull(foodcategoryname, "This foodCategoryKey must not be null");
			checkNotNull(keyId, "This keyId must not be null");
			final FoodCategory foodCategory = foodcategoryAppService.isFoodCategoryExists(foodcategoryname, keyId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodCategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
