package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodMenuItemAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodMenuItem;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/foodmenuitems")
@Group(name = "/foodmenuitems", title = "foodmenuitems")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodMenuItemEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodMenuItemEndPoint.class);
	@Autowired
	private FoodMenuItemAppService foodmenuitemAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodMenuItem(@RequestBody @Nonnull FoodMenuItem foodmenuitem) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: " + foodmenuitem);
			}
			checkState(isFoodMenuItemExists(foodmenuitem.getFoodName()).getData() == null,
					"This record is already exits, Please provide different combination of data");

			foodmenuitem = foodmenuitemAppService.create(foodmenuitem);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenuitem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenuitem);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenuitem);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}

	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodMenuItem(@PathParam("id") @Nonnull final BigInteger foodMenuId,
			@RequestBody @Nonnull FoodMenuItem foodmenuitem) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: " + foodmenuitem);
			}
			checkState(foodMenuId.equals(foodmenuitem.getId()), "Provided resource not matching with data");
			foodmenuitem = foodmenuitemAppService.update(foodmenuitem);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenuitem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenuitem);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenuitem);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodMenuItem(@PathParam("id") @Nonnull final BigInteger foodmenuitemId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: " + foodmenuitemId);
			}
			checkNotNull(foodmenuitemId, "This foodmenuitemId must not be null");
			foodmenuitemAppService.delete(foodmenuitemId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: ");
			}
			List<FoodMenuItem> items = foodmenuitemAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodmenuitemId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: " + foodmenuitemId);
			}
			checkNotNull(foodmenuitemId, "This foodmenuitemId must not be null");
			final FoodMenuItem foodmenuitem = foodmenuitemAppService.findById(foodmenuitemId);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenuitem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchFoodMenuItems(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenuitem :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<FoodMenuItem> items = foodmenuitemAppService.search(search);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validatefoodmenuitems/{foodmenuitemname}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isFoodMenuItemExists(
			@PathParam(value = "foodmenuitemname") @Nullable final String foodmenuitemname) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isFoodMenuItemExists");
			}
			if (foodmenuitemname == null) {
				throw new BadRequestException("Please provide either foodmenuitemname");
			}

			final FoodMenuItem foodMenuItem = foodmenuitemAppService.isFoodMenuItemExists(foodmenuitemname);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodMenuItem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/copyfoodmenuitem/{foodmenuitem}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse copyFoodMenuItems(@PathParam(value = "foodmenuitem") @Nonnull BigInteger foodMenuItemId) {
		try {
			checkNotNull(foodMenuItemId, "foodMenuItemId must not be null");
			final FoodMenuItem foodMenuItem = foodmenuitemAppService.copyItem(foodMenuItemId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodMenuItem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}

	}

	/*
	 * @PUT
	 * 
	 * @Path(
	 * "/customfooditems/{foodmenuitem}/additionalitems/{additionalitemlistid}")
	 * 
	 * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
	 * MediaType.APPLICATION_XML })
	 * 
	 * @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
	 * MediaType.TEXT_PLAIN }) public FeastHubResponse changeToCustomFoodItem(
	 * 
	 * @PathParam(value = "foodmenuitem") @Nonnull BigInteger
	 * complexFoodMenuItemId,
	 * 
	 * @PathParam(value = "additionalitemlistid") @Nonnull BigInteger
	 * additionalItemListId,
	 * 
	 * @RequestBody List<CustomFoodCategoriesItems> customFoodCategoriesItems) {
	 * try { checkNotNull(complexFoodMenuItemId,
	 * "foodMenuItemId must not be null"); checkNotNull(additionalItemListId,
	 * "additionalItemListId must not be null");
	 * checkNotNull(customFoodCategoriesItems,
	 * "customFoodCategoriesItems must not be null");
	 * checkState(customFoodCategoriesItems.size() >0,
	 * "There must be atleast one item to customize"); final FoodMenuItem
	 * foodMenuItem =
	 * foodmenuitemAppService.changeToCustomFoodItem(complexFoodMenuItemId,
	 * additionalItemListId, customFoodCategoriesItems); return
	 * ResponseUtil.getResponse("200", "Operation completed Successfully",
	 * foodMenuItem); } catch (IllegalStateException e) {
	 * LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("412", e.getMessage()); } catch (Exception
	 * e) { LOG.error(e.getMessage(), e); super.logEror(e); return
	 * ResponseUtil.getErrorResponse("500", e.getMessage()); } }
	 */
	@PUT
	@Path("/additionalfooditems/{foodmenuitem}/customitems/{customitemid}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse changeToAdditionalItem(
			@PathParam(value = "foodmenuitem") @Nonnull BigInteger complexFoodMenuItemId,
			@PathParam(value = "customitemid") @Nonnull BigInteger additionalItemListId) {
		try {
			checkNotNull(complexFoodMenuItemId, "foodMenuItemId must not be null");
			checkNotNull(additionalItemListId, "additionalItemListId must not be null");
			final FoodMenuItem foodMenuItem = foodmenuitemAppService.changeToAdditionalItem(complexFoodMenuItemId,
					additionalItemListId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodMenuItem);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}

	}

}
