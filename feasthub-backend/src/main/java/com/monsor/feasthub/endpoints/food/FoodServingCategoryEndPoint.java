package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodServingCategoryAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodMenuServingCategory;
import com.monsor.feasthub.response.FeastHubResponse;
/**
 * @author Amit
 *
 */
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/foodservingcategories")
@Group(name = "/foodservingcategories", title = "foodservingcategories")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodServingCategoryEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodServingCategoryEndPoint.class);
	@Autowired
	private FoodServingCategoryAppService foodservingcategoryAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodMenuServingCategory(
			@RequestBody @Nonnull FoodMenuServingCategory foodservingcategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodservingcategory :: " + foodservingcategory);
			}
			foodservingcategory = foodservingcategoryAppService.create(foodservingcategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodservingcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodservingcategory);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodservingcategory);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodMenuServingCategory(
			@PathParam("id") @Nonnull final BigInteger foodservingcategoryId,
			@RequestBody @Nonnull FoodMenuServingCategory foodservingcategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodservingcategory :: " + foodservingcategory);
			}
			checkState(foodservingcategoryId.equals(foodservingcategory.getId()),
					"Provided resource not matching with data");
			foodservingcategory = foodservingcategoryAppService.update(foodservingcategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodservingcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodServingCategory(
			@PathParam("id") @Nonnull final BigInteger foodservingcategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodservingcategory :: " + foodservingcategoryId);
			}
			checkNotNull(foodservingcategoryId, "This foodservingcategoryId must not be null");
			foodservingcategoryAppService.delete(foodservingcategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodservingcategory :: ");
			}
			List<FoodMenuServingCategory> items = foodservingcategoryAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodservingcategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodservingcategory :: " + foodservingcategoryId);
			}
			checkNotNull(foodservingcategoryId, "This foodservingcategoryId must not be null");
			final FoodMenuServingCategory foodservingcategory = foodservingcategoryAppService
					.findById(foodservingcategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodservingcategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
