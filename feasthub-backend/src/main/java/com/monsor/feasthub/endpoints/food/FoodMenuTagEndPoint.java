package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodMenuTagAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodMenuTag;
import com.monsor.feasthub.response.FeastHubResponse;
/**
 * @author Amit
 *
 */
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/foodmenutags")
@Group(name = "/foodmenutags", title = "foodmenutags")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodMenuTagEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodMenuTagEndPoint.class);
	@Autowired
	private FoodMenuTagAppService foodmenutagAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodMenuTag(@RequestBody @Nonnull FoodMenuTag foodmenutag) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenutag :: " + foodmenutag);
			}
			foodmenutag = foodmenutagAppService.create(foodmenutag);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenutag);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenutag);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenutag);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodMenuTag(@PathParam("id") @Nonnull final BigInteger foodmenutagId,
			@RequestBody @Nonnull FoodMenuTag foodmenutag) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenutag :: " + foodmenutag);
			}
			checkState(foodmenutagId.equals(foodmenutag.getId()), "Provided resource not matching with data");

			foodmenutag = foodmenutagAppService.update(foodmenutag);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenutag);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenutag);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodmenutag);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodMenuTag(@PathParam("id") @Nonnull final BigInteger foodmenutagId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenutag :: " + foodmenutagId);
			}
			checkNotNull(foodmenutagId, "This foodmenutagId must not be null");
			foodmenutagAppService.delete(foodmenutagId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenutag :: ");
			}
			List<FoodMenuTag> items = foodmenutagAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodmenutagId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodmenutag :: " + foodmenutagId);
			}
			checkNotNull(foodmenutagId, "This foodmenutagId must not be null");
			final FoodMenuTag foodmenutag = foodmenutagAppService.findById(foodmenutagId);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodmenutag);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
