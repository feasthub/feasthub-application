package com.monsor.feasthub.endpoints.event;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.EventCuisineListingAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.events.EventCuisineListing;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/eventcuisinelistings")
@Group(name = "/eventcuisinelistings", title = "eventcuisinelistings")
@HttpCode("500>Internal Server Error,200>Success Response")
public class EventCuisineListingEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(EventCuisineListingEndPoint.class);
	@Autowired
	private EventCuisineListingAppService eventcuisinelistingAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse createEventCuisineListing(@RequestBody @Nonnull EventCuisineListing eventcuisinelisting) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelisting);
			}
			checkNotNull(eventcuisinelisting, "deliverycharge can not be null");

			checkState(isEventCuisineListingExists(eventcuisinelisting.getId()) != null,
					"This record is already exits, Please provide different combination of data");

			eventcuisinelisting = eventcuisinelistingAppService.create(eventcuisinelisting);
			return ResponseUtil.getResponse("200", "Operation completed successfully", eventcuisinelisting);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse updateEventCuisineListing(@PathParam("id") @Nonnull final BigInteger eventcuisinelistingId,
			@RequestBody @Nonnull EventCuisineListing eventcuisinelisting) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelisting);
			}
			checkNotNull(eventcuisinelisting, "deliverycharge can not be null");
			checkNotNull(eventcuisinelistingId, "Invalid resource requested to update");
			checkState(eventcuisinelistingId.equals(eventcuisinelisting.getId()),
					"Invalid Delivery Charges, Please provide valid deliver charges");

			eventcuisinelisting = eventcuisinelistingAppService.update(eventcuisinelisting);
			return ResponseUtil.getResponse("200", "Operation completed successfully", eventcuisinelisting);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventcuisinelisting);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventcuisinelisting);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse deleteEventCuisineListing(
			@PathParam("id") @Nonnull final BigInteger eventcuisinelistingId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelistingId);
			}
			eventcuisinelistingAppService.delete(eventcuisinelistingId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventcuisinelisting :: ");
			}
			List<EventCuisineListing> items = eventcuisinelistingAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger eventcuisinelistingId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelistingId);
			}
			checkNotNull(eventcuisinelistingId, "This eventcuisinelistingId must not be null");
			final EventCuisineListing eventcuisinelisting = eventcuisinelistingAppService
					.findById(eventcuisinelistingId);

			return ResponseUtil.getResponse("200", "Operation completed successfully", eventcuisinelisting);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validateeventcuisinelistings/{eventcuisinelistingname}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isEventCuisineListingExists(
			@PathParam(value = "eventcuisinelistingname") @Nullable final BigInteger eventcuisinelistingname) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isEventCuisineListingExists");
			}
			if (eventcuisinelistingname == null) {
				throw new BadRequestException("Please provide either eventcuisinelistingname");
			}

			final EventCuisineListing eventCuisineListing = eventcuisinelistingAppService
					.isEventCuisineListingExists(eventcuisinelistingname);

			return ResponseUtil.getResponse("200", "Operation completed successfully", eventCuisineListing);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
