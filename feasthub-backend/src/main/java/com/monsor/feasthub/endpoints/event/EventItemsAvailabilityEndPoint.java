package com.monsor.feasthub.endpoints.event;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.EventItemsAvailabilityAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.events.EventItemsAvailability;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/eventitemsavailabilitys")
@Group(name = "/eventitemsavailabilitys", title = "eventitemsavailabilitys")
@HttpCode("500>Internal Server Error,200>Success Response")
public class EventItemsAvailabilityEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(EventItemsAvailabilityEndPoint.class);
	@Autowired
	private EventItemsAvailabilityAppService eventitemsavailabilityAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createEventItemsAvailability(
			@RequestBody @Nonnull EventItemsAvailability eventitemsavailability) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailability);
			}
			checkNotNull(eventitemsavailability.getEventId(), "This eventId must not be null");
			eventitemsavailability = eventitemsavailabilityAppService.create(eventitemsavailability);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", eventitemsavailability);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventitemsavailability);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventitemsavailability);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateEventItemsAvailability(
			@RequestBody @Nonnull EventItemsAvailability eventitemsavailability) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailability);
			}
			checkNotNull(eventitemsavailability.getEventId(), "This eventId must not be null");
			eventitemsavailability = eventitemsavailabilityAppService.update(eventitemsavailability);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", eventitemsavailability);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventitemsavailability);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, eventitemsavailability);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteEventItemsAvailability(
			@RequestBody @Nonnull final BigInteger eventitemsavailabilityId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailabilityId);
			}
			checkNotNull(eventitemsavailabilityId, "This eventitemsavailabilityId must not be null");
			eventitemsavailabilityAppService.delete(eventitemsavailabilityId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: ");
			}
			List<EventItemsAvailability> items = eventitemsavailabilityAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger eventitemsavailabilityId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailabilityId);
			}
			checkNotNull(eventitemsavailabilityId, "This eventitemsavailabilityId must not be null");
			final EventItemsAvailability eventitemsavailability = eventitemsavailabilityAppService
					.findById(eventitemsavailabilityId);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", eventitemsavailability);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchEventItemsAvailabilitys(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register eventitemsavailability :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<EventItemsAvailability> items = eventitemsavailabilityAppService.searchEventItemsAvailabilitys(search);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	// 1
	@GET
	@Path("/validateeventitemsavailabilitys/{eventid}/{foodid}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isResourceExists(@PathParam(value = "eventid") @Nullable final java.math.BigInteger eventid,
			@PathParam(value = "foodid") @Nullable final java.math.BigInteger foodid) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isEventItemsAvailabilityExists");
			}
			checkNotNull(eventid, "This eventId must not be null");
			checkNotNull(foodid, "This foodId must not be null");

			final List<EventItemsAvailability> items = eventitemsavailabilityAppService.isResourceExists(eventid,
					foodid);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
