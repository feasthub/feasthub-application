package com.monsor.feasthub.endpoints.app;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.app.BusinessTypesToServicesMappingAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.app.BusinessTypesToServicesMapping;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/businesstypestoservicesmappings")
@Group(name = "/businesstypestoservicesmappings", title = "businesstypestoservicesmappings")
@HttpCode("500>Internal Server Error,200>Success Response")
public class BusinessTypesToServicesMappingEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(BusinessTypesToServicesMappingEndPoint.class);
	@Autowired
	private BusinessTypesToServicesMappingAppService businesstypestoservicesmappingAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createBusinessTypesToServicesMapping(
			@RequestBody @Nonnull BusinessTypesToServicesMapping businesstypestoservicesmapping) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmapping);
			}
			checkNotNull(businesstypestoservicesmapping.getAdditionalServiceOffering(),
					"This additionalServiceOffering must not be null");
			checkNotNull(businesstypestoservicesmapping.getServicesOfferingTypes(),
					"This servicesOfferingTypes must not be null");
			businesstypestoservicesmapping = businesstypestoservicesmappingAppService
					.create(businesstypestoservicesmapping);
			return ResponseUtil.getResponse("200", "Operation completed successfully", businesstypestoservicesmapping);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, businesstypestoservicesmapping);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, businesstypestoservicesmapping);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateBusinessTypesToServicesMapping(
			@RequestBody @Nonnull BusinessTypesToServicesMapping businesstypestoservicesmapping) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmapping);
			}
			checkNotNull(businesstypestoservicesmapping.getAdditionalServiceOffering(),
					"This additionalServiceOffering must not be null");
			checkNotNull(businesstypestoservicesmapping.getServicesOfferingTypes(),
					"This servicesOfferingTypes must not be null");
			businesstypestoservicesmapping = businesstypestoservicesmappingAppService
					.update(businesstypestoservicesmapping);
			return ResponseUtil.getResponse("200", "Operation completed successfully", businesstypestoservicesmapping);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, businesstypestoservicesmapping);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, businesstypestoservicesmapping);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteBusinessTypesToServicesMapping(
			@RequestBody @Nonnull final BigInteger businesstypestoservicesmappingId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmappingId);
			}
			checkNotNull(businesstypestoservicesmappingId, "This businesstypestoservicesmappingId must not be null");
			businesstypestoservicesmappingAppService.delete(businesstypestoservicesmappingId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register businesstypestoservicesmapping :: ");
			}
			List<BusinessTypesToServicesMapping> items = businesstypestoservicesmappingAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger businesstypestoservicesmappingId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmappingId);
			}
			checkNotNull(businesstypestoservicesmappingId, "This businesstypestoservicesmappingId must not be null");
			final BusinessTypesToServicesMapping businesstypestoservicesmapping = businesstypestoservicesmappingAppService
					.findById(businesstypestoservicesmappingId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", businesstypestoservicesmapping);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
