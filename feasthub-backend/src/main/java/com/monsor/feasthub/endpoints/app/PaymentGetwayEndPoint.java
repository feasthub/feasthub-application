package com.monsor.feasthub.endpoints.app;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.PaymentGatewayAppService;
import com.monsor.feasthub.appservice.mobile.orderupdate.OrderTransactionAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;
import com.monsor.feasthub.jpa.model.payment.PaymentGateway;
import com.monsor.feasthub.payment.dto.WebHookRequestDTO;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/p1.0/feasthub/paymentgateway")
@Group(name = "/paymentgateway", title = "paymentgateway")
@HttpCode("500>Internal Server Error,200>Success Response")
public class PaymentGetwayEndPoint extends BaseEndPoint {

    private static final Logger LOG = LoggerFactory.getLogger(PaymentGetwayEndPoint.class);

    @Autowired
    private PaymentGatewayAppService paymentGatewayAppService;

    @Autowired
    private OrderTransactionAppService orderTransactionAppService;

    @GET
    @Path("/")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse listGateway() {

	try {
	    if ( LOG.isDebugEnabled() ) {
		LOG.debug("Inside PaymentGatewayEndpoint::listGateway  :: ");
	    }

	    List<PaymentGateway> gateways = paymentGatewayAppService.findAll();
	    return ResponseUtil.getResponse("200", "Operation completed successfully", gateways);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }
    
    
    public static  <T> WebHookRequestDTO parseJsonToObject(String messageAsString, Class<T> valueType) {
   	try {
   		ObjectMapper mapper = new ObjectMapper();
   		return (WebHookRequestDTO) mapper.readValue(messageAsString, valueType);
   	} catch (IOException exception) {
   		exception.printStackTrace();
   	}
   	return null;
   }

    @POST
    @Path("/webhook")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
   // public void webHookCall(@RequestBody WebHookRequestDTO request) {

    public void webHookCall(@RequestParam String payloadMessage) {
	 super.logMessage(payloadMessage);
	
	WebHookRequestDTO request  = parseJsonToObject(payloadMessage, WebHookRequestDTO.class);
	try {
	    super.logMessage(request);
	    checkNotNull(request, "request can not be null");
	    checkNotNull(request.getPayload(), "Payload can not be null");
	    checkNotNull(request.getPayload().getPayment(), "Payment info can not be null");
	    checkNotNull(request.getPayload().getPayment().getEntity(), "Entity info can not be null");
	    checkNotNull(request.getPayload().getPayment().getEntity().getId(), "Id can not be null");

	    String id = request.getPayload().getPayment().getEntity().getId();

	    OrderTransaction order = orderTransactionAppService.searchByOrderId(id);
	    checkNotNull(order, "No order found with matching order id");
	    if ( "payment.authorized".equalsIgnoreCase(request.getEvent()) ) {
		order.setPaymentStatus(PaymentStatus.CONFIRM);
		orderTransactionAppService.update(order);
		orderTransactionAppService.updatePaymentStatus(id, request.getPayload().getPayment().getEntity()
			.getAmount(), 0, request.getEvent());
		;
	    } else {
		order.setPaymentStatus(PaymentStatus.DECLINED);
		orderTransactionAppService.update(order);
		orderTransactionAppService.updatePaymentStatus(id, null, -1, request.getEvent());
	    }
	} catch (IllegalStateException e) {
	    super.logEror(e,request);
	} catch (Exception e) {
	    super.logEror(e,request);
	}

    }
}
