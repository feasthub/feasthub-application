package com.monsor.feasthub.endpoints.useraddress;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.user.UserAddressAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.user.UserAddress;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/users/{userId}/address")
@Group(name = "/useraddress", title = "useraddress")
@HttpCode("500>Internal Server Error,200>Success Response")
public class UserAddressEndPoint extends AuthorizedBaseEndPoint {
    private static final Logger LOG = LoggerFactory.getLogger(UserAddressEndPoint.class);

    @Autowired
    private UserAddressAppService userAddressAppService;

    @POST
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse createUserAddress(@PathParam("userId") BigInteger userId,
	    @RequestBody @Nonnull UserAddress userAddress) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside create useraddress :: " + userAddress);
	    }
	    if (userAddress.getUserId().compareTo(userId) == 0) {
		checkNotNull(userAddress.getUserId(), "This userid must not be null");
		checkNotNull(userAddress.getAddress(), "This address must not be null");
		checkNotNull(userAddress.getAddressType(), "This address type must not be null");
		if (userAddress.getDefaultDeliveryAddressFlag() != null && userAddress.getDefaultDeliveryAddressFlag()) {
		    markOtherAddressAsNotDefault(userId);
		}
		userAddress = userAddressAppService.create(userAddress);
		return ResponseUtil.getResponse("200", "Operation completed successfully",
			userAddress);
	    } else {
		return ResponseUtil.getErrorResponse("300", "Conflict in User id's");
	    }

	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e,userAddress);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e,userAddress);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    private void markOtherAddressAsNotDefault(BigInteger userId) {
	// TODO Auto-generated method stub
	List<UserAddress> listAddress = userAddressAppService.findByUserId(userId);
	if(!CollectionUtils.isEmpty(listAddress)){
	listAddress.stream().filter(item -> item.getDefaultDeliveryAddressFlag())
		.forEach(selectItem -> {
		    selectItem.setDefaultDeliveryAddressFlag(false);
		    userAddressAppService.update(selectItem);
		});
	}
    }

    @PUT
    @Path("/{addressId}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse updateUserAddress(@PathParam("userId") BigInteger userId,
	    @PathParam("addressId") @Nonnull final BigInteger addressId,
	    @RequestBody @Nonnull UserAddress userAddress) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside update user address :: " + userAddress);
	    }
	    checkNotNull(userAddress, "This address object must not be null");
	    checkNotNull(userAddress.getUserId(), "This userid must not be null");
	    checkNotNull(userAddress.getAddress(), "This address must not be null");
	    checkNotNull(userAddress.getAddressType(), "This address type must not be null");
	    /*
	     * we check if user is in URL parameter and inside Address object
	     * are same or not we also check if address about to be updated
	     * exists for that user or not
	     */

	    UserAddress address = findByAddressId(addressId);

	    if ((userAddress.getUserId().compareTo(userId) == 0)) {
		if (null != address && null != address.getUserId()
			&& address.getUserId().compareTo(userId) == 0) {
		    userAddress.setId(addressId);
		    if (userAddress.getDefaultDeliveryAddressFlag()) {
			markOtherAddressAsNotDefault(userId);
		    }
		    userAddress = userAddressAppService.update(userAddress);
		    return ResponseUtil.getResponse("200", "Operation completed successfully",
			    userAddress);
		} else {
		    return ResponseUtil.getErrorResponse("412", "This address does not exists");
		}

	    } else {
		return ResponseUtil.getErrorResponse("300", "Conflict in User id's");
	    }

	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @DELETE
    @Path("/{addressId}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse deleteUserAddress(@PathParam("userId") BigInteger userId,
	    @PathParam("addressId") @Nonnull final BigInteger addressId) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside delete user address :: " + addressId);
	    }
	    checkNotNull(addressId, "This addressId must not be null");
	    UserAddress address = findByAddressId(addressId);
	    if (null != address && address.getUserId().compareTo(userId) == 0) {
		userAddressAppService.delete(addressId);
		return ResponseUtil.getResponse("200", "Operation completed successfully");
	    } else {
		return ResponseUtil.getResponse("300", "Conflict in User Id's");
	    }

	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    /*
     * @GET
     * 
     *  
     * 
     * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
     * MediaType.APPLICATION_XML })
     * 
     * @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
     * MediaType.TEXT_PLAIN }) public FeastHubResponse findAll() { try { if
     * (LOG.isDebugEnabled()) { LOG.debug("Inside findAll address :: "); }
     * List<UserAddress> items = userAddressAppService.findAll(); return
     * ResponseUtil.getResponse("200", "Operation completed successfully",
     * items); } catch (IllegalStateException e) { LOG.error(e.getMessage(), e);
     * super.logEror(e); return ResponseUtil.getErrorResponse("412",
     * e.getMessage()); } catch (Exception e) { LOG.error(e.getMessage(), e);
     * super.logEror(e); return ResponseUtil.getErrorResponse("500",
     * e.getMessage()); } }
     */
    @GET
    @Path("/{addressId}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findById(@PathParam("addressId") @Nonnull final BigInteger addressId) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside findById userAddress :: " + addressId);
	    }
	    checkNotNull(addressId, "This fheventId must not be null");
	    final UserAddress userAddress = findByAddressId(addressId);

	    return ResponseUtil.getResponse("200", "Operation completed successfully", userAddress);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findByUserId(@PathParam("userId") BigInteger userId) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside findByUserId userAddress :: " + userId);
	    }
	    checkNotNull(userId, "This userId must not be null");
	    final List<UserAddress> userAddress = userAddressAppService.findByUserId(userId);

	    return ResponseUtil.getResponse("200", "Operation completed successfully", userAddress);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    private UserAddress findByAddressId(BigInteger addressId) {
	return userAddressAppService.findById(addressId);
    }

}
