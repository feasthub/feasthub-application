package com.monsor.feasthub.endpoints.mobile;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodMenuItemAppService;
import com.monsor.feasthub.appservice.mobile.UserHistoryFavouriteItemAppService;
import com.monsor.feasthub.appservice.mobile.orderupdate.OrderTransactionAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.dto.UserHistoryDTO;
import com.monsor.feasthub.jpa.model.enums.OrderStatus;
import com.monsor.feasthub.jpa.model.food.FoodMenuItem;
import com.monsor.feasthub.jpa.model.order.OrderDelivery;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;
import com.monsor.feasthub.jpa.model.order.OrderTransactionView;
import com.monsor.feasthub.jpa.model.user.UserHistoryFavouriteItem;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/userfavhistories")
@Group(name = "/foodreceipes", title = "foodreceipes")
@HttpCode("500>Internal Server Error,200>Success Response")
public class UserHistoryFavouriteItemEndPoint extends AuthorizedBaseEndPoint {

    private static final Logger LOG = LoggerFactory.getLogger(UserHistoryFavouriteItemEndPoint.class);

    @Autowired
    private UserHistoryFavouriteItemAppService userHistoryFavItemAppService;

    @Autowired
    private OrderTransactionAppService orderTransactionAppService;

    @Autowired
    private FoodMenuItemAppService foodmenuitemAppService;

    @GET

    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findAll() {

	try {
	    if ( LOG.isDebugEnabled() ) {
		LOG.debug("Inside findAll UserHistoryFavouriteItem :: ");
	    }
	    List<UserHistoryFavouriteItem> items = userHistoryFavItemAppService.findAll();
	    return ResponseUtil.getResponse("200", "Operation completed successfully", items);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    /*
     * to get history for a particular user
     */
    @GET
    @Path("/histories/users/{id}/page/{page}/itemperpage/{itemperpage}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findHistoryByUserId(@PathParam("id") @Nonnull final BigInteger userId,
	    @PathParam("page") int page, @PathParam("itemperpage") int itemperpage) {

	try {
	    if ( LOG.isDebugEnabled() ) {
		LOG.debug("Inside findById UserHistoryFavouriteItem :: " + userId);
	    }
	    itemperpage = 10;
	    checkNotNull(userId, "This userHisFavId must not be null");
	    final List<OrderTransactionView> userHistory = orderTransactionAppService.findAllOrderByUserId(userId,
		    new PageRequest(page, itemperpage)).getContent();
	    UserHistoryDTO userHistoryDTO = new UserHistoryDTO();
	    Date date = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	    final Date date1 = sdf.parse(sdf.format(date));
	    userHistory.stream().filter(item1 -> item1.getPurchaserUserId().equals(userId)).forEach(item1 -> {
		if ( item1.getOrderDate().compareTo(date1) == 0 ) {
		    userHistoryDTO.getTodaysOrders().add(item1);
		} else {
		    final OrderDelivery delivery = item1.getOrderDelivery().get(item1.getOrderDelivery().size() - 1);
		    if ( delivery.getDeliveryDate().compareTo(date1) < 1 ) {
			userHistoryDTO.getPastOrders().add(item1);
		    } else {
			userHistoryDTO.getLiveOrders().add(item1);
		    }
		}

	    });

	    return ResponseUtil.getResponse("200", "Operation completed successfully", userHistoryDTO);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    /*
     * to get fav. items for a particular user
     */
    @GET
    @Path("/favourite/users/{userid}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findFavByUserId(@PathParam("userid") @Nonnull final BigInteger userId) {

	try {
	    if ( LOG.isDebugEnabled() ) {
		LOG.debug("Inside findFavByUserId UserHistoryFavouriteItem :: " + userId);
	    }
	    checkNotNull(userId, "This userHisFavId must not be null");
	    final List<UserHistoryFavouriteItem> userFavItems = userHistoryFavItemAppService.findFavByUserId(userId);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", userFavItems);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    /*
     * @POST
     * 
     * @Path("/histories")
     * 
     * 
     * 
     * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
     * MediaType.APPLICATION_XML }) public void createUserHistoryFavouriteItem(
     * 
     * @RequestBody @Nonnull final UserHistoryFavouriteItem userHistoryFavItem)
     * { if (LOG.isDebugEnabled()) { LOG.debug(
     * "Inside  createUserHistoryFavouriteItem :: " + userHistoryFavItem); }
     * checkNotNull(userHistoryFavItem.getUserId(),
     * "This userId must not be null");
     * checkNotNull(userHistoryFavItem.getFoodMenuId(),
     * "This FoodMenuId must not be null"); final
     * UserHistoryFavouriteItemRepository userHistoryFavItemAppService =
     * userHistoryFavItemService .getuserHistoryFavItemAppService();
     * userHistoryFavItemAppService.save(userHistoryFavItem); }
     */

    @PUT
    @Path("/favourite/users/{userid}/history/{foodMenuId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse makeHistoryTofavourite(@PathParam("userid") @Nonnull final BigInteger userId,
	    @PathParam("foodMenuId") @Nonnull final BigInteger foodMenuId) {

	try {
	    if ( LOG.isDebugEnabled() ) {
		LOG.debug("Inside findFavByUserId UserHistoryFavouriteItem :: " + userId);
	    }
	    checkNotNull(foodMenuId, "This foodMenuId must not be null");
	    FoodMenuItem userFoodMenuId = foodmenuitemAppService.findById(foodMenuId);
	    UserHistoryFavouriteItem item = new UserHistoryFavouriteItem();
	    item.setFavoriteFlag(true);
	    item.setFoodMenuItem(userFoodMenuId);
	    item.setUserId(userId);
	    item = userHistoryFavItemAppService.create(item);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", item);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

}
