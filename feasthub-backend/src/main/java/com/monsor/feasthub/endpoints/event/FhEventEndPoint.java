package com.monsor.feasthub.endpoints.event;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.event.FhEventAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.events.FhEvent;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/fhevents")
@Group(name = "/fhevents", title = "fhevents")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FhEventEndPoint extends BaseEndPoint {

    private static final Logger LOG = LoggerFactory.getLogger(FhEventEndPoint.class);

    @Autowired
    private FhEventAppService fheventAppService;

    @POST
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse createFhEvent(@RequestBody @Nonnull FhEvent fhevent) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside register fhevent :: " + fhevent);
	    }
	    checkNotNull(fhevent.getBusinessType(), "This businessType must not be null");
	    checkState(isFhEventExists(fhevent.getEventName()).getData() == null,
		    "Event already exists");

	    fhevent = fheventAppService.create(fhevent);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", fhevent);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findAll() {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside register fhevent :: ");
	    }
	    List<FhEvent> items = fheventAppService.findAll();
	    return ResponseUtil.getResponse("200", "Operation completed successfully", items);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
    @Path("/{id}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger fheventId) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside register fhevent :: " + fheventId);
	    }
	    checkNotNull(fheventId, "This fheventId must not be null");
	    final FhEvent fhevent = fheventAppService.findById(fheventId);

	    return ResponseUtil.getResponse("200", "Operation completed successfully", fhevent);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
    @Path("/search")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse searchFhEvents(
	    @QueryParam(value = "search") @Nonnull final String search) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside register fhevent :: " + search);
	    }
	    checkNotNull(search, "This search must not be null");
	    List<FhEvent> items = fheventAppService.searchFhEvents(search);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", items);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
    @Path("/validatefhevents/{fheventname}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse isFhEventExists(
	    @PathParam(value = "fheventname") @Nullable final String fheventname) {
	try {
	    if (LOG.isDebugEnabled()) {
		LOG.debug("Inside isFhEventExists");
	    }
	    if (fheventname == null) {
		throw new BadRequestException("Please provide either fheventname");
	    }

	    final FhEvent fhEvent = fheventAppService.isFhEventExists(fheventname);

	    return ResponseUtil.getResponse("200", "Operation completed successfully", fhEvent);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }
}
