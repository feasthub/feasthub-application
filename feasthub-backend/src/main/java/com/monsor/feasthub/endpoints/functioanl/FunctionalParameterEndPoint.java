package com.monsor.feasthub.endpoints.functioanl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.functioanl.FunctionalParameterAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.fuctional.FunctionalParameter;
import com.monsor.feasthub.response.FeastHubResponse;
/**
 * @author Amit
 *
 */
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/functionalparameters")
@Group(name = "/functionalparameters", title = "functionalparameters")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FunctionalParameterEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FunctionalParameterEndPoint.class);
	@Autowired
	private FunctionalParameterAppService functionalparameterAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFunctionalParameter(@RequestBody @Nonnull FunctionalParameter functionalparameter) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameter :: " + functionalparameter);
			}
			checkNotNull(functionalparameter.getCategoryId(), "This categoryId must not be null");
			checkNotNull(functionalparameter.getFunctionalParametersKey(),
					"This functionalParametersKey must not be null");
			checkState(isResourceExists(functionalparameter.getFunctionalParametersKey()) != null,
					"This record is already exits, Please provide different combination of data");
			functionalparameter = functionalparameterAppService.create(functionalparameter);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameter);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameter);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameter);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFunctionalParameter(@RequestBody @Nonnull FunctionalParameter functionalparameter) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameter :: " + functionalparameter);
			}
			checkNotNull(functionalparameter.getCategoryId(), "This categoryId must not be null");
			checkNotNull(functionalparameter.getFunctionalParametersKey(),
					"This functionalParametersKey must not be null");
			functionalparameter = functionalparameterAppService.update(functionalparameter);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameter);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFunctionalParameter(@RequestBody @Nonnull final BigInteger functionalparameterId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameter :: " + functionalparameterId);
			}
			checkNotNull(functionalparameterId, "This functionalparameterId must not be null");
			functionalparameterAppService.delete(functionalparameterId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameter :: ");
			}
			List<FunctionalParameter> items = functionalparameterAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger functionalparameterId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameter :: " + functionalparameterId);
			}
			checkNotNull(functionalparameterId, "This functionalparameterId must not be null");
			final FunctionalParameter functionalparameter = functionalparameterAppService
					.findById(functionalparameterId);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameter);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	// 0
	@GET
	@Path("/validatefunctionalparameters/{functionalparameterskey}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isResourceExists(
			@PathParam(value = "functionalparameterskey") @Nullable final java.lang.String functionalParametersKey) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isFunctionalParameterExists");
			}
			checkNotNull(functionalParametersKey, "This functionalParametersKey must not be null");

			final FunctionalParameter functionalParameter = functionalparameterAppService
					.isResourceExists(functionalParametersKey);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalParameter);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
