package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodReceipeAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodMenuReceipe;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/foodreceipes")
@Group(name = "/foodreceipes", title = "foodreceipes")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodReceipeEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodReceipeEndPoint.class);
	@Autowired
	private FoodReceipeAppService foodreceipeAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodMenuReceipe(@RequestBody @Nonnull FoodMenuReceipe foodreceipe) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: " + foodreceipe);
			}
			checkNotNull(foodreceipe.getFoodCategoryId(), "This foodCategoryId must not be null");
			checkNotNull(foodreceipe.getPrimaryIngredientsList(), "This primaryIngredientsList must not be null");
			foodreceipe = foodreceipeAppService.create(foodreceipe);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodreceipe);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodMenuReceipe(@PathParam("id") @Nonnull final BigInteger foodreceipeId,
			@RequestBody @Nonnull FoodMenuReceipe foodreceipe) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: " + foodreceipe);
			}

			checkNotNull(foodreceipe.getFoodCategoryId(), "This foodCategoryId must not be null");
			checkState(foodreceipeId.equals(foodreceipe.getId()), "Requested resource not found");
			checkNotNull(foodreceipe.getPrimaryIngredientsList(), "This primaryIngredientsList must not be null");
			foodreceipe = foodreceipeAppService.update(foodreceipe);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodreceipe);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodReceipe(@PathParam("id") @Nonnull final BigInteger foodreceipeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: " + foodreceipeId);
			}
			checkNotNull(foodreceipeId, "This foodreceipeId must not be null");
			foodreceipeAppService.delete(foodreceipeId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: ");
			}
			List<FoodMenuReceipe> items = foodreceipeAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodreceipeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: " + foodreceipeId);
			}
			checkNotNull(foodreceipeId, "This foodreceipeId must not be null");
			final FoodMenuReceipe foodreceipe = foodreceipeAppService.findById(foodreceipeId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodreceipe);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchFoodReceipes(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodreceipe :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<FoodMenuReceipe> items = foodreceipeAppService.search(search);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validatefoodreceipes/{foodreceipename}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isFoodReceipeExists(
			@PathParam(value = "foodreceipename") @Nullable final BigInteger foodcategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isFoodReceipeExists");
			}
			if (foodcategoryId == null) {
				throw new BadRequestException("Please provide either foodreceipename");
			}

			final FoodMenuReceipe foodMenuReceipe = foodreceipeAppService.isFoodMenuReceipeExists(foodcategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodMenuReceipe);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
