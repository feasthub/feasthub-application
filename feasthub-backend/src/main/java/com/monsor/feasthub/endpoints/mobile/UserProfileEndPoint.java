/**
 * 
 */
package com.monsor.feasthub.endpoints.mobile;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.amazons3.AmazonS3;
import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.common.ImageUtils;
import com.monsor.feasthub.dto.ChangePasswordDTO;
import com.monsor.feasthub.dto.UserProfilePicDTO;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/users")
@Group(name = "/registrations", title = "registrations")
@HttpCode("500>Internal Server Error,200>Success Response")
public class UserProfileEndPoint extends AuthorizedBaseEndPoint implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(UserProfileEndPoint.class);

    @Autowired
    private UserProfileAppService userProfileAppService;

    @PUT
    @Path("/updateuser/{userid}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse updateProfile(@PathParam(value = "userid") final BigInteger userId,
	    @RequestBody @Nonnull final FhUser user) {

	try {
	    checkNotNull(user, "This user must not be null");
	    checkNotNull(userId, "Invalid user requested to update");
	    checkState(userId.equals(user.getId()), "Invalid user requested to update");
	    userProfileAppService.update(user);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, user);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, user);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    @PUT
    @Path("/changepassword/{username}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse changePassword(@PathParam(value = "username") final String username,
	    @RequestBody final ChangePasswordDTO changePassword) {

	try {
	    checkNotNull(changePassword, "This userid must not be null");
	    checkState(StringUtils.isNotEmpty(username), "This userid must not be null");
	    checkState(StringUtils.isNotEmpty(changePassword.getPassword()), "This password must not be null");
	    checkState(StringUtils.isNotEmpty(changePassword.getNewPassword()), "This newPassword must not be null");
	    checkState(!changePassword.getPassword().equals(changePassword.getNewPassword()),
		    "New password and old password can not be same.");
	    userProfileAppService.changePassword(username, changePassword.getPassword(), changePassword
		    .getNewPassword());
	    final FhUser user = userProfileAppService.findByUsername(username);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, changePassword);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, changePassword);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    @GET
    @Path("/userId/{userid}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findByUserID(@PathParam(value = "userid") final BigInteger userId) {

	try {
	    checkNotNull(userId, "This userid must not be null");

	    final FhUser user = userProfileAppService.findById(userId);
	    ;
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @GET
    @Path("/userName/{userName}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse findByUserName(@PathParam(value = "userName") final String userName, @QueryParam(
	value = "deviceId") final String deviceId, @QueryParam(value = "deviceMake") final String deviceMake) {

	try {
	    checkNotNull(userName, "This userName must not be null");

	    FhUser user = userProfileAppService.findByUsername(userName);
	    if ( StringUtils.isNoneBlank(deviceId, deviceMake) ) {
		user.setDeviceId(StringUtils.isNotBlank(deviceId) ? deviceId : user.getDeviceId());
		user.setDeviceMake(StringUtils.isNotBlank(deviceMake) ? deviceMake : user.getDeviceMake());
		user = userProfileAppService.update(user);
	    }
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @PUT
    @Path("/user/{userId}/profilepics")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse updateProfilePics(@PathParam(value = "userId") final BigInteger userId,
	    @RequestBody UserProfilePicDTO userProfilePicsDTO) {

	try {
	    checkNotNull(userId, "This userId must not be null");
	    checkNotNull(userProfilePicsDTO, "This userProfilePicsDTO must not be null");
	    checkNotNull(userProfilePicsDTO.getProfilePhoto(), "User profile pics must not be null");
	    final FhUser user = userProfileAppService.updateProfilePic(userId, userProfilePicsDTO.getProfilePhoto());

	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }

    @PUT
    @Path("/uploadimage/{userid}")
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse uploadImage(@PathParam(value = "userid") final BigInteger userId,
	    @RequestBody UserProfilePicDTO userProfilePicsDTO) {

	try {
	    checkNotNull(userId, "This userId must not be null");
	    checkNotNull(userProfilePicsDTO, "This userProfilePicsDTO must not be null");
	    checkNotNull(userProfilePicsDTO.getProfilePhoto(), "User  pics must not be null");
	    final FhUser user = userProfileAppService.findById(userId);
	   

	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	    String filename = sdf.format(new Date());
	    String uploadedURL = AmazonS3.uploadImage("feasthubusers", "feasthub-profilepics", filename + "_" + user
		    .getUserName() + ".png", ImageUtils.scaleImageAsString(userProfilePicsDTO.getProfilePhoto(), 100,
			    100));
	    user.setProfilePhoto(uploadedURL);
	    userProfileAppService.update(user);
	    return ResponseUtil.getResponse("200", "Operation completed successfully", user);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }
}
