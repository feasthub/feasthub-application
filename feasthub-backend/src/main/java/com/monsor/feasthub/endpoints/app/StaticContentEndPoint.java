package com.monsor.feasthub.endpoints.app;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.StaticContentAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.FrontPageStaticContent;
import com.monsor.feasthub.jpa.model.enums.StaticContent;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;


@Path("/v1.0/feasthub/staticcontent")
@Group(name = "/staticcontent", title = "staticcontent")
@HttpCode("500>Internal Server Error,200>Success Response")
public class StaticContentEndPoint extends BaseEndPoint {
	private static final Logger LOG = LoggerFactory.getLogger(StaticContentEndPoint.class);
	@Autowired
	private StaticContentAppService staticContentAppService;

	@GET
	@Path("/type/{pageType}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse staticContentByType(@PathParam("pageType") StaticContent staticContent) {
		try {
			
			checkNotNull(staticContent, "Pagetype must not be null");
			checkState(staticContent instanceof StaticContent , "Invalid page type");
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside PaymentGatewayEndpoint::staticContentByType  :: ");
			}

			FrontPageStaticContent content = staticContentAppService.findContentByType(staticContent);
			return ResponseUtil.getResponse("200", "Operation completed successfully", content);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse allStaticContent() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside PaymentGatewayEndpoint::allStaticContent  :: ");
			}

			List<FrontPageStaticContent> content = staticContentAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", content);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
