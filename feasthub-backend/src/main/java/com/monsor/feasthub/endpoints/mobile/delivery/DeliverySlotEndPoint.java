package com.monsor.feasthub.endpoints.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.delivery.DeliverySlotAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlot;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/deliveryslots")
@Group(name = "/deliveryslots", title = "deliveryslots")
@HttpCode("500>Internal Server Error,200>Success Response")
public class DeliverySlotEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(DeliverySlotEndPoint.class);
	@Autowired
	private DeliverySlotAppService deliveryslotAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse createDeliverySlot(@RequestBody @Nonnull DeliverySlot deliveryslot) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslot :: " + deliveryslot);
			}

			deliveryslot = deliveryslotAppService.create(deliveryslot);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryslot);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslot);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslot);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse updateDeliverySlot(@PathParam(value = "id") final BigInteger deliverySlotId,
			@RequestBody @Nonnull DeliverySlot deliveryslot) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslot :: " + deliveryslot);
			}

			checkNotNull(deliverySlotId, "Invalid resource id");
			checkState(deliverySlotId.equals(deliveryslot.getId()), "Invalid resource");

			deliveryslot = deliveryslotAppService.update(deliveryslot);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryslot);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse deleteDeliverySlot(@PathParam("id") @Nonnull final BigInteger deliveryslotId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslot :: " + deliveryslotId);
			}
			checkNotNull(deliveryslotId, "This deliveryslotId must not be null");
			deliveryslotAppService.delete(deliveryslotId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslot :: ");
			}
			List<DeliverySlot> items = deliveryslotAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger deliveryslotId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslot :: " + deliveryslotId);
			}
			checkNotNull(deliveryslotId, "This deliveryslotId must not be null");
			final DeliverySlot deliveryslot = deliveryslotAppService.findById(deliveryslotId);

			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryslot);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
