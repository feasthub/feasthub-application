package com.monsor.feasthub.endpoints.orderupdate;

import java.math.BigInteger;

import javax.annotation.Nonnull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.orderupdate.OrderTransactionAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.dto.OrderPaymentDTO;
import com.monsor.feasthub.dto.OrderStatusDTO;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/ordermanagement/{userid}")
@Group(name = "/ordermanagement", title = "ordermanagement")
@HttpCode("500>Internal Server Error,200>Success Response")
public class OrderTransactionEndPoint extends AuthorizedBaseEndPoint {
    private static final Logger LOG = LoggerFactory.getLogger(OrderTransactionEndPoint.class);

    @Autowired
    private OrderTransactionAppService orderTxnEventAppService;

    @POST
    @Path("/placeorder")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse placeOrder(@PathParam("userid") BigInteger userId,
	    @RequestBody @Nonnull OrderTransaction order) {
	try {

	    order = orderTxnEventAppService.create(userId, order);
	    return ResponseUtil.getResponse("200", "Operation Completed Successfully", order);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, order);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, order);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    @PUT
    @Path("/confirmorder/{orderid}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse confirmOrder(@PathParam("orderid") String orderId,
	    @RequestBody OrderPaymentDTO orderPaymentDetails) {
	try {
	    OrderTransaction order = orderTxnEventAppService.searchByOrderId(orderId);
	    order = orderTxnEventAppService.confirmPayment(order, orderPaymentDetails);
	    return ResponseUtil.getResponse("200", "Operation Completed Successfully", order);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, orderPaymentDetails);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e, orderPaymentDetails);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    @GET
    @Path("/{orderid}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse getOrder(@PathParam("orderid") @Nonnull BigInteger orderTransactionId) {
	try {

	    OrderTransaction order = orderTxnEventAppService.findById(orderTransactionId);

	    return ResponseUtil.getResponse("200", "Operation Completed Successfully", order);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}

    }

    @GET
    @Path("/status/{orderid}")
     
    @RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
    public FeastHubResponse getOrderStatus(@PathParam("orderid") @Nonnull String orderId) {
	try {

	    OrderTransaction order = orderTxnEventAppService.searchByOrderId(orderId);
	    OrderStatusDTO orderStatus = new OrderStatusDTO(order.getOrderId(),
		    order.getOrderStatus());
	    return ResponseUtil.getResponse("200", "Operation Completed Successfully", orderStatus);
	} catch (IllegalStateException e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("412", e.getMessage());
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    super.logEror(e);
	    return ResponseUtil.getErrorResponse("500", e.getMessage());
	}
    }
}
