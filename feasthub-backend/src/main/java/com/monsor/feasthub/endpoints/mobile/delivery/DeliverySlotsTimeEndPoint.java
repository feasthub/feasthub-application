package com.monsor.feasthub.endpoints.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.mobile.delivery.DeliverySlotsTimeAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlotsTime;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/deliveryslotstimes")
@Group(name = "/deliveryslotstimes", title = "deliveryslotstimes")
@HttpCode("500>Internal Server Error,200>Success Response")
public class DeliverySlotsTimeEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(DeliverySlotsTimeEndPoint.class);
	@Autowired
	private DeliverySlotsTimeAppService deliverySlotsTimeAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse createDeliverySlotsTime(@RequestBody @Nonnull DeliverySlotsTime deliveryslotstime) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside deliveryslotstime :: " + deliveryslotstime);
			}

			checkNotNull(deliveryslotstime, "deliveryslotstime");

			deliveryslotstime = deliverySlotsTimeAppService.create(deliveryslotstime);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryslotstime);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslotstime);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslotstime);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse updateDeliverySlotsTime(@PathParam("id") @Nonnull final BigInteger deliveryslotstimeId,
			@RequestBody @Nonnull final DeliverySlotsTime deliveryslotstime) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside  deliveryslotstime :: " + deliveryslotstime);
			}
			checkNotNull(deliveryslotstimeId, "This deliveryslotstimeId must not be null");
			checkState(deliveryslotstimeId.equals(deliveryslotstime.getId()), "Invalid resource");
			deliverySlotsTimeAppService.update(deliveryslotstime);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslotstime);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, deliveryslotstime);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public FeastHubResponse deleteDeliverySlotsTime(@RequestBody @Nonnull final BigInteger deliveryslotstimeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside  deliveryslotstime :: " + deliveryslotstimeId);
			}
			checkNotNull(deliveryslotstimeId, "This deliveryslotstimeId must not be null");
			final DeliverySlotsTime deliveryslotstime = deliverySlotsTimeAppService.findById(deliveryslotstimeId);
			deliverySlotsTimeAppService.delete(deliveryslotstimeId);
			return ResponseUtil.getResponse("200", "Operation completed successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslotstime :: ");
			}
			List<DeliverySlotsTime> items = deliverySlotsTimeAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger deliveryslotstimeId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register deliveryslotstime :: " + deliveryslotstimeId);
			}
			checkNotNull(deliveryslotstimeId, "This deliveryslotstimeId must not be null");
			final DeliverySlotsTime deliveryslotstime = deliverySlotsTimeAppService.findById(deliveryslotstimeId);
			return ResponseUtil.getResponse("200", "Operation completed successfully", deliveryslotstime);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
