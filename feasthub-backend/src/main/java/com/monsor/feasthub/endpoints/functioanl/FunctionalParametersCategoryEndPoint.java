package com.monsor.feasthub.endpoints.functioanl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.functioanl.FunctionalParametersCategoryAppService;
import com.monsor.feasthub.common.BaseEndPoint;
import com.monsor.feasthub.jpa.model.fuctional.FunctionalParametersCategory;
import com.monsor.feasthub.response.FeastHubResponse;
/**
 * @author Amit
 *
 */
import com.monsor.feasthub.util.ResponseUtil;

@Path("/v1.0/feasthub/functionalparameterscategorys")
@Group(name = "/functionalparameterscategorys", title = "functionalparameterscategorys")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FunctionalParametersCategoryEndPoint extends BaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FunctionalParametersCategoryEndPoint.class);
	@Autowired
	private FunctionalParametersCategoryAppService functionalparameterscategoryAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFunctionalParametersCategory(
			@RequestBody @Nonnull FunctionalParametersCategory functionalparameterscategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategory);
			}
			functionalparameterscategory = functionalparameterscategoryAppService.create(functionalparameterscategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameterscategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameterscategory);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameterscategory);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFunctionalParametersCategory(
			@RequestBody @Nonnull FunctionalParametersCategory functionalparameterscategory) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategory);
			}
			functionalparameterscategory = functionalparameterscategoryAppService.update(functionalparameterscategory);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameterscategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameterscategory);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, functionalparameterscategory);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFunctionalParametersCategory(
			@RequestBody @Nonnull final BigInteger functionalparameterscategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategoryId);
			}
			checkNotNull(functionalparameterscategoryId, "This functionalparameterscategoryId must not be null");
			functionalparameterscategoryAppService.delete(functionalparameterscategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameterscategory :: ");
			}
			List<FunctionalParametersCategory> items = functionalparameterscategoryAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/categoryId/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger functionalparameterscategoryId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategoryId);
			}
			checkNotNull(functionalparameterscategoryId, "This functionalparameterscategoryId must not be null");
			final FunctionalParametersCategory functionalparameterscategory = functionalparameterscategoryAppService
					.findById(functionalparameterscategoryId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameterscategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/categoryName/{categoryName}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse getFunctionalparamByName(
			@PathParam(value = "categoryName") @Nullable final java.lang.String categoryName) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside getFunctionalparamByName");
			}
			checkNotNull(categoryName, "This categoryName must not be null");

			final FunctionalParametersCategory functionalparameterscategory = functionalparameterscategoryAppService
					.getFunctionalparamByName(categoryName);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", functionalparameterscategory);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

}
