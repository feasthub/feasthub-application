package com.monsor.feasthub.endpoints.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cuubez.visualizer.annotation.Group;
import com.cuubez.visualizer.annotation.HttpCode;
import com.monsor.feasthub.appservice.food.FoodIngredientAppService;
import com.monsor.feasthub.common.AuthorizedBaseEndPoint;
import com.monsor.feasthub.jpa.model.food.FoodIngredient;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.util.ResponseUtil;

/**
 * @author Amit
 *
 */

@Path("/v1.0/feasthub/foodingredients")
@Group(name = "/foodingredients", title = "foodingredients")
@HttpCode("500>Internal Server Error,200>Success Response")
public class FoodIngredientEndPoint extends AuthorizedBaseEndPoint {

	private static final Logger LOG = LoggerFactory.getLogger(FoodIngredientEndPoint.class);
	@Autowired
	private FoodIngredientAppService foodingredientAppService;

	@POST
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse createFoodIngredient(@RequestBody @Nonnull FoodIngredient foodingredient) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: " + foodingredient);
			}

			checkState(isFoodIngredientExists(foodingredient.getFoodReceipeId()) == null,
					"This record is already exits, Please provide different combination of data");

			foodingredient = foodingredientAppService.create(foodingredient);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodingredient);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodingredient);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodingredient);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@PUT
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse updateFoodIngredient(@PathParam("id") @Nonnull BigInteger foodingredientId,
			@RequestBody @Nonnull FoodIngredient foodingredient) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: " + foodingredient);
			}
			checkState(foodingredientId.equals(foodingredient.getFoodReceipeId()), "Mo matching resource found");
			foodingredient = foodingredientAppService.update(foodingredient);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodingredient);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodingredient);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e, foodingredient);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@DELETE
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse deleteFoodIngredient(@PathParam("id") @Nonnull final BigInteger foodingredientId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: " + foodingredientId);
			}
			checkNotNull(foodingredientId, "This foodingredientId must not be null");
			foodingredientAppService.delete(foodingredientId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully");
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findAll() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: ");
			}
			List<FoodIngredient> items = foodingredientAppService.findAll();
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/{id}")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse findById(@PathParam("id") @Nonnull final BigInteger foodingredientId) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: " + foodingredientId);
			}
			checkNotNull(foodingredientId, "This foodingredientId must not be null");
			final FoodIngredient foodingredient = foodingredientAppService.findById(foodingredientId);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodingredient);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/search")
	 
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse searchFoodIngredients(@QueryParam(value = "search") @Nonnull final String search) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside register foodingredient :: " + search);
			}
			checkNotNull(search, "This search must not be null");
			List<FoodIngredient> items = foodingredientAppService.search(search);
			return ResponseUtil.getResponse("200", "Operation completed Successfully", items);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}

	@GET
	@Path("/validatefoodingredients/{foodingredientname}")
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN })
	public FeastHubResponse isFoodIngredientExists(
			@PathParam(value = "foodingredientname") @Nullable final BigInteger foodingredientname) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Inside isFoodIngredientExists");
			}
			if (foodingredientname == null) {
				throw new BadRequestException("Please provide either foodingredientname");
			}

			final FoodIngredient foodIngredient = foodingredientAppService.findById(foodingredientname);

			return ResponseUtil.getResponse("200", "Operation completed Successfully", foodIngredient);
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("412", e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.logEror(e);
			return ResponseUtil.getErrorResponse("500", e.getMessage());
		}
	}
}
