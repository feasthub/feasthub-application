package com.monsor.feasthub.ui.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.monsor.feasthub.appservice.food.FoodMenuItemAppService;
import com.monsor.feasthub.jpa.model.food.FoodMenuItem;

@ViewScoped
@ManagedBean
public class FoodMenuItemController extends BaseController {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @ManagedProperty("#{foodMenuItemAppService}")
    private FoodMenuItemAppService foodMenuItemAppService;
    
    public List<FoodMenuItem> listFoodMenuItems(){
	return foodMenuItemAppService.findAll();
    }

    public FoodMenuItemAppService getFoodMenuItemAppService() {
        return foodMenuItemAppService;
    }

    public void setFoodMenuItemAppService(FoodMenuItemAppService foodMenuItemAppService) {
        this.foodMenuItemAppService = foodMenuItemAppService;
    }
}
