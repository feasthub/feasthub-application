/**
 * 
 */
package com.monsor.feasthub.ui.converter;

import java.math.BigInteger;

import javax.annotation.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.jpa.model.user.FhUser;

/**
 * @author Amit Sharma
 *
 */
@ManagedBean
@RequestScoped
public class UserConverter implements Converter {

    @Autowired
    private UserProfileAppService userProfileAppService;

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.
     * FacesContext, javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String name) {
	// TODO Auto-generated method stub
	return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.
     * FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object userIdObj) {
	// TODO Auto-generated method stub

	BigInteger userId = (BigInteger) userIdObj;
	FhUser user = userProfileAppService.findById(userId);
	return user.getName();
    }

}