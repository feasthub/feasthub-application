/**
 * 
 */
package com.monsor.feasthub.ui.controller;

import java.math.BigInteger;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.monsor.feasthub.appservice.food.FoodCategoryAppService;
import com.monsor.feasthub.jpa.model.food.FoodCategory;

/**
 * @author Amit Sharma
 *
 */
@ViewScoped
@ManagedBean
public class FoodCategoryController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5699016800792359701L;

	private BigInteger categoryId;

	private FoodCategory foodCategory = new FoodCategory();

	@ManagedProperty("#{foodCategoryAppService}")
	private FoodCategoryAppService foodCategoryAppService;

	public List<FoodCategory> listCategories() {
		if (categoryId != null) {
			return foodCategoryAppService.findAllByParentId(foodCategory.getKeyId());
		} else {
			return foodCategoryAppService.findAll();
		}
	}
	
	
	public List<FoodCategory> listParentCategory() {
		return foodCategoryAppService.getAllParentCategory();
	}

	public FoodCategoryAppService getFoodCategoryAppService() {
		return foodCategoryAppService;
	}

	public void setFoodCategoryAppService(FoodCategoryAppService foodCategoryAppService) {
		this.foodCategoryAppService = foodCategoryAppService;
	}

	public void updateFoodCategory() {
		if (foodCategory.getId() == null) {
			foodCategory = foodCategoryAppService.findById(categoryId);
		}
	}

	public BigInteger getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(BigInteger categoryId) {
		this.categoryId = categoryId;
	}

	public FoodCategory getFoodCategory() {
		return foodCategory;
	}

	public void setFoodCategory(FoodCategory foodCategory) {
		this.foodCategory = foodCategory;
	}
	
	public String performSave(){
		try{
			if(foodCategory.getId() == null){
				foodCategoryAppService.create(foodCategory);
			}else{
				foodCategoryAppService.update(foodCategory);
			}
			return "pretty:foodcategories";
		}catch(Exception e){
			e.printStackTrace();
			return ""; 
		}
	}
	
}
