/**
 * This source is part of the Project Tracker version 1.0 Software System and
 *  is copyrighted by Info Intensify System.
 *
 *  All rights reserved. No part of this work may be reproduced, stored in a retrieval system,
 *  adopted or transmitted in any form or by any means, electronic, mechanical, photographic,
 *  graphic, optic recording or otherwise, translated in any language or computer language,
 *  without the prior written permission of Info Intensify System.
 *
 
 *
 *  Copyright � [2011]- Info Intensify System. All rights reserved.
 */
package com.monsor.feasthub.ui.common;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * @author Amit
 *
 */
public class MessageHandler {
	public static void addInfo(String message) {  
        FacesContext.getCurrentInstance().addMessage("backerror", new FacesMessage(FacesMessage.SEVERITY_INFO,"", message));  
		
    }  
  
    public static void addWarn(String message) {  
        FacesContext.getCurrentInstance().addMessage("backerror", new FacesMessage(FacesMessage.SEVERITY_WARN,"Warnning Message:", message));  
    }  
  
    public static void addError(String message) {  
        FacesContext.getCurrentInstance().addMessage("backerror", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error Message :", message));  
    }  
  
    public static void addError(String clientId, String message) {  
        FacesContext.getCurrentInstance().addMessage(clientId, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error Message :"+message, message));  
    } 
    
    public static void addFatal(String message) {  
        FacesContext.getCurrentInstance().addMessage("backerror", new FacesMessage(FacesMessage.SEVERITY_FATAL,"Fatal Message :", message));  
    }
    
}
