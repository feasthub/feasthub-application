package com.monsor.feasthub.ui.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedProperty;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.monsor.feasthub.jpa.model.FeasthubError;
import com.monsor.feasthub.jpa.repository.FeasthubErrorRepository;
import com.monsor.feasthub.ui.bean.UserSessionBean;

public class BaseController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1076549217831875130L;

    @ManagedProperty("#{userSessionBean}")
    private UserSessionBean userSessionBean;

    public UserSessionBean getUserSessionBean() {
	return userSessionBean;
    }

    public void setUserSessionBean(UserSessionBean userSessionBean) {
	this.userSessionBean = userSessionBean;
    }

    @Autowired
    private FeasthubErrorRepository feasthubErrorRepository;

    public void logEror(Throwable e) {
	StringBuilder jsonInString = new StringBuilder("");
	
	try {
	    feasthubErrorRepository.save(new FeasthubError(e.getMessage(),
		    ExceptionUtils.getStackTrace(e), jsonInString.toString(), ""));
	    e.printStackTrace();
	} catch (Exception e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}

    }

}
