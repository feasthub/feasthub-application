package com.monsor.feasthub.ui.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.jpa.model.user.FhUser;

@ManagedBean
@RequestScoped
public class LoginController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2470076408751875028L;

	private String username;

	private String password;

	@ManagedProperty("#{userProfileAppService}")
	private UserProfileAppService userProfileAppService;

	public String performLogin() {
		final FhUser user = userProfileAppService.performLogin(username, password);
		this.getUserSessionBean().setUser(user);
		return "pretty:dashboard";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserProfileAppService getUserProfileAppService() {
		return userProfileAppService;
	}

	public void setUserProfileAppService(UserProfileAppService userProfileAppService) {
		this.userProfileAppService = userProfileAppService;
	}

}