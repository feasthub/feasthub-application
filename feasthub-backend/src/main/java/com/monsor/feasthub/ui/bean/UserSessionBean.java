/**
 * 
 */
package com.monsor.feasthub.ui.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.monsor.feasthub.jpa.model.user.FhUser;

/**
 * @author amit
 *
 */

@ManagedBean
@SessionScoped
public class UserSessionBean implements Serializable{
	private FhUser user;

	public FhUser getUser() {
		return user;
	}

	public void setUser(FhUser user) {
		this.user = user;
	}

}
