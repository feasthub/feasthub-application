package com.monsor.feasthub.ui.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.monsor.feasthub.appservice.mobile.delivery.DeliverySlotAppService;
import com.monsor.feasthub.appservice.mobile.delivery.DeliverySlotsTimeAppService;
import com.monsor.feasthub.appservice.mobile.orderupdate.OrderTransactionAppService;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlotsTime;
import com.monsor.feasthub.jpa.model.enums.OrderStatus;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;

@ViewScoped
@ManagedBean
public class OrderController extends BaseController {

    /**
     * 
     */

    private int noOfPage = 0;

    private BigInteger orderId;

    private int recordPerPage = 10;
    
    private OrderTransaction viewOrder;

    private static final long serialVersionUID = 1L;

    private Date orderDate = new Date();

    private List<String> orderStatusIn = new ArrayList<String>();

    private List<OrderTransaction> recordPage;

    @ManagedProperty("#{orderTransactionAppService}")
    private OrderTransactionAppService orderTransactionAppService;
    
    @ManagedProperty("#{deliverySlotAppService}")
    private DeliverySlotAppService deliverySlotAppService;
    
    @ManagedProperty("#{deliverySlotsTimeAppService}")
    private DeliverySlotsTimeAppService deliverySlotsTimeAppService;


    public List<OrderStatus> listStatus() {
	return Arrays.asList(OrderStatus.values());
    }

    @PostConstruct
    public void init() {
	orderStatusIn.clear();
	orderStatusIn.add(OrderStatus.CONFIRMED_BY_COMPANY.toString());
	orderStatusIn.add(OrderStatus.BEING_PREPARED.toString());
	orderStatusIn.add(OrderStatus.OUT_FOR_DELIVERY.toString());
	orderStatusIn.add(OrderStatus.PACKING_IN_PROGRESS.toString());
	orderStatusIn.add(OrderStatus.DELIVERED.toString());
	orderStatusIn.add(OrderStatus.DELIVERED_TO_NEIGHBOUR.toString());
	orderStatusIn.add(OrderStatus.COMPLETED.toString());
    }

    public List<OrderTransaction> listOrders() {
	if (noOfPage == 0) {
	    searchOrder();
	}
	return recordPage;
    }

     
    private void searchOrder() {
	recordPage = orderTransactionAppService.listAllOrder(orderDate);
	recordPage = recordPage.stream()
		.filter(item -> orderStatusIn.contains(item.getOrderStatus().toString()))
		.collect(Collectors.toList());

    }
    
    public void initOrderInfo(){
	if(this.orderId !=null){
	  viewOrder =  orderTransactionAppService.findById(orderId);
	}
    }

    public void updateStatus(OrderTransaction order, OrderStatus orderStatus) {
	order.setOrderStatus(orderStatus);
	orderTransactionAppService.update(order);
    }

    public void performSearch() {

	searchOrder();
    }

    public Date getOrderDate() {
	return orderDate;
    }

    public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
    }

    public OrderTransactionAppService getOrderTransactionAppService() {
	return orderTransactionAppService;
    }

    public void setOrderTransactionAppService(
	    OrderTransactionAppService orderTransactionAppService) {
	this.orderTransactionAppService = orderTransactionAppService;
    }

    public void nextPage() {

    }

    public int getNoOfPage() {
	return noOfPage;
    }

    public void setNoOfPage(int noOfPage) {
	this.noOfPage = noOfPage;
    }

    public int getRecordPerPage() {
	return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
	this.recordPerPage = recordPerPage;
    }

    public List<OrderTransaction> getRecordPage() {
	return recordPage;
    }

    public void setRecordPage(List<OrderTransaction> recordPage) {
	this.recordPage = recordPage;
    }

    public BigInteger getOrderId() {
	return orderId;
    }

    public void setOrderId(BigInteger orderId) {
	this.orderId = orderId;
    }

    public List<String> getOrderStatusIn() {
	return orderStatusIn;
    }

    public void setOrderStatusIn(List<String> orderStatusIn) {
	this.orderStatusIn = orderStatusIn;
    }

    public OrderTransaction getViewOrder() {
        return viewOrder;
    }

    public void setViewOrder(OrderTransaction viewOrder) {
        this.viewOrder = viewOrder;
    }

    public DeliverySlotAppService getDeliverySlotAppService() {
        return deliverySlotAppService;
    }

    public void setDeliverySlotAppService(DeliverySlotAppService deliverySlotAppService) {
        this.deliverySlotAppService = deliverySlotAppService;
    }

    public DeliverySlotsTimeAppService getDeliverySlotsTimeAppService() {
        return deliverySlotsTimeAppService;
    }

    public void setDeliverySlotsTimeAppService(
    	DeliverySlotsTimeAppService deliverySlotsTimeAppService) {
        this.deliverySlotsTimeAppService = deliverySlotsTimeAppService;
    }
    
    public String getDeliverySlot(BigInteger id){
	return deliverySlotAppService.findById(id).getPartOfDay();
    }
    
    public String getDeliverySlotsTime(BigInteger id){
   	DeliverySlotsTime time = deliverySlotsTimeAppService.findById(id);
   	return String.format("%s - %s", time.getSlotsTimingStartsAt(),time.getSlotsTimingEndsAt());
       }
}
