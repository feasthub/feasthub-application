package com.monsor.feasthub.ui.controller;

import org.primefaces.event.FlowEvent;

public class WizardController extends BaseController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5018625825743258725L;
	private int totalSteps = 3;
	private String currentStep = "step1";
	private boolean showPrevious = false;
	private boolean showNext = true;
	private boolean showSubmit = false;

	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totolSteps) {
		this.totalSteps = totolSteps;
	}

	public String getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(String currentStep) {
		this.currentStep = currentStep;
	}

	public WizardController() {
		totalSteps =3;
	}

	public WizardController(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	public String onFlowListener(FlowEvent event) {
		String newStep = event.getNewStep();
		this.currentStep = newStep;
		if ("step1".equals(newStep)) {
			showNext = true;
			showSubmit = false;
			showPrevious = false;
		} else if (("step" + totalSteps).equals(newStep)) {
			showNext = false;
			showSubmit = true;
			showPrevious = true;
		} else {
			showNext = true;
			showSubmit = false;
			showPrevious = true;
		}

		return currentStep;

	}

	public boolean getShowPrevious() {
		return showPrevious;
	}

	public void setShowPrevious(boolean showPrevious) {
		this.showPrevious = showPrevious;
	}

	public boolean getShowNext() {
		return showNext;
	}

	public void setShowNext(boolean showNext) {
		this.showNext = showNext;
	}

	public boolean getShowSubmit() {
		return showSubmit;
	}

	public void setShowSubmit(boolean showSubmit) {
		this.showSubmit = showSubmit;
	}
}
