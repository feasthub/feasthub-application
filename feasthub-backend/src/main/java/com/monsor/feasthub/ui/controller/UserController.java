package com.monsor.feasthub.ui.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.glassfish.jersey.internal.util.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import com.monsor.amazons3.AmazonS3;
import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.appservice.user.UserAddressAppService;
import com.monsor.feasthub.common.ImageUtils;
import com.monsor.feasthub.jpa.model.app.City;
import com.monsor.feasthub.jpa.model.app.Country;
import com.monsor.feasthub.jpa.model.app.State;
import com.monsor.feasthub.jpa.model.enums.AddressType;
import com.monsor.feasthub.jpa.model.enums.UserSalutation;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.jpa.model.user.UserAddress;
import com.monsor.feasthub.jpa.service.CountriesService;
import com.monsor.feasthub.ui.common.MessageHandler;

@ManagedBean
@ViewScoped
public class UserController extends WizardController {

    /**
     * 
     */
    private static final long serialVersionUID = 5752996374231576777L;

    private BigInteger userId;

    private FhUser currentUser = new FhUser();

    private List<UserAddress> userAddresses;

    private UserAddress userAddress;

    private boolean isAddressAvaiable;

    @ManagedProperty("#{countriesService}")
    private CountriesService countriesService;

    private List<State> stateList;

    private List<City> cityList;

    @ManagedProperty("#{userAddressAppService}")
    private UserAddressAppService userAddressAppService;

    private List<Country> countryList;

    @ManagedProperty("#{userProfileAppService}")
    private UserProfileAppService userProfileAppService;

    public UserController () {
	super(3);
	// TODO Auto-generated constructor stub
    }

    public List<FhUser> listUsers() {

	return userProfileAppService.findAll();
    }

    public List<AddressType> getAddressTypes() {

	return Arrays.asList(AddressType.values());
    }

    public UserSalutation[] getUserSalutation() {

	return UserSalutation.values();
    }

    public List<Country> getCountryList() {

	List<Country> country = new ArrayList<>();
	country.add(countriesService.getCountriesRepository().findOneByCountryCode("IN"));
	return country;
    }

    public void createUser() {

	currentUser = new FhUser();
	userAddress = new UserAddress();
    }

    public void updateUser() {

	System.out.println("User Id ::" + userId);
	if ( currentUser.getId() == null ) {
	    currentUser = userProfileAppService.findById(userId);
	    userAddresses = userAddressAppService.findByUserId(userId);
	    System.out.println("User Details ::" + currentUser.getName());

	    Country country = countriesService.getCountriesRepository().findOneByCountryCode("IN");
	    stateList = country.getStates();
	    if ( userAddress != null && userAddress.getAddressType() != null ) {
		State state = country.getStates().stream().filter(item -> item.getName().equals(userAddress
			.getStateCode())).findFirst().orElse(null);
		cityList = (List<City>) state.getCities().stream().filter(item -> item.getCity().equals(userAddress
			.getCity()));
	    }
	}
    }

    public void saveUser() {

	try {
	    if ( currentUser != null ) {
		if ( BigInteger.ZERO.equals(currentUser.getId()) || currentUser.getId() == null ) {
		    currentUser = userProfileAppService.create(currentUser);
		} else {
		    currentUser = userProfileAppService.update(currentUser);
		}
		if ( currentUser != null && isAddressAvaiable ) {
		    for ( UserAddress address : userAddresses ) {
			address.setUserId(currentUser.getId());
			if ( address.getId() != null && BigInteger.ZERO.equals(address.getId()) ) {
			    userAddressAppService.update(address);
			} else {
			    userAddressAppService.create(address);
			}
		    }
		}
	    }
	    MessageHandler.addInfo("Operation completed successfully");
	} catch (Exception e) {
	    logEror(e);
	    MessageHandler.addError(e.getMessage());
	}

    }

    public FhUser getCurrentUser() {

	return currentUser;
    }

    public void setCurrentUser(FhUser currentUser) {

	this.currentUser = currentUser;
    }

    public UserProfileAppService getUserProfileAppService() {

	return userProfileAppService;
    }

    public void setUserProfileAppService(UserProfileAppService userProfileAppService) {

	this.userProfileAppService = userProfileAppService;
    }

    public UserAddress getUserAddress() {

	return userAddress;
    }

    public void setUserAddress(UserAddress userAdddres) {

	this.userAddress = userAdddres;
    }

    public BigInteger getUserId() {

	return userId;
    }

    public void setUserId(BigInteger userId) {

	this.userId = userId;
    }

    public boolean isAddressAvaiable() {

	return isAddressAvaiable;
    }

    public void setAddressAvaiable(boolean isAddressAvaiable) {

	this.isAddressAvaiable = isAddressAvaiable;
    }

    public CountriesService getCountriesService() {

	return countriesService;
    }

    public void setCountriesService(CountriesService countriesService) {

	this.countriesService = countriesService;
    }

    public List<State> getStateList() {

	return stateList;
    }

    public void setStateList(List<State> stateList) {

	this.stateList = stateList;
    }

    public List<City> getCityList() {

	return cityList;
    }

    public void setCityList(List<City> cityList) {

	this.cityList = cityList;
    }

    public UserAddressAppService getUserAddressAppService() {

	return userAddressAppService;
    }

    public void setUserAddressAppService(UserAddressAppService userAddressAppService) {

	this.userAddressAppService = userAddressAppService;
    }

    public List<UserAddress> getUserAddresses() {

	return userAddresses;
    }

    public void setUserAddresses(List<UserAddress> userAddresses) {

	this.userAddresses = userAddresses;
    }

    public void launchAddressPopup() {

	userAddress = new UserAddress();
	isAddressAvaiable = true;
	RequestContext.getCurrentInstance().execute("PF('address').show();");
    }

    public void onStateChange() {

	State stateitem = this.getStateList().stream().filter(item -> item.getName().equals(this.userAddress
		.getStateCode())).findFirst().orElse(null);
	if ( stateitem != null ) {
	    this.cityList = stateitem.getCities();
	}
    }

    public void addAddress() {

	userAddresses.add(userAddress);
	RequestContext.getCurrentInstance().execute("PF('address').hide();");
    }

    public String removeUser() {

	if ( currentUser != null ) {
	    userProfileAppService.delete(currentUser.getId());
	    return "pretty:users";
	}
	return null;
    }

    public void setCountryList(List<Country> countryList) {

	this.countryList = countryList;
    }

    public void handleFileUpload(FileUploadEvent event) {
	System.out.println("Inside file uplaod");
	try {
	    String uploadedURL = AmazonS3.uploadImage("feasthubusers", "feasthub-profilepics", event.getFile().getFileName()
	    	+ "_" + currentUser.getUserName() + ".png", ImageUtils.scaleImageAsString(Base64.decodeAsString(event.getFile().getContents()),
	    		100, 100));
	    currentUser.setProfilePhoto(uploadedURL);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
}
