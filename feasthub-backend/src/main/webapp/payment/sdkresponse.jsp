
<%@page contentType="text/xml"%>

<%@page import="java.util.*"%>
<%@page import="javax.crypto.Mac"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>

<%!

    private static String toHex(byte[] bytes) {
        StringBuilder buffer = new StringBuilder(bytes.length * 2);
        String str;
        for (Byte b : bytes) {
            str = Integer.toHexString(b);
            int len = str.length();
            if (len == 8)
            {
                buffer.append(str.substring(6));
            } else if (str.length() == 2){
                buffer.append(str);
            }else {
                buffer.append("0" + str);
            }
        }
        return buffer.toString();
        }
    public static boolean verifyChecksum(String secretKey, String checksumInput,String zaakChecksum)throws Exception{
       
        byte[] dataToEncryptByte = checksumInput.getBytes();
        byte[] keyBytes = secretKey.getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKeySpec);
        byte[] checksumByte = mac.doFinal(dataToEncryptByte);
        String checksum = toHex(checksumByte);
        System.out.println("checksumInput: "+new Date()+" "+checksumInput);
        System.out.println("checksum calculated: "+new Date()+" "+checksum);
        if(zaakChecksum.equals(checksum))
            return true;
        return false;

    }


%>
<%
        System.out.println("response orderid: "+request.getParameter("orderId")+" Payment Mathod: "+request.getParameter("paymentMethod"));

        String orderid=request.getParameter("orderId");
        Double amount = 0d;


        String status="1";
        String statusMsg=request.getParameter("responseDescription");
        try{
            amount= Double.parseDouble(request.getParameter("amount"))/100;
        }catch (Exception e) {
            System.out.println("Amount parsing FAILED: orderid: "+request.getParameter("orderId")+" AMOUNT : "+request.getParameter("amount"));
        }
      
        Enumeration<String> en = request.getParameterNames();
        List<String> list = Collections.list(en);
       
       

        String orderId = request.getParameter("orderId");
        String responseCode = request.getParameter("responseCode");
        String responseDescription = request.getParameter("responseDescription");
        String checksum = request.getParameter("checksum");   
        String amounts = request.getParameter("amount");
        String paymentMethod = request.getParameter("paymentMethod");
        String cardhashid = request.getParameter("cardhashid");
                       

        if("wallet".equals(paymentMethod)){


            //{amount=296700, responseCode=0, responseDescription=Transaction completed successfully, paymentMethod=wallet, orderId=399312556}
           
String checksumInput="'"+orderId+"''"+amount+"''"+responseCode+"''"+""+responseDescription+"'";
System.out.println("Wallet checksum checksumInput "+checksumInput);
            boolean isChecksumValid = false;
           
            if(checksum != null){
                String MOBIKWIK_SECRET_KEY="MBK9002"; // Put your MobiKwik secret key here
                isChecksumValid = verifyChecksum(MOBIKWIK_SECRET_KEY, checksumInput, checksum);

                System.out.println("isChecksumValid at"+new Date()+" "+isChecksumValid);
            }
            if( isChecksumValid && "0".equals(request.getParameter("responseCode"))) {
                 status="0";
                }
            }


        
        else
        {
            String checksumInput = "'"+orderId+"''"+responseCode+"''"+responseDescription+"''"+amounts+"''"+paymentMethod+"''"+cardhashid+"'";
            System.out.println("checksumInput "+checksumInput);
            boolean isChecksumValid = false;
           
            if(checksum != null){
                String ZAAKPAY_SECRET_KEY="8d3a89f971f742a581caf9b9f8848db9"; // Put your Zaakpay secret key here
                isChecksumValid = verifyChecksum(ZAAKPAY_SECRET_KEY, checksumInput, checksum);

                System.out.println("isChecksumValid at"+new Date()+" "+isChecksumValid);
            }
            if( isChecksumValid && "100".equals(request.getParameter("responseCode"))) {
                 status="0";
                }
            }
%>

<paymentResponse>
 <orderid><%= orderid%></orderid>
<amount><%= amount%></amount>
<status><%= status%></status>
<statusMsg><%= statusMsg%></statusMsg>
</paymentResponse>

