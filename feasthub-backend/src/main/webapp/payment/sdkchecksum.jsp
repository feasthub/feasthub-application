
<%@page import="java.util.*"%>
<%@page import="javax.crypto.Mac"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<%@page import="java.text.*"%>
<%@page import="java.util.zip.Adler32"%>
<%@page import="java.math.BigDecimal"%>



<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%!public static String getCurrentDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public static String getAllNotEmptyParamValue2(HttpServletRequest request) {

		String orderid = request.getParameter("orderid");
		String amount = request.getParameter("amount");

		BigDecimal bd1 = new BigDecimal(amount);
		BigDecimal bd2 = new BigDecimal("100");
		BigDecimal bdResult = bd1.multiply(bd2);
		double dResult = bdResult.doubleValue();
		amount = (int) dResult + "";
		String mid = request.getParameter("mid");
		String cur = "INR";
		String mode = "0";
		String txnDate = getCurrentDate();
		String ip = request.getParameter("ipAddr");
		String allNonEmptyParamValueExceptChecksum = "'" + amount + "''" + ip + "''" + txnDate + "''" + cur + "''" + mid
				+ "''" + orderid + "''" + mode + "'";
		System.out.println("allNonEmptyParamValueExceptChecksum: " + allNonEmptyParamValueExceptChecksum);
		return allNonEmptyParamValueExceptChecksum;

	}

	private static String toHex(byte[] bytes) {
		StringBuilder buffer = new StringBuilder(bytes.length * 2);
		String str;
		for (Byte b : bytes) {
			str = Integer.toHexString(b);
			int len = str.length();
			if (len == 8) {
				buffer.append(str.substring(6));
			} else if (str.length() == 2) {
				buffer.append(str);
			} else {
				buffer.append("0" + str);
			}
		}
		return buffer.toString();
	}

	public static String calculateChecksum(String secretKey, String allParamValue) throws Exception {

		byte[] dataToEncryptByte = allParamValue.getBytes();
		byte[] keyBytes = secretKey.getBytes();
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(secretKeySpec);
		byte[] checksumByte = mac.doFinal(dataToEncryptByte);
		String checksum = toHex(checksumByte);
		System.out.println("checksum: " + checksum);
		return checksum;
	}

	public static String getWalletChecksum(HttpServletRequest request) throws Exception {
		System.out.println("Wallet checksum");
		String MOBIKWIK_SECRET_KEY = "MBK9002"; //For test Environment, MBK9002, this is the secret key, replace it when you go live
		System.out.println("Secret key is : " + MOBIKWIK_SECRET_KEY);
		String amount = request.getParameter("amount");
		String mid = request.getParameter("mid");
		String orderId = request.getParameter("orderid");

		String checksumInput = "'" + amount + "''" + orderId + "''" + mid + "'";
		System.out.println("Wallet checksum checksumInput " + checksumInput);

		return calculateChecksum(MOBIKWIK_SECRET_KEY, checksumInput);

	}%>
<%
	String checksum = "";
	// If you are integrating Zaakpay then enter the detail below
	String ZAAKPAY_SECRET_KEY = "8d3a89f971f742a581caf9b9f8848db9";
	//String ZAAKPAY_SECRET_KEY="239de630f35343b28e5b0a14896397bf"; // Put your Zaakpay secret key here, here a sample secret key is entered
	if (request.getParameter("pgName").equals("Zaakpay")) {
		System.out.println(" Merchant is Zaakpay");
		checksum = calculateChecksum(ZAAKPAY_SECRET_KEY, getAllNotEmptyParamValue2(request));
	} else //MobiKwik is integrated, the function below will be called
		checksum = getWalletChecksum(request);
%>

<checksum> <status>SUCCESS</status> <checksumvalue><%=checksum%></checksumValue></checksum>




