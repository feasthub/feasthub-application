package com.monsor.feasthub.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class EventListItemDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigInteger fhEventId;
	private EventDetailsDTO eventDetails;
	private EventCuisineDTO eventCuisine;

	public BigInteger getFhEventId() {
		return fhEventId;
	}

	public void setFhEventId(BigInteger fhEventId) {
		this.fhEventId = fhEventId;
	}

	public EventDetailsDTO getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(EventDetailsDTO eventDetails) {
		this.eventDetails = eventDetails;
	}

	public EventCuisineDTO getEventCuisine() {
		return eventCuisine;
	}

	public void setEventCuisine(EventCuisineDTO eventCuisine) {
		this.eventCuisine = eventCuisine;
	}
}
