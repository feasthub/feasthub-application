package com.monsor.feasthub.dto;

import java.io.Serializable;

public class OrderUpdateDTO implements Serializable {

	private static final long serialVersionUID = 1860254477880680474L;
	private String additionalNoteByUser;
	private String cashAuditId;
	private String eventId;
	private String feastPointAuditId;
	private String firstDeliveryDate;
	private String orderPurchasedModeType;
	private String orderedQuantity;
	private String paymentStatus;
	private String purchaserUserId;
	private String sellerUserId;
	private String transactionDate;
	private String amount;
	private String currency;
	private Boolean scheduleOrdering;
	private String [] listingDays;
	private String deliveryAddressId;
	private String transactionPurpose;
	private String paymentGatewayUsed;
	

	public String getPaymentGatewayUsed() {
		return paymentGatewayUsed;
	}

	public void setPaymentGatewayUsed(String paymentGatewayUsed) {
		this.paymentGatewayUsed = paymentGatewayUsed;
	}

	public String getTransactionPurpose() {
		return transactionPurpose;
	}

	public void setTransactionPurpose(String transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}

	public String getAdditionalNoteByUser() {
		return additionalNoteByUser;
	}

	public void setAdditionalNoteByUser(String additionalNoteByUser) {
		this.additionalNoteByUser = additionalNoteByUser;
	}

	public String getCashAuditId() {
		return cashAuditId;
	}

	public void setCashAuditId(String cashAuditId) {
		this.cashAuditId = cashAuditId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getFeastPointAuditId() {
		return feastPointAuditId;
	}

	public void setFeastPointAuditId(String feastPointAuditId) {
		this.feastPointAuditId = feastPointAuditId;
	}

	public String getFirstDeliveryDate() {
		return firstDeliveryDate;
	}

	public void setFirstDeliveryDate(String firstDeliveryDate) {
		this.firstDeliveryDate = firstDeliveryDate;
	}

	public String getOrderPurchasedModeType() {
		return orderPurchasedModeType;
	}

	public void setOrderPurchasedModeType(String orderPurchasedModeType) {
		this.orderPurchasedModeType = orderPurchasedModeType;
	}

	public String getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(String orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPurchaserUserId() {
		return purchaserUserId;
	}

	public void setPurchaserUserId(String purchaserUserId) {
		this.purchaserUserId = purchaserUserId;
	}

	public String getSellerUserId() {
		return sellerUserId;
	}

	public void setSellerUserId(String sellerUserId) {
		this.sellerUserId = sellerUserId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getScheduleOrdering() {
		return scheduleOrdering;
	}

	public void setScheduleOrdering(Boolean scheduleOrdering) {
		this.scheduleOrdering = scheduleOrdering;
	}
	public String[] getListingDays() {
		return listingDays;
	}

	public void setListingDays(String[] listingDays) {
		this.listingDays = listingDays;
	}
	public String getDeliveryAddressId() {
		return deliveryAddressId;
	}

	public void setDeliveryAddressId(String deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}

}
