package com.monsor.feasthub.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class EventCuisineDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String eventCuisineId;
	private double addCommision;
	private double addDelivery;
	private double addOtherCharge;
	private double addTaxes;
	private BigInteger deliveryChargeId;
	private BigInteger deliverySlot;
	private BigInteger eventId;
	private double finalOfferedPrice;
	private double finalOfferedPriceFp;
	private BigInteger foodMenuId;
	private String note;
	private double offeredPrice;
	private int offeredQuantity;

	public String getEventCuisineId() {
		return eventCuisineId;
	}

	public void setEventCuisineId(String eventCuisineId) {
		this.eventCuisineId = eventCuisineId;
	}

	public double getAddCommision() {
		return addCommision;
	}

	public void setAddCommision(double addCommision) {
		this.addCommision = addCommision;
	}

	public double getAddDelivery() {
		return addDelivery;
	}

	public void setAddDelivery(double addDelivery) {
		this.addDelivery = addDelivery;
	}

	public double getAddOtherCharge() {
		return addOtherCharge;
	}

	public void setAddOtherCharge(double addOtherCharge) {
		this.addOtherCharge = addOtherCharge;
	}

	public double getAddTaxes() {
		return addTaxes;
	}

	public void setAddTaxes(double addTaxes) {
		this.addTaxes = addTaxes;
	}

	public BigInteger getDeliveryChargeId() {
		return deliveryChargeId;
	}

	public void setDeliveryChargeId(BigInteger deliveryChargeId) {
		this.deliveryChargeId = deliveryChargeId;
	}

	public BigInteger getDeliverySlot() {
		return deliverySlot;
	}

	public void setDeliverySlot(BigInteger deliverySlot) {
		this.deliverySlot = deliverySlot;
	}

	public BigInteger getEventId() {
		return eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public double getFinalOfferedPrice() {
		return finalOfferedPrice;
	}

	public void setFinalOfferedPrice(double finalOfferedPrice) {
		this.finalOfferedPrice = finalOfferedPrice;
	}

	public double getFinalOfferedPriceFp() {
		return finalOfferedPriceFp;
	}

	public void setFinalOfferedPriceFp(double finalOfferedPriceFp) {
		this.finalOfferedPriceFp = finalOfferedPriceFp;
	}

	public BigInteger getFoodMenuId() {
		return foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public int getOfferedQuantity() {
		return offeredQuantity;
	}

	public void setOfferedQuantity(int offeredQuantity) {
		this.offeredQuantity = offeredQuantity;
	}

}
