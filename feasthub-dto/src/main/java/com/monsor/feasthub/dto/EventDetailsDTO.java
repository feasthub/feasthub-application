/**
 * 
 */
package com.monsor.feasthub.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

/**
 * @author abhishekvashistha
 *
 */
public class EventDetailsDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String additionalComments;
	private String additionalServiceOfferingType;
	private String businessType;
	private BigInteger deliveryLocationId;
	private BigInteger displayCategoryId;
	private String eventImageUploadedFlag;
	private Date eventListingEndDate;
	private Time eventListingEndTime;
	private int eventListingNoOfDaysLive;
	private Time eventListingShowtimeEndTime;
	private Time eventListingShowtimeStartTime;
	private Date eventListingStartDate;
	private Time eventListingStartTime;
	private BigInteger eventLocationAddressId;
	private String eventName;
	private BigInteger eventOfferedBy;
	private String serviceOfferingType;
	private String transactionModeFpCash;
	private String userCanBlockOnlineFlag;
	private String userCanPurchaseOnlineFlag;

	public String getAdditionalComments() {
		return additionalComments;
	}

	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}

	public String getAdditionalServiceOfferingType() {
		return additionalServiceOfferingType;
	}

	public void setAdditionalServiceOfferingType(String additionalServiceOfferingType) {
		this.additionalServiceOfferingType = additionalServiceOfferingType;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public BigInteger getDeliveryLocationId() {
		return deliveryLocationId;
	}

	public void setDeliveryLocationId(BigInteger deliveryLocationId) {
		this.deliveryLocationId = deliveryLocationId;
	}

	public BigInteger getDisplayCategoryId() {
		return displayCategoryId;
	}

	public void setDisplayCategoryId(BigInteger displayCategoryId) {
		this.displayCategoryId = displayCategoryId;
	}

	public String getEventImageUploadedFlag() {
		return eventImageUploadedFlag;
	}

	public void setEventImageUploadedFlag(String eventImageUploadedFlag) {
		this.eventImageUploadedFlag = eventImageUploadedFlag;
	}

	public Date getEventListingEndDate() {
		return eventListingEndDate;
	}

	public void setEventListingEndDate(Date eventListingEndDate) {
		this.eventListingEndDate = eventListingEndDate;
	}

	public Time getEventListingEndTime() {
		return eventListingEndTime;
	}

	public void setEventListingEndTime(Time eventListingEndTime) {
		this.eventListingEndTime = eventListingEndTime;
	}

	public int getEventListingNoOfDaysLive() {
		return eventListingNoOfDaysLive;
	}

	public void setEventListingNoOfDaysLive(int eventListingNoOfDaysLive) {
		this.eventListingNoOfDaysLive = eventListingNoOfDaysLive;
	}

	public Time getEventListingShowtimeEndTime() {
		return eventListingShowtimeEndTime;
	}

	public void setEventListingShowtimeEndTime(Time eventListingShowtimeEndTime) {
		this.eventListingShowtimeEndTime = eventListingShowtimeEndTime;
	}

	public Time getEventListingShowtimeStartTime() {
		return eventListingShowtimeStartTime;
	}

	public void setEventListingShowtimeStartTime(Time eventListingShowtimeStartTime) {
		this.eventListingShowtimeStartTime = eventListingShowtimeStartTime;
	}

	public Date getEventListingStartDate() {
		return eventListingStartDate;
	}

	public void setEventListingStartDate(Date eventListingStartDate) {
		this.eventListingStartDate = eventListingStartDate;
	}

	public Time getEventListingStartTime() {
		return eventListingStartTime;
	}

	public void setEventListingStartTime(Time eventListingStartTime) {
		this.eventListingStartTime = eventListingStartTime;
	}

	public BigInteger getEventLocationAddressId() {
		return eventLocationAddressId;
	}

	public void setEventLocationAddressId(BigInteger eventLocationAddressId) {
		this.eventLocationAddressId = eventLocationAddressId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public BigInteger getEventOfferedBy() {
		return eventOfferedBy;
	}

	public void setEventOfferedBy(BigInteger eventOfferedBy) {
		this.eventOfferedBy = eventOfferedBy;
	}

	public String getServiceOfferingType() {
		return serviceOfferingType;
	}

	public void setServiceOfferingType(String serviceOfferingType) {
		this.serviceOfferingType = serviceOfferingType;
	}

	public String getTransactionModeFpCash() {
		return transactionModeFpCash;
	}

	public void setTransactionModeFpCash(String transactionModeFpCash) {
		this.transactionModeFpCash = transactionModeFpCash;
	}

	public String getUserCanBlockOnlineFlag() {
		return userCanBlockOnlineFlag;
	}

	public void setUserCanBlockOnlineFlag(String userCanBlockOnlineFlag) {
		this.userCanBlockOnlineFlag = userCanBlockOnlineFlag;
	}

	public String getUserCanPurchaseOnlineFlag() {
		return userCanPurchaseOnlineFlag;
	}

	public void setUserCanPurchaseOnlineFlag(String userCanPurchaseOnlineFlag) {
		this.userCanPurchaseOnlineFlag = userCanPurchaseOnlineFlag;
	}

}
