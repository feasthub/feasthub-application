package com.monsor.feasthub.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class FeedbackAndReviewDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5470568258416040889L;

	private BigInteger id;

	private BigInteger userId;

	private BigInteger foodMenuId;

	private BigInteger eventId;

	private String rating;

	private String reviewComment;

	private String reviewDate;

	private Boolean markedAsOffensive;

	private Boolean deletedFlag;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public BigInteger getFoodMenuId() {
		return foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public BigInteger getEventId() {
		return eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getReviewComment() {
		return reviewComment;
	}

	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}

	

	public String getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(String reviewDate) {
		this.reviewDate = reviewDate;
	}

	public Boolean getMarkedAsOffensive() {
		return markedAsOffensive;
	}

	public void setMarkedAsOffensive(Boolean markedAsOffensive) {
		this.markedAsOffensive = markedAsOffensive;
	}

	public Boolean getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	
}
