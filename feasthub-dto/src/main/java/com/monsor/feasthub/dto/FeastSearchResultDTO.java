/**
 * 
 */
package com.monsor.feasthub.dto;

import java.io.Serializable;
import java.util.List;

import com.sun.corba.se.spi.ior.Identifiable;

/**
 * @author abhishekvashistha
 *
 */
public class FeastSearchResultDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Identifiable> eventList;

	public List<Identifiable> getEventList() {
		return eventList;
	}

	public void setEventList(List<Identifiable> eventList) {
		this.eventList = eventList;
	}

}
