/**
 * 
 */
package com.monsor.feasthub.dto;

import java.io.Serializable;

/**
 * @author amit
 *
 */
public class UserLoginDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 358134418179028190L;
	private String username;
	private String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
