package com.monsor.feasthub.dto;

import java.io.Serializable;

import com.monsor.feasthub.jpa.model.enums.PaymentGateway;
import com.monsor.feasthub.jpa.model.enums.TransactionMode;
import com.monsor.feasthub.jpa.model.enums.TransactionPurpose;



public class OrderPaymentDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderId;
	private String transactionId;
	private TransactionMode transactionMode;
	private double amount;
	private PaymentGateway paymentGateway;
	private TransactionPurpose transactionPurpose;
	private String chequeNumebr;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	public PaymentGateway getPaymentGateway() {
		return paymentGateway;
	}
	public void setPaymentGateway(PaymentGateway paymentGateway) {
		this.paymentGateway = paymentGateway;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public TransactionMode getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(TransactionMode transactionMode) {
		this.transactionMode = transactionMode;
	}
	public TransactionPurpose getTransactionPurpose() {
		return transactionPurpose;
	}
	public void setTransactionPurpose(TransactionPurpose transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}
	public String getChequeNumebr() {
		return chequeNumebr;
	}
	public void setChequeNumebr(String chequeNumebr) {
		this.chequeNumebr = chequeNumebr;
	}
}
