/**
 * 
 */
package com.monsor.feasthub.dto;

import com.monsor.feasthub.jpa.model.enums.OrderStatus;

/**
 * @author Amit Sharma
 *
 */
public class OrderStatusDTO implements FeastHubDTO {
    /**
     * 
     */
    private static final long serialVersionUID = -6911042943041883195L;

    private String orderId;

    private OrderStatus orderStatus;

    public OrderStatusDTO(String orderId, OrderStatus orderStatus) {
	super();
	this.orderId = orderId;
	this.orderStatus = orderStatus;
    }

    public OrderStatusDTO() {
	super();
	// TODO Auto-generated constructor stub
    }

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
	return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
	this.orderStatus = orderStatus;
    }
}
