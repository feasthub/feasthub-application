package com.monsor.feasthub.dto;

import java.io.Serializable;

public class ChangePasswordDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String password;
	
	private String newPassword;
	
	private String confirmPassword;
	
	public String getPassword() {
		return password;
	}
	@Override
	public String toString() {
		return "ChangePasswordDTO [password=" + password + ", newPasword=" + newPassword + ", confirmPassword=" + confirmPassword + "]";
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPasword) {
		this.newPassword = newPasword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	
}
