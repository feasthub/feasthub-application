package com.monsor.feasthub.dto;

import java.util.ArrayList;
import java.util.List;

import com.monsor.feasthub.jpa.model.common.Identifiable;

public class UserHistoryDTO implements  FeastHubDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Identifiable> todaysOrders = new ArrayList<>();
	
	private List<Identifiable> liveOrders = new ArrayList<>();
	
	private List<Identifiable> pastOrders = new ArrayList<>();

	public List<Identifiable> getLiveOrders() {
		return liveOrders;
	}

	public void setLiveOrders(List<Identifiable> liveOrders) {
		this.liveOrders = liveOrders;
	}

	public List<Identifiable> getPastOrders() {
		return pastOrders;
	}

	public void setPastOrders(List<Identifiable> pastOrders) {
		this.pastOrders = pastOrders;
	}

	public List<Identifiable> getTodaysOrders() {
	    return todaysOrders;
	}

	public void setTodaysOrders(List<Identifiable> todaysOrders) {
	    this.todaysOrders = todaysOrders;
	}

}
