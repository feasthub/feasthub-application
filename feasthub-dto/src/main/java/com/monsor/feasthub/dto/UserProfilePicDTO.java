/**
 * 
 */
package com.monsor.feasthub.dto;

import java.io.Serializable;

/**
 * @author Amit Sharma
 *
 */
public class UserProfilePicDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String profilePhoto;

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}
}
