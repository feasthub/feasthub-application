package com.monsor.feasthub.dto;

import java.io.Serializable;

public class UserHistoryFavouriteItemDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3809374697363687335L;
	private String userId;
	private String foodMenuId;
	private Boolean favFlag;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFoodMenuId() {
		return foodMenuId;
	}

	public void setFoodMenuId(String foodMenuId) {
		this.foodMenuId = foodMenuId;
	}


	public Boolean getFavFlag() {
		return favFlag;
	}

	public void setFavFlag(Boolean favFlag) {
		this.favFlag = favFlag;
	}

	@Override
	public String toString() {
		return "UserHistoryFavouriteItemDTO [userId=" + userId + ", foodMenuId=" + foodMenuId + ", favFlag=" + favFlag
				+ "]";
	}
}
