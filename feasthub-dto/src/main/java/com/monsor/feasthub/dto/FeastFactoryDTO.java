package com.monsor.feasthub.dto;

import java.io.Serializable;

public class FeastFactoryDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2818094106253093171L;

	private String eventId;
	private String foodMenuId;
	private String unit;
	private String orderedQuantity;
	private String deliveryDate;
	private String deliveryTime;
	private String additionalNoteByUser;
	private String purchaserUserId;
	private String sellerUserId;
	private String deliveryAddressId;

	public String getEventId() {
		return eventId;
	}
	public String getDeliveryAddressId() {
		return deliveryAddressId;
	}
	public void setDeliveryAddressId(String deliveryAddressId) {
		this.deliveryAddressId = deliveryAddressId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getFoodMenuId() {
		return foodMenuId;
	}
	public void setFoodMenuId(String foodMenuId) {
		this.foodMenuId = foodMenuId;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getOrderedQuantity() {
		return orderedQuantity;
	}
	public void setOrderedQuantity(String orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getAdditionalNoteByUser() {
		return additionalNoteByUser;
	}
	public void setAdditionalNoteByUser(String additionalNoteByUser) {
		this.additionalNoteByUser = additionalNoteByUser;
	}
	public String getPurchaserUserId() {
		return purchaserUserId;
	}
	public void setPurchaserUserId(String purchaserUserId) {
		this.purchaserUserId = purchaserUserId;
	}
	public String getSellerUserId() {
		return sellerUserId;
	}
	public void setSellerUserId(String sellerUserId) {
		this.sellerUserId = sellerUserId;
	}
	
	
	
	
}
