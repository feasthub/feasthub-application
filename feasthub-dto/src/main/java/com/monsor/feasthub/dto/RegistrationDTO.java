/**
 * 
 */
package com.monsor.feasthub.dto;

import java.io.Serializable;

/**
 * @author Amit
 *
 */
public class RegistrationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6351331580614333692L;

	private String userName;
	private String mobileNumber;
	private String emailId;
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "RegistrationDTO [userName=" + userName + ", mobileNumber=" + mobileNumber + ", emailId=" + emailId
				+ ", password=" + password + "]";
	}
}
