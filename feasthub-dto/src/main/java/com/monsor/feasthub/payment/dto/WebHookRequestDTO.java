package com.monsor.feasthub.payment.dto;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookRequestDTO implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String event;
    private String entity;
    private List<String> contains;
    private WebHookPayloadDTO payload;
    private String created_at;
    
    public String getEvent() {
    
        return event;
    }
    
    public void setEvent(String event) {
    
        this.event = event;
    }
    
    public String getEntity() {
    
        return entity;
    }
    
    public void setEntity(String entity) {
    
        this.entity = entity;
    }
    
    public List<String> getContains() {
    
        return contains;
    }
    
    public void setContains(List<String> contains) {
    
        this.contains = contains;
    }
    
    public WebHookPayloadDTO getPayload() {
    
        return payload;
    }
    
    public void setPayload(WebHookPayloadDTO payload) {
    
        this.payload = payload;
    }
    
    public String getCreated_at() {
    
        return created_at;
    }
    
    public void setCreated_at(String created_at) {
    
        this.created_at = created_at;
    }

    @Override
    public String toString() {

	return "WebHookRequestDTO [event=" + event + ", entity=" + entity + ", contains=" + contains + ", payload="
		+ payload + ", created_at=" + created_at + "]";
    }
}
