package com.monsor.feasthub.payment.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookPaymentDTO implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 6362189024527751773L;
    private WebHookEntityDTO entity;

    
    public WebHookEntityDTO getEntity() {
    
        return entity;
    }

    
    public void setEntity(WebHookEntityDTO entity) {
    
        this.entity = entity;
    }


    @Override
    public String toString() {

	return "WebHookPaymentDTO [entity=" + entity + "]";
    }
    
    
    
}
