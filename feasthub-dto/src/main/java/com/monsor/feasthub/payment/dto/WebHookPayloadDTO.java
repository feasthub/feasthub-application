package com.monsor.feasthub.payment.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookPayloadDTO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private WebHookPaymentDTO payment;

    
    public WebHookPaymentDTO getPayment() {
    
        return payment;
    }

    
    public void setPayment(WebHookPaymentDTO payment) {
    
        this.payment = payment;
    }
    

}
