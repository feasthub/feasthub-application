package com.monsor.feasthub.payment.dto;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebHookEntityDTO implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 2126181575646542862L;
    private String id;
    private String entity;
    private Double amount;
    private String currency;
    private String status;
    private String amount_refunded;
    private String refund_status;
    private String email;
    private String contact;
    private String error_code;
    private String error_description;
    private String[] notes;
    private String created_at;
    private String order_id;
    private String international;
    private String method;
    private String captured;
    private String description;
    private String card_id;
    private String bank;
    private String wallet;
    private String vpa;
    private String fee;
    
    
    public String getId() {
    
        return id;
    }
    
    public void setId(String id) {
    
        this.id = id;
    }
    
    public String getEntity() {
    
        return entity;
    }
    
    public void setEntity(String entity) {
    
        this.entity = entity;
    }
    
    public Double getAmount() {
    
        return amount;
    }
    
    public void setAmount(Double amount) {
    
        this.amount = amount;
    }
    
    public String getCurrency() {
    
        return currency;
    }
    
    public void setCurrency(String currency) {
    
        this.currency = currency;
    }
    
    public String getStatus() {
    
        return status;
    }
    
    public void setStatus(String status) {
    
        this.status = status;
    }
    
    public String getAmount_refunded() {
    
        return amount_refunded;
    }
    
    public void setAmount_refunded(String amount_refund) {
    
        this.amount_refunded = amount_refund;
    }
    
    public String getRefund_status() {
    
        return refund_status;
    }
    
    public void setRefund_status(String refund_status) {
    
        this.refund_status = refund_status;
    }
    
    public String getEmail() {
    
        return email;
    }
    
    public void setEmail(String email) {
    
        this.email = email;
    }
    
    public String getContact() {
    
        return contact;
    }
    
    public void setContact(String contact) {
    
        this.contact = contact;
    }
    
    public String getError_code() {
    
        return error_code;
    }
    
    public void setError_code(String error_code) {
    
        this.error_code = error_code;
    }
    
    public String getError_description() {
    
        return error_description;
    }
    
    public void setError_description(String error_description) {
    
        this.error_description = error_description;
    }
    
    public String[] getNotes() {
    
        return notes;
    }
    
    public void setNotes(String[] notes) {
    
        this.notes = notes;
    }
    
    public String getCreated_at() {
    
        return created_at;
    }
    
    public void setCreated_at(String created_at) {
    
        this.created_at = created_at;
    }

    
    public String getOrder_id() {
    
        return order_id;
    }

    
    public void setOrder_id(String order_id) {
    
        this.order_id = order_id;
    }

    
    public String getInternational() {
    
        return international;
    }

    
    public void setInternational(String international) {
    
        this.international = international;
    }

    
    public String getMethod() {
    
        return method;
    }

    
    public void setMethod(String method) {
    
        this.method = method;
    }

    
    public String getCaptured() {
    
        return captured;
    }

    
    public void setCaptured(String captured) {
    
        this.captured = captured;
    }

    
    public String getDescription() {
    
        return description;
    }

    
    public void setDescription(String description) {
    
        this.description = description;
    }

    
    public String getCard_id() {
    
        return card_id;
    }

    
    public void setCard_id(String card_id) {
    
        this.card_id = card_id;
    }

    
    public String getBank() {
    
        return bank;
    }

    
    public void setBank(String bank) {
    
        this.bank = bank;
    }

    
    public String getWallet() {
    
        return wallet;
    }

    
    public void setWallet(String wallet) {
    
        this.wallet = wallet;
    }

    
    public String getVpa() {
    
        return vpa;
    }

    
    public void setVpa(String vpa) {
    
        this.vpa = vpa;
    }
}
