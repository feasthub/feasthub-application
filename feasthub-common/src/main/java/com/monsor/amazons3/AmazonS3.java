package com.monsor.amazons3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.monsor.Base64.Base64;

public class AmazonS3 {

    private static final String SUFFIX = "/";

    private static final String ACCESS_KEY = "AKIAJC6EWB2MU5OSCUYA";

    private static final String SECRET_KEY = "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";

    private static final Logger LOG = LoggerFactory.getLogger(AmazonS3.class);

    public static String deleteImage(String bucketName, String fileName) {

	String url = null;
	AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
	try {
	    ClientConfiguration clientConfig = new ClientConfiguration();
	    clientConfig.setProtocol(Protocol.HTTP);
	    AmazonS3Client s3client = new AmazonS3Client(credentials);
	    s3client.deleteObject(new DeleteObjectRequest(bucketName,fileName));
	} catch (Exception e) {
	   e.printStackTrace();

	}

	return url;

    }

    public static String uploadImage(String bucketName, String folderName, String file, String encoded) throws Exception {

	// credentials object identifying user for authentication
	// user must have AWSConnector and AmazonS3FullAccess for
	// this example to work
	String url = null;
	AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
	try {
	    byte[] bI = Base64.decode(encoded);
	    ClientConfiguration clientConfig = new ClientConfiguration();
	    clientConfig.setProtocol(Protocol.HTTP);
	    // create a client connection based on credentials
	    AmazonS3Client s3client = new AmazonS3Client(credentials);
	    // create bucket - name must be unique for all S3 users
	    // String bucketName = "feasthub-pics";

	    s3client.createBucket(bucketName);

	    // list buckets
	    createFolder(bucketName, folderName, s3client);
	    InputStream fis = new ByteArrayInputStream(bI);
	    // upload file to folder and set it to public
	    // String fileName = folderName + SUFFIX + "awstest.png";
	    String fileName = folderName + SUFFIX + file;
	    ObjectMetadata metadata = new ObjectMetadata();
	    metadata.setContentLength(bI.length);
	    metadata.setContentType("image/png");
	    metadata.setCacheControl("public, max-age=31536000");
	    s3client.putObject(new PutObjectRequest(bucketName, fileName, fis, metadata).withCannedAcl(
		    CannedAccessControlList.PublicRead));
	    // https://s3.amazonaws.com/feasthub-pic2/testfolder2/awstest.png
	    url = new StringBuilder("https://s3.amazonaws.com/").append(bucketName).append("/").append(folderName)
		    .append("/").append(file).toString();

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    throw new Exception(e);
	}
	return url;

    }

    public static void createFolder(String bucketName, String folderName, AmazonS3Client client) {

	// create meta-data for your folder and set content-length to 0
	ObjectMetadata metadata = new ObjectMetadata();
	metadata.setContentLength(0);
	// create empty content
	InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
	// create a PutObjectRequest passing the folder name suffixed by /
	PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + SUFFIX, emptyContent,
		metadata);
	// send request to S3 to create folder
	client.putObject(putObjectRequest);

    }

}
