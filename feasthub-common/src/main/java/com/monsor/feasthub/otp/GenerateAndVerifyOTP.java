package com.monsor.feasthub.otp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.monsor.feasthub.response.SingleDTOResponse;
import com.monsor.feasthub.response.SuccessResponse;

public class GenerateAndVerifyOTP {
	private final String USER_AGENT = "Mozilla/5.0";
	// Base URL
	private static String baseUrl;
	private static final Logger LOG = LoggerFactory.getLogger(GenerateAndVerifyOTP.class);
	
	// Your application key
	private static String applicationKey;

	public GenerateAndVerifyOTP(String baseUrl, String applicationKey) {
		GenerateAndVerifyOTP.baseUrl = baseUrl;
		GenerateAndVerifyOTP.applicationKey = applicationKey;
	}

	public SuccessResponse verifyOTP(String countryCode, String mobileNumber, String oneTimePassword) {

		URL obj;
		StringBuffer response = null;
		try {
			obj = new URL(baseUrl + "verifyOTP");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("application-Key", applicationKey);

			String urlParameters = "{\"countryCode\":\"" + countryCode + "\",\"mobileNumber\":\"" + mobileNumber
					+ "\",\"oneTimePassword\":\"" + oneTimePassword + "\"}";
			System.out.println(urlParameters);
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return new SuccessResponse(String.valueOf(responseCode), response.toString());
			// print result
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public SuccessResponse generateOTP(String countryCode, String mobileNumber, Boolean generateOTP) throws Exception {

		URL obj = new URL(baseUrl + "generateOTP");
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("application-Key", applicationKey);

		String urlParameters = "{\"countryCode\":\"" + countryCode + "\",\"mobileNumber\":\"" + mobileNumber
				+ "\",\"getGeneratedOTP\":\"" + generateOTP + "\"}";

		System.out.println(urlParameters);
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		return new SuccessResponse(String.valueOf(responseCode), response.toString());
		

	}

	/*
	 * public ClientResponse generateOTP(String countryCode, String
	 * mobileNumber, Boolean generateOTP){ ClientResponse response=null; try {
	 * Client client = Client.create(); String Url = baseUrl+"/generateOTP";
	 * WebResource webResource = client.resource(Url);
	 * 
	 * HashMap<String, String> requestBodyMap = new HashMap<String, String>();
	 * requestBodyMap.put("countryCode",countryCode);
	 * requestBodyMap.put("mobileNumber",mobileNumber);
	 * requestBodyMap.put("getGeneratedOTP",generateOTP.toString()); JSONObject
	 * requestBodyJsonObject = new JSONObject(requestBodyMap); String input =
	 * requestBodyJsonObject.toString();
	 * 
	 * response = webResource.type(MediaType.APPLICATION_JSON)
	 * .header("application-Key", applicationKey) .post(ClientResponse.class,
	 * input);
	 * 
	 * String output = response.getEntity(String.class);
	 * System.out.println(output); //fetch your oneTimePassword and save it to
	 * session or db } catch (Exception e) { LOG.error(e.getMessage(), e);
	 * super.logEror(e); } return response; }
	 * 
	 * public ClientResponse verifyOTP(String countryCode, String mobileNumber,
	 * String oneTimePassword) { ClientResponse response=null; try { Client
	 * client = Client.create(); String Url = baseUrl+"/verifyOTP"; WebResource
	 * webResource = client.resource(Url); HashMap<String, String>
	 * requestBodyMap = new HashMap<String, String>();
	 * requestBodyMap.put("countryCode",countryCode);
	 * requestBodyMap.put("mobileNumber",mobileNumber);
	 * requestBodyMap.put("oneTimePassword",oneTimePassword); JSONObject
	 * requestBodyJsonObject = new JSONObject(requestBodyMap); String input =
	 * requestBodyJsonObject.toString();
	 * 
	 * response = webResource.type(MediaType.APPLICATION_JSON)
	 * .header("application-Key", applicationKey) .post(ClientResponse.class,
	 * input);
	 * 
	 * String output = response.getEntity(String.class); //fetch your
	 * oneTimePassword from session or db //and compare it with the OTP sent
	 * from clien } catch (Exception e) { LOG.error(e.getMessage(), e);
	 * super.logEror(e); } return response; }
	 */
}
