/**
 * 
 */
package com.monsor.feasthub.order;

/**
 * @author amit
 *
 */
public enum OrderPaymentStatus {
	PAYMENT_INITIATED, PAYMENT_RECEVIED, PAYMENT_DECLINED, PAYMENT_REFUND
}
