package com.monsor.feasthub.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ReadOnlyJPARepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

	T findOne(ID id);


}
