package com.monsor.feasthub.jpa.model.enums;

public enum StaticContent {
    CONTACT_US,
    ABOUT_US,
    TERM_AND_CONDITION,
    CORPORATE_POLICY,
    OUR_FOOD,
    CODE_OF_CONDUCT,
    PRIVACY_POLICY,
    DISCLAIMER;
}
