package com.monsor.feasthub.jpa.model.enums;

public enum PaymentGateway {
    ZAAKPAY("Zaakpay"),
    MOBIKWIK("MobiKwik"),
    PAYTM("Paytm");

    private String value;

    PaymentGateway(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public PaymentGateway getItem() {
	return this;
    }
}
