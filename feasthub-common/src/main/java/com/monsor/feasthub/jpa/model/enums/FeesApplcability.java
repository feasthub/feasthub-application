package com.monsor.feasthub.jpa.model.enums;

public enum FeesApplcability {
    LISTING("Listing"),
    ITEM_TOTAL("item Total"),
    SUB_TOTAL("Sub Total"),
    TOTAL("Total"),
    ON_TAXES("On Taxes"),
    ON_ADDITIONAL_FEES("On Additional Fees");

    private String value;

    FeesApplcability(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public FeesApplcability getItem() {
	return this;
    }
}
