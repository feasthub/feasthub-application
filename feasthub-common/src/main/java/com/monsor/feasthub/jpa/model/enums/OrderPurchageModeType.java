package com.monsor.feasthub.jpa.model.enums;

public enum OrderPurchageModeType {
    CASH("Cash"),
    FEAST_POINT("Feast Point");
    private String value;

    OrderPurchageModeType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public OrderPurchageModeType getItem() {
	return this;
    }
}
