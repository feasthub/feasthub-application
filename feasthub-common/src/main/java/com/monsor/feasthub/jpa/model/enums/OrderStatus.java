package com.monsor.feasthub.jpa.model.enums;

public enum OrderStatus {
    CREATED("Created"),
    PAID("Paid"),
    CANCELLED("Cancelled"),
    CONFIRMED_BY_COMPANY("Confirm By Company"),
    BEING_PREPARED("Being Prepared"),
    PACKING_IN_PROGRESS("Packing In Progress"),
    DELIVERED("Delivered"),
    COMPLETED("Completed"),
    OUT_FOR_DELIVERY("Out For Delivery"),
    DELIVERED_TO_NEIGHBOUR("Delivered To Neighboure");

    private String value;

    OrderStatus(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public OrderStatus getItem() {
	return this;
    }
}
