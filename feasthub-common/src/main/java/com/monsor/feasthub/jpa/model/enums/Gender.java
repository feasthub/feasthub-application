/**
 * 
 */
package com.monsor.feasthub.jpa.model.enums;

/**
 * @author Amit Sharma
 *
 */
public enum Gender {
    MALE("Male"),
    FEMALE("Female");
    private String value;

    Gender(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public Gender getItem() {
	return this;
    }
}
