package com.monsor.feasthub.jpa.model.enums;

public enum BusinessTypes {
    CONSUMER("Customer"),
    HOMEBASE("Homebase"),
    CATERER_RESTAUTRER("Caterer Restautrer"),
    SERVICES("Services"),
    FH("Feasthub"),
    FH_FRANCHISE("Feasthub Franchise"),
    FH_EXTENSION("Feasthub Extension");

    private String value;

    BusinessTypes(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public BusinessTypes getItem() {
	return this;
    }
}
