package com.monsor.feasthub.jpa.model.enums;

public enum TransactionPurpose {
    RETAIL_PURCHASE("Retails Purchage"),
    BULK_PURCHASE("Bulk Purchage");

    private String value;

    TransactionPurpose(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public TransactionPurpose getItem() {
	return this;
    }
}
