package com.monsor.feasthub.jpa.model.enums;

public enum UserType {
    FH_USER("Feasthub User"),
    RETAIL_CONSUMER("Retail Consumer"),
    FH_CORP_PARTNER("Feasthub Corporate Partner"),
    FH_ADMIN("Feasthub Admin"),
    FH_SYS_ADMIN("Feasthub System Admin"),
    FH_USER_SUPPORT("Feasthub Support"),
    FH_SELLER("Feasthub Seller"),
    CORPORATE_CONSUMER("Corporate Consumer"),
    FH_OPERATION_MANAGER("Feasthub Operation Manager"),
    FH_OPERATION_DELIVERY_AGENT("Feasthub Operation Delivery Agent");

    private String value;

    UserType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public UserType getItem() {
	return this;
    }
}
