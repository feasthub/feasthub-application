package com.monsor.feasthub.jpa.model.enums;

public enum PaymentStatus {
    REFUND("Refund"),
    INITIATED("Initiated"),
    CASH_ON_DELIVERY("Cash On Delivery"),
    NOT_INITIATED("not Initiated"),
    DECLINED("Declined"),
    FRAUD("Fraud"),
    COD_NOT_HONORED("Cash Not Honored"),
    CONFIRM("Confirm");

    private String value;

    PaymentStatus(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public PaymentStatus getItem() {
	return this;
    }
}
