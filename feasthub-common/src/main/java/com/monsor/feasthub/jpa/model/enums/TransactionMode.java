package com.monsor.feasthub.jpa.model.enums;

public enum TransactionMode {
    CASH("Cash"),
    FEAST_POINT("Feast Point"),
    RTGS("RTGS"),
    NEFT("NEFT"),
    CHEQUE("Cheque"),
    MOBILE_PAYMENT("Make Payment"),
    E_WALLET("E Wallet"),
    SEND_PAYMENT_LINK("Send Payment Link");

    private String value;

    TransactionMode(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public TransactionMode getItem() {
	return this;
    }
}
