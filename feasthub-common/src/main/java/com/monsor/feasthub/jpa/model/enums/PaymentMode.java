package com.monsor.feasthub.jpa.model.enums;

public enum PaymentMode {
    PAID_IN("Paid In"),
    PAID_OUT("Paid Out");
    private String value;

    PaymentMode(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public PaymentMode getItem() {
	return this;
    }
}
