package com.monsor.feasthub.jpa.model.enums;

public enum DeliveryStatus {
    YET_TO_DELIVERED("Yet To Delivered"),
    DELIVERY_INITIATED("Delivery Initiated"),
    OUT_FOR_DELIVERY("Out For Delivery"),
    DELIVERED("Delivered"),
    DELIVERED_TO_NEIGHBOUR("Delivered To Neighbour"),
    DELIVERY_FAILED("Delivery Failed"),
    WAITINGFOR_CUSTOMER_TAKE_AWAY("Waiting Customer to Take Away");
    private String value;

    DeliveryStatus(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public DeliveryStatus getItem() {
	return this;
    }
}
