package com.monsor.feasthub.jpa.model.enums;

public enum AdditionalServiceOffering {
	IN_PREMISES_RETAIL_COOKED_FOOD_DELIVERY("In Premises Retail Cooked Food Delivery"), 
	IN_PREMISES_RETAIL_COOKED_FOOD_TAKEAWAY("In Premises Retail Cooked Food Takeaway"), 
	AT_CUSTOMER_PREMISES("At Customer Premises"), 
	FINE_DINING_EXPERIENCE_AT_CUSTOMER_PREMISES("Find Dining Exp. at Customer Premises"), 
	IN_PREMISES_INSTITUTIONAL_COOKED_FOOD_DELIVERY("In Premises Industrial Cooked Food Delivery"), 
	IN_PREMISES_BULK_COOKED_FOOD_DELIVERY("In Premises Bulk Cooked Delivery"), 
	IN_PREMISES_BULK_COOKED_FOOD_TAKEAWAY("In Premises Bulk Cooked Takeaway"),
	IN_PREMISES_RETAIL_CROSS_SALES_ITEMS("In Premises Cross sales items for retails sales"),
	IN_PREMISES_BULK_CROSS_SALES_ITEMS("Cross sales items for BULK Sales");
	
	private String value;
	AdditionalServiceOffering(String value){
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	
	public AdditionalServiceOffering getItem(){
		return this;
	}
}
