package com.monsor.feasthub.jpa.model.enums;

public enum ServiceOfferingTypes {
    HOME_COOK_TAKEAWAY("Home Cook Takeaway"),
    HOME_COOK_DELIVERY("Home Cook Delivery"),
    HOME_COOK_HOST("Home Cook Host"),
    BULK_COOKING("Bulk Cooking"),
    COOKS_ON_HIRE("Cooks On Hire"),
    CHEF_ON_HIRE("Chef On Hire"),
    BANQUET_SERVER("Banquet Server"),
    NUTRITIONIST_SERVICE("Nutritionist Services"),
    EVENT_PLANNER("Event Planner"),
    SPECIALITY_FOOD("Speciality Food"),
    COOKING_LESSON("Cooking Lesson"),
    IN_PREMISES_COOKED_FOOD("In Premises Cooked Food"),
    IN_PREMISES_MANUFACTURING("In Premises Manufacturing");

    private String value;

    ServiceOfferingTypes(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public ServiceOfferingTypes getItem() {
	return this;
    }
}
