package com.monsor.feasthub.jpa.model.enums;

public enum BusinessStructure {
    INDIVIDUAL("Individual"),
    PROPRIETORSHIP("Proprietorship"),
    PARTNERSHIP("Partnership"),
    PRIVATE_LIMITED("Private Limited"),
    PUBLIC_COMPANY("Public Company"),
    MNC("Multi National Company"),
    GOVT_ORGANISTAION("Govt. Organisation"),
    NGO("NGO"),
    FH("Feasthub");

    private String value;

    BusinessStructure(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public BusinessStructure getItem() {
	return this;
    }
}
