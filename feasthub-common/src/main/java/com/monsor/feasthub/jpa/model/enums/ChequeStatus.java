package com.monsor.feasthub.jpa.model.enums;

public enum ChequeStatus {
    INITIATED("Initiated"),
    CLEARED("Cleared"),
    BOUNCE("Bounce"),
    RETURNED("Returned"),
    STOPPED("Stopped"),
    COLLECTED("Collected");

    private String value;

    ChequeStatus(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public ChequeStatus getItem() {
	return this;
    }
}
