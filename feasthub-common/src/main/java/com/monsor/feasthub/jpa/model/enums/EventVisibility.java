package com.monsor.feasthub.jpa.model.enums;

public enum EventVisibility {
    PRIVATE("Private"),
    PUBLIC("Public");
    private String value;

    EventVisibility(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public EventVisibility getItem() {
	return this;
    }
}
