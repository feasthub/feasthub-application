package com.monsor.feasthub.jpa.model.enums;

public enum UserSalutation {
    MR("Mr."),
    MRS("Mrs."),
    DR("Dr."),
    ER("Er."),
    MISS("Miss"),
    MASTER("Mister");

    private String value;

    UserSalutation(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public UserSalutation getItem() {
	return this;
    }
}
