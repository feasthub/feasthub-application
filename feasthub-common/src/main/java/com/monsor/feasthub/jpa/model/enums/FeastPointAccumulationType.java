package com.monsor.feasthub.jpa.model.enums;

public enum FeastPointAccumulationType {
    REFERRED("Referred"),
    PURCHASED("Purchased"),
    PERSONAL_GIFT("Personal Gift"),
    PARTNER_OFFERING_THRU_COUPONS("Partner Offering Through Coopuns"),
    PARTNER_PROMOTIONS("Partmer Promotions"),
    SELLER_EARNED("Seller Earned"),
    CORPORATE_GIFT("Corporate Gift"),
    FSHB_BONUS_POINTS("Feasthub Bonus");

    private String value;

    FeastPointAccumulationType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public FeastPointAccumulationType getItem() {
	return this;
    }

}
