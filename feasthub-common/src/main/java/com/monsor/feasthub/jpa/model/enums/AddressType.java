package com.monsor.feasthub.jpa.model.enums;

public enum AddressType {
    OFFICE("Office"),
    HOME("Home"),
    PERMANENT("Permanent"),
    CURRENT("Current");

    private String value;

    AddressType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public AddressType getItem() {
	return this;
    }

}
