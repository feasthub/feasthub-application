package com.monsor.feasthub.jpa.model.common;

import java.io.Serializable;
import java.math.BigInteger;

public interface Identifiable extends Serializable {
	public BigInteger getId();

	public void setId(BigInteger id);
}
