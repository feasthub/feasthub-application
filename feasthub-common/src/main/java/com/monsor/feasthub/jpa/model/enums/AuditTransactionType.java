package com.monsor.feasthub.jpa.model.enums;

public enum AuditTransactionType {
    PAID_IN("Paid In"),
    PAID_OUT("Paid Out");
    private String value;

    AuditTransactionType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public AuditTransactionType getItem() {
	return this;
    }
}
