package com.monsor.feasthub.jpa.model.enums;

public enum FhAdditionalChargeFeesType {
    LISTING_FEES("Listing Fees"),
    COMMISION("Commision"),
    SPONSERED_LISTING("Sponsered");

    private String value;

    FhAdditionalChargeFeesType(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }

    public FhAdditionalChargeFeesType getItem() {
	return this;
    }
}
