package com.monsor.feasthub.response;

import com.monsor.feasthub.jpa.model.common.Identifiable;

public class SingleObjectResponse<T extends Identifiable> extends SuccessResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6017731707519514702L;
	private T data;

	public Identifiable getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public SingleObjectResponse(String responseCode, String responseMessage, T data) {
		super(responseCode, responseMessage);
		this.data = data;
	}

}
