package com.monsor.feasthub.response;

import java.io.Serializable;

public class SuccessResponse implements FeastHubResponse, Serializable {
	private String responseCode;
	private String responseMessage;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public SuccessResponse(String responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	@Override
	public Object getData() {
		// TODO Auto-generated method stub
		return null;
	}
}
