package com.monsor.feasthub.response;

import com.monsor.feasthub.dto.FeastHubDTO;

public class SingleDTOResponse<T extends FeastHubDTO> extends SuccessResponse {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6017731707519514702L;
	private T data;

	public FeastHubDTO getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public SingleDTOResponse(String responseCode, String responseMessage, T data) {
		super(responseCode, responseMessage);
		this.data = data;
	}

}
