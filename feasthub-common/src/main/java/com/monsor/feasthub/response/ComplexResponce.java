package com.monsor.feasthub.response;

import java.util.List;

import com.monsor.feasthub.jpa.model.common.Identifiable;

public class ComplexResponce<T extends Identifiable> extends SuccessResponse {
	private List<T> data;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public ComplexResponce(String responseCode, String responseMessage, List<T> data) {
		super(responseCode, responseMessage);
		this.data = data;
	}
}
