package com.monsor.feasthub.util;

import java.util.List;

import com.monsor.feasthub.dto.FeastHubDTO;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.response.ComplexResponce;
import com.monsor.feasthub.response.ErrorResponse;
import com.monsor.feasthub.response.FeastHubResponse;
import com.monsor.feasthub.response.SingleDTOResponse;
import com.monsor.feasthub.response.SingleObjectResponse;
import com.monsor.feasthub.response.SuccessResponse;

public class ResponseUtil {

	public static FeastHubResponse getResponse(String code, String message) {
		return new SuccessResponse(code, message);
	}

	public static FeastHubResponse getErrorResponse(String code, String message) {
		return new ErrorResponse(code, message);
	}

	public static FeastHubResponse getResponse(String code, String message, Identifiable data) {
		return new SingleObjectResponse(code, message, data);
	}

	public static FeastHubResponse getResponse(String code, String message, List<? extends Identifiable> data) {
		return new ComplexResponce(code, message, data);
	}
	
	public static FeastHubResponse getResponse(String code, String message, FeastHubDTO data) {
		return new SingleDTOResponse(code, message, data);
	}

	

}
