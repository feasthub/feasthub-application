package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the feast_point_general_rule database table.
 * 
 */
@Entity
@Table(name = "feast_point_general_rule")
@NamedQueries({
		@NamedQuery(name = "FeastPointGeneralRule.searchFeastPointGeneralRulesIgnoreCase", query = "SELECT f FROM FeastPointGeneralRule f "
				+ "WHERE " + " f.coupneCodeExpired = :searchString "),

		@NamedQuery(name = "FeastPointGeneralRule.findBycoupneCodeExpired", query = "SELECT f FROM FeastPointGeneralRule f "
				+ "WHERE " + " f.coupneCodeExpired = :coupneCodeExpired "),
		@NamedQuery(name = "FeastPointGeneralRule.findDuplicate", query = "SELECT f FROM FeastPointGeneralRule f "
				+ "WHERE " + " f.couponCode = :couponCode ") })

public class FeastPointGeneralRule implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FEAST_POINT_GENERAL_RULE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@CheckForDuplicate
	@Searchable
	@Column(name = "COUPNE_CODE_EXPIRED")
	private String coupneCodeExpired;

	@Column(name = "COUPON_CODE")
	private String couponCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "COUPON_CODE_EXPIRY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date couponCodeExpiryDate;

	@Column(name = "FEAST_POINT_BALANCE_ID")
	private BigInteger feastPointBalanceId;

	@Temporal(TemporalType.DATE)
	@Column(name = "FP_EXPIRY_DATES")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date fpExpiryDates;

	@Column(name = "FP_POINTS")
	private int fpPoints;

	@Temporal(TemporalType.DATE)
	@Column(name = "PROMOTION_EXPIRY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date promotionExpiryDate;

	@Column(name = "PROMOTION_EXPIRY_FLAG")
	private String promotionExpiryFlag;

	@Column(name = "PROMOTION_EXPIRY_TIME")
	private Time promotionExpiryTime;

	@Temporal(TemporalType.DATE)
	@Column(name = "PROMOTIONS_START_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date promotionsStartDate;

	@Column(name = "PROMOTIONS_START_TIME")
	private Time promotionsStartTime;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public FeastPointGeneralRule() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCoupneCodeExpired() {
		return this.coupneCodeExpired;
	}

	public void setCoupneCodeExpired(String coupneCodeExpired) {
		this.coupneCodeExpired = coupneCodeExpired;
	}

	public String getCouponCode() {
		return this.couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public Date getCouponCodeExpiryDate() {
		return this.couponCodeExpiryDate;
	}

	public void setCouponCodeExpiryDate(Date couponCodeExpiryDate) {
		this.couponCodeExpiryDate = couponCodeExpiryDate;
	}

	public BigInteger getFeastPointBalanceId() {
		return this.feastPointBalanceId;
	}

	public void setFeastPointBalanceId(BigInteger feastPointBalanceId) {
		this.feastPointBalanceId = feastPointBalanceId;
	}

	public Date getFpExpiryDates() {
		return this.fpExpiryDates;
	}

	public void setFpExpiryDates(Date fpExpiryDates) {
		this.fpExpiryDates = fpExpiryDates;
	}

	public int getFpPoints() {
		return this.fpPoints;
	}

	public void setFpPoints(int fpPoints) {
		this.fpPoints = fpPoints;
	}

	public Date getPromotionExpiryDate() {
		return this.promotionExpiryDate;
	}

	public void setPromotionExpiryDate(Date promotionExpiryDate) {
		this.promotionExpiryDate = promotionExpiryDate;
	}

	public String getPromotionExpiryFlag() {
		return this.promotionExpiryFlag;
	}

	public void setPromotionExpiryFlag(String promotionExpiryFlag) {
		this.promotionExpiryFlag = promotionExpiryFlag;
	}

	public Time getPromotionExpiryTime() {
		return this.promotionExpiryTime;
	}

	public void setPromotionExpiryTime(Time promotionExpiryTime) {
		this.promotionExpiryTime = promotionExpiryTime;
	}

	public Date getPromotionsStartDate() {
		return this.promotionsStartDate;
	}

	public void setPromotionsStartDate(Date promotionsStartDate) {
		this.promotionsStartDate = promotionsStartDate;
	}

	public Time getPromotionsStartTime() {
		return this.promotionsStartTime;
	}

	public void setPromotionsStartTime(Time promotionsStartTime) {
		this.promotionsStartTime = promotionsStartTime;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}