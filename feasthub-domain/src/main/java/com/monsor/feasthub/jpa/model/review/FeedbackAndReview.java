package com.monsor.feasthub.jpa.model.review;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.view.FhUserViewForReview;

/**
 * The persistent class for the feedback_and_review database table.
 * 
 */
@Entity
@Table(name = "feast_event_review")

@NamedQueries({
		@NamedQuery(name = "FeedbackAndReview.sumOfRatings", query = "SELECT sum(f.rating) FROM FeedbackAndReview f "
				+ "WHERE " + " f.eventId = ?1") })
public class FeedbackAndReview implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FEAST_EVENT_REVIEW_ID")
	private BigInteger id;

	@OneToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
	private FhUserViewForReview userInfo;

	@JsonIgnore
	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	@JsonIgnore
	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@Column(name = "RATING")
	private String rating;

	@Column(name = "REVIEW_COMMENT")
	private String reviewComment;

	@Temporal(TemporalType.DATE)
	@Column(name = "REVIEW_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date reviewDate;

	@Column(name = "MARKED_AS_OFFENSIVE")
	private Boolean markedAsOffensive;

	@JsonIgnore
	@Column(name = "DELETED_FLAG")
	private Boolean deletedFlag;

	public FeedbackAndReview() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FeedbackAndReview(FhUserViewForReview user, BigInteger foodMenuId, BigInteger eventId, String rating,
			String reviewComment, Boolean markedAsOffensive, Boolean deletedFlag, Date reviewDate) {
		this.userInfo = user;
		this.foodMenuId = foodMenuId;
		this.eventId = eventId;
		this.rating = rating;
		this.reviewComment = reviewComment;
		this.markedAsOffensive = markedAsOffensive;
		this.deletedFlag = deletedFlag;
		this.reviewDate = reviewDate;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public FhUserViewForReview getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(FhUserViewForReview userInfo) {
		this.userInfo = userInfo;
	}

	public BigInteger getFoodMenuId() {
		return foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public BigInteger getEventId() {
		return eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getReviewComment() {
		return reviewComment;
	}

	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public Boolean getMarkedAsOffensive() {
		return markedAsOffensive;
	}

	public void setMarkedAsOffensive(Boolean markedAsOffensive) {
		this.markedAsOffensive = markedAsOffensive;
	}

	public Boolean getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(Boolean deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

}