package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.food.FoodMenuServingCategory;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;

/**
 * The persistent class for the EVENT_CUISINE_LISTING database table.
 * 
 */
@Entity
@Table(name = "event_cuisine_listing")
@NamedQueries({
		@NamedQuery(name = "EventCuisineListing.searchEventCuisineListingsIgnoreCase", query = "SELECT f FROM EventCuisineListing f "
				+ "WHERE " + " f.eventId = :searchString " + " or " + " f.foodMenuItem.id = :searchString ") })
public class EventCuisineListingView implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_CUISINE_LISTING_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@JsonIgnore
	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@JsonIgnore
	@Column(name = "NOTE")
	private String note;

	@JsonIgnore
	@Column(name = "OFFERED_QUANTITY")
	private int offeredQuantity;

	@JsonIgnore
	@Column(name = "MAX_OFFERED_QUANTITY_QUICK_SERVICE")
	private int maxOfferedQUantityQuickService;

	@JsonIgnore
	@OneToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "food_menu_serving_category", joinColumns = @JoinColumn(name = "FOOD_SERVING_SIZE_CATEGORY_TYPE_ID"), inverseJoinColumns = @JoinColumn(name = "FOOD_SERVING_SIZE_CATEGORY_TYPE_ID"))
	private FoodMenuServingCategory foodMenuServingCategory;

	@OneToOne
	@JoinColumn(name = "FOOD_MENU_ID")
	private FoodMenuItemView foodMenuItem;

	public EventCuisineListingView() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getEventId() {
		return eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getOfferedQuantity() {
		return offeredQuantity;
	}

	public void setOfferedQuantity(int offeredQuantity) {
		this.offeredQuantity = offeredQuantity;
	}

	public int getMaxOfferedQUantityQuickService() {
		return maxOfferedQUantityQuickService;
	}

	public void setMaxOfferedQUantityQuickService(int maxOfferedQUantityQuickService) {
		this.maxOfferedQUantityQuickService = maxOfferedQUantityQuickService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public FoodMenuItemView getFoodMenuItem() {
		return foodMenuItem;
	}

	public void setFoodMenuItem(FoodMenuItemView foodMenuItem) {
		this.foodMenuItem = foodMenuItem;
	}

	public FoodMenuServingCategory getFoodMenuServingCategory() {
		return foodMenuServingCategory;
	}

	public void setFoodMenuServingCategory(FoodMenuServingCategory foodMenuServingCategory) {
		this.foodMenuServingCategory = foodMenuServingCategory;
	}

}