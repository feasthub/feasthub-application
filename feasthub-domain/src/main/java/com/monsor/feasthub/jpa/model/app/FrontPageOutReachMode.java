/**
 * 
 */
package com.monsor.feasthub.jpa.model.app;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * @author Amit Sharma
 *
 */
@Entity
@Table(name="front_page_outreach_mode")
@NamedQuery(name = "FrontPageOutReachMode.findAll", query = "SELECT f FROM FrontPageOutReachMode f")
public class FrontPageOutReachMode implements Identifiable {

    /**
     * 
     */
    private static final long serialVersionUID = 5181449752166251525L;
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    
    @Column(name="outreach_type")
    private String outreachType;
    
    @Column(name="outreach_device")
    private String outreachDevice;
    
    @Column(name="no_of_slides")
    private Integer noOfSlides;
    
    @OneToMany(mappedBy="frontPageOutreachId",fetch=FetchType.EAGER)
    private List<FrontPageOutReachImage> images;
    

    public String getOutreachType() {
        return outreachType;
    }

    public void setOutreachType(String outreachType) {
        this.outreachType = outreachType;
    }

    public String getOutreachDevice() {
        return outreachDevice;
    }

    public void setOutreachDevice(String outreachDevice) {
        this.outreachDevice = outreachDevice;
    }

    public Integer getNoOfSlides() {
        return noOfSlides;
    }

    public void setNoOfSlides(Integer noOfSlides) {
        this.noOfSlides = noOfSlides;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public List<FrontPageOutReachImage> getImages() {
        return images;
    }

    public void setImages(List<FrontPageOutReachImage> images) {
        this.images = images;
    }

}
