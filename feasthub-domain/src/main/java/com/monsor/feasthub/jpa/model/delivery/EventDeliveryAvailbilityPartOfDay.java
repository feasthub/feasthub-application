package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the event_delivery_availbility_part_of_day database
 * table.
 * 
 */
@Entity
@Table(name = "event_delivery_availbility_part_of_day")
@NamedQuery(name = "EventDeliveryAvailbilityPartOfDay.findAll", query = "SELECT e FROM EventDeliveryAvailbilityPartOfDay e")
public class EventDeliveryAvailbilityPartOfDay implements Serializable, Identifiable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_DELIVERY_AVAILBILITY_ID")
	private BigInteger id;

	@Column(name = "PART_OF_DAY_NAME")
	private String partOfDayName;

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "EVENT_DELIVERY_AVAILBILITY_ID")
	private List<EventDeliveryPartOfDayToSlotMapping> deliveryPartToSlotMapping;

	public EventDeliveryAvailbilityPartOfDay() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getPartOfDayName() {
		return this.partOfDayName;
	}

	public void setPartOfDayName(String partOfDayName) {
		this.partOfDayName = partOfDayName;
	}

	public List<EventDeliveryPartOfDayToSlotMapping> getDeliveryPartToSlotMapping() {
		return deliveryPartToSlotMapping;
	}

	public void setDeliveryPartToSlotMapping(List<EventDeliveryPartOfDayToSlotMapping> deliveryPartToSlotMapping) {
		this.deliveryPartToSlotMapping = deliveryPartToSlotMapping;
	}

}