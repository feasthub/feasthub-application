package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

@Entity
@Table(name = "countries")
public class Country implements Identifiable, Serializable {
	@Id
	@Column(name = "id")
	private BigInteger id;

	@Column(name = "sortname")
	private String countryCode;

	@Column(name = "name")
	private String country;

	@OneToMany(mappedBy = "country", fetch = FetchType.EAGER)
	private List<State> states;

	@Override
	public BigInteger getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		// TODO Auto-generated method stub
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

}
