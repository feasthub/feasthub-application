package com.monsor.feasthub.jpa.model.audit;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.enums.TransactionMode;
import com.monsor.feasthub.jpa.model.enums.TransactionPurpose;

/**
 * The persistent class for the transaction_mode_citrus database table.
 * 
 */
@Entity
@Table(name = "transaction_online_audit")
@NamedQuery(name = "TransactionOnlineAudit.findAll", query = "SELECT t FROM TransactionOnlineAudit t")
public class TransactionOnlineAudit implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ONLINE_AUDIT_ID")
	private BigInteger id;

	@Column(name = "ORDER_TRANSACTION_ID")
	private BigInteger orderTransactionId;

	@Enumerated
	@Column(name = "TRANSACTION_MODE")
	private TransactionMode transactionMode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRANSACTION_DATE")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+5:30")
	private Date transactionDate;

	@Enumerated
	@Column(name = "PAYMENT_STATUS")
	private PaymentStatus paymentStatus;

	@Column(name = "CURRENCY")
	private String currency;

	@Enumerated
	@Column(name = "TRANSACTION_PURPOSE")
	private TransactionPurpose transactionPurpose;

	public TransactionOnlineAudit() {
	}

	public BigInteger getOrderTransactionId() {
		return this.orderTransactionId;
	}

	public void setOrderTransactionId(BigInteger orderTransactionId) {
		this.orderTransactionId = orderTransactionId;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public TransactionMode getTransactionMode() {
		return transactionMode;
	}

	public void setTransactionMode(TransactionMode transactionMode) {
		this.transactionMode = transactionMode;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public TransactionPurpose getTransactionPurpose() {
		return transactionPurpose;
	}

	public void setTransactionPurpose(TransactionPurpose transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}

}