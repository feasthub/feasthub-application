package com.monsor.feasthub.jpa.model.otp;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the feasthub_otp database table.
 * 
 */
@Entity
@Table(name = "feasthub_otp")
@NamedQuery(name = "FeasthubOtp.findAll", query = "SELECT f FROM FeasthubOtp f")
public class FeasthubOtp implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigInteger id;

	@Column(name = "current_status")
	private String currentStatus;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "previous_status")
	private String previousStatus;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "state")
	private String state;

	@Column(name = "otp")
	private String otp;

	public FeasthubOtp() {
	}

	public FeasthubOtp(String countryCode, String mobileNumber, String currentStatus) {
		this.countryCode = countryCode;
		this.mobileNumber = mobileNumber;
		this.currentStatus = currentStatus;

	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCurrentStatus() {
		return this.currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPreviousStatus() {
		return this.previousStatus;
	}

	public void setPreviousStatus(String previousStatus) {
		this.previousStatus = previousStatus;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}