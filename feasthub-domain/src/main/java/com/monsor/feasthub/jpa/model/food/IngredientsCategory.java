package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the ingredients_category database table.
 * 
 */
@Entity
@Table(name = "master_ingredients_category")
@NamedQuery(name = "IngredientsCategory.findAll", query = "SELECT i FROM IngredientsCategory i")
public class IngredientsCategory implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INGREDIENTS_CATEGORY_ID")
	private BigInteger id;

	@Column(name = "INGREDIENTS_CATEGORY_CODE")
	private String ingredientsCode;

	@Column(name = "INGREDIENTS_CATEGORY_NAME")
	private String ingredientsCategoryName;

	public IngredientsCategory() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIngredientsCategoryName() {
		return this.ingredientsCategoryName;
	}

	public void setIngredientsCategoryName(String ingredientsCategoryName) {
		this.ingredientsCategoryName = ingredientsCategoryName;
	}

	public String getIngredientsCode() {
		return ingredientsCode;
	}

	public void setIngredientsCode(String ingredientsCode) {
		this.ingredientsCode = ingredientsCode;
	}

}