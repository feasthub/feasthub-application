package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the apps_secrets database table.
 * 
 */
@Entity
@Table(name = "apps_secrets")
@NamedQueries({ @NamedQuery(name = "AppsSecret.findAll", query = "SELECT a FROM AppsSecret a"),
		@NamedQuery(name = "AppsSecret.searchAppsSecretsIgnoreCase", query = "SELECT f FROM AppsSecret f "

		), @NamedQuery(name = "AppsSecret.findDuplicate", query = "SELECT f FROM AppsSecret f " + "WHERE " + " f.appsName = :appsName and f.clientId = :clientId ") })
public class AppsSecret implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "APPS_SECRET_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Required
	@CheckForDuplicate
	@Column(name = "APPS_NAME")
	private String appsName;

	@Required
	@CheckForDuplicate
	@Column(name = "CLIENT_ID")
	private String clientId;

	@Column(name = "CLIENT_SECRET")
	private String clientSecret;

	public AppsSecret() {
	}

	public String getAppsName() {
		return this.appsName;
	}

	public void setAppsName(String appsName) {
		this.appsName = appsName;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return this.clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

}