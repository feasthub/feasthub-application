package com.monsor.feasthub.jpa.model.partners;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the fh_seller_memberships database table.
 * 
 */
@Entity
@Table(name = "fh_seller_memberships")
@NamedQuery(name = "FhSellerMembership.findAll", query = "SELECT f FROM FhSellerMembership f")
public class FhSellerMembership implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Column(name = "APPLICABLE_TAX_STRUCTURE_ID")
	private BigInteger applicableTaxStructureId;

	@Column(name = "BUSINESS_TYPES_KEY")
	private BigInteger businessTypesKey;

	@Temporal(TemporalType.DATE)
	@Column(name = "EXPIRY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date expiryDate;

	@Column(name = "MEMBERSHIP_TYPE_ID_KEY")
	private BigInteger membershipTypeIdKey;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SELLER_MEMBERSHIP_ID")
	private BigInteger id;

	@Column(name = "SERVICE_ID_KEY")
	private BigInteger serviceIdKey;

	@Temporal(TemporalType.DATE)
	@Column(name = "START_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date startDate;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public FhSellerMembership() {
	}

	public BigInteger getApplicableTaxStructureId() {
		return this.applicableTaxStructureId;
	}

	public void setApplicableTaxStructureId(BigInteger applicableTaxStructureId) {
		this.applicableTaxStructureId = applicableTaxStructureId;
	}

	public BigInteger getBusinessTypesKey() {
		return this.businessTypesKey;
	}

	public void setBusinessTypesKey(BigInteger businessTypesKey) {
		this.businessTypesKey = businessTypesKey;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigInteger getMembershipTypeIdKey() {
		return this.membershipTypeIdKey;
	}

	public void setMembershipTypeIdKey(BigInteger membershipTypeIdKey) {
		this.membershipTypeIdKey = membershipTypeIdKey;
	}

	public BigInteger getServiceIdKey() {
		return this.serviceIdKey;
	}

	public void setServiceIdKey(BigInteger serviceIdKey) {
		this.serviceIdKey = serviceIdKey;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}