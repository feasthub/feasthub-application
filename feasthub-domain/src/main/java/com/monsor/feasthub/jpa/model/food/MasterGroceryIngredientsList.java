package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the ingredients_list database table.
 * 
 */
@Entity
@Table(name = "master_grocery_ingredients_list")
@NamedQuery(name = "MasterGroceryIngredientsList.findAll", query = "SELECT i FROM MasterGroceryIngredientsList i")
public class MasterGroceryIngredientsList implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_INGREDIENT_ID")
	private BigInteger id;

	@Column(name = "FOOD_INGREDIENT_NAME")
	private String ingredientName;

	@Column(name = "FOOD_INGREDIENT_CODE")
	private String ingredientCode;

	@Column(name = "FOOD_INGREDIENTS_CATEGORY_KEY")
	private BigInteger ingredientsCategoryKey;

	@Column(name = "FOOD_INGREDIENT_MEASURE_TYPE")
	private String ingredientsMeasureType;

	public String getIngredientCode() {
		return ingredientCode;
	}

	public void setIngredientCode(String ingredientCode) {
		this.ingredientCode = ingredientCode;
	}

	public String getIngredientsMeasureType() {
		return ingredientsMeasureType;
	}

	public void setIngredientsMeasureType(String ingredientsMeasureType) {
		this.ingredientsMeasureType = ingredientsMeasureType;
	}

	public MasterGroceryIngredientsList() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getIngredientName() {
		return this.ingredientName;
	}

	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}

	public BigInteger getIngredientsCategoryKey() {
		return this.ingredientsCategoryKey;
	}

	public void setIngredientsCategoryKey(BigInteger ingredientsCategoryKey) {
		this.ingredientsCategoryKey = ingredientsCategoryKey;
	}

}