package com.monsor.feasthub.jpa.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;

/**
 * The persistent class for the external_resource_endpoints database table.
 * 
 */
@Entity
@Table(name = "external_resource_endpoints")
@NamedQuery(name = "ExternalResourceEndpoint.findAll", query = "SELECT e FROM ExternalResourceEndpoint e")
public class ExternalResourceEndpoint implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "EXTERNAL_RESOURCE_ENDPOINT_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "EXTERNAL_SERVER_NAME")
	private String externalServerName;

	@Column(name = "RESOURCE_ENDPOINT_URL")
	private String resourceEndpointUrl;

	@Column(name = "RESOURCE_NAME")
	private String resourceName;

	private String scopes;

	public ExternalResourceEndpoint() {
	}

	public String getExternalServerName() {
		return this.externalServerName;
	}

	public void setExternalServerName(String externalServerName) {
		this.externalServerName = externalServerName;
	}

	public String getResourceEndpointUrl() {
		return this.resourceEndpointUrl;
	}

	public void setResourceEndpointUrl(String resourceEndpointUrl) {
		this.resourceEndpointUrl = resourceEndpointUrl;
	}

	public String getResourceName() {
		return this.resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getScopes() {
		return this.scopes;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

}