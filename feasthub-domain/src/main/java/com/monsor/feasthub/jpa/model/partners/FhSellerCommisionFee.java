package com.monsor.feasthub.jpa.model.partners;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the fh_seller_commision_fees database table.
 * 
 */
@Entity
@Table(name = "fh_seller_commision_fees")
@NamedQuery(name = "FhSellerCommisionFee.findAll", query = "SELECT f FROM FhSellerCommisionFee f")
public class FhSellerCommisionFee implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SELLER_COMMISION_FEE_ID")
	private BigInteger id;

	@Column(name = "BUSINESS_TYPES")
	private String businessTypes;

	@Column(name = "FEES_APPLCABILITY")
	private String feesApplcability;

	@Column(name = "FEES_TYPE")
	private String feesType;

	@Column(name = "FH_SELLER_COMMISION_FEES_ID")
	private BigInteger fhSellerCommisionFeesId;

	@Column(name = "MEMBERSHIP_TYPE_ID")
	private String membershipTypeId;

	@Column(name = "PERCENTAGE_CHARGE")
	private double percentageCharge;

	@Column(name = "SERVICES_OFFERING_TYPES")
	private String servicesOfferingTypes;

	public FhSellerCommisionFee() {
	}

	public String getBusinessTypes() {
		return this.businessTypes;
	}

	public void setBusinessTypes(String businessTypes) {
		this.businessTypes = businessTypes;
	}

	public String getFeesApplcability() {
		return this.feesApplcability;
	}

	public void setFeesApplcability(String feesApplcability) {
		this.feesApplcability = feesApplcability;
	}

	public String getFeesType() {
		return this.feesType;
	}

	public void setFeesType(String feesType) {
		this.feesType = feesType;
	}

	public BigInteger getFhSellerCommisionFeesId() {
		return this.fhSellerCommisionFeesId;
	}

	public void setFhSellerCommisionFeesId(BigInteger fhSellerCommisionFeesId) {
		this.fhSellerCommisionFeesId = fhSellerCommisionFeesId;
	}

	public String getMembershipTypeId() {
		return this.membershipTypeId;
	}

	public void setMembershipTypeId(String membershipTypeId) {
		this.membershipTypeId = membershipTypeId;
	}

	public double getPercentageCharge() {
		return this.percentageCharge;
	}

	public void setPercentageCharge(double percentageCharge) {
		this.percentageCharge = percentageCharge;
	}

	public String getServicesOfferingTypes() {
		return this.servicesOfferingTypes;
	}

	public void setServicesOfferingTypes(String servicesOfferingTypes) {
		this.servicesOfferingTypes = servicesOfferingTypes;
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}