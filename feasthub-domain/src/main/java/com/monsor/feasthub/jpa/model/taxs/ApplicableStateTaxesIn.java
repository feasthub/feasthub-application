package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the applicable_state_taxes_in database table.
 * 
 */
@Entity
@Table(name = "applicable_state_taxes_in")
@NamedQueries({ @NamedQuery(name = "ApplicableStateTaxesIn.findAll", query = "SELECT a FROM ApplicableStateTaxesIn a"),

		@NamedQuery(name = "ApplicableStateTaxesIn.searchApplicableStateTaxesInsIgnoreCase", query = "SELECT f FROM ApplicableStateTaxesIn f where "
				+ " f.countryCode = :searchString or" + " f.stateCode = :searchString or"
				+ " f.stateTaxRate = :searchString or" + " f.stateTaxType = :searchString "),
		@NamedQuery(name = "ApplicableStateTaxesIn.findDuplicate", query = "SELECT f FROM ApplicableStateTaxesIn f "
				+ "WHERE " + " f.countryCode = :countryCode and" + " f.stateCode = :stateCode and"
				+ " f.stateTaxRate = :stateTaxRate and" + " f.stateTaxType = :stateTaxType ") })
public class ApplicableStateTaxesIn implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAX_RATES_ID")
	private BigInteger id;

	@Required
	@CheckForDuplicate
	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Required
	@CheckForDuplicate
	@Column(name = "STATE_CODE")
	private String stateCode;

	@Column(name = "STATE_TAX_APPLICABILITY")
	private String stateTaxApplicability;

	@Required
	@CheckForDuplicate
	@Column(name = "STATE_TAX_RATE")
	private double stateTaxRate;

	@Required
	@CheckForDuplicate
	@Column(name = "STATE_TAX_TYPE")
	private String stateTaxType;

	public ApplicableStateTaxesIn() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateTaxApplicability() {
		return this.stateTaxApplicability;
	}

	public void setStateTaxApplicability(String stateTaxApplicability) {
		this.stateTaxApplicability = stateTaxApplicability;
	}

	public double getStateTaxRate() {
		return this.stateTaxRate;
	}

	public void setStateTaxRate(double stateTaxRate) {
		this.stateTaxRate = stateTaxRate;
	}

	public String getStateTaxType() {
		return this.stateTaxType;
	}

	public void setStateTaxType(String stateTaxType) {
		this.stateTaxType = stateTaxType;
	}

}