/**
 * 
 */
package com.monsor.feasthub.jpa.model.payment;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * @author Amit Sharma
 *
 */
@Entity
@Table(name = "payment_gateway")
@NamedQuery(name = "PaymentGateway.findAll", query = "SELECT f FROM PaymentGateway f")
public class PaymentGateway  implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name = "id")
	private BigInteger id;

	@Column(name="pg_name")
	private String name;
	
	

	@Column(name="app_key")
	private String appKey;
	
	@Column(name="app_secret")
	private String appSecret;
	
	@JsonIgnore
	@Column(name="DELIVERY_LOCATION_ID")
	private BigInteger deliveryLocationId;
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	
	public BigInteger getDeliveryLocationId() {
	
	    return deliveryLocationId;
	}

	
	public void setDeliveryLocationId(BigInteger deliveryLocationId) {
	
	    this.deliveryLocationId = deliveryLocationId;
	}
}
