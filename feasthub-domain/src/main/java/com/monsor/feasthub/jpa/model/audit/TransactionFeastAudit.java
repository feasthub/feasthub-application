package com.monsor.feasthub.jpa.model.audit;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.enums.TransactionMode;
import com.monsor.feasthub.jpa.model.enums.TransactionPurpose;

/**
 * The persistent class for the transaction_feast_audit database table.
 * 
 */
@Entity
@Table(name = "transaction_feast_audit")
@NamedQuery(name = "TransactionFeastAudit.findAll", query = "SELECT t FROM TransactionFeastAudit t")
public class TransactionFeastAudit implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FEAST_AUDIT_ID")
	private BigInteger id;

	@Column(name = "FP_CREDIT_USER_ID")
	private BigInteger fpCreditUserId;

	@Column(name = "FP_DEBIT_USER_ID")
	private BigInteger fpDebitUserId;

	@Column(name = "FP_POINTS")
	private long fpPoints;

	@Column(name = "FP_TYPE")
	private String fpType;

	@Column(name = "ORDER_TRANSACTION_ID")
	private BigInteger orderTransactionId;

	@Temporal(TemporalType.DATE)
	@Column(name = "TRANSACTION_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date transactionDate;

	@Column(name = "TRANSACTION_PURPOSE")
	private TransactionPurpose transactionPurpose;

	@Column(name = "TRANSACTION_TIME")
	private Time transactionTime;

	@Column(name = "TRANSACTION_TYPE")
	private TransactionMode transactionType;

	public TransactionFeastAudit() {
	}

	public BigInteger getFpCreditUserId() {
		return this.fpCreditUserId;
	}

	public void setFpCreditUserId(BigInteger fpCreditUserId) {
		this.fpCreditUserId = fpCreditUserId;
	}

	public BigInteger getFpDebitUserId() {
		return this.fpDebitUserId;
	}

	public void setFpDebitUserId(BigInteger fpDebitUserId) {
		this.fpDebitUserId = fpDebitUserId;
	}

	public long getFpPoints() {
		return this.fpPoints;
	}

	public void setFpPoints(long fpPoints) {
		this.fpPoints = fpPoints;
	}

	public String getFpType() {
		return this.fpType;
	}

	public void setFpType(String fpType) {
		this.fpType = fpType;
	}

	public BigInteger getOrderTransactionId() {
		return this.orderTransactionId;
	}

	public void setOrderTransactionId(BigInteger orderTransactionId) {
		this.orderTransactionId = orderTransactionId;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Time getTransactionTime() {
		return this.transactionTime;
	}

	public void setTransactionTime(Time transactionTime) {
		this.transactionTime = transactionTime;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public void setTransactionPurpose(TransactionPurpose transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}

	public void setTransactionType(TransactionMode transactionType) {
		this.transactionType = transactionType;
	}

}