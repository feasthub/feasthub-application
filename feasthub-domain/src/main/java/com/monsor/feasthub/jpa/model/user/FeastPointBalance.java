package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;

/**
 * The persistent class for the feast_point_balance database table.
 * 
 */
@Entity
@Table(name = "feast_point_balance")

public class FeastPointBalance implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "FEAST_POINT_BALANCE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "FP_BALANCE")
	private BigInteger fpBalance;

	@Column(name = "FP_TYPE")
	private String fpType;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public FeastPointBalance() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFpBalance() {
		return this.fpBalance;
	}

	public void setFpBalance(BigInteger fpBalance) {
		this.fpBalance = fpBalance;
	}

	public String getFpType() {
		return this.fpType;
	}

	public void setFpType(String fpType) {
		this.fpType = fpType;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}