package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_receipe database table.
 * 
 */
@Entity
@Table(name = "food_menu_receipe")
@NamedQueries({
		@NamedQuery(name = "FoodMenuReceipe.searchFoodMenuReceipesIgnoreCase", query = "SELECT f FROM FoodMenuReceipe f "
				+ "WHERE " + " f.foodCategoryId = :searchString " + " or "
				+ " f.primaryIngredientsList = :searchString "),

		@NamedQuery(name = "findByfoodCategoryId", query = "SELECT f FROM FoodMenuReceipe f " + "WHERE "
				+ " f.foodCategoryId = :foodCategoryId ") })
public class FoodMenuReceipe implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FOOD_RECEIPE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "`FOOD_CATEGORY_ID`")
	private BigInteger foodCategoryId;

	@Column(name = "`PRIMARY_INGREDIENTS_LIST`")
	private String primaryIngredientsList;

	@Column(name = "RECEIPE_DES1")
	private String receipeDes1;

	@Column(name = "RECEIPE_DES2")
	private String receipeDes2;

	@Column(name = "RECEIPE_DOC_FILE")
	private String receipeDocFile;

	@Column(name = "RECEIPE_URL1")
	private String receipeUrl1;

	@Column(name = "RECEIPE_URL2")
	private String receipeUrl2;

	@Column(name = "SECONDARY_INGREDIENT_LIST")
	private String secondaryIngredientList;

	@Column(name = "SPICES_LIST")
	private String spicesList;

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "FOOD_RECEIPE_ID")
	private List<FoodRawMaterial> foodRawMaterials;

	/*
	 * @OneToOne(fetch = FetchType.EAGER)
	 * 
	 * @JoinTable(name = "food_ingredients", joinColumns = @JoinColumn(name =
	 * "FOOD_RECEIPE_ID") , inverseJoinColumns = @JoinColumn(name =
	 * "FOOD_INGREDIENT_ID") ) private FoodIngredient foodIngredient;
	 * 
	 * public FoodIngredient getFoodIngredient() { return foodIngredient; }
	 * 
	 * public void setFoodIngredient(FoodIngredient foodIngredient) {
	 * this.foodIngredient = foodIngredient; }
	 */
	public FoodMenuReceipe() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFoodCategoryId() {
		return this.foodCategoryId;
	}

	public void setFoodCategoryId(BigInteger foodCategoryId) {
		this.foodCategoryId = foodCategoryId;
	}

	public String getPrimaryIngredientsList() {
		return this.primaryIngredientsList;
	}

	public void setPrimaryIngredientsList(String primaryIngredientsList) {
		this.primaryIngredientsList = primaryIngredientsList;
	}

	public String getReceipeDes1() {
		return this.receipeDes1;
	}

	public void setReceipeDes1(String receipeDes1) {
		this.receipeDes1 = receipeDes1;
	}

	public String getReceipeDes2() {
		return this.receipeDes2;
	}

	public void setReceipeDes2(String receipeDes2) {
		this.receipeDes2 = receipeDes2;
	}

	public String getReceipeDocFile() {
		return this.receipeDocFile;
	}

	public void setReceipeDocFile(String receipeDocFile) {
		this.receipeDocFile = receipeDocFile;
	}

	public String getReceipeUrl1() {
		return this.receipeUrl1;
	}

	public void setReceipeUrl1(String receipeUrl1) {
		this.receipeUrl1 = receipeUrl1;
	}

	public String getReceipeUrl2() {
		return this.receipeUrl2;
	}

	public void setReceipeUrl2(String receipeUrl2) {
		this.receipeUrl2 = receipeUrl2;
	}

	public String getSecondaryIngredientList() {
		return this.secondaryIngredientList;
	}

	public void setSecondaryIngredientList(String secondaryIngredientList) {
		this.secondaryIngredientList = secondaryIngredientList;
	}

	public String getSpicesList() {
		return this.spicesList;
	}

	public void setSpicesList(String spicesList) {
		this.spicesList = spicesList;
	}

	public List<FoodRawMaterial> getFoodRowMaterials() {
		return foodRawMaterials;
	}

	public void setFoodRowMaterials(List<FoodRawMaterial> foodRawMaterials) {
		this.foodRawMaterials = foodRawMaterials;
	}

}