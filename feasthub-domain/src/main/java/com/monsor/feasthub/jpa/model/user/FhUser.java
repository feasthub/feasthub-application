package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.Gender;
import com.monsor.feasthub.jpa.model.enums.UserType;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the fh_user database table.
 * 
 */
@Entity
@Table(name = "fh_user")
@NamedQueries({ @NamedQuery(name = "FhUser.findAll", query = "SELECT f FROM FhUser f") })
public class FhUser implements Serializable, Identifiable {
    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_OF_BIRTH_INCORPORTAION")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+5:30")
    private Date dateOfBirthIncorportaion;

    @Column(name = "DEFAULT_EMAIL_ID")
    private String defaultEmailId;

    @Column(name = "DEFAULT_LANDLINE_NUMBER")
    private String defaultLandlineNumber;

    @Column(name = "DEFAULT_MOBILE_NUMBER")
    private String defaultMobileNumber;

    @Searchable
    @Column(name = "EMAIL_VERIFIED_FLAG")
    private Boolean emailVerifiedFlag;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "GENDER")
    @Enumerated
    private Gender gender;

    @Column(name = "LANGUAGE_CODE")
    private String languageCode;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "MOBILE_VERIFIED_FLAG")
    private Boolean mobileVerifiedFlag;

    private String password;

    @Column(name = "PRIMARY_ROLE")
    private String primaryRole;

    @Column(name = "PROFILE_PHOTO")
    private String profilePhoto;

    @Column(name = "REFERRAL_ID")
    private BigInteger referralId;

    @Column(name = "REGISTERED_COUNTRY_CODE")
    private String registeredCountryCode;

    private String salutation;

    @Column(name = "SUBSCRIPTION_FLAG")
    private Boolean subscriptionFlag;

    @Column(name = "USER_AUTH_METHOD")
    private String userAuthMethod;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "USER_NAME")
    private String userName;

    @Enumerated
    @Column(name = "USER_TYPE")
    private UserType userType;
    
    @Column(name = "DEVICE_ID")
    private String deviceId;

    @Column(name = "DEVICE_MAKE")
    private String deviceMake;

    public FhUser() {
    }

    public FhUser(String userName, String defaultEmailId, String defaultMobileNumber,
	    String password, Boolean mobileVerified) {
	super();
	this.defaultEmailId = defaultEmailId;
	this.defaultMobileNumber = defaultMobileNumber;
	this.password = password;
	this.userName = userName;
	this.mobileVerifiedFlag = mobileVerified;
    }

    public Date getDateOfBirthIncorportaion() {
	return this.dateOfBirthIncorportaion;
    }

    public void setDateOfBirthIncorportaion(Date dateOfBirthIncorportaion) {
	this.dateOfBirthIncorportaion = dateOfBirthIncorportaion;
    }

    public String getDefaultEmailId() {
	return this.defaultEmailId;
    }

    public void setDefaultEmailId(String defaultEmailId) {
	this.defaultEmailId = defaultEmailId;
    }

    public String getDefaultLandlineNumber() {
	return this.defaultLandlineNumber;
    }

    public void setDefaultLandlineNumber(String defaultLandlineNumber) {
	this.defaultLandlineNumber = defaultLandlineNumber;
    }

    public String getDefaultMobileNumber() {
	return this.defaultMobileNumber;
    }

    public void setDefaultMobileNumber(String defaultMobileNumber) {
	this.defaultMobileNumber = defaultMobileNumber;
    }

    public Boolean getEmailVerifiedFlag() {
	return emailVerifiedFlag;
    }

    public void setEmailVerifiedFlag(Boolean emailVerifiedFlag) {
	this.emailVerifiedFlag = emailVerifiedFlag;
    }

    public Boolean getSubscriptionFlag() {
	return subscriptionFlag;
    }

    public void setSubscriptionFlag(Boolean subscriptionFlag) {
	this.subscriptionFlag = subscriptionFlag;
    }

    public void setMobileVerifiedFlag(Boolean mobileVerifiedFlag) {
	this.mobileVerifiedFlag = mobileVerifiedFlag;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public Gender getGender() {
	return this.gender;
    }

    public void setGender(Gender gender) {
	this.gender = gender;
    }

    public String getLanguageCode() {
	return this.languageCode;
    }

    public void setLanguageCode(String languageCode) {
	this.languageCode = languageCode;
    }

    public String getLastName() {
	return this.lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getMiddleName() {
	return this.middleName;
    }

    public void setMiddleName(String middleName) {
	this.middleName = middleName;
    }

    public Boolean getMobileVerifiedFlag() {
	return mobileVerifiedFlag;
    }

    public String getPassword() {
	return this.password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getPrimaryRole() {
	return this.primaryRole;
    }

    public void setPrimaryRole(String primaryRole) {
	this.primaryRole = primaryRole;
    }

    public String getProfilePhoto() {
	return this.profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
	this.profilePhoto = profilePhoto;
    }

    public BigInteger getReferralId() {
	return this.referralId;
    }

    public void setReferralId(BigInteger referralId) {
	this.referralId = referralId;
    }

    public String getRegisteredCountryCode() {
	return this.registeredCountryCode;
    }

    public void setRegisteredCountryCode(String registeredCountryCode) {
	this.registeredCountryCode = registeredCountryCode;
    }

    public String getSalutation() {
	return this.salutation;
    }

    public void setSalutation(String salutation) {
	this.salutation = salutation;
    }

    public String getUserAuthMethod() {
	return this.userAuthMethod;
    }

    public void setUserAuthMethod(String userAuthMethod) {
	this.userAuthMethod = userAuthMethod;
    }

    @Override
    public BigInteger getId() {
	return id;
    }

    @Override
    public void setId(BigInteger id) {
	this.id = id;
    }

    public String getUserName() {
	return this.userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public UserType getUserType() {
	return this.userType;
    }

    public void setUserType(UserType userType) {
	this.userType = userType;
    }

    public String getName() {
	
	return String.format("%s, %s", this.firstName==null?"":firstName, this.lastName==null?"":lastName);
    }

    public String getCompanyName() {
	return companyName;
    }

    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }

    
    public String getDeviceId() {
    
        return deviceId;
    }

    
    public void setDeviceId(String deviceId) {
    
        this.deviceId = deviceId;
    }

    
    public String getDeviceMake() {
    
        return deviceMake;
    }

    
    public void setDeviceMake(String deviceMake) {
    
        this.deviceMake = deviceMake;
    }

}