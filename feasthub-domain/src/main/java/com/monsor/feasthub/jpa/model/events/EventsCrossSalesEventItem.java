package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.view.FhCrossEventView;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;

/**
 * The persistent class for the events_cross_sales_event_items database table.
 * 
 */
@Entity
@Table(name = "events_cross_sales_event_items")
@NamedQuery(name = "EventsCrossSalesEventItem.findAll", query = "SELECT e FROM EventsCrossSalesEventItem e")
public class EventsCrossSalesEventItem implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_CROSS_SALES_EVENT_ITEM_ID")
	private BigInteger id;

	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	@Column(name = "MAIN_EVENT_ID")
	private BigInteger mainEventId;

	@OneToOne
	@JoinColumn(name = "CROSS_SALES_EVENT_ID", referencedColumnName = "EVENT_ID")
	private FhCrossEventView fhCrossEvent;

	@OneToOne
	@JoinColumn(name = "CROSS_SALES_FOOD_MENU_ID", referencedColumnName = "FOOD_MENU_ID")
	private FoodMenuItemView foodMenuItemView;

	public EventsCrossSalesEventItem() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public BigInteger getMainEventId() {
		return this.mainEventId;
	}

	public void setMainEventId(BigInteger mainEventId) {
		this.mainEventId = mainEventId;
	}

	public FhCrossEventView getFhCrossEvent() {
		return fhCrossEvent;
	}

	public void setFhCrossEvent(FhCrossEventView fhCrossEvent) {
		this.fhCrossEvent = fhCrossEvent;
	}

	public FoodMenuItemView getFoodMenuItemView() {
		return foodMenuItemView;
	}

	public void setFoodMenuItemView(FoodMenuItemView foodMenuItemView) {
		this.foodMenuItemView = foodMenuItemView;
	}

}