package com.monsor.feasthub.jpa.model.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the order_customised_cusine database table.
 * 
 */
@Entity
@Table(name = "order_customised_cusine")
@NamedQuery(name = "OrderCustomisedCusine.findAll", query = "SELECT o FROM OrderCustomisedCusine o")
public class OrderCustomisedCusine implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_CUSTOMISED_CUSINE_ID")
	private BigInteger id;

	@Column(name = "CUSTOMISED_FOOD_MENU_ID")
	private BigInteger customisedFoodMenuId;

	@Column(name = "DEFAULT_FOOD_MENU_ID")
	private BigInteger defaultFoodMenuId;
	
	@Column(name = "DEFAULT_FOOD_MENU_NAME")
	private String defaultFoodMenuName;

	@Column(name = "CUSTOMISED_FOOD_MENU_NAME")
	private String customisedFoodMenuName;
	
	@Column(name = "ADDITIONAL_COST")
	private double additinalCost;

	@Column(name = "FOOD_SERVING_SIZE_CATEGORY_TYPE_ID")
	private BigInteger foodServingSizeCategoryId;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ORDER_EVENT_ID")
	private OrderEvent orderEventId;

	@Column(name = "ORDERED_QUANTITY")
	private int orderedQuantity;

	public OrderCustomisedCusine() {
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getCustomisedFoodMenuId() {
		return customisedFoodMenuId;
	}

	public void setCustomisedFoodMenuId(BigInteger customisedFoodMenuId) {
		this.customisedFoodMenuId = customisedFoodMenuId;
	}

	public BigInteger getDefaultFoodMenuId() {
		return defaultFoodMenuId;
	}

	public void setDefaultFoodMenuId(BigInteger defaultFoodMenuId) {
		this.defaultFoodMenuId = defaultFoodMenuId;
	}

	public BigInteger getFoodServingSizeCategoryId() {
		return foodServingSizeCategoryId;
	}

	public void setFoodServingSizeCategoryId(BigInteger fooeMenuServingCategoryId) {
		this.foodServingSizeCategoryId = fooeMenuServingCategoryId;
	}

	public int getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(int orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public OrderEvent getOrderEventId() {
		return orderEventId;
	}

	public void setOrderEventId(OrderEvent orderEventId) {
		this.orderEventId = orderEventId;
	}

	
	public String getDefaultFoodMenuName() {
	
	    return defaultFoodMenuName;
	}

	
	public void setDefaultFoodMenuName(String defaultFoodMenuName) {
	
	    this.defaultFoodMenuName = defaultFoodMenuName;
	}

	
	public String getCustomisedFoodMenuName() {
	
	    return customisedFoodMenuName;
	}

	
	public void setCustomisedFoodMenuName(String customisedFoodMenuName) {
	
	    this.customisedFoodMenuName = customisedFoodMenuName;
	}

	
	public double getAdditinalCost() {
	
	    return additinalCost;
	}

	
	public void setAdditinalCost(double additinalCost) {
	
	    this.additinalCost = additinalCost;
	}

}