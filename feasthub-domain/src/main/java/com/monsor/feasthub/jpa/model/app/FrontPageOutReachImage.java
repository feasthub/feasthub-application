/**
 * 
 */
package com.monsor.feasthub.jpa.model.app;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * @author Amit Sharma
 *
 */
@Entity
@Table(name="front_page_outreach_images")
@NamedQuery(name = "FrontPageOutReachImage.findAll", query = "SELECT f FROM FrontPageOutReachImage f")
public class FrontPageOutReachImage implements Identifiable {

    /**
     * 
     */
    private static final long serialVersionUID = 5181449752166251525L;
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;
    
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="front_page_outreach_id")
    private FrontPageOutReachMode frontPageOutreachId;
    
    @Column(name="image_loc")
    private String imageLoc;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public FrontPageOutReachMode getFrontPageOutreachId() {
        return frontPageOutreachId;
    }

    public void setFrontPageOutreachId(FrontPageOutReachMode frontPageOutreachId) {
        this.frontPageOutreachId = frontPageOutreachId;
    }

    public String getImageLoc() {
        return imageLoc;
    }

    public void setImageLoc(String imageLoc) {
        this.imageLoc = imageLoc;
    }
}
