package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the food_menu_items database table.
 * 
 */
@Entity
@Table(name = "food_menu_items")
@NamedQueries({
		@NamedQuery(name = "FoodMenuItem.searchByFoodMenuItemsIgnoreCaseContaining", query = "SELECT f FROM FoodMenuItem f "
				+ "WHERE "
				+ " f.foodName like :searchString or f.foodShortDescription like :searchString or f.foodLongDescription like :searchString"),

		@NamedQuery(name = "findByFoodName", query = "SELECT f FROM FoodMenuItem f " + "WHERE "
				+ " f.foodName = :foodName ") })
public class FoodMenuItem implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_MENU_ID")
	private BigInteger id;

	@Searchable
	@CheckForDuplicate
	@Column(name = "FOOD_NAME")
	private String foodName;

	@Column(name = "PACKAGED_ITEM_FLAG")
	private Boolean packagedItemFlag;

	@Column(name = "FOOD_LONG_DESCRIPTION")
	private String foodLongDescription;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "FOOD_RECEIPE_ID")
	private FoodMenuReceipe foodMenuReceipe;

	@Column(name = "FOOD_SHORT_DESCRIPTION")
	private String foodShortDescription;

	@Column(name = "IMAGE1_URL")
	private String image1Url;

	@Column(name = "IMAGE2_URL")
	private String image2Url;

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "foodMenuId")
	private List<PackagedFoodMenuItemsList> packagedFoodMenuItemsList;
	/*
	 * @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade =
	 * CascadeType.ALL)
	 * 
	 * @JoinColumn(name = "FOOD_MENU_ID") private List<AdditionalFoodItemsList>
	 * additionalFoodItemsList;
	 * 
	 */

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "FOOD_MENU_ID")
	private List<FoodMenuCategoryMapping> foodMenuCategoryMappings;

	public FoodMenuItem() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Boolean getPackagedItemFlag() {
		return packagedItemFlag;
	}

	public void setPackagedItemFlag(Boolean packagedItemFlag) {
		this.packagedItemFlag = packagedItemFlag;
	}

	public String getFoodLongDescription() {
		return foodLongDescription;
	}

	public void setFoodLongDescription(String foodLongDescription) {
		this.foodLongDescription = foodLongDescription;
	}

	public FoodMenuReceipe getFoodMenuReceipe() {
		return foodMenuReceipe;
	}

	public void setFoodMenuReceipe(FoodMenuReceipe foodMenuReceipe) {
		this.foodMenuReceipe = foodMenuReceipe;
	}

	public String getFoodShortDescription() {
		return foodShortDescription;
	}

	public void setFoodShortDescription(String foodShortDescription) {
		this.foodShortDescription = foodShortDescription;
	}

	public String getImage1Url() {
		return image1Url;
	}

	public void setImage1Url(String image1Url) {
		this.image1Url = image1Url;
	}

	public String getImage2Url() {
		return image2Url;
	}

	public void setImage2Url(String image2Url) {
		this.image2Url = image2Url;
	}

	public List<FoodMenuCategoryMapping> getFoodMenuCategoryMappings() {
		return foodMenuCategoryMappings;
	}

	public void setFoodMenuCategoryMappings(List<FoodMenuCategoryMapping> foodMenuCategoryMappings) {
		this.foodMenuCategoryMappings = foodMenuCategoryMappings;
	}

	public List<PackagedFoodMenuItemsList> getPackagedFoodMenuItemsList() {
		return packagedFoodMenuItemsList;
	}

	public void setPackagedFoodMenuItemsList(List<PackagedFoodMenuItemsList> packagedFoodMenuItemsList) {
		this.packagedFoodMenuItemsList = packagedFoodMenuItemsList;
	}

}