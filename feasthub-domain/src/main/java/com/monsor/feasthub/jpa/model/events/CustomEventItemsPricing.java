package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the custom_event_items_pricing database table.
 * 
 */
@Entity
@Table(name = "custom_event_items_pricing")
@NamedQuery(name = "CustomEventItemsPricing.findAll", query = "SELECT c FROM CustomEventItemsPricing c")
public class CustomEventItemsPricing implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CUSTOM_EVENT_ITEMS_PRICING_ID")
	private BigInteger id;

	public BigInteger getEventId() {
		return eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	@Column(name = "TOP_UP_PRICE")
	private double topUpPrice;

	public CustomEventItemsPricing() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public double getTopUpPrice() {
		return this.topUpPrice;
	}

	public void setTopUpPrice(double topUpPrice) {
		this.topUpPrice = topUpPrice;
	}

}