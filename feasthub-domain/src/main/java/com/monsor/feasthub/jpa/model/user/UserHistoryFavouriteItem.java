package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.food.FoodMenuItem;

/**
 * The persistent class for the user_history_favourite_items database table.
 * 
 */
@Entity
@Table(name = "user_history_favourite_items")
@NamedQueries({
		@NamedQuery(name = "UserHistoryFavouriteItem.searchUserHistoryFavouriteItemIgnoreCase", query = "SELECT f FROM UserHistoryFavouriteItem f "
				+ "WHERE " + " f.favoriteFlag = :searchString "),
		@NamedQuery(name = "findHistoryyByUserId", query = "SELECT f FROM UserHistoryFavouriteItem f " + "WHERE "
				+ " f.userId = :userId "),
		@NamedQuery(name = "findFavByUserId", query = "SELECT f FROM UserHistoryFavouriteItem f " + "WHERE "
				+ " f.userId = :userId and f.favoriteFlag =:favoriteFlag") })
public class UserHistoryFavouriteItem implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "USER_HISTORY_FAV_ITEM_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	/*
	 * @ObjectTypeConverter(name = "gender", objectType = BooleanFlag.class,
	 * dataType = String.class, conversionValues = {
	 * 
	 * @ConversionValue(objectValue = "Yes", dataValue = "1"),
	 * 
	 * @ConversionValue(objectValue = "No", dataValue = "0") })
	 */

	@Column(name = "FAVORITE_FLAG")
	private Boolean favoriteFlag;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FOOD_MENU_ID")
	private FoodMenuItem foodMenuItem;

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_ORDER_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date lastOrderDate;

	@Column(name = "NO_OF_TIMES_ORDERED")
	private int noOfTimesOrdered;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public UserHistoryFavouriteItem() {
	}

	public UserHistoryFavouriteItem(BigInteger userId, FoodMenuItem foodMenuItem, Boolean favFlag) {
		super();
		this.userId = userId;
		this.foodMenuItem = foodMenuItem;
		this.favoriteFlag = favFlag;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public Boolean getFavoriteFlag() {
		return favoriteFlag;
	}

	public void setFavoriteFlag(Boolean favoriteFlag) {
		this.favoriteFlag = favoriteFlag;
	}

	public FoodMenuItem getFoodMenuItem() {
		return foodMenuItem;
	}

	public void setFoodMenuItem(FoodMenuItem foodMenuItem) {
		this.foodMenuItem = foodMenuItem;
	}

	public Date getLastOrderDate() {
		return this.lastOrderDate;
	}

	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}

	public int getNoOfTimesOrdered() {
		return this.noOfTimesOrdered;
	}

	public void setNoOfTimesOrdered(int noOfTimesOrdered) {
		this.noOfTimesOrdered = noOfTimesOrdered;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}