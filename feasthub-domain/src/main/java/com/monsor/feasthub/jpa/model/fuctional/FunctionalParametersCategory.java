package com.monsor.feasthub.jpa.model.fuctional;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the functional_parameters_categories database table.
 * 
 */
@Entity
@Table(name = "functional_parameters_categories")
@NamedQuery(name = "FunctionalParametersCategory.findAll", query = "SELECT f FROM FunctionalParametersCategory f")
public class FunctionalParametersCategory implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CATEGORY_ID")
	private BigInteger id;

	@Column(name = "CATEGORY_NAME")
	private String categoryName;

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "CATEGORY_ID")
	private List<FunctionalParameter> functionalParameters;

	public FunctionalParametersCategory() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<FunctionalParameter> getFunctionalParameters() {
		return functionalParameters;
	}

	public void setFunctionalParameters(List<FunctionalParameter> functionalParameters) {
		this.functionalParameters = functionalParameters;
	}

}