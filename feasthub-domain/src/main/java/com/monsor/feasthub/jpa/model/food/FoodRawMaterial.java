package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the food_raw_materials database table.
 * 
 */
@Entity
@Table(name = "food_raw_materials")
@NamedQuery(name = "FoodRawMaterial.findAll", query = "SELECT f FROM FoodRawMaterial f")
public class FoodRawMaterial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FOOD_RAW_MATERIAL_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Required
	@Column(name = "FOOD_RECEIPE_ID")
	private BigInteger foodReceipeId;

	@Required
	@Column(name = "FOOD_INGREDIENT_ID")
	private BigInteger foodIngredientId;

	@Column(name = "MEASURE_UNITS")
	private String measureUnits;

	@Required
	@Column(name = "QUANTITY")
	private String quantity;

	@Column(name = "PRIMARY_INGREDIENT_FLAG")
	private String primaryIngredientFlag;

	public FoodRawMaterial() {
	}

	public BigInteger getFoodReceipeId() {
		return this.foodReceipeId;
	}

	public void setFoodReceipeId(BigInteger foodReceipeId) {
		this.foodReceipeId = foodReceipeId;
	}

	public BigInteger getFoodIngredientId() {
		return this.foodIngredientId;
	}

	public void setFoodIngredientId(BigInteger ingredientId) {
		this.foodIngredientId = ingredientId;
	}

	public String getMeasureUnits() {
		return this.measureUnits;
	}

	public void setMeasureUnits(String measureUnits) {
		this.measureUnits = measureUnits;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}