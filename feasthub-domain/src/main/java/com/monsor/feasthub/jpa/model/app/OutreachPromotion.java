package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the CURRENT_PROMOTIONS database table.
 * 
 */
@Entity
@Table(name = "outreach_promotions")
@NamedQueries({ @NamedQuery(name = "OutreachPromotion.findAll", query = "SELECT c FROM OutreachPromotion c"),
		@NamedQuery(name = "OutreachPromotion.searchOutreachPromotionsIgnoreCase", query = "SELECT f FROM OutreachPromotion f "
				+ "WHERE " + " f.promoCode = :searchString "),
		@NamedQuery(name = "OutreachPromotion.findDuplicate", query = "SELECT f FROM OutreachPromotion f " + "WHERE "
				+ " f.promoCode = :promoCode ") })
public class OutreachPromotion implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "OUTREACH_PROMOTION_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Required
	@CheckForDuplicate
	@Searchable
	@Column(name = "PROMO_CODE")
	private String promoCode;

	@Column(name = "DELIVERY_LOCATION_ID")
	private BigInteger deliveryLocationId;

	@Column(name = "PROMO_TEXT")
	private String promoText;
	
	@Column(name = "IS_HTML")
	private boolean isHtml;
	
	
	public OutreachPromotion() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public BigInteger getDeliveryLocationId() {
		return this.deliveryLocationId;
	}

	public void setDeliveryLocationId(BigInteger deliveryLocationId) {
		this.deliveryLocationId = deliveryLocationId;
	}

	
	public String getPromoText() {
	
	    return promoText;
	}

	
	public void setPromoText(String promoText) {
	
	    this.promoText = promoText;
	}

	

}