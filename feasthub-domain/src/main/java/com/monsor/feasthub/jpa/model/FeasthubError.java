package com.monsor.feasthub.jpa.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

@Entity
@Table(name = "feasthub_errors")
public class FeasthubError implements Serializable, Identifiable {

	public FeasthubError() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4008052377552711455L;

	@Id
	@Column(name = "ERROR_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Lob
	@Column(name = "ERROR_MESSAGE")
	private String errorMessage;

	@Lob
	@Column(name = "REQUEST_CONTENT")
	private String requestContent;

	@Lob
	@Column(name = "STACK_TRACE")
	private String stackTrace;

	@Column(name = "REQUEST_URL")
	private String requestUrl;

	@Override
	public BigInteger getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		// TODO Auto-generated method stub
		this.id = id;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public FeasthubError(String errorMessage, String requestContent, String stackTrace, String requestUrl) {
		super();
		this.errorMessage = errorMessage;
		this.requestContent = requestContent;
		this.stackTrace = stackTrace;
		this.requestUrl = requestUrl;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public String getRequestContent() {
		return requestContent;
	}

	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

}
