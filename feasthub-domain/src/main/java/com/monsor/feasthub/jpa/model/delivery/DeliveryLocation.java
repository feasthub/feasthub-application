package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the delivery_locations database table.
 * 
 */
@Entity
@Table(name = "delivery_location")
@NamedQueries({ @NamedQuery(name = "DeliveryLocation.findAll", query = "SELECT d FROM DeliveryLocation d"),
		@NamedQuery(name = "DeliveryLocation.findDuplicate", query = "SELECT f FROM DeliveryLocation f " + "WHERE "
				+ " f.areaName = :areaName and " + " f.cityCode = :cityCode and " + " f.countryCode = :countryCode and"
				+ " f.postalCode = :postalCode and" + " f.stateCode = :stateCode ") })

public class DeliveryLocation implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DELIVERY_LOCATION_ID")
	private BigInteger id;

	@Required
	@CheckForDuplicate
	@Column(name = "AREA_NAME")
	private String areaName;

	@Required
	@CheckForDuplicate
	@Column(name = "CITY_CODE")
	private String cityCode;

	@Required
	@CheckForDuplicate
	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "DISTANCE_UPPER_RANGE_OF_COVERAGE_AREA")
	private int distanceUpperRangeOfCoverageArea;

	@Column(name = "LATTITUDE")
	private double lattitude;

	@Column(name = "LONGITUDE")
	private double longitude;

	@Required
	@CheckForDuplicate
	@Column(name = "POSTAL_CODE")
	private String postalCode;

	@Required
	@CheckForDuplicate
	@Column(name = "STATE_CODE")
	private String stateCode;

	public DeliveryLocation() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getDistanceUpperRangeOfCoverageArea() {
		return this.distanceUpperRangeOfCoverageArea;
	}

	public void setDistanceUpperRangeOfCoverageArea(int distanceUpperRangeOfCoverageArea) {
		this.distanceUpperRangeOfCoverageArea = distanceUpperRangeOfCoverageArea;
	}

	public double getLattitude() {
		return this.lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

}