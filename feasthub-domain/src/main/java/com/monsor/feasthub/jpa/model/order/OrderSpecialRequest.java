package com.monsor.feasthub.jpa.model.order;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the order_special_request database table.
 * 
 */
@Entity
@Table(name = "order_special_request")
@NamedQuery(name = "OrderSpecialRequest.findAll", query = "SELECT o FROM OrderSpecialRequest o")
public class OrderSpecialRequest implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_SPECIAL_REQUEST_ID")
	private BigInteger id;

	@Column(name = "AMOUNT")
	private double amount;

	@Column(name = "MEASURING_UNITS")
	private String measuringUnits;

	@Column(name = "ORDER_ID")
	private String orderId;

	@Column(name = "QUANTITY")
	private String quantity;

	@Column(name = "REQUESTED_ITEM_NAME")
	private String requestedItemName;

	@Column(name = "ORDER_NOTE")
	private String orderNote;

	public OrderSpecialRequest() {
	}

	public String getOrderNote() {
		return orderNote;
	}

	public void setOrderNote(String orderNote) {
		this.orderNote = orderNote;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMeasuringUnits() {
		return this.measuringUnits;
	}

	public void setMeasuringUnits(String measuringUnits) {
		this.measuringUnits = measuringUnits;
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getRequestedItemName() {
		return this.requestedItemName;
	}

	public void setRequestedItemName(String requestedItemName) {
		this.requestedItemName = requestedItemName;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}