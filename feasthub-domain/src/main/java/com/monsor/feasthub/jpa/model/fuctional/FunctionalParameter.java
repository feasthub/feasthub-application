package com.monsor.feasthub.jpa.model.fuctional;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the functional_parameters database table.
 * 
 */
@Entity
@Table(name = "functional_parameters")
@NamedQueries({ @NamedQuery(name = "FunctionalParameter.findAll", query = "SELECT f FROM FunctionalParameter f"),
		@NamedQuery(name = "FunctionalParameter.findDuplicate", query = "SELECT f FROM FunctionalParameter f "
				+ "WHERE " + " f.functionalParametersKey = :functionalParametersKey ") })
public class FunctionalParameter implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FUNCTIONAL_PARAMETER_ID")
	private BigInteger id;

	@Required
	@Column(name = "CATEGORY_ID")
	private BigInteger categoryId;

	@Required
	@CheckForDuplicate
	@Column(name = "FUNCTIONAL_PARAMETERS_KEY")
	private String functionalParametersKey;

	@Column(name = "FUNCTIONAL_PARAMETERS_VALUE")
	private String functionalParametersValue;

	public FunctionalParameter() {
	}

	public BigInteger getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(BigInteger categoryId) {
		this.categoryId = categoryId;
	}

	public String getFunctionalParametersKey() {
		return this.functionalParametersKey;
	}

	public void setFunctionalParametersKey(String functionalParametersKey) {
		this.functionalParametersKey = functionalParametersKey;
	}

	public String getFunctionalParametersValue() {
		return this.functionalParametersValue;
	}

	public void setFunctionalParametersValue(String functionalParametersValue) {
		this.functionalParametersValue = functionalParametersValue;
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}