package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_serving_quantity database table.
 * 
 */
@Entity
@Table(name = "future_usage_not_used_now_food_menu_serving_quantity")
@NamedQuery(name = "FoodMenuServingQuantity.findAll", query = "SELECT f FROM FoodMenuServingQuantity f")
public class FoodMenuServingQuantity implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_SERVING_QUANTITY_ID")
	private BigInteger id;

	@Column(name = "ADDITIONAL_FOOD_MENU_ID")
	private BigInteger additionalFoodMenuId;

	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	@Column(name = "FOOD_SERVING_CATEGORY_KEY")
	private BigInteger foodServingCategoryKey;

	@Column(name = "MEASURE_UNITS")
	private String measureUnits;

	@Column(name = "QUANTITY")
	private String quantity;

	public FoodMenuServingQuantity() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getAdditionalFoodMenuId() {
		return this.additionalFoodMenuId;
	}

	public void setAdditionalFoodMenuId(BigInteger additionalFoodMenuId) {
		this.additionalFoodMenuId = additionalFoodMenuId;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public BigInteger getFoodServingCategoryKey() {
		return this.foodServingCategoryKey;
	}

	public void setFoodServingCategoryKey(BigInteger foodServingCategoryKey) {
		this.foodServingCategoryKey = foodServingCategoryKey;
	}

	public String getMeasureUnits() {
		return this.measureUnits;
	}

	public void setMeasureUnits(String measureUnits) {
		this.measureUnits = measureUnits;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

}