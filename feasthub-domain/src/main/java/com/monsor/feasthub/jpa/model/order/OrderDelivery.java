package com.monsor.feasthub.jpa.model.order;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.DeliveryStatus;

/**
 * The persistent class for the order_transaction_delivery database table.
 * 
 */
@Entity
@Table(name = "order_delivery")
@NamedQuery(name = "OrderDelivery.findAll", query = "SELECT o FROM OrderDelivery o")
public class OrderDelivery implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Column(name = "DELIVERY_BY")
	private String deliveryBy;

	@Temporal(TemporalType.DATE)
	@Column(name = "DELIVERY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date deliveryDate;

	@Column(name = "DELIVERY_SLOTS_ID")
	private BigInteger deliverySlotsId;

	@Column(name = "DELIVERY_SLOTS_TIME_ID")
	private BigInteger deliverySlotsTimeId;

	@Column(name = "DELIVERY_STATUS")
	private DeliveryStatus deliveryStatus;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "ORDER_TRANSACTION_ID")
	private OrderTransaction orderTransactionId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_DELIVERY_ID")
	private BigInteger id;

	public OrderDelivery() {
	}

	public String getDeliveryBy() {
		return this.deliveryBy;
	}

	public void setDeliveryBy(String deliveryBy) {
		this.deliveryBy = deliveryBy;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public BigInteger getDeliverySlotsId() {
		return this.deliverySlotsId;
	}

	public void setDeliverySlotsId(BigInteger deliverySlotsId) {
		this.deliverySlotsId = deliverySlotsId;
	}

	public BigInteger getDeliverySlotsTimeId() {
		return this.deliverySlotsTimeId;
	}

	public void setDeliverySlotsTimeId(BigInteger deliverySlotsTimeId) {
		this.deliverySlotsTimeId = deliverySlotsTimeId;
	}

	public DeliveryStatus getDeliveryStatus() {
		return this.deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public OrderTransaction getOrderTransactionId() {
		return orderTransactionId;
	}

	public void setOrderTransactionId(OrderTransaction orderTransactionId) {
		this.orderTransactionId = orderTransactionId;
	}

}