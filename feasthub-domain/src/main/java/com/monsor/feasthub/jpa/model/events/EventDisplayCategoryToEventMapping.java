package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the event_display_category_to_event_mapping database
 * table.
 * 
 */
@Entity
@Table(name = "event_display_category_to_event_mapping")
@NamedQuery(name = "EventDisplayCategoryToEventMapping.findAll", query = "SELECT e FROM EventDisplayCategoryToEventMapping e")
public class EventDisplayCategoryToEventMapping implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_DISPLAY_CATEGORY_TO_EVENT_MAPPING_ID")
	private BigInteger id;

	@JsonIgnore
	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@OneToOne
	@JoinColumn(name = "EVENT_DISPLAY_CATEGORY_ID", referencedColumnName = "EVENT_DISPLAY_CATEGORY_ID")
	private EventDisplayCategory displayCategory;

	public EventDisplayCategoryToEventMapping() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public EventDisplayCategory getDisplayCategory() {
		return displayCategory;
	}

	public void setDisplayCategory(EventDisplayCategory displayCategory) {
		this.displayCategory = displayCategory;
	}

	public BigInteger getEventId() {
		return this.eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

}