/**
 * 
 */
package com.monsor.feasthub.jpa.model.util;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * @author amit
 *
 */
@Entity
@Table(name = "order_id_sequence")
public class OrderIdSequence implements Identifiable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private BigInteger id;

	@Column(name = "ORDER_ID_PREFIX")
	private String orderIdPrefix;

	@Column(name = "SEQUENCE_NUMBER")
	private BigInteger sequenceNumber;

	public OrderIdSequence(String orderIdPrefix, BigInteger sequenceNumber) {
		super();
		this.orderIdPrefix = orderIdPrefix;
		this.sequenceNumber = sequenceNumber;
	}

	public OrderIdSequence() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getOrderIdPrefix() {
		return orderIdPrefix;
	}

	public void setOrderIdPrefix(String orderIdPrefix) {
		this.orderIdPrefix = orderIdPrefix;
	}

	public BigInteger getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(BigInteger sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

}
