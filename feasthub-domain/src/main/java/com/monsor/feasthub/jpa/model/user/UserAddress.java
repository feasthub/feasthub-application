package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.AddressType;

/**
 * The persistent class for the user_address database table.
 * 
 */
@Entity
@Table(name = "user_address")
@NamedQuery(name = "UserAddress.findAll", query = "SELECT u FROM UserAddress u")
public class UserAddress implements Serializable, Identifiable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ADDRESS_ID")
	private BigInteger id;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "CITY")
	private String city;

	@Column(name = "COUNTRY_CODE")
	private String countryCode = "IN";

	@Column(name = "DEFAULT_DELIVERY_ADDRESS_FLAG")
	private Boolean defaultDeliveryAddressFlag;

	@Column(name = "LANDMARK")
	private String landmark;

	@Column(name = "LATITUDE")
	private String latitude;

	@Column(name = "LONGITUDE")
	private String longitude;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PINCODE")
	private String pincode;

	@Column(name = "STATE_CODE")
	private String stateCode;

	@Column(name = "USER_ID")
	private BigInteger userId;

	@Column(name = "ZIPPR_CODE")
	private String zipprCode;

	@Enumerated
	@Column(name = "ADDRESS_TYPE")
	private AddressType addressType;

	public UserAddress() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public String getZipprCode() {
		return this.zipprCode;
	}

	public void setZipprCode(String zipprCode) {
		this.zipprCode = zipprCode;
	}

	public AddressType getAddressType() {
		return addressType;
	}

	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}

	public Boolean getDefaultDeliveryAddressFlag() {
		return defaultDeliveryAddressFlag;
	}

	public void setDefaultDeliveryAddressFlag(Boolean defaultDeliveryAddressFlag) {
		this.defaultDeliveryAddressFlag = defaultDeliveryAddressFlag;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}