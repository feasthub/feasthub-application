/**
 * 
 */
package com.monsor.feasthub.jpa.model.transaction;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;

@Entity
@Table(name = "OrderTransactionStatus")
@NamedQuery(name = "OrderTransactionStatus.findAll", query = "SELECT t FROM OrderTransactionStatus t")
public class OrderTransactionStatus implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7061864584817436334L;
	@Id
	@Column(name = "ORDER_TRANSACTION_STATUS_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "ORDER_TRANSACTION_ID")
	private BigInteger orderTransactionId;

	@Column(name = "TRANSACTION_ID")
	private BigInteger transactionId;

	@Column(name = "ORDER_ID")
	private String orderId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRANSACTION_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date transationDate;

	@Enumerated
	@Column(name = "PAYMENT_STATUS")
	private PaymentStatus paymentStatus;

	@Column(name = "STATUS_MESSAGE")
	private String statusMessage;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getOrderTransactionId() {
		return orderTransactionId;
	}

	public void setOrderTransactionId(BigInteger orderTransactionId) {
		this.orderTransactionId = orderTransactionId;
	}

	public BigInteger getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(BigInteger transactionId) {
		this.transactionId = transactionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getTransationDate() {
		return transationDate;
	}

	public void setTransationDate(Date transationDate) {
		this.transationDate = transationDate;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
