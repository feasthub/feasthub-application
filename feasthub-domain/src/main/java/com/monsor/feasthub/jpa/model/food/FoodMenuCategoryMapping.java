package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_menu_category_mapping database table.
 * 
 */
@Entity
@Table(name = "food_menu_category_mapping")
@NamedQuery(name = "FoodMenuCategoryMapping.findAll", query = "SELECT f FROM FoodMenuCategoryMapping f")
public class FoodMenuCategoryMapping implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FOOD_MENU_CATEGORY_MAPPING_ID")
	private BigInteger id;
	
	@JsonIgnore
	@Column(name = "ADDITIONAL_TAG_FLAG")
	private int additionalTagFlag;
	@JsonIgnore
	@Column(name = "FOOD_CATEGORY_PARENT_ID")
	private BigInteger foodCategoryParentId;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "FOOD_CATEGORY_ID")
	private FoodCategory foodCategory;

	@JsonIgnore
	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;
	
	@Column(name = "DISPLAY_FLAG")
	private Boolean displayFlag;
	

	public FoodMenuCategoryMapping() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public int getAdditionalTagFlag() {
		return this.additionalTagFlag;
	}

	public void setAdditionalTagFlag(int additionalTagFlag) {
		this.additionalTagFlag = additionalTagFlag;
	}

	public BigInteger getFoodCategoryParentId() {
		return this.foodCategoryParentId;
	}

	public void setFoodCategoryParentId(BigInteger foodCategoryParentId) {
		this.foodCategoryParentId = foodCategoryParentId;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public FoodCategory getFoodCategory() {
		return foodCategory;
	}

	public void setFoodCategory(FoodCategory foodCategory) {
		this.foodCategory = foodCategory;
	}

	
	public Boolean getDisplayFlag() {
	
	    return displayFlag;
	}

	
	public void setDisplayFlag(Boolean displayFlag) {
	
	    this.displayFlag = displayFlag;
	}

}