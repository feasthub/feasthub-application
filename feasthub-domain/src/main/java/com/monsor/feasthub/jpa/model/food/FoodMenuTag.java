package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_menu_tags database table.
 * 
 */
@Entity
@Table(name = "food_menu_tags")
@NamedQuery(name = "FoodMenuTag.findAll", query = "SELECT f FROM FoodMenuTag f")
public class FoodMenuTag implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FOOD_MENU_TAG_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "ADDITIONAL_FOOD_CATEGORY_KEY_TAGS_FLAG")
	private Boolean additionalFoodCategoryKeyTagsFlag;

	@Column(name = "FOOD_CATEGORY_KEY")
	private BigInteger foodCategoryKey;

	@Column(name = "FOOD_DISPLAY_CATEGORY_KEY_FLAG")
	private Boolean foodDisplayCategoryKeyFlag;

	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	public FoodMenuTag() {
	}

	public Boolean getAdditionalFoodCategoryKeyTagsFlag() {
		return this.additionalFoodCategoryKeyTagsFlag;
	}

	public void setAdditionalFoodCategoryKeyTagsFlag(Boolean additionalFoodCategoryKeyTagsFlag) {
		this.additionalFoodCategoryKeyTagsFlag = additionalFoodCategoryKeyTagsFlag;
	}

	public BigInteger getFoodCategoryKey() {
		return this.foodCategoryKey;
	}

	public void setFoodCategoryKey(BigInteger foodCategoryKey) {
		this.foodCategoryKey = foodCategoryKey;
	}

	public Boolean getFoodDisplayCategoryKeyFlag() {
		return this.foodDisplayCategoryKeyFlag;
	}

	public void setFoodDisplayCategoryKeyFlag(Boolean foodDisplayCategoryKeyFlag) {
		this.foodDisplayCategoryKeyFlag = foodDisplayCategoryKeyFlag;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

}