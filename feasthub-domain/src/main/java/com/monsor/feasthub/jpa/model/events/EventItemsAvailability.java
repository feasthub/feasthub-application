package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the EVENT_ITEMS_AVAILABILITY database table.
 * 
 */
@Entity
@Table(name = "event_items_availability")
@NamedQueries({
		@NamedQuery(name = "EventItemsAvailability.searchEventItemsAvailabilitysIgnoreCase", query = "SELECT f FROM EventItemsAvailability f "
				+ "WHERE " + " f.eventId = :searchString "),
		@NamedQuery(name = "EventItemsAvailability.findDuplicate", query = "SELECT f FROM EventItemsAvailability f "
				+ "WHERE " + " f.eventId = :eventId "), })
public class EventItemsAvailability implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "EVENT_ITEMS_AVAILABILITY_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@JsonIgnore
	@Column(name = "BLOCKED_QUANTITY")
	private int blockedQuantity;

	@Column(name = "CURRENT_AVAILABLE_QUANTITY")
	private int currentAvailableQuantity;

	@JsonIgnore
	@Column(name = "MAX_OFFERED_QUANTITY_FOR_QUICK_SERVICE")
	private int maxOfferedQtyForQuickService;

	@JsonIgnore
	@Column(name = "QUICK_SERVICE_SOLD_QUANTITY")
	private int quickServiceSoldQuantity;

	@JsonIgnore
	@Column(name = "TOTAL_SOLD_QUANTITY")
	private int totalSoldQuantity;

	@JsonIgnore
	@Column(name = "QUICK_SERVICE_AVAILABLE_QUANTITY")
	private int quickServiceAvailableQuantity;

	@JsonIgnore
	@Column(name = "SCHEDULED_SOLD_QUANTITY")
	private int scheduledSoldQuantity;

	@Required
	@Searchable
	@CheckForDuplicate
	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@JsonIgnore
	@Temporal(TemporalType.DATE)
	@Column(name = "LISTING_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date listingDate;

	@JsonIgnore
	@Column(name = "OFFERED_QUANTITY")
	private int offeredQuantity;

	public EventItemsAvailability() {
	}

	public EventItemsAvailability(BigInteger eventId, int offeredQuantity) {
		this.eventId = eventId;
		this.offeredQuantity = offeredQuantity;
		this.listingDate = new Date();
		this.totalSoldQuantity = 0;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public int getBlockedQuantity() {
		return this.blockedQuantity;
	}

	public void setBlockedQuantity(int blockedQuantity) {
		this.blockedQuantity = blockedQuantity;
	}

	public int getCurrentAvailableQuantity() {
		return this.currentAvailableQuantity;
	}

	public void setCurrentAvailableQuantity(int currentAvailableQuantity) {
		this.currentAvailableQuantity = currentAvailableQuantity;
	}

	public int getMaxOfferedQtyForQuickService() {
		return maxOfferedQtyForQuickService;
	}

	public void setMaxOfferedQtyForQuickService(int maxOfferedQtyForQuickService) {
		this.maxOfferedQtyForQuickService = maxOfferedQtyForQuickService;
	}

	public int getQuickServiceSoldQuantity() {
		return quickServiceSoldQuantity;
	}

	public void setQuickServiceSoldQuantity(int quickServiceSoldQuantity) {
		this.quickServiceSoldQuantity = quickServiceSoldQuantity;
	}

	public int getTotalSoldQuantity() {
		return totalSoldQuantity;
	}

	public void setTotalSoldQuantity(int totalSoldQuantity) {
		this.totalSoldQuantity = totalSoldQuantity;
	}

	public int getQuickServiceAvailableQuantity() {
		return quickServiceAvailableQuantity;
	}

	public void setQuickServiceAvailableQuantity(int quickServiceAvailableQuantity) {
		this.quickServiceAvailableQuantity = quickServiceAvailableQuantity;
	}

	public int getScheduledSoldQuantity() {
		return scheduledSoldQuantity;
	}

	public void setScheduledSoldQuantity(int scheduledSoldQuantity) {
		this.scheduledSoldQuantity = scheduledSoldQuantity;
	}

	public Date getListingDate() {
		return listingDate;
	}

	public void setListingDate(Date listingDate) {
		this.listingDate = listingDate;
	}

	public BigInteger getEventId() {
		return this.eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public int getOfferedQuantity() {
		return this.offeredQuantity;
	}

	public void setOfferedQuantity(int offeredQuantity) {
		this.offeredQuantity = offeredQuantity;
	}

}