package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the event_delivery_part_of_day_to_slot_mapping
 * database table.
 * 
 */
@Entity
@Table(name = "event_delivery_part_of_day_to_slot_mapping")
@NamedQuery(name = "EventDeliveryPartOfDayToSlotMapping.findAll", query = "SELECT e FROM EventDeliveryPartOfDayToSlotMapping e")
public class EventDeliveryPartOfDayToSlotMapping implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_DELIVERY_PART_TO_SLOT_MAPPING_ID")
	private BigInteger id;

	@Column(name = "DELIVERY_SLOTS_ID")
	private BigInteger deliverySlotsId;

	@JsonIgnore
	@Column(name = "EVENT_DELIVERY_AVAILBILITY_ID")
	private BigInteger eventDeliveryAvailbilityId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "event_delivery_slot", joinColumns = @JoinColumn(name = "DELIVERY_SLOTS_ID"), inverseJoinColumns = @JoinColumn(name = "DELIVERY_SLOTS_ID"))
	private DeliverySlot deliverySlot;

	public EventDeliveryPartOfDayToSlotMapping() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getDeliverySlotsId() {
		return this.deliverySlotsId;
	}

	public void setDeliverySlotsId(BigInteger deliverySlotsId) {
		this.deliverySlotsId = deliverySlotsId;
	}

	public BigInteger getEventDeliveryAvailbilityId() {
		return this.eventDeliveryAvailbilityId;
	}

	public void setEventDeliveryAvailbilityId(BigInteger eventDeliveryAvailbilityId) {
		this.eventDeliveryAvailbilityId = eventDeliveryAvailbilityId;
	}

	public DeliverySlot getDeliverySlot() {
		return deliverySlot;
	}

	public void setDeliverySlot(DeliverySlot deliverySlot) {
		this.deliverySlot = deliverySlot;
	}

}