package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;

/**
 * The persistent class for the delivery_slots_time database table.
 * 
 */
@Entity
@Table(name = "event_delivery_slots_time")
@NamedQueries({ @NamedQuery(name = "DeliverySlotsTime.findAll", query = "SELECT d FROM DeliverySlotsTime d"),
		@NamedQuery(name = "DeliverySlotsTime.findDuplicate", query = "SELECT f FROM DeliverySlotsTime f " + "WHERE "
				+ " f.id = :deliverySlotsTimeId and" + " f.slotsTimingEndsAt = :slotsTimingEndsAt and"
				+ " f.slotsTimingStartsAt = :slotsTimingStartsAt ") })
public class DeliverySlotsTime implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "DELIVERY_SLOTS_TIME_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@JsonIgnore
	@Column(name = "DELIVERY_SLOTS_ID")
	private BigInteger deliverySlotsId;

	@Column(name = "SLOTS_TIMING_STARTS_AT")
	private int slotsTimingStartsAt;

	@Column(name = "SLOTS_TIMING_ENDS_AT")
	private int slotsTimingEndsAt;

	public DeliverySlotsTime() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getDeliverySlotsId() {
		return deliverySlotsId;
	}

	public void setDeliverySlotsId(BigInteger deliverySlotsId) {
		this.deliverySlotsId = deliverySlotsId;
	}

	public int getSlotsTimingEndsAt() {
		return this.slotsTimingEndsAt;
	}

	public void setSlotsTimingEndsAt(int slotsTimingEndsAt) {
		this.slotsTimingEndsAt = slotsTimingEndsAt;
	}

	public int getSlotsTimingStartsAt() {
		return this.slotsTimingStartsAt;
	}

	public void setSlotsTimingStartsAt(int slotsTimingStartsAt) {
		this.slotsTimingStartsAt = slotsTimingStartsAt;
	}

}