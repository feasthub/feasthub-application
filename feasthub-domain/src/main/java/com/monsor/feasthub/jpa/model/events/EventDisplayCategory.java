package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the event_display_category database table.
 * 
 */
@Entity
@Table(name = "event_display_category")
@NamedQuery(name = "EventDisplayCategory.findAll", query = "SELECT e FROM EventDisplayCategory e")
public class EventDisplayCategory implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "EVENT_DISPLAY_CATEGORY_ID")
	private BigInteger id;

	@Column(name = "DISPLAY_CATEGORY_NAME")
	private String displayCategoryName;

	@Column(name = "DISPLAY_FLAG")
	private Boolean displayFlag;

	@Column(name = "DISPLAY_ORDER")
	private int displayOrder;

	@Column(name = "PROMOTIONAL_COLLECTION_FLAG")
	private int promotionalCollectionFlag;

	@Column(name = "SHOWCASE_IMAGE_URL")
	private String showcaseImageURL;

	public EventDisplayCategory() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getDisplayCategoryName() {
		return this.displayCategoryName;
	}

	public void setDisplayCategoryName(String displayCategoryName) {
		this.displayCategoryName = displayCategoryName;
	}

	public Boolean getDisplayFlag() {
		return displayFlag;
	}

	public void setDisplayFlag(Boolean displayFlag) {
		this.displayFlag = displayFlag;
	}

	public int getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public int getPromotionalCollectionFlag() {
		return this.promotionalCollectionFlag;
	}

	public void setPromotionalCollectionFlag(int promotionalCollectionFlag) {
		this.promotionalCollectionFlag = promotionalCollectionFlag;
	}

	public String getShowcaseImageURL() {
		return showcaseImageURL;
	}

	public void setShowcaseImageURL(String showcaseImageURL) {
		this.showcaseImageURL = showcaseImageURL;
	}

}