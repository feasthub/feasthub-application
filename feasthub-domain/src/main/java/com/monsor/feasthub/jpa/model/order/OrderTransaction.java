package com.monsor.feasthub.jpa.model.order;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.audit.TransactionCashAudit;
import com.monsor.feasthub.jpa.model.audit.TransactionFeastAudit;
import com.monsor.feasthub.jpa.model.enums.OrderStatus;
import com.monsor.feasthub.jpa.model.enums.PaymentMode;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.payment.PaymentGateway;
import com.monsor.feasthub.jpa.model.user.UserAddress;

/**
 * The persistent class for the order_transaction database table.
 * 
 */
@Entity
@Table(name = "order_transaction")
@NamedQueries({ @NamedQuery(
    name = "OrderTransaction.findAll", query = "SELECT o FROM OrderTransaction o"),
	@NamedQuery(
	    name = "findAllByOrderDateAndOrderStatusIn",
	    query = "SELECT o FROM OrderTransaction o where o.orderDate = :orderDate and o.orderStatus in (:orderStatus)") })
public class OrderTransaction
	implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_TRANSACTION_ID")
    private BigInteger id;

    @Column(name = "ADDITIONAL_NOTE_BY_USER")
    private String additionalNoteByUser;

    /***
     * all Cash related items will be filled here like : COD NET-BAnking
     */
    @JoinColumn(name = "CASH_AUDIT_ID")
    private TransactionCashAudit cashAuditId;

    @Transient
    private BigInteger addressId;

    @OneToOne
    @JoinColumn(name = "DELIVERY_ADDRESS_ID")
    private UserAddress deliveryAddressId;

    @JoinColumn(name = "FEAST_AUDIT_ID")
    private TransactionFeastAudit feastPointAuditId;

    @Temporal(TemporalType.DATE)
    @Column(name = "FIRST_DELIVERY_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+5:30")
    private Date firstDeliveryDate;

    @Column(name = "ORDER_HAS_SPECIAL_REQUEST")
    private Boolean orderHasSpecialRequest;

    @Column(name = "ORDER_ID")
    private String orderId;

    @Column(name = "ORDER_PURCHASED_MODE_TYPE")
    private String orderPurchasedModeType;

    /**
     * ORDER_CREATED functional paramter, category table ---
     * 
     */
    @Enumerated
    @Column(name = "ORDER_STATUS")
    private OrderStatus orderStatus;

    /**
     * Related to payment gateway
     */
    @Column(name = "PAYMENT_STATUS")
    private PaymentStatus paymentStatus;

    @Column(name = "PURCHASER_USER_ID")
    private BigInteger purchaserUserId;

    @Column(name = "REFUND_STATUS")
    private String refundStatus;

    @Temporal(TemporalType.DATE)
    @Column(name = "TRANSACTION_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+5:30")
    private Date transactionDate;

    @Column(name = "COLLECTION_IN_CASH")
    private String collectionInCash;

    @Column(name = "COLLECTION_IN_FP")
    private String collectionInFP;

    @Column(name = "TAXES")
    private String taxes;

    @Column(name = "TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "TOTAL_TAX")
    private Double totalTax;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "orderTransactionId")
    @OrderBy("deliveryDate ASC")
    private List<OrderDelivery> orderDelivery;

    @Enumerated
    @Column(name = "PAYMENT_MODE")
    private PaymentMode paymentMode;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "orderTransactionId")
    private List<OrderEvent> orderEvents;

    @Column(name = "ORDER_DATE")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+5:30")
    private Date orderDate;
    
    @Transient
    private PaymentGateway paymentGateway;

    public OrderTransaction() {
    }

    public String getAdditionalNoteByUser() {
	return additionalNoteByUser;
    }

    public void setAdditionalNoteByUser(String additionalNoteByUser) {
	this.additionalNoteByUser = additionalNoteByUser;
    }

    public UserAddress getDeliveryAddressId() {
	return deliveryAddressId;
    }

    public void setDeliveryAddressId(UserAddress deliveryAddressId) {
	this.deliveryAddressId = deliveryAddressId;
    }

    public Date getFirstDeliveryDate() {
	return firstDeliveryDate;
    }

    public void setFirstDeliveryDate(Date firstDeliveryDate) {
	this.firstDeliveryDate = firstDeliveryDate;
    }

    public Boolean getOrderHasSpecialRequest() {
	return orderHasSpecialRequest;
    }

    public void setOrderHasSpecialRequest(Boolean orderHasSpecialRequest) {
	this.orderHasSpecialRequest = orderHasSpecialRequest;
    }

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public String getOrderPurchasedModeType() {
	return orderPurchasedModeType;
    }

    public void setOrderPurchasedModeType(String orderPurchasedModeType) {
	this.orderPurchasedModeType = orderPurchasedModeType;
    }

    public OrderStatus getOrderStatus() {
	return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
	this.orderStatus = orderStatus;
    }

    public PaymentStatus getPaymentStatus() {
	return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
	this.paymentStatus = paymentStatus;
    }

    public BigInteger getPurchaserUserId() {
	return purchaserUserId;
    }

    public void setPurchaserUserId(BigInteger purchaserUserId) {
	this.purchaserUserId = purchaserUserId;
    }

    public String getRefundStatus() {
	return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
	this.refundStatus = refundStatus;
    }

    public Date getTransactionDate() {
	return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
	this.transactionDate = transactionDate;
    }

    @Override
    public BigInteger getId() {
	return id;
    }

    @Override
    public void setId(BigInteger id) {
	this.id = id;
    }

    public TransactionCashAudit getCashAuditId() {
	return cashAuditId;
    }

    public void setCashAuditId(TransactionCashAudit cashAuditId) {
	this.cashAuditId = cashAuditId;
    }

    public TransactionFeastAudit getFeastPointAuditId() {
	return feastPointAuditId;
    }

    public void setFeastPointAuditId(TransactionFeastAudit feastPointAuditId) {
	this.feastPointAuditId = feastPointAuditId;
    }

    public String getCollectionInCash() {
	return collectionInCash;
    }

    public void setCollectionInCash(String collectionInCash) {
	this.collectionInCash = collectionInCash;
    }

    public String getCollectionInFP() {
	return collectionInFP;
    }

    public void setCollectionInFP(String collectionInFP) {
	this.collectionInFP = collectionInFP;
    }

    public String getTaxes() {
	return taxes;
    }

    public void setTaxes(String taxes) {
	this.taxes = taxes;
    }

    public List<OrderEvent> getOrderEvents() {
	return orderEvents;
    }

    public void setOrderEvents(List<OrderEvent> orderEvents) {
	this.orderEvents = orderEvents;
    }

    public Double getTotalAmount() {
	return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
	this.totalAmount = totalAmount;
    }

    public Double getTotalTax() {
	return totalTax;
    }

    public void setTotalTax(Double totalTax) {
	this.totalTax = totalTax;
    }

    public BigInteger getAddressId() {
	return addressId;
    }

    public void setAddressId(BigInteger addressId) {
	this.addressId = addressId;
    }

    public PaymentMode getPaymentMode() {
	return paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
	this.paymentMode = paymentMode;
    }

    public void setOrderDelivery(List<OrderDelivery> orderDelivery) {
	this.orderDelivery = orderDelivery;
    }

    public List<OrderDelivery> getOrderDelivery() {
	return orderDelivery;
    }

    public Date getOrderDate() {
	return orderDate;
    }

    public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
    }

    
    public PaymentGateway getPaymentGateway() {
    
        return paymentGateway;
    }

    
    public void setPaymentGateway(PaymentGateway paymentGateway) {
    
        this.paymentGateway = paymentGateway;
    }

}