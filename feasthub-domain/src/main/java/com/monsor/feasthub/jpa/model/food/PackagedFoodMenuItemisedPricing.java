package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;

/**
 * The persistent class for the packaged_food_menu_itemised_pricing database
 * table.
 * 
 */
@Entity
@Table(name = "packaged_food_menu_itemised_pricing")
@NamedQuery(name = "PackagedFoodMenuItemisedPricing.findAll", query = "SELECT p FROM PackagedFoodMenuItemisedPricing p")
public class PackagedFoodMenuItemisedPricing implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PACKAGED_FOOD_MENU_ITEMISED_PRICING_ID")
	private BigInteger id;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "FOOD_MENU_ID")
	private FoodMenuItemView foodMenuId;

	@Column(name = "TOP_UP_PRICE")
	private double topUpPrice;

	public PackagedFoodMenuItemisedPricing() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public FoodMenuItemView getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(FoodMenuItemView foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public double getTopUpPrice() {
		return this.topUpPrice;
	}

	public void setTopUpPrice(double topUpPrice) {
		this.topUpPrice = topUpPrice;
	}

}