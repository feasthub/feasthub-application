package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;

/**
 * The persistent class for the
 * packaged_food_item_to_alternative_option_item_list database table.
 * 
 */
@Entity
@Table(name = "packaged_food_item_to_alternative_option_item_list")
@NamedQuery(name = "PackagedFoodItemToAlternativeOptionItemList.findAll", query = "SELECT p FROM PackagedFoodItemToAlternativeOptionItemList p")
public class PackagedFoodItemToAlternativeOptionItemList implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PACKAGED_FOOD_ITEM_TO_ALTERNATIVE_OPTION_ITEM_LIST_ID")
	private BigInteger id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ALTERNATIVE_OPTIONAL_FOOD_MENU_ID")
	private FoodMenuItemView alternativeOptionalFoodMenuId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PACKAGED_FOOD_MENU_ITEMISED_PRICING_ID")
	private PackagedFoodMenuItemisedPricing packagedFoodMenuItemisedPricingId;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "PACKAGED_FOOD_MENU_ITEMS_LIST_ID")
	private PackagedFoodMenuItemsList packagedFoodMenuItemsListId;

	public PackagedFoodItemToAlternativeOptionItemList() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public FoodMenuItemView getAlternativeOptionalFoodMenuId() {
		return this.alternativeOptionalFoodMenuId;
	}

	public void setAlternativeOptionalFoodMenuId(FoodMenuItemView alternativeOptionalFoodMenuId) {
		this.alternativeOptionalFoodMenuId = alternativeOptionalFoodMenuId;
	}

	public PackagedFoodMenuItemisedPricing getPackagedFoodMenuItemisedPricingId() {
		return this.packagedFoodMenuItemisedPricingId;
	}

	public void setPackagedFoodMenuItemisedPricingId(
			PackagedFoodMenuItemisedPricing packagedFoodMenuItemisedPricingId) {
		this.packagedFoodMenuItemisedPricingId = packagedFoodMenuItemisedPricingId;
	}

	public PackagedFoodMenuItemsList getPackagedFoodMenuItemsListId() {
		return this.packagedFoodMenuItemsListId;
	}

	public void setPackagedFoodMenuItemsListId(PackagedFoodMenuItemsList packagedFoodMenuItemsListId) {
		this.packagedFoodMenuItemsListId = packagedFoodMenuItemsListId;
	}

}