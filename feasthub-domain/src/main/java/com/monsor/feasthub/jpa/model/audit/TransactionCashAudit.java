package com.monsor.feasthub.jpa.model.audit;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.enums.ChequeStatus;
import com.monsor.feasthub.jpa.model.enums.PaymentMode;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.enums.TransactionMode;
import com.monsor.feasthub.jpa.model.enums.TransactionPurpose;

/**
 * The persistent class for the transaction_cash_audit database table.
 * 
 */
@Entity
@Table(name = "transaction_cash_audit")
@NamedQuery(name = "TransactionCashAudit.findAll", query = "SELECT t FROM TransactionCashAudit t")
public class TransactionCashAudit implements Serializable, com.monsor.feasthub.jpa.model.common.Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CASH_AUDIT_ID")
	private BigInteger id;

	@Column(name = "AMOUNT")
	private double amount;

	@Column(name = "CURRENCY")
	private String currency;

	@Column(name = "IS_FULL_REFUND")
	private Boolean isFullRefund;

	@Column(name = "IS_PARTIAL_REFUND")
	private Boolean isPartialRefund;

	@Column(name = "ORDER_TRANSACTION_ID")
	private BigInteger orderTransactionId;

	@Column(name = "PAYMENT_GATEWAY_USED")
	private Boolean paymentGatewayUsed;

	@Column(name = "PAYMENT_MODE")
	private PaymentMode paymentMode;

	@Column(name = "PAYMENT_STATUS")
	private PaymentStatus paymentStatus;

	@Column(name = "REFUND_AGAINST_CASH_AUDIT_ID")
	private BigInteger refundAgainstCashAuditId;

	@Temporal(TemporalType.DATE)
	@Column(name = "TRANSACTION_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date transactionDate;

	@Column(name = "TRANSACTION_PURPOSE")
	private TransactionPurpose transactionPurpose;

	@Column(name = "TRANSACTION_TIME")
	private Time transactionTime;

	@Enumerated
	@Column(name = "TRANSACTION_TYPE")
	private TransactionMode transactionType;

	@Enumerated
	@Column(name = "CHEQUE_STATUS")
	private ChequeStatus chequeStatus;

	@Column(name = "CHEQUE_NUMBER")
	private String chequeNumber;

	public TransactionCashAudit() {
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getIsFullRefund() {
		return this.isFullRefund;
	}

	public void setIsFullRefund(Boolean isFullRefund) {
		this.isFullRefund = isFullRefund;
	}

	public Boolean getIsPartialRefund() {
		return this.isPartialRefund;
	}

	public void setIsPartialRefund(Boolean isPartialRefund) {
		this.isPartialRefund = isPartialRefund;
	}

	public BigInteger getOrderTransactionId() {
		return this.orderTransactionId;
	}

	public void setOrderTransactionId(BigInteger orderTransactionId) {
		this.orderTransactionId = orderTransactionId;
	}

	public Boolean getPaymentGatewayUsed() {
		return this.paymentGatewayUsed;
	}

	public void setPaymentGatewayUsed(Boolean paymentGatewayUsed) {
		this.paymentGatewayUsed = paymentGatewayUsed;
	}

	public BigInteger getRefundAgainstCashAuditId() {
		return this.refundAgainstCashAuditId;
	}

	public void setRefundAgainstCashAuditId(BigInteger refundAgainstCashAuditId) {
		this.refundAgainstCashAuditId = refundAgainstCashAuditId;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Time getTransactionTime() {
		return this.transactionTime;
	}

	public void setTransactionTime(Time transactionTime) {
		this.transactionTime = transactionTime;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public TransactionPurpose getTransactionPurpose() {
		return transactionPurpose;
	}

	public void setTransactionPurpose(TransactionPurpose transactionPurpose) {
		this.transactionPurpose = transactionPurpose;
	}

	public TransactionMode getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionMode transactionType) {
		this.transactionType = transactionType;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public ChequeStatus getChequeStatus() {
		return chequeStatus;
	}

	public void setChequeStatus(ChequeStatus chequeStatus) {
		this.chequeStatus = chequeStatus;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

}