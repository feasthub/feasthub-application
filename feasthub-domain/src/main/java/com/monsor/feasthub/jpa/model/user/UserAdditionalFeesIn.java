package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the user_additional_fees_in database table.
 * 
 */
@Entity
@Table(name = "user_additional_fees_in")
@NamedQuery(name = "UserAdditionalFeesIn.findAll", query = "SELECT u FROM UserAdditionalFeesIn u")
public class UserAdditionalFeesIn implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ADDITIONAL_FEES_ID")
	private BigInteger id;

	@Column(name = "USER_DEFINED_FEE_1_APPLICABILITY")
	private String userDefinedFee1Applicability;

	@Column(name = "USER_DEFINED_FEE_1_FLAG")
	private String userDefinedFee1Flag;

	@Column(name = "USER_DEFINED_FEE_2_APPLICABILITY")
	private String userDefinedFee2Applicability;

	@Column(name = "USER_DEFINED_FEE_2_FLAG")
	private String userDefinedFee2Flag;

	@Column(name = "USER_DEFINED_FEE_3_APPLICABILITY")
	private String userDefinedFee3Applicability;

	@Column(name = "USER_DEFINED_FEE_3_FLAG")
	private String userDefinedFee3Flag;

	@Column(name = "USER_DEFINED_FEE_4_APPLICABILITY")
	private String userDefinedFee4Applicability;

	@Column(name = "USER_DEFINED_FEE_4_FLAG")
	private String userDefinedFee4Flag;

	@Column(name = "USER_DEFINED_FEE_5_APPLICABILITY")
	private String userDefinedFee5Applicability;

	@Column(name = "USER_DEFINED_FEE_5_FLAG")
	private String userDefinedFee5Flag;

	@Column(name = "USER_DEFINED_FEE_NAME_1")
	private String userDefinedFeeName1;

	@Column(name = "USER_DEFINED_FEE_NAME_2")
	private String userDefinedFeeName2;

	@Column(name = "USER_DEFINED_FEE_NAME_3")
	private String userDefinedFeeName3;

	@Column(name = "USER_DEFINED_FEE_NAME_4")
	private String userDefinedFeeName4;

	@Column(name = "USER_DEFINED_FEE_NAME_5")
	private String userDefinedFeeName5;

	@Column(name = "USER_DEFINED_FEE_RATE_1")
	private double userDefinedFeeRate1;

	@Column(name = "USER_DEFINED_FEE_RATE_2")
	private double userDefinedFeeRate2;

	@Column(name = "USER_DEFINED_FEE_RATE_3")
	private double userDefinedFeeRate3;

	@Column(name = "USER_DEFINED_FEE_RATE_4")
	private double userDefinedFeeRate4;

	@Column(name = "USER_DEFINED_FEE_RATE_5")
	private double userDefinedFeeRate5;

	public UserAdditionalFeesIn() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getUserDefinedFee1Applicability() {
		return this.userDefinedFee1Applicability;
	}

	public void setUserDefinedFee1Applicability(String userDefinedFee1Applicability) {
		this.userDefinedFee1Applicability = userDefinedFee1Applicability;
	}

	public String getUserDefinedFee1Flag() {
		return this.userDefinedFee1Flag;
	}

	public void setUserDefinedFee1Flag(String userDefinedFee1Flag) {
		this.userDefinedFee1Flag = userDefinedFee1Flag;
	}

	public String getUserDefinedFee2Applicability() {
		return this.userDefinedFee2Applicability;
	}

	public void setUserDefinedFee2Applicability(String userDefinedFee2Applicability) {
		this.userDefinedFee2Applicability = userDefinedFee2Applicability;
	}

	public String getUserDefinedFee2Flag() {
		return this.userDefinedFee2Flag;
	}

	public void setUserDefinedFee2Flag(String userDefinedFee2Flag) {
		this.userDefinedFee2Flag = userDefinedFee2Flag;
	}

	public String getUserDefinedFee3Applicability() {
		return this.userDefinedFee3Applicability;
	}

	public void setUserDefinedFee3Applicability(String userDefinedFee3Applicability) {
		this.userDefinedFee3Applicability = userDefinedFee3Applicability;
	}

	public String getUserDefinedFee3Flag() {
		return this.userDefinedFee3Flag;
	}

	public void setUserDefinedFee3Flag(String userDefinedFee3Flag) {
		this.userDefinedFee3Flag = userDefinedFee3Flag;
	}

	public String getUserDefinedFee4Applicability() {
		return this.userDefinedFee4Applicability;
	}

	public void setUserDefinedFee4Applicability(String userDefinedFee4Applicability) {
		this.userDefinedFee4Applicability = userDefinedFee4Applicability;
	}

	public String getUserDefinedFee4Flag() {
		return this.userDefinedFee4Flag;
	}

	public void setUserDefinedFee4Flag(String userDefinedFee4Flag) {
		this.userDefinedFee4Flag = userDefinedFee4Flag;
	}

	public String getUserDefinedFee5Applicability() {
		return this.userDefinedFee5Applicability;
	}

	public void setUserDefinedFee5Applicability(String userDefinedFee5Applicability) {
		this.userDefinedFee5Applicability = userDefinedFee5Applicability;
	}

	public String getUserDefinedFee5Flag() {
		return this.userDefinedFee5Flag;
	}

	public void setUserDefinedFee5Flag(String userDefinedFee5Flag) {
		this.userDefinedFee5Flag = userDefinedFee5Flag;
	}

	public String getUserDefinedFeeName1() {
		return this.userDefinedFeeName1;
	}

	public void setUserDefinedFeeName1(String userDefinedFeeName1) {
		this.userDefinedFeeName1 = userDefinedFeeName1;
	}

	public String getUserDefinedFeeName2() {
		return this.userDefinedFeeName2;
	}

	public void setUserDefinedFeeName2(String userDefinedFeeName2) {
		this.userDefinedFeeName2 = userDefinedFeeName2;
	}

	public String getUserDefinedFeeName3() {
		return this.userDefinedFeeName3;
	}

	public void setUserDefinedFeeName3(String userDefinedFeeName3) {
		this.userDefinedFeeName3 = userDefinedFeeName3;
	}

	public String getUserDefinedFeeName4() {
		return this.userDefinedFeeName4;
	}

	public void setUserDefinedFeeName4(String userDefinedFeeName4) {
		this.userDefinedFeeName4 = userDefinedFeeName4;
	}

	public String getUserDefinedFeeName5() {
		return this.userDefinedFeeName5;
	}

	public void setUserDefinedFeeName5(String userDefinedFeeName5) {
		this.userDefinedFeeName5 = userDefinedFeeName5;
	}

	public double getUserDefinedFeeRate1() {
		return this.userDefinedFeeRate1;
	}

	public void setUserDefinedFeeRate1(double userDefinedFeeRate1) {
		this.userDefinedFeeRate1 = userDefinedFeeRate1;
	}

	public double getUserDefinedFeeRate2() {
		return this.userDefinedFeeRate2;
	}

	public void setUserDefinedFeeRate2(double userDefinedFeeRate2) {
		this.userDefinedFeeRate2 = userDefinedFeeRate2;
	}

	public double getUserDefinedFeeRate3() {
		return this.userDefinedFeeRate3;
	}

	public void setUserDefinedFeeRate3(double userDefinedFeeRate3) {
		this.userDefinedFeeRate3 = userDefinedFeeRate3;
	}

	public double getUserDefinedFeeRate4() {
		return this.userDefinedFeeRate4;
	}

	public void setUserDefinedFeeRate4(double userDefinedFeeRate4) {
		this.userDefinedFeeRate4 = userDefinedFeeRate4;
	}

	public double getUserDefinedFeeRate5() {
		return this.userDefinedFeeRate5;
	}

	public void setUserDefinedFeeRate5(double userDefinedFeeRate5) {
		this.userDefinedFeeRate5 = userDefinedFeeRate5;
	}

}