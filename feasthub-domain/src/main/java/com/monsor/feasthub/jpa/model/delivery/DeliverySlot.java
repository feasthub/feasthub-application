package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;

/**
 * The persistent class for the delivery_slot database table.
 * 
 */
@Entity
@Table(name = "event_delivery_slot")
@NamedQueries({ @NamedQuery(name = "DeliverySlot.findAll", query = "SELECT d FROM DeliverySlot d"),
		@NamedQuery(name = "DeliverySlot.findDuplicate", query = "SELECT f FROM DeliverySlot f " + "WHERE "
				+ " f.businessTypes = :businessTypes and " + " f.servicesOfferingTypes = :servicesOfferingTypes ") })
public class DeliverySlot implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@Column(name = "DELIVERY_SLOTS_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "BUSINESS_TYPES")
	private String businessTypes;

	@Column(name = "PART_OF_DAY")
	private String partOfDay;

	@Column(name = "SERVICES_OFFERING_TYPES")
	private String servicesOfferingTypes;

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "DELIVERY_SLOTS_ID")
	private List<DeliverySlotsTime> deliverySlotsTime;

	public DeliverySlot() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getBusinessTypes() {
		return this.businessTypes;
	}

	public void setBusinessTypes(String businessTypes) {
		this.businessTypes = businessTypes;
	}

	public String getServicesOfferingTypes() {
		return this.servicesOfferingTypes;
	}

	public void setServicesOfferingTypes(String servicesOfferingTypes) {
		this.servicesOfferingTypes = servicesOfferingTypes;
	}

	public List<DeliverySlotsTime> getDeliverySlotsTime() {
		return deliverySlotsTime;
	}

	public void setDeliverySlotsTime(List<DeliverySlotsTime> deliverySlotsTime) {
		this.deliverySlotsTime = deliverySlotsTime;
	}

	public String getPartOfDay() {
		return partOfDay;
	}

	public void setPartOfDay(String partOfDay) {
		this.partOfDay = partOfDay;
	}

}