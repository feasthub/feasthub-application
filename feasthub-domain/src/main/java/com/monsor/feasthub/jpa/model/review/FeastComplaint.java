package com.monsor.feasthub.jpa.model.review;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the FEAST_COMPLAINTS database table.
 * 
 */
@Entity
@Table(name = "FEAST_COMPLAINTS")
@NamedQuery(name = "FeastComplaint.findAll", query = "SELECT f FROM FeastComplaint f")
public class FeastComplaint implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FEAST_COMPLAINT_ID")
	private BigInteger id;

	@Temporal(TemporalType.DATE)
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date date;

	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@Column(name = "OS_COMPLAINT_NUMBER")
	private String osComplaintNumber;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public FeastComplaint() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigInteger getEventId() {
		return this.eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public String getOsComplaintNumber() {
		return this.osComplaintNumber;
	}

	public void setOsComplaintNumber(String osComplaintNumber) {
		this.osComplaintNumber = osComplaintNumber;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}