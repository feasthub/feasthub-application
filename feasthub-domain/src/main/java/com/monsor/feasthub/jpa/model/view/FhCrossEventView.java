package com.monsor.feasthub.jpa.model.view;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.Searchable;

@Entity
@Table(name = "fh_events")
public class FhCrossEventView implements Serializable, Identifiable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 168957829375489093L;
	@Id
	@Column(name = "EVENT_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Column(name = "EVENT_LISTING_SHOWTIME_END_TIME")
	private Time eventListingShowtimeEndTime;

	@Column(name = "EVENT_LISTING_SHOWTIME_START_TIME")
	private Time eventListingShowtimeStartTime;

	@Searchable
	@Column(name = "EVENT_NAME")
	private String eventName;

	@Searchable
	@Column(name = "EVENT_OFFERED_BY")
	private BigInteger eventOfferedBy;

	@Column(name = "TAX_STRUCTURE_ID")
	private BigInteger taxStructureId;

	@Column(name = "DELIVERY_CHARGE_ID")
	private BigInteger deliveryChargeId;

	@Column(name = "EVENT_DELIVERY_AVAILBILITY_ID")
	private BigInteger eventItemsAvailabilityId;

	@Column(name = "DELIVERY_LOCATION_ID")
	private BigInteger deliveryLocationId;

	@Column(name = "OFFERED_PRICE")
	private Double offeredPrice;

	@Column(name = "ADD_TAXES")
	private Double addTaxes;

	@Column(name = "ADD_DELIVERY")
	private Double addDelivery;

	@Column(name = "ADD_OTHER_CHARGE")
	private BigInteger addOtherCharge;

	@Column(name = "FINAL_OFFERED_PRICE")
	private int finalOfferedPrice;

	@Column(name = "FINAL_OFFERED_PRICE_FP")
	private int finalOfferedPriceFP;

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public Time getEventListingShowtimeEndTime() {
		return eventListingShowtimeEndTime;
	}

	public void setEventListingShowtimeEndTime(Time eventListingShowtimeEndTime) {
		this.eventListingShowtimeEndTime = eventListingShowtimeEndTime;
	}

	public Time getEventListingShowtimeStartTime() {
		return eventListingShowtimeStartTime;
	}

	public void setEventListingShowtimeStartTime(Time eventListingShowtimeStartTime) {
		this.eventListingShowtimeStartTime = eventListingShowtimeStartTime;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public BigInteger getEventOfferedBy() {
		return eventOfferedBy;
	}

	public void setEventOfferedBy(BigInteger eventOfferedBy) {
		this.eventOfferedBy = eventOfferedBy;
	}

	public BigInteger getTaxStructureId() {
		return taxStructureId;
	}

	public void setTaxStructureId(BigInteger taxStructureId) {
		this.taxStructureId = taxStructureId;
	}

	public BigInteger getDeliveryChargeId() {
		return deliveryChargeId;
	}

	public void setDeliveryChargeId(BigInteger deliveryChargeId) {
		this.deliveryChargeId = deliveryChargeId;
	}

	public BigInteger getEventItemsAvailabilityId() {
		return eventItemsAvailabilityId;
	}

	public void setEventItemsAvailabilityId(BigInteger eventItemsAvailabilityId) {
		this.eventItemsAvailabilityId = eventItemsAvailabilityId;
	}

	public BigInteger getDeliveryLocationId() {
		return deliveryLocationId;
	}

	public void setDeliveryLocationId(BigInteger deliveryLocationId) {
		this.deliveryLocationId = deliveryLocationId;
	}

	public Double getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(Double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public Double getAddTaxes() {
		return addTaxes;
	}

	public void setAddTaxes(Double addTaxes) {
		this.addTaxes = addTaxes;
	}

	public Double getAddDelivery() {
		return addDelivery;
	}

	public void setAddDelivery(Double addDelivery) {
		this.addDelivery = addDelivery;
	}

	public BigInteger getAddOtherCharge() {
		return addOtherCharge;
	}

	public void setAddOtherCharge(BigInteger addOtherCharge) {
		this.addOtherCharge = addOtherCharge;
	}

	public int getFinalOfferedPrice() {
		return finalOfferedPrice;
	}

	public void setFinalOfferedPrice(int finalOfferedPrice) {
		this.finalOfferedPrice = finalOfferedPrice;
	}

	public int getFinalOfferedPriceFP() {
		return finalOfferedPriceFP;
	}

	public void setFinalOfferedPriceFP(int finalOfferedPriceFP) {
		this.finalOfferedPriceFP = finalOfferedPriceFP;
	}

}
