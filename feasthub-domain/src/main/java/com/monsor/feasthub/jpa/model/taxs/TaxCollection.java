package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the tax_collection database table.
 * 
 */
@Entity
@Table(name = "tax_collection")
@NamedQuery(name = "TaxCollection.findAll", query = "SELECT t FROM TaxCollection t")
public class TaxCollection implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAX_COLLECTION_ID")
	private BigInteger id;

	@Column(name = "EVENT_ID")
	private BigInteger eventId;

	@Column(name = "EVENT_SALES_TRANS_ID")
	private BigInteger eventSalesTransId;

	@Column(name = "TAX_COLLECTION_CONVERTED_TO_CASH")
	private double taxCollectionConvertedToCash;

	@Column(name = "TAX_COLLECTION_IN_CASH")
	private double taxCollectionInCash;

	@Column(name = "TAX_COLLECTION_IN_FP")
	private double taxCollectionInFp;

	@Column(name = "TAX_SETTLEMENT_COMPLETED_FLAG")
	private byte taxSettlementCompletedFlag;

	@Column(name = "TAX_STRUCTURE_ID")
	private BigInteger taxStructureId;

	@Temporal(TemporalType.DATE)
	@Column(name = "TRANSACTION_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date transactionDate;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public TaxCollection() {
	}

	public BigInteger getEventId() {
		return this.eventId;
	}

	public void setEventId(BigInteger eventId) {
		this.eventId = eventId;
	}

	public BigInteger getEventSalesTransId() {
		return this.eventSalesTransId;
	}

	public void setEventSalesTransId(BigInteger eventSalesTransId) {
		this.eventSalesTransId = eventSalesTransId;
	}

	public double getTaxCollectionConvertedToCash() {
		return this.taxCollectionConvertedToCash;
	}

	public void setTaxCollectionConvertedToCash(double taxCollectionConvertedToCash) {
		this.taxCollectionConvertedToCash = taxCollectionConvertedToCash;
	}

	public double getTaxCollectionInCash() {
		return this.taxCollectionInCash;
	}

	public void setTaxCollectionInCash(double taxCollectionInCash) {
		this.taxCollectionInCash = taxCollectionInCash;
	}

	public double getTaxCollectionInFp() {
		return this.taxCollectionInFp;
	}

	public void setTaxCollectionInFp(double taxCollectionInFp) {
		this.taxCollectionInFp = taxCollectionInFp;
	}

	public byte getTaxSettlementCompletedFlag() {
		return this.taxSettlementCompletedFlag;
	}

	public void setTaxSettlementCompletedFlag(byte taxSettlementCompletedFlag) {
		this.taxSettlementCompletedFlag = taxSettlementCompletedFlag;
	}

	public BigInteger getTaxStructureId() {
		return this.taxStructureId;
	}

	public void setTaxStructureId(BigInteger taxStructureId) {
		this.taxStructureId = taxStructureId;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

}