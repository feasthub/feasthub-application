package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlot;
import com.monsor.feasthub.jpa.model.events.EventDisplayCategory;

public class StartUpConfig implements Serializable, Identifiable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7425790029733697679L;

	private BigInteger id;

	private List<EventDisplayCategory> eventDisplayCategories;

	private List<DeliverySlot> eventDeliverySlots;

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public List<EventDisplayCategory> getEventDisplayCategories() {
		return eventDisplayCategories;
	}

	public void setEventDisplayCategories(List<EventDisplayCategory> eventDisplayCategories) {
		this.eventDisplayCategories = eventDisplayCategories;
	}

	public List<DeliverySlot> getEventDeliverySlots() {
		return eventDeliverySlots;
	}

	public void setEventDeliverySlots(List<DeliverySlot> eventDeliverySlots) {
		this.eventDeliverySlots = eventDeliverySlots;
	}

}
