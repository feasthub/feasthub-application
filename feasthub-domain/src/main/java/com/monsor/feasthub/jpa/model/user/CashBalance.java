package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the cash_balance database table.
 * 
 */
@Entity
@Table(name = "cash_balance")
@NamedQuery(name = "CashBalance.findAll", query = "SELECT c FROM CashBalance c")
public class CashBalance implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CASH_BALANCE_ID")
	private BigInteger id;

	@Column(name = "CASH_CURRENCY")
	private String cashCurrency;

	@Column(name = "TOTAL_CASH_AMOUNT")
	private BigDecimal totalCashAmount;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public CashBalance() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCashCurrency() {
		return this.cashCurrency;
	}

	public void setCashCurrency(String cashCurrency) {
		this.cashCurrency = cashCurrency;
	}

	public BigDecimal getTotalCashAmount() {
		return this.totalCashAmount;
	}

	public void setTotalCashAmount(BigDecimal totalCashAmount) {
		this.totalCashAmount = totalCashAmount;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}