package com.monsor.feasthub.jpa.model.order;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.AdditionalServiceOffering;
import com.monsor.feasthub.jpa.model.enums.BusinessTypes;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.events.FhEventView;

/**
 * The persistent class for the order_transaction_events database table.
 * 
 */
@Entity
@Table(name = "order_events")
@NamedQuery(name = "OrderEvent.findAll", query = "SELECT o FROM OrderEvent o")
public class OrderEvent implements Serializable, Identifiable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_EVENT_ID")
    private BigInteger id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_TRANSACTION_ID")
    private OrderTransaction orderTransactionId;

    @Column(name = "SELLER_USER_ID")
    private BigInteger sellerUserId;

    @Transient
    private BigInteger eventId;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = false)
    @JoinColumn(name = "EVENT_ID")
    private FhEventView eventDetails;

    @Column(name = "ORDERED_QUANTITY")
    private int orderedQuantity;

    @Column(name = "BUSINESS_TYPE")
    private BusinessTypes businessType;

    @Column(name = "SERVICE_OFFERING_TYPE")
    private ServiceOfferingTypes serviceOfferingType;

    @Column(name = "ADDITIONAL_SERVICE_OFFERING_TYPE")
    private AdditionalServiceOffering additionalServiceOfferingType;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "orderEventId")
    private List<OrderCustomisedCusine> orderCustomizeCusine;

    public OrderEvent() {
    }

    public OrderEvent(BigInteger eventId, BigInteger sellerUserId) {
	super();
	this.eventId = eventId;
	this.sellerUserId = sellerUserId;
    }

    public BigInteger getSellerUserId() {
	return this.sellerUserId;
    }

    public void setSellerUserId(BigInteger sellerUserId) {
	this.sellerUserId = sellerUserId;
    }

    public BigInteger getEventId() {
	return eventId;
    }

    public void setEventId(BigInteger eventId) {
	this.eventId = eventId;
    }

    public int getOrderedQuantity() {
	return orderedQuantity;
    }

    public void setOrderedQuantity(int orderedQuantity) {
	this.orderedQuantity = orderedQuantity;
    }

    public BusinessTypes getBusinessType() {
	return businessType;
    }

    public void setBusinessType(BusinessTypes businessType) {
	this.businessType = businessType;
    }

    @Override
    public BigInteger getId() {
	return id;
    }

    @Override
    public void setId(BigInteger id) {
	this.id = id;
    }

    public OrderTransaction getOrderTransactionId() {
	return orderTransactionId;
    }

    public void setOrderTransactionId(OrderTransaction orderTransactionId) {
	this.orderTransactionId = orderTransactionId;
    }

    public ServiceOfferingTypes getServiceOfferingType() {
	return serviceOfferingType;
    }

    public void setServiceOfferingType(ServiceOfferingTypes serviceOfferingType) {
	this.serviceOfferingType = serviceOfferingType;
    }

    public AdditionalServiceOffering getAdditionalServiceOfferingType() {
	return additionalServiceOfferingType;
    }

    public void setAdditionalServiceOfferingType(
	    AdditionalServiceOffering additionalServiceOfferingType) {
	this.additionalServiceOfferingType = additionalServiceOfferingType;
    }

    public List<OrderCustomisedCusine> getOrderCustomizeCusine() {
	return orderCustomizeCusine;
    }

    public void setOrderCustomizeCusine(List<OrderCustomisedCusine> orderCustomizeCusine) {
	this.orderCustomizeCusine = orderCustomizeCusine;
    }

    public FhEventView getEventDetails() {
	return eventDetails;
    }

    public void setEventDetails(FhEventView eventDetails) {
	this.eventDetails = eventDetails;
    }

}