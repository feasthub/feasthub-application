package com.monsor.feasthub.jpa.model.events;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.AdditionalServiceOffering;
import com.monsor.feasthub.jpa.model.enums.BusinessTypes;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the FH_EVENTS database table.
 * 
 */
@Entity
@Table(name = "fh_events")
@NamedQueries({ @NamedQuery(name = "FhEventView.searchFhEventsIgnoreCase", query = "SELECT f FROM FhEvent f " + "WHERE "
	+ " f.businessType = :searchString " + " or " + " f.eventName = :searchString " + " or "
	+ " f.eventOfferedBy = :searchString " + " or " + " f.userCanBlockOnlineFlag = :searchString " + " or "
	+ " f.userCanPurchaseOnlineFlag = :searchString "),

	@NamedQuery(name = "findBybusinessType", query = "SELECT f FROM FhEvent f " + "WHERE "
		+ " f.businessType = :businessType ") })
public class FhEventView implements Serializable, Identifiable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "EVENT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @JsonIgnore
    @Column(name = "ADDITIONAL_COMMENTS")
    private String additionalComments;

    @JsonIgnore
    @Column(name = "EVENT_IMAGE_UPLOADED_FLAG")
    private Boolean eventImageUploadedFlag;

    @JsonIgnore
    @Temporal(TemporalType.DATE)
    @Column(name = "EVENT_LISTING_END_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm a", timezone = "GMT+5:30")
    private Date eventListingEndDate;

    @JsonIgnore
    @Column(name = "EVENT_LISTING_NO_OF_DAYS_LIVE")
    private int eventListingNoOfDaysLive;

    @JsonIgnore
    @Column(name = "EVENT_LISTING_SHOWTIME_END_TIME")
    private Time eventListingShowtimeEndTime;

    @JsonIgnore
    @Column(name = "EVENT_LISTING_SHOWTIME_START_TIME")
    private Time eventListingShowtimeStartTime;

    @JsonIgnore
    @Temporal(TemporalType.DATE)
    @Column(name = "EVENT_LISTING_START_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm a", timezone = "GMT+5:30")
    private Date eventListingStartDate;

    
    @Searchable
    @Column(name = "EVENT_NAME")
    private String eventName;

    @JsonIgnore
    @Searchable
    @Column(name = "USER_CAN_BLOCK_ONLINE_FLAG")
    private Boolean userCanBlockOnlineFlag;

    @JsonIgnore
    @Searchable
    @Column(name = "USER_CAN_PURCHASE_ONLINE_FLAG")
    private Boolean userCanPurchaseOnlineFlag;

    @JsonIgnore
    @Column(name = "ITEM_AVAILABLE_FOR_SCHEDULED_ORDERING")
    private Boolean isScheduleOrdering;

    @JsonIgnore
    @Column(name = "ITEM_AVAILABLE_FOR_PROMOTION")
    private Boolean isPromotion;

    @JsonIgnore
    @Column(name = "TAX_STRUCTURE_ID")
    private BigInteger taxStructureId;

    @JsonIgnore
    @Column(name = "DELIVERY_CHARGE_ID")
    private BigInteger deliveryChargeId;

    @JsonIgnore
    @Column(name = "DELIVERY_LOCATION_ID")
    private BigInteger deliveryLocationId;

    @Column(name = "OFFERED_PRICE")
    private Double offeredPrice;

    @JsonIgnore
    @Column(name = "ADD_COMMISION")
    private Double addCommission;

    @JsonIgnore
    @Column(name = "ADD_TAXES")
    private Double addTaxes;

    @JsonIgnore
    @Column(name = "ADD_DELIVERY")
    private Double addDelivery;

    @JsonIgnore
    @Column(name = "ADD_OTHER_CHARGE")
    private BigInteger addOtherCharge;

    @JsonIgnore
    @Column(name = "FINAL_OFFERED_PRICE")
    private Double finalOfferedPrice;

    @JsonIgnore
    @Column(name = "FINAL_OFFERED_PRICE_FP")
    private Double finalOfferedPriceFP;

    @JsonIgnore
    @Column(name = "EVENT_AVERAGE_REVIEW")
    private Double eventAverageReview;

    @JsonIgnore
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ADDITIONAL_SERVICE_OFFERING_TYPE")
    private AdditionalServiceOffering additionalServiceOfferingType;

    @JsonIgnore
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "SERVICE_OFFERING_TYPE")
    private ServiceOfferingTypes serviceOfferingType;

    @JsonIgnore
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "BUSINESS_TYPE")
    private BusinessTypes businessType;

    @JsonIgnore
    @Column(name = "EVENT_OFFERED_BY")
    private BigInteger eventOfferedBy;

    @JsonIgnore
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "event_cuisine_listing", joinColumns = @JoinColumn(name = "EVENT_ID"),
	inverseJoinColumns = @JoinColumn(name = "EVENT_CUISINE_LISTING_ID"))
    private EventCuisineListing eventCuisine;

    @Override
    public BigInteger getId() {

	return id;
    }

    @Override
    public void setId(BigInteger id) {

	this.id = id;
    }

    public String getAdditionalComments() {

	return additionalComments;
    }

    public void setAdditionalComments(String additionalComments) {

	this.additionalComments = additionalComments;
    }

    public Boolean getEventImageUploadedFlag() {

	return eventImageUploadedFlag;
    }

    public void setEventImageUploadedFlag(Boolean eventImageUploadedFlag) {

	this.eventImageUploadedFlag = eventImageUploadedFlag;
    }

    public Date getEventListingEndDate() {

	return eventListingEndDate;
    }

    public void setEventListingEndDate(Date eventListingEndDate) {

	this.eventListingEndDate = eventListingEndDate;
    }

   

    public int getEventListingNoOfDaysLive() {

	return eventListingNoOfDaysLive;
    }

    public void setEventListingNoOfDaysLive(int eventListingNoOfDaysLive) {

	this.eventListingNoOfDaysLive = eventListingNoOfDaysLive;
    }

    public Time getEventListingShowtimeEndTime() {

	return eventListingShowtimeEndTime;
    }

    public void setEventListingShowtimeEndTime(Time eventListingShowtimeEndTime) {

	this.eventListingShowtimeEndTime = eventListingShowtimeEndTime;
    }

    public Time getEventListingShowtimeStartTime() {

	return eventListingShowtimeStartTime;
    }

    public void setEventListingShowtimeStartTime(Time eventListingShowtimeStartTime) {

	this.eventListingShowtimeStartTime = eventListingShowtimeStartTime;
    }

    public Date getEventListingStartDate() {

	return eventListingStartDate;
    }

    public void setEventListingStartDate(Date eventListingStartDate) {

	this.eventListingStartDate = eventListingStartDate;
    }

    

    public String getEventName() {

	return eventName;
    }

    public void setEventName(String eventName) {

	this.eventName = eventName;
    }

    public Boolean getUserCanBlockOnlineFlag() {

	return userCanBlockOnlineFlag;
    }

    public void setUserCanBlockOnlineFlag(Boolean userCanBlockOnlineFlag) {

	this.userCanBlockOnlineFlag = userCanBlockOnlineFlag;
    }

    public Boolean getUserCanPurchaseOnlineFlag() {

	return userCanPurchaseOnlineFlag;
    }

    public void setUserCanPurchaseOnlineFlag(Boolean userCanPurchaseOnlineFlag) {

	this.userCanPurchaseOnlineFlag = userCanPurchaseOnlineFlag;
    }

    public Boolean getIsScheduleOrdering() {

	return isScheduleOrdering;
    }

    public void setIsScheduleOrdering(Boolean isScheduleOrdering) {

	this.isScheduleOrdering = isScheduleOrdering;
    }

    public Boolean getIsPromotion() {

	return isPromotion;
    }

    public void setIsPromotion(Boolean isPromotion) {

	this.isPromotion = isPromotion;
    }

    public BigInteger getTaxStructureId() {

	return taxStructureId;
    }

    public void setTaxStructureId(BigInteger taxStructureId) {

	this.taxStructureId = taxStructureId;
    }

    public BigInteger getDeliveryChargeId() {

	return deliveryChargeId;
    }

    public void setDeliveryChargeId(BigInteger deliveryChargeId) {

	this.deliveryChargeId = deliveryChargeId;
    }

    public BigInteger getDeliveryLocationId() {

	return deliveryLocationId;
    }

    public void setDeliveryLocationId(BigInteger deliveryLocationId) {

	this.deliveryLocationId = deliveryLocationId;
    }

    public Double getOfferedPrice() {

	return offeredPrice;
    }

    public void setOfferedPrice(Double offeredPrice) {

	this.offeredPrice = offeredPrice;
    }

    public Double getAddCommission() {

	return addCommission;
    }

    public void setAddCommission(Double addCommission) {

	this.addCommission = addCommission;
    }

    public Double getAddTaxes() {

	return addTaxes;
    }

    public void setAddTaxes(Double addTaxes) {

	this.addTaxes = addTaxes;
    }

    public Double getAddDelivery() {

	return addDelivery;
    }

    public void setAddDelivery(Double addDelivery) {

	this.addDelivery = addDelivery;
    }

    public BigInteger getAddOtherCharge() {

	return addOtherCharge;
    }

    public void setAddOtherCharge(BigInteger addOtherCharge) {

	this.addOtherCharge = addOtherCharge;
    }

    public Double getFinalOfferedPrice() {

	return finalOfferedPrice;
    }

    public void setFinalOfferedPrice(Double finalOfferedPrice) {

	this.finalOfferedPrice = finalOfferedPrice;
    }

    public Double getFinalOfferedPriceFP() {

	return finalOfferedPriceFP;
    }

    public void setFinalOfferedPriceFP(Double finalOfferedPriceFP) {

	this.finalOfferedPriceFP = finalOfferedPriceFP;
    }

    public Double getEventAverageReview() {

	return eventAverageReview;
    }

    public void setEventAverageReview(Double eventAverageReview) {

	this.eventAverageReview = eventAverageReview;
    }

    public AdditionalServiceOffering getAdditionalServiceOfferingType() {

	return additionalServiceOfferingType;
    }

    public void setAdditionalServiceOfferingType(AdditionalServiceOffering additionalServiceOfferingType) {

	this.additionalServiceOfferingType = additionalServiceOfferingType;
    }

    public ServiceOfferingTypes getServiceOfferingType() {

	return serviceOfferingType;
    }

    public void setServiceOfferingType(ServiceOfferingTypes serviceOfferingType) {

	this.serviceOfferingType = serviceOfferingType;
    }

    public BusinessTypes getBusinessType() {

	return businessType;
    }

    public void setBusinessType(BusinessTypes businessType) {

	this.businessType = businessType;
    }

    public BigInteger getEventOfferedBy() {

	return eventOfferedBy;
    }

    public void setEventOfferedBy(BigInteger eventOfferedBy) {

	this.eventOfferedBy = eventOfferedBy;
    }

    public EventCuisineListing getEventCuisine() {

	return eventCuisine;
    }

    public void setEventCuisine(EventCuisineListing eventCuisine) {

	this.eventCuisine = eventCuisine;
    }

}
