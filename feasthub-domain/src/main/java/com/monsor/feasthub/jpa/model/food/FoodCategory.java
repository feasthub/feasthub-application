package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_category database table.
 * 
 */
@Entity
@Table(name = "food_category")
@NamedQueries({ @NamedQuery(name = "FoodCategory.findAll", query = "SELECT f FROM FoodCategory f"),
		@NamedQuery(name = "FoodCategory.searchFoodCategorysIgnoreCaseContaining", query = "SELECT f FROM FoodCategory f "
				+ "WHERE " + " f.keyId like :searchString " + " or " + " f.foodCategoryName like :searchString "),
		@NamedQuery(name = "findFirstByParentId", query = "SELECT f FROM FoodCategory f WHERE f.keyId = :parentId "),
		@NamedQuery(name = "FoodCategory.findDuplicate", query = "SELECT f FROM FoodCategory f "
				+ "WHERE f.foodCategoryName = :foodCategoryName or f.keyId = :keyId") })
public class FoodCategory implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_CATEGORY_ID")
	private BigInteger id;
	
	@JsonIgnore
	@Column(name = "FLAT_TAGS_FLAG")
	private Boolean flatTagsFlag;
	
	@Column(name = "FOOD_CATEGORY_CODE")
	private String foodCategoryCode;

	@JsonIgnore
	@Column(name = "FOOD_CATEGORY_DESC")
	private String foodCategoryDesc;

	@Column(name = "FOOD_CATEGORY_NAME")
	private String foodCategoryName;

	@JsonIgnore
	@Column(name = "KEY_ID")
	private String keyId;

	@JsonIgnore
	@Column(name = "PARENT_NODE_FLAG")
	private Boolean parentNodeFlag;
	
	
	@JsonIgnore
	@Column(name = "PARENT_ID")
	private String parentId;

	public FoodCategory() {
	}

	public Boolean getFlatTagsFlag() {
		return this.flatTagsFlag;
	}

	public void setFlatTagsFlag(Boolean flatTagsFlag) {
		this.flatTagsFlag = flatTagsFlag;
	}

	public String getFoodCategoryCode() {
		return this.foodCategoryCode;
	}

	public void setFoodCategoryCode(String foodCategoryCode) {
		this.foodCategoryCode = foodCategoryCode;
	}

	public String getFoodCategoryDesc() {
		return this.foodCategoryDesc;
	}

	public void setFoodCategoryDesc(String foodCategoryDesc) {
		this.foodCategoryDesc = foodCategoryDesc;
	}

	public String getFoodCategoryName() {
		return this.foodCategoryName;
	}

	public void setFoodCategoryName(String foodCategoryName) {
		this.foodCategoryName = foodCategoryName;
	}

	public String getKeyId() {
		return this.keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public Boolean getParentNodeFlag() {
		if(parentNodeFlag == true){
			this.parentId = null;
		}
		return parentNodeFlag;
	}

	public void setParentNodeFlag(Boolean parentNodeFlag) {
		this.parentNodeFlag = parentNodeFlag;
	}

	public String getParentId() {
		return this.parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}
}