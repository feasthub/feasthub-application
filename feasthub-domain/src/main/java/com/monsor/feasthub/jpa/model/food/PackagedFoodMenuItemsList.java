package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;

/**
 * The persistent class for the packaged_food_menu_items_list database table.
 * 
 */
@Entity
@Table(name = "packaged_food_menu_items_list")
@NamedQuery(name = "PackagedFoodMenuItemsList.findAll", query = "SELECT p FROM PackagedFoodMenuItemsList p")
public class PackagedFoodMenuItemsList implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PACKAGED_FOOD_MENU_ITEMS_LIST_ID")
	private BigInteger id;

	@OneToOne
	@JoinColumn(name = "ADDITIONAL_FOOD_MENU_ID")
	private FoodMenuItemView additionalFoodMenuId;

	@Column(name = "ALTERNATIVE_OPTIONS_AVAILABILITY_FLAG")
	private Boolean alternativeOptionsAvailabilityFlag;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "packagedFoodMenuItemsListId")
	private List<PackagedFoodItemToAlternativeOptionItemList> optionalFoodMenuItemList;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "PACKAGED_FOOD_MENU_ID")
	private FoodMenuItem foodMenuId;

	public PackagedFoodMenuItemsList() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public FoodMenuItemView getAdditionalFoodMenuId() {
		return this.additionalFoodMenuId;
	}

	public void setAdditionalFoodMenuId(FoodMenuItemView additionalFoodMenuId) {
		this.additionalFoodMenuId = additionalFoodMenuId;
	}

	public Boolean getAlternativeOptionsAvailabilityFlag() {
		return alternativeOptionsAvailabilityFlag;
	}

	public void setAlternativeOptionsAvailabilityFlag(Boolean alternativeOptionsAvailabilityFlag) {
		this.alternativeOptionsAvailabilityFlag = alternativeOptionsAvailabilityFlag;
	}

	public List<PackagedFoodItemToAlternativeOptionItemList> getOptionalFoodMenuItemList() {
		return optionalFoodMenuItemList;
	}

	public void setOptionalFoodMenuItemList(
			List<PackagedFoodItemToAlternativeOptionItemList> optionalFoodMenuItemList) {
		this.optionalFoodMenuItemList = optionalFoodMenuItemList;
	}

	public FoodMenuItem getFoodMenuId() {
		return foodMenuId;
	}

	public void setFoodMenuId(FoodMenuItem foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

}