package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the oauth2_token database table.
 * 
 */
@Entity
@Table(name = "oauth2_token")
@NamedQuery(name = "Oauth2Token.findAll", query = "SELECT o FROM Oauth2Token o")
public class Oauth2Token implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "OAUTH2_TOKEN_ID")
	private BigInteger id;

	@Column(name = "AUTH_SERVER_NAME")
	private String authServerName;

	@Column(name = "EXTERNAL_ACCESS_TOKEN")
	private String externalAccessToken;

	@Column(name = "EXTERNAL_ACCESS_TOKEN_EXPIRES_IN")
	private String externalAccessTokenExpiresIn;

	@Column(name = "FSHB_ACCESS_TOKEN")
	private String fshbAccessToken;

	@Column(name = "FSHB_ACCESS_TOKEN_EXPIRES_IN")
	private String fshbAccessTokenExpiresIn;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public Oauth2Token() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getAuthServerName() {
		return this.authServerName;
	}

	public void setAuthServerName(String authServerName) {
		this.authServerName = authServerName;
	}

	public String getExternalAccessToken() {
		return this.externalAccessToken;
	}

	public void setExternalAccessToken(String externalAccessToken) {
		this.externalAccessToken = externalAccessToken;
	}

	public String getExternalAccessTokenExpiresIn() {
		return this.externalAccessTokenExpiresIn;
	}

	public void setExternalAccessTokenExpiresIn(String externalAccessTokenExpiresIn) {
		this.externalAccessTokenExpiresIn = externalAccessTokenExpiresIn;
	}

	public String getFshbAccessToken() {
		return this.fshbAccessToken;
	}

	public void setFshbAccessToken(String fshbAccessToken) {
		this.fshbAccessToken = fshbAccessToken;
	}

	public String getFshbAccessTokenExpiresIn() {
		return this.fshbAccessTokenExpiresIn;
	}

	public void setFshbAccessTokenExpiresIn(String fshbAccessTokenExpiresIn) {
		this.fshbAccessTokenExpiresIn = fshbAccessTokenExpiresIn;
	}

}