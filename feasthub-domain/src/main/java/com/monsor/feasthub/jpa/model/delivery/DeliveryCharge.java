package com.monsor.feasthub.jpa.model.delivery;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.BusinessTypes;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the delivery_charge database table.
 * 
 */
@Entity
@Table(name = "delivery_charge")
@NamedQueries({ @NamedQuery(name = "DeliveryCharge.findAll", query = "SELECT d FROM DeliveryCharge d"),
		@NamedQuery(name = "DeliveryCharge.findDuplicate", query = "SELECT f FROM DeliveryCharge f " + "WHERE "
				+ " f.businessTypes = :businessTypes and" + " f.cityCode = :cityCode and "
				+ " f.countryCode = :countryCode and" + " f.distanceLowerRange = :distanceLowerRange and"
				+ " f.distanceUpperRange = :distanceUpperRange ") })
public class DeliveryCharge implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@CheckForDuplicate
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DELIVERY_CHARGE_ID")
	private BigInteger id;

	@Required
	@CheckForDuplicate
	@Column(name = "BUSINESS_TYPES")
	private BusinessTypes businessTypes;

	@Required
	@CheckForDuplicate
	@Column(name = "CITY_CODE")
	private String cityCode;

	@Required
	@CheckForDuplicate
	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Required
	@CheckForDuplicate
	@Column(name = "DISTANCE_LOWER_RANGE")
	private int distanceLowerRange;

	@Required
	@CheckForDuplicate
	@Column(name = "DISTANCE_UPPER_RANGE")
	private int distanceUpperRange;

	@Column(name = "FEES_APPLCABILITY")
	private String feesApplcability;

	@Column(name = "PRICE_IN_CASH")
	private int priceInCash;

	@Column(name = "PRICE_IN_FP")
	private int priceInFp;

	@Column(name = "SERVICES_OFFERING_TYPES")
	private ServiceOfferingTypes servicesOfferingTypes;

	@Column(name = "STATE_CODE")
	private String stateCode;

	public DeliveryCharge() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getDistanceLowerRange() {
		return this.distanceLowerRange;
	}

	public void setDistanceLowerRange(int distanceLowerRange) {
		this.distanceLowerRange = distanceLowerRange;
	}

	public int getDistanceUpperRange() {
		return this.distanceUpperRange;
	}

	public void setDistanceUpperRange(int distanceUpperRange) {
		this.distanceUpperRange = distanceUpperRange;
	}

	public String getFeesApplcability() {
		return this.feesApplcability;
	}

	public void setFeesApplcability(String feesApplcability) {
		this.feesApplcability = feesApplcability;
	}

	public int getPriceInCash() {
		return this.priceInCash;
	}

	public void setPriceInCash(int priceInCash) {
		this.priceInCash = priceInCash;
	}

	public int getPriceInFp() {
		return this.priceInFp;
	}

	public void setPriceInFp(int priceInFp) {
		this.priceInFp = priceInFp;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public ServiceOfferingTypes getServicesOfferingTypes() {
		return servicesOfferingTypes;
	}

	public void setServicesOfferingTypes(ServiceOfferingTypes servicesOfferingTypes) {
		this.servicesOfferingTypes = servicesOfferingTypes;
	}

	public BusinessTypes getBusinessTypes() {
		return businessTypes;
	}

	public void setBusinessTypes(BusinessTypes businessTypes) {
		this.businessTypes = businessTypes;
	}

}