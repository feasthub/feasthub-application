package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;

/**
 * The persistent class for the user_feast_point_detail database table.
 * 
 */
@Entity
@Table(name = "user_feast_point_detail")
@NamedQueries({ @NamedQuery(name = "UserFeastPointDetail.findAll", query = "SELECT u FROM UserFeastPointDetail u"),
		@NamedQuery(name = "UserFeastPointDetail.findDuplicate", query = "SELECT f FROM UserFeastPointDetail f "
				+ "WHERE " + " f.couponCode = :couponCode ") })
public class UserFeastPointDetail implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "FP_ID")
	private BigInteger id;

	@Column(name = "FEAST_POINT_BALANCE_ID")
	private BigInteger feastPointBalanceId;

	@Column(name = "USER_ID")
	private BigInteger userId;

	@Column(name = "COUPNE_CODE_EXPIRED")
	private String coupneCodeExpired;

	@CheckForDuplicate
	@Column(name = "COUPON_CODE")
	private String couponCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "COUPON_CODE_EXPIRY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date couponCodeExpiryDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "FP_EXPIRY_DATES")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date fpExpiryDates;

	@Column(name = "FP_POINTS")
	private int fpPoints;

	@Temporal(TemporalType.DATE)
	@Column(name = "PROMOTION_EXPIRY_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date promotionExpiryDate;

	@Column(name = "PROMOTION_EXPIRY_FLAG")
	private String promotionExpiryFlag;

	@Column(name = "PROMOTION_EXPIRY_TIME")
	private Time promotionExpiryTime;

	@Temporal(TemporalType.DATE)
	@Column(name = "PROMOTIONS_START_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date promotionsStartDate;

	@Column(name = "PROMOTIONS_START_TIME")
	private Time promotionsStartTime;

	public UserFeastPointDetail() {
	}

	public String getCoupneCodeExpired() {
		return this.coupneCodeExpired;
	}

	public void setCoupneCodeExpired(String coupneCodeExpired) {
		this.coupneCodeExpired = coupneCodeExpired;
	}

	public String getCouponCode() {
		return this.couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public Date getCouponCodeExpiryDate() {
		return this.couponCodeExpiryDate;
	}

	public void setCouponCodeExpiryDate(Date couponCodeExpiryDate) {
		this.couponCodeExpiryDate = couponCodeExpiryDate;
	}

	public Date getFpExpiryDates() {
		return this.fpExpiryDates;
	}

	public void setFpExpiryDates(Date fpExpiryDates) {
		this.fpExpiryDates = fpExpiryDates;
	}

	public int getFpPoints() {
		return this.fpPoints;
	}

	public void setFpPoints(int fpPoints) {
		this.fpPoints = fpPoints;
	}

	public Date getPromotionExpiryDate() {
		return this.promotionExpiryDate;
	}

	public void setPromotionExpiryDate(Date promotionExpiryDate) {
		this.promotionExpiryDate = promotionExpiryDate;
	}

	public String getPromotionExpiryFlag() {
		return this.promotionExpiryFlag;
	}

	public void setPromotionExpiryFlag(String promotionExpiryFlag) {
		this.promotionExpiryFlag = promotionExpiryFlag;
	}

	public Time getPromotionExpiryTime() {
		return this.promotionExpiryTime;
	}

	public void setPromotionExpiryTime(Time promotionExpiryTime) {
		this.promotionExpiryTime = promotionExpiryTime;
	}

	public Date getPromotionsStartDate() {
		return this.promotionsStartDate;
	}

	public void setPromotionsStartDate(Date promotionsStartDate) {
		this.promotionsStartDate = promotionsStartDate;
	}

	public Time getPromotionsStartTime() {
		return this.promotionsStartTime;
	}

	public void setPromotionsStartTime(Time promotionsStartTime) {
		this.promotionsStartTime = promotionsStartTime;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFeastPointBalanceId() {
		return feastPointBalanceId;
	}

	public void setFeastPointBalanceId(BigInteger feastPointBalanceId) {
		this.feastPointBalanceId = feastPointBalanceId;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}
}