package com.monsor.feasthub.jpa.model.user;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the offerer_history_favourite_items database table.
 * 
 */
@Entity
@Table(name = "offerer_history_favourite_items")
@NamedQuery(name = "OffererHistoryFavouriteItem.findAll", query = "SELECT o FROM OffererHistoryFavouriteItem o")
public class OffererHistoryFavouriteItem implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "OFFEREE_HISTORY_FAVOURITE_ITEM_ID")
	private BigInteger id;

	@Column(name = "FAVORITE_FLAG")
	private String favoriteFlag;

	@Column(name = "FOOD_MENU_ID")
	private BigInteger foodMenuId;

	@Temporal(TemporalType.DATE)
	@Column(name = "LAST_ORDER_DATE")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy", timezone="GMT+5:30") private Date lastOrderDate;

	@Column(name = "NO_OF_TIMES_OFFERED")
	private int noOfTimesOffered;

	@Column(name = "USER_ID")
	private BigInteger userId;

	public OffererHistoryFavouriteItem() {
	}

	public String getFavoriteFlag() {
		return this.favoriteFlag;
	}

	public void setFavoriteFlag(String favoriteFlag) {
		this.favoriteFlag = favoriteFlag;
	}

	public BigInteger getFoodMenuId() {
		return this.foodMenuId;
	}

	public void setFoodMenuId(BigInteger foodMenuId) {
		this.foodMenuId = foodMenuId;
	}

	public Date getLastOrderDate() {
		return this.lastOrderDate;
	}

	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}

	public int getNoOfTimesOffered() {
		return this.noOfTimesOffered;
	}

	public void setNoOfTimesOffered(int noOfTimesOffered) {
		this.noOfTimesOffered = noOfTimesOffered;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

}