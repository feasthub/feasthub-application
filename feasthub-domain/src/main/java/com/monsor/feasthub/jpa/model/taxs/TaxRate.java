package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the tax_rates database table.
 * 
 */
@Entity
@Table(name = "tax_rates")
@NamedQuery(name = "TaxRate.findAll", query = "SELECT t FROM TaxRate t")
public class TaxRate implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAX_RATES_ID")
	private BigInteger id;

	@Column(name = "STATE_TAX_APPLICABILITY")
	private String stateTaxApplicability;

	@Column(name = "STATE_TAX_RATE")
	private double stateTaxRate;

	@Column(name = "STATE_TAX_TYPE")
	private String stateTaxType;

	public TaxRate() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getStateTaxApplicability() {
		return this.stateTaxApplicability;
	}

	public void setStateTaxApplicability(String stateTaxApplicability) {
		this.stateTaxApplicability = stateTaxApplicability;
	}

	public double getStateTaxRate() {
		return this.stateTaxRate;
	}

	public void setStateTaxRate(double stateTaxRate) {
		this.stateTaxRate = stateTaxRate;
	}

	public String getStateTaxType() {
		return this.stateTaxType;
	}

	public void setStateTaxType(String stateTaxType) {
		this.stateTaxType = stateTaxType;
	}

}