package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the food_serving_category database table.
 * 
 */
@Entity
@Table(name = "food_menu_serving_category")
@NamedQuery(name = "FoodMenuServingCategory.findAll", query = "SELECT f FROM FoodMenuServingCategory f")
public class FoodMenuServingCategory implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_SERVING_SIZE_CATEGORY_TYPE_ID")
	private BigInteger id;

	@Column(name = "FOOD_SERVING_CATEGORY_NAME")
	private String foodServingCategoryName;

	@Column(name = "ONE_SERVING_UNIT_QUANTITY")
	private int oneServingCategoryName;

	@Column(name = "MEASURE_UNITS")
	private String measureUnits;

	public FoodMenuServingCategory() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getFoodServingCategoryName() {
		return this.foodServingCategoryName;
	}

	public void setFoodServingCategoryName(String foodServingCategoryName) {
		this.foodServingCategoryName = foodServingCategoryName;
	}

	public int getOneServingCategoryName() {
		return oneServingCategoryName;
	}

	public void setOneServingCategoryName(int oneServingCategoryName) {
		this.oneServingCategoryName = oneServingCategoryName;
	}

	public String getMeasureUnite() {
		return measureUnits;
	}

	public void setMeasureUnite(String measureUnits) {
		this.measureUnits = measureUnits;
	}

}