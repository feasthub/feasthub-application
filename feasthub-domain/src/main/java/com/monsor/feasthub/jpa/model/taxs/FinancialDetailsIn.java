package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the financial_details_in database table.
 * 
 */
@Entity
@Table(name = "user_financial_details_in")
@NamedQuery(name = "FinancialDetailsIn.findAll", query = "SELECT f FROM FinancialDetailsIn f")
public class FinancialDetailsIn implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FINANCIAL_DETAIL_ID")
	private BigInteger id;

	@Column(name = "BANK_ACCOUNT_HOLDER_NAME_1")
	private String bankAccountHolderName1;

	@Column(name = "BANK_ACCOUNT_HOLDER_NAME_2")
	private String bankAccountHolderName2;

	@Column(name = "BANK_ACCOUNT_NUMBER_1")
	private String bankAccountNumber1;

	@Column(name = "BANK_ACCOUNT_NUMBER_2")
	private String bankAccountNumber2;

	@Column(name = "BANK_AUTO_TRANSFER_ACTIVE_FLAG")
	private String bankAutoTransferActiveFlag;

	@Column(name = "BANK_BRANCH_1")
	private String bankBranch1;

	@Column(name = "BANK_BRANCH_2")
	private String bankBranch2;

	@Column(name = "BANK_CITY_1")
	private String bankCity1;

	@Column(name = "BANK_CITY_2")
	private String bankCity2;

	@Column(name = "BANK_IFSC_CODE_1")
	private String bankIfscCode1;

	@Column(name = "BANK_IFSC_CODE_2")
	private String bankIfscCode2;

	@Column(name = "BANK_IS_DEFAULT_ACCOUNT_1_FLAG")
	private String bankIsDefaultAccount1Flag;

	@Column(name = "BANK_IS_DEFAULT_ACCOUNT_2_FLAG")
	private String bankIsDefaultAccount2Flag;

	@Column(name = "BANK_NAME_1")
	private String bankName1;

	@Column(name = "BANK_NAME_2")
	private String bankName2;

	@Column(name = "BANK_VERIFIED_1")
	private String bankVerified1;

	@Column(name = "BANK_VERIFIED_2")
	private String bankVerified2;

	@Column(name = "CURRENCY_CODE")
	private String currencyCode;

	@Column(name = "DEFAULT_OPERATIONAL_ADDRESS_ID")
	private BigInteger defaultOperationalAddressId;

	@Column(name = "ENTERPRISE_NAME")
	private String enterpriseName;

	@Column(name = "KYC_COMPLIANT_FLAG")
	private String kycCompliantFlag;

	@Column(name = "OTHER_ASSOCIATED_ADDRESS_ID")
	private BigInteger otherAssociatedAddressId;

	@Column(name = "PAN")
	private String pan;

	@Column(name = "TIN")
	private String tin;

	@Column(name = "TIN_OPERATION_ADDRESS_ID")
	private BigInteger tinOperationAddressId;

	@Column(name = "USER_ID")
	private BigInteger userId;

	@Column(name = "VAT")
	private String vat;

	public FinancialDetailsIn() {
	}

	public String getBankAccountHolderName1() {
		return this.bankAccountHolderName1;
	}

	public void setBankAccountHolderName1(String bankAccountHolderName1) {
		this.bankAccountHolderName1 = bankAccountHolderName1;
	}

	public String getBankAccountHolderName2() {
		return this.bankAccountHolderName2;
	}

	public void setBankAccountHolderName2(String bankAccountHolderName2) {
		this.bankAccountHolderName2 = bankAccountHolderName2;
	}

	public String getBankAccountNumber1() {
		return this.bankAccountNumber1;
	}

	public void setBankAccountNumber1(String bankAccountNumber1) {
		this.bankAccountNumber1 = bankAccountNumber1;
	}

	public String getBankAccountNumber2() {
		return this.bankAccountNumber2;
	}

	public void setBankAccountNumber2(String bankAccountNumber2) {
		this.bankAccountNumber2 = bankAccountNumber2;
	}

	public String getBankAutoTransferActiveFlag() {
		return this.bankAutoTransferActiveFlag;
	}

	public void setBankAutoTransferActiveFlag(String bankAutoTransferActiveFlag) {
		this.bankAutoTransferActiveFlag = bankAutoTransferActiveFlag;
	}

	public String getBankBranch1() {
		return this.bankBranch1;
	}

	public void setBankBranch1(String bankBranch1) {
		this.bankBranch1 = bankBranch1;
	}

	public String getBankBranch2() {
		return this.bankBranch2;
	}

	public void setBankBranch2(String bankBranch2) {
		this.bankBranch2 = bankBranch2;
	}

	public String getBankCity1() {
		return this.bankCity1;
	}

	public void setBankCity1(String bankCity1) {
		this.bankCity1 = bankCity1;
	}

	public String getBankCity2() {
		return this.bankCity2;
	}

	public void setBankCity2(String bankCity2) {
		this.bankCity2 = bankCity2;
	}

	public String getBankIfscCode1() {
		return this.bankIfscCode1;
	}

	public void setBankIfscCode1(String bankIfscCode1) {
		this.bankIfscCode1 = bankIfscCode1;
	}

	public String getBankIfscCode2() {
		return this.bankIfscCode2;
	}

	public void setBankIfscCode2(String bankIfscCode2) {
		this.bankIfscCode2 = bankIfscCode2;
	}

	public String getBankIsDefaultAccount1Flag() {
		return this.bankIsDefaultAccount1Flag;
	}

	public void setBankIsDefaultAccount1Flag(String bankIsDefaultAccount1Flag) {
		this.bankIsDefaultAccount1Flag = bankIsDefaultAccount1Flag;
	}

	public String getBankIsDefaultAccount2Flag() {
		return this.bankIsDefaultAccount2Flag;
	}

	public void setBankIsDefaultAccount2Flag(String bankIsDefaultAccount2Flag) {
		this.bankIsDefaultAccount2Flag = bankIsDefaultAccount2Flag;
	}

	public String getBankName1() {
		return this.bankName1;
	}

	public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}

	public String getBankName2() {
		return this.bankName2;
	}

	public void setBankName2(String bankName2) {
		this.bankName2 = bankName2;
	}

	public String getBankVerified1() {
		return this.bankVerified1;
	}

	public void setBankVerified1(String bankVerified1) {
		this.bankVerified1 = bankVerified1;
	}

	public String getBankVerified2() {
		return this.bankVerified2;
	}

	public void setBankVerified2(String bankVerified2) {
		this.bankVerified2 = bankVerified2;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigInteger getDefaultOperationalAddressId() {
		return this.defaultOperationalAddressId;
	}

	public void setDefaultOperationalAddressId(BigInteger defaultOperationalAddressId) {
		this.defaultOperationalAddressId = defaultOperationalAddressId;
	}

	public String getEnterpriseName() {
		return this.enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getKycCompliantFlag() {
		return this.kycCompliantFlag;
	}

	public void setKycCompliantFlag(String kycCompliantFlag) {
		this.kycCompliantFlag = kycCompliantFlag;
	}

	public BigInteger getOtherAssociatedAddressId() {
		return this.otherAssociatedAddressId;
	}

	public void setOtherAssociatedAddressId(BigInteger otherAssociatedAddressId) {
		this.otherAssociatedAddressId = otherAssociatedAddressId;
	}

	public String getPan() {
		return this.pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public BigInteger getTinOperationAddressId() {
		return this.tinOperationAddressId;
	}

	public void setTinOperationAddressId(BigInteger tinOperationAddressId) {
		this.tinOperationAddressId = tinOperationAddressId;
	}

	public BigInteger getUserId() {
		return this.userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public String getVat() {
		return this.vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

}