package com.monsor.feasthub.jpa.model.app;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.Required;

/**
 * The persistent class for the business_types_to_services_mapping database
 * table.
 * 
 */
@Entity
@Table(name = "business_types_to_services_mapping")
@NamedQuery(name = "BusinessTypesToServicesMapping.findAll", query = "SELECT b FROM BusinessTypesToServicesMapping b")
public class BusinessTypesToServicesMapping implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "BUSINESS_TYPES_TO_SERVICE_MAPPING_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger id;

	@Required
	@Column(name = "ADDITIONAL_SERVICE_OFFERING")
	private String additionalServiceOffering;

	@Column(name = "BUSINESS_TYPES_TAGS")
	private String businessTypesTags;

	@Required
	@Column(name = "SERVICES_OFFERING_TYPES")
	private String servicesOfferingTypes;

	public BusinessTypesToServicesMapping() {
	}

	public String getAdditionalServiceOffering() {
		return this.additionalServiceOffering;
	}

	public void setAdditionalServiceOffering(String additionalServiceOffering) {
		this.additionalServiceOffering = additionalServiceOffering;
	}

	public String getBusinessTypesTags() {
		return this.businessTypesTags;
	}

	public void setBusinessTypesTags(String businessTypesTags) {
		this.businessTypesTags = businessTypesTags;
	}

	public String getServicesOfferingTypes() {
		return this.servicesOfferingTypes;
	}

	public void setServicesOfferingTypes(String servicesOfferingTypes) {
		this.servicesOfferingTypes = servicesOfferingTypes;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger mappingId) {
		this.id = mappingId;
	}

}