package com.monsor.feasthub.jpa.model.view;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

@Entity
@Table(name = "fh_user")
public class FhUserView implements Serializable, Identifiable {

    /**
     * 
     */
    private static final long serialVersionUID = -9035306046454668186L;

    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "DEFAULT_EMAIL_ID")
    private String defaultEmailId;

    @Column(name = "DEFAULT_LANDLINE_NUMBER")
    private String defaultLandlineNumber;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "PRIMARY_ROLE")
    private String primaryRole;

    @Column(name = "PROFILE_PHOTO")
    private String profilePhoto;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "DEVICE_ID")
    private String deviceId;

    @Column(name = "DEVICE_MAKE")
    private String deviceMake;

    @Override
    public BigInteger getId() {

	return id;
    }

    @Override
    public void setId(BigInteger id) {

	this.id = id;
    }

    public String getDefaultEmailId() {

	return defaultEmailId;
    }

    public void setDefaultEmailId(String defaultEmailId) {

	this.defaultEmailId = defaultEmailId;
    }

    public String getDefaultLandlineNumber() {

	return defaultLandlineNumber;
    }

    public void setDefaultLandlineNumber(String defaultLandlineNumber) {

	this.defaultLandlineNumber = defaultLandlineNumber;
    }

    public String getFirstName() {

	return firstName;
    }

    public void setFirstName(String firstName) {

	this.firstName = firstName;
    }

    public String getLastName() {

	return lastName;
    }

    public void setLastName(String lastName) {

	this.lastName = lastName;
    }

    public String getMiddleName() {

	return middleName;
    }

    public void setMiddleName(String middleName) {

	this.middleName = middleName;
    }

    public String getPrimaryRole() {

	return primaryRole;
    }

    public void setPrimaryRole(String primaryRole) {

	this.primaryRole = primaryRole;
    }

    public String getProfilePhoto() {

	return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {

	this.profilePhoto = profilePhoto;
    }

    public String getCompanyName() {

	return companyName;
    }

    public void setCompanyName(String companyName) {

	this.companyName = companyName;
    }

    public String getUserName() {

	return userName;
    }

    public void setUserName(String userName) {

	this.userName = userName;
    }

    
    public String getDeviceId() {
    
        return deviceId;
    }

    
    public void setDeviceId(String deviceId) {
    
        this.deviceId = deviceId;
    }

    
    public String getDeviceMake() {
    
        return deviceMake;
    }

    
    public void setDeviceMake(String deviceMake) {
    
        this.deviceMake = deviceMake;
    }

}
