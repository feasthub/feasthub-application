package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Required;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the taxes_structure_in database table.
 * 
 */
@Entity
@Table(name = "taxes_structure_in")
@NamedQueries({ @NamedQuery(name = "TaxesStructureIn.findAll", query = "SELECT t FROM TaxesStructureIn t"),
		@NamedQuery(name = "TaxesStructureIn.searchTaxesStructureInsIgnoreCase", query = "SELECT f FROM TaxesStructureIn f "
				+ "WHERE "
				+ " f.businessTypes = :searchString or  f.countryCode = :searchString  or  f.servicesOfferingTypes = :searchString or"
				+ " f.stateCode = :searchString "),
		@NamedQuery(name = "TaxesStructureIn.findDuplicate", query = "SELECT f FROM TaxesStructureIn f " + "WHERE "
				+ " f.businessTypes = :businessTypes and  f.countryCode = :countryCode  and  f.servicesOfferingTypes = :servicesOfferingTypes and"
				+ " f.stateCode = :stateCode ") })
public class TaxesStructureIn implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TAX_STRUCTURE_ID")
	private BigInteger id;

	@Required
	@Searchable
	@CheckForDuplicate
	@Column(name = "BUSINESS_TYPES")
	private String businessTypes;

	@Required
	@Searchable
	@CheckForDuplicate
	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Required
	@Searchable
	@CheckForDuplicate
	@Column(name = "SERVICES_OFFERING_TYPES")
	private String servicesOfferingTypes;

	@Required
	@Searchable
	@CheckForDuplicate
	@Column(name = "STATE_CODE")
	private String stateCode;

	@Column(name = "TAX_NAME")
	private String taxName;

	@Column(name = "TAX_TYPE")
	private String taxType;

	public TaxesStructureIn() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getBusinessTypes() {
		return this.businessTypes;
	}

	public void setBusinessTypes(String businessTypes) {
		this.businessTypes = businessTypes;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getServicesOfferingTypes() {
		return this.servicesOfferingTypes;
	}

	public void setServicesOfferingTypes(String servicesOfferingTypes) {
		this.servicesOfferingTypes = servicesOfferingTypes;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
}