package com.monsor.feasthub.jpa.model.taxs;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;

/**
 * The persistent class for the tax_structure_tax_rates database table.
 * 
 */
@Entity
@Table(name = "tax_structure_tax_rates")
@NamedQuery(name = "TaxStructureTaxRate.findAll", query = "SELECT t FROM TaxStructureTaxRate t")
public class TaxStructureTaxRate implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "TAX_STRUCTURE_TAX_RATE_ID")
	private BigInteger id;

	@Column(name = "TAX_RATE_ID")
	private BigInteger taxRateId;

	@Column(name = "TAX_STRUCTURE_ID")
	private BigInteger taxStructureId;

	public TaxStructureTaxRate() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getTaxRateId() {
		return this.taxRateId;
	}

	public void setTaxRateId(BigInteger taxRateId) {
		this.taxRateId = taxRateId;
	}

	public BigInteger getTaxStructureId() {
		return this.taxStructureId;
	}

	public void setTaxStructureId(BigInteger taxStructureId) {
		this.taxStructureId = taxStructureId;
	}

}