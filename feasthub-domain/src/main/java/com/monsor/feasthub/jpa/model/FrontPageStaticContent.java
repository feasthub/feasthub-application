package com.monsor.feasthub.jpa.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.enums.StaticContent;

@Entity
@Table(name = "front_page_static_content")
@NamedQuery(name = "FrontPageStaticContent.findAll", query = "SELECT f FROM FrontPageStaticContent f")
public class FrontPageStaticContent implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name = "id")
	private BigInteger id;

	@Column(name="is_html")
	private boolean isHtml;
	
	@Enumerated
	@Column(name="page_id")
	private StaticContent pageId;
	
	
	@Column(name="page_content")
	private String pageContent;

	public BigInteger getId() {
		return id;
	}


	public void setId(BigInteger id) {
		this.id = id;
	}


	public boolean isHtml() {
		return isHtml;
	}


	public void setHtml(boolean isHtml) {
		this.isHtml = isHtml;
	}


	public StaticContent getPageId() {
		return pageId;
	}


	public void setPageId(StaticContent pageId) {
		this.pageId = pageId;
	}


	public String getPageContent() {
		return pageContent;
	}


	public void setPageContent(String pageContent) {
		this.pageContent = pageContent;
	}


}
