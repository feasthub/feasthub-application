package com.monsor.feasthub.jpa.model.food;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.monsor.feasthub.jpa.model.common.Identifiable;
import com.monsor.feasthub.jpa.model.framework.CheckForDuplicate;
import com.monsor.feasthub.jpa.model.framework.Searchable;

/**
 * The persistent class for the food_ingredients database table.
 * 
 */
@Entity
@Table(name = "food_ingredients")
@NamedQueries({
		@NamedQuery(name = "FoodIngredient.searchFoodIngredientsIgnoreCase", query = "SELECT f FROM FoodIngredient f "
				+ "WHERE " + " f.foodReceipeId = :searchString "),
		@NamedQuery(name = "findByfoodReceipeId", query = "SELECT f FROM FoodIngredient f " + "WHERE "
				+ " f.foodReceipeId = :foodReceipeId "),
		@NamedQuery(name = "FoodIngredient.findByfoodReceipeId", query = "SELECT f FROM FoodIngredient f " + "WHERE "
				+ " f.foodReceipeId = :foodReceipeId ") })
public class FoodIngredient implements Serializable, Identifiable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FOOD_INGREDIENT_ID")
	private BigInteger id;

	@CheckForDuplicate
	@Searchable
	@Column(name = "FOOD_RECEIPE_ID")
	private BigInteger foodReceipeId;

	@Column(name = "PRIMARY_INGREDIENTS_LIST")
	private String primaryIngredientsList;

	@Column(name = "SECONDARY_INGREDIENT_LIST")
	private String secondaryIngredientList;

	@Column(name = "SPICES_LIST")
	private String spicesList;

	public FoodIngredient() {
	}

	@Override
	public BigInteger getId() {
		return id;
	}

	@Override
	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getFoodReceipeId() {
		return this.foodReceipeId;
	}

	public void setFoodReceipeId(BigInteger foodReceipeId) {
		this.foodReceipeId = foodReceipeId;
	}

	public String getPrimaryIngredientsList() {
		return this.primaryIngredientsList;
	}

	public void setPrimaryIngredientsList(String primaryIngredientsList) {
		this.primaryIngredientsList = primaryIngredientsList;
	}

	public String getSecondaryIngredientList() {
		return this.secondaryIngredientList;
	}

	public void setSecondaryIngredientList(String secondaryIngredientList) {
		this.secondaryIngredientList = secondaryIngredientList;
	}

	public String getSpicesList() {
		return this.spicesList;
	}

	public void setSpicesList(String spicesList) {
		this.spicesList = spicesList;
	}

}