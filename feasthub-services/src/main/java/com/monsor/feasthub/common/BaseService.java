package com.monsor.feasthub.common;

import java.math.BigInteger;
import java.util.List;

import com.monsor.feasthub.jpa.model.common.Identifiable;

// Mocked implementation
public interface BaseService<T extends Identifiable> {

	public T findById(BigInteger id);

	public List<T> findAll();

	public T create(T entity);

	public T update(T entity);

	public void delete(BigInteger id);

}
