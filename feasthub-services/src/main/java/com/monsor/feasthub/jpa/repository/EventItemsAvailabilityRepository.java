package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.events.EventItemsAvailability;

@Repository
public interface EventItemsAvailabilityRepository
		extends CrudRepository<EventItemsAvailability, BigInteger>, JpaSpecificationExecutor<EventItemsAvailability> {

	@Nonnull
	public List<EventItemsAvailability> searchEventItemsAvailabilitysIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public EventItemsAvailability findDuplicate(@Nullable final BigInteger eventid, @Nullable final BigInteger foodid);

	@Nullable
	public EventItemsAvailability findByEventId(@Nullable final BigInteger eventId);

	@Nullable
	public EventItemsAvailability findByEventIdAndListingDate(@Nonnull final BigInteger eventId,
			@Nonnull final Date listingDate);

}
