package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.user.UserHistoryFavouriteItem;

@Repository
public interface UserHistoryFavouriteItemRepository extends CrudRepository<UserHistoryFavouriteItem, BigInteger>,
		JpaSpecificationExecutor<UserHistoryFavouriteItem> {

	@Nonnull
	public List<UserHistoryFavouriteItem> searchUserHistoryFavouriteItemIgnoreCase(@Nonnull final String searchString);

	@Nonnull
	public List<UserHistoryFavouriteItem> findHistoryyByUserId(BigInteger userId);

	@Nonnull
	public List<UserHistoryFavouriteItem> findFavByUserIdAndFavoriteFlagTrue(BigInteger userId);

}
