package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.user.FhUser;

@Repository
public interface UserRepository extends CrudRepository<FhUser, BigInteger> {
	@Nonnull
	public List<FhUser> findAllByUserNameOrDefaultEmailIdOrDefaultMobileNumber(@Nullable final String username,
			@Nullable final String emailid, @Nullable final String mobileNumber);

	@Nullable
	public FhUser findByUserNameAndPassword(@Nullable final String username, @Nullable final String password);

	@Nullable
	public FhUser findByUserName(@Nullable final String username);

	@Nullable
	public FhUser findByDefaultMobileNumber(@Nullable final String defaultMobileNumber);
}
