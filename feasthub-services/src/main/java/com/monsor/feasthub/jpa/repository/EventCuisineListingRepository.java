package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.events.EventCuisineListing;

@Repository
public interface EventCuisineListingRepository
		extends CrudRepository<EventCuisineListing, BigInteger>, JpaSpecificationExecutor<EventCuisineListing> {
	@Nullable
	public EventCuisineListing findByEventId(@Nullable final BigInteger eventId);

}
