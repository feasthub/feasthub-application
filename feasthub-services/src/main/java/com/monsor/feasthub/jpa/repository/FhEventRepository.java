package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.enums.BusinessTypes;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.events.FhEvent;

@Repository
public interface FhEventRepository extends CrudRepository<FhEvent, BigInteger>, JpaSpecificationExecutor<FhEvent> {

	@Nonnull
	public List<FhEvent> searchFhEventsIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public FhEvent findByBusinessType(BusinessTypes businessType);

	@Nullable
	public List<FhEvent> findByServiceOfferingType(ServiceOfferingTypes serviceType);

	@Nullable
	public FhEvent findOneByEventNameIgnoreCase(String eventName);

}
