package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.IngredientsListRepository;

/**
 * @author Amit
 *
 */

@Component
public class IngredientsListService {
	private final IngredientsListRepository ingredientslistRepository;

	@Autowired
	IngredientsListService(@Nonnull final IngredientsListRepository ingredientslistRepository) {
		this.ingredientslistRepository = ingredientslistRepository;
	}

	@Nonnull
	public IngredientsListRepository getIngredientsListRepository() {
		return ingredientslistRepository;
	}
}
