package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FinancialDetailsInRepository;

/**
 * @author Amit
 *
 */

@Component
public class FinancialDetailsInService {
	private final FinancialDetailsInRepository financialdetailsinRepository;

	@Autowired
	FinancialDetailsInService(@Nonnull final FinancialDetailsInRepository financialdetailsinRepository) {
		this.financialdetailsinRepository = financialdetailsinRepository;
	}

	@Nonnull
	public FinancialDetailsInRepository getFinancialDetailsInRepository() {
		return financialdetailsinRepository;
	}
}
