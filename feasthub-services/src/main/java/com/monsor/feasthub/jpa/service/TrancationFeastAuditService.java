package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.TransactionFeastAuditRepository;

@Component
public class TrancationFeastAuditService {
	private final TransactionFeastAuditRepository cashAuditRepository;

	@Autowired
	TrancationFeastAuditService(@Nonnull final TransactionFeastAuditRepository cashAuditRepository) {
		this.cashAuditRepository = cashAuditRepository;
	}

	@Nonnull
	public TransactionFeastAuditRepository getTrancationFeastAuditRepository() {
		return cashAuditRepository;
	}
}
