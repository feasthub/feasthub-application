package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.taxs.ApplicableStateTaxesIn;

@Repository
public interface ApplicableStateTaxesInRepository
		extends CrudRepository<ApplicableStateTaxesIn, BigInteger>, JpaSpecificationExecutor<ApplicableStateTaxesIn> {

	@Nullable
	public ApplicableStateTaxesIn findDuplicate(
			@PathParam(value = "countrycode") @Nullable final java.lang.String countrycode,

			@PathParam(value = "statecode") @Nullable final java.lang.String statecode,

			@PathParam(value = "statetaxrate") @Nullable final double statetaxrate,

			@PathParam(value = "statetaxtype") @Nullable final java.lang.String statetaxtype

	);
}
