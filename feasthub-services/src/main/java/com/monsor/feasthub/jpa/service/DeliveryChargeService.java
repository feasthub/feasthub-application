package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.DeliveryChargeRepository;

/**
 * @author Amit
 *
 */

@Component
public class DeliveryChargeService {
	private final DeliveryChargeRepository deliverychargeRepository;

	@Autowired
	DeliveryChargeService(@Nonnull final DeliveryChargeRepository deliverychargeRepository) {
		this.deliverychargeRepository = deliverychargeRepository;
	}

	@Nonnull
	public DeliveryChargeRepository getDeliveryChargeRepository() {
		return deliverychargeRepository;
	}
}
