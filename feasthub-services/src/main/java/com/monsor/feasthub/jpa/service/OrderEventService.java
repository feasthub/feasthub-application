package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderEventRepository;

@Component
public class OrderEventService {
	private final OrderEventRepository orderEventRepository;

	@Autowired
	OrderEventService(@Nonnull final OrderEventRepository orderEventRepository) {
		this.orderEventRepository = orderEventRepository;
	}

	@Nonnull
	public OrderEventRepository getOrderEventRepository() {
		return orderEventRepository;
	}
}
