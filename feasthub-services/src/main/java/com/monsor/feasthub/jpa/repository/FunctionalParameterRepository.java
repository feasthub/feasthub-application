package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.fuctional.FunctionalParameter;

@Repository
public interface FunctionalParameterRepository
		extends CrudRepository<FunctionalParameter, BigInteger>, JpaSpecificationExecutor<FunctionalParameter> {

	@Nullable
	public FunctionalParameter findByFunctionalParametersKey(String functionalparameterskey);
}
