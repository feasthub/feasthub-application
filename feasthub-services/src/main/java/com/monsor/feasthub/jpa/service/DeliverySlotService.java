package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.DeliverySlotRepository;

/**
 * @author Amit
 *
 */

@Component
public class DeliverySlotService {
	private final DeliverySlotRepository deliveryslotRepository;

	@Autowired
	DeliverySlotService(@Nonnull final DeliverySlotRepository deliveryslotRepository) {
		this.deliveryslotRepository = deliveryslotRepository;
	}

	@Nonnull
	public DeliverySlotRepository getDeliverySlotRepository() {
		return deliveryslotRepository;
	}
}
