package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.delivery.DeliveryCharge;

@Repository
public interface DeliveryChargeRepository
		extends CrudRepository<DeliveryCharge, BigInteger>, JpaSpecificationExecutor<DeliveryCharge> {

	@Nullable
	public DeliveryCharge findDuplicate(
			@PathParam(value = "businesstypes") @Nullable final java.lang.String businesstypes,

			@PathParam(value = "citycode") @Nullable final java.lang.String citycode,

			@PathParam(value = "countrycode") @Nullable final java.lang.String countrycode,

			@PathParam(value = "distancelowerrange") @Nullable final int distancelowerrange,

			@PathParam(value = "distanceupperrange") @Nullable final int distanceupperrange

	);
}
