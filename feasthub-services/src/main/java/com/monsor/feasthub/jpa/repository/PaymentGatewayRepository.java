package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nonnull;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.payment.PaymentGateway;

@Repository
public interface PaymentGatewayRepository
		extends CrudRepository<PaymentGateway, BigInteger>, JpaSpecificationExecutor<PaymentGateway> {
    @Nonnull	
    public PaymentGateway findOneByDeliveryLocationId(@Nonnull BigInteger deliveryLocationId);
}
