package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.EventCuisineListingRepository;

/**
 * @author Amit
 *
 */

@Component
public class EventCuisineListingService {
	private final EventCuisineListingRepository eventcuisinelistingRepository;

	@Autowired
	EventCuisineListingService(@Nonnull final EventCuisineListingRepository eventcuisinelistingRepository) {
		this.eventcuisinelistingRepository = eventcuisinelistingRepository;
	}

	@Nonnull
	public EventCuisineListingRepository getEventCuisineListingRepository() {
		return eventcuisinelistingRepository;
	}
}
