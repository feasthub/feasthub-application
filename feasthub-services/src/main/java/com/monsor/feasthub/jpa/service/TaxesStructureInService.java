package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.TaxesStructureInRepository;

/**
 * @author Amit
 *
 */

@Component
public class TaxesStructureInService {
	private final TaxesStructureInRepository taxesstructureinRepository;

	@Autowired
	TaxesStructureInService(@Nonnull final TaxesStructureInRepository taxesstructureinRepository) {
		this.taxesstructureinRepository = taxesstructureinRepository;
	}

	@Nonnull
	public TaxesStructureInRepository getTaxesStructureInRepository() {
		return taxesstructureinRepository;
	}
}
