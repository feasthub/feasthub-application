package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.DeliverySlotsTimeRepository;

/**
 * @author Amit
 *
 */

@Component
public class DeliverySlotsTimeService {
	private final DeliverySlotsTimeRepository deliveryslotstimeRepository;

	@Autowired
	DeliverySlotsTimeService(@Nonnull final DeliverySlotsTimeRepository deliveryslotstimeRepository) {
		this.deliveryslotstimeRepository = deliveryslotstimeRepository;
	}

	@Nonnull
	public DeliverySlotsTimeRepository getDeliverySlotsTimeRepository() {
		return deliveryslotstimeRepository;
	}
}
