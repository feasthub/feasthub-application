package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.UserAddressRepository;

@Component
public class UserAddressService {
	private final UserAddressRepository userAddressRepository;

	@Autowired
	UserAddressService(@Nonnull final UserAddressRepository userAddressRepository) {
		this.userAddressRepository = userAddressRepository;
	}

	@Nonnull
	public UserAddressRepository getUserAddressRepository() {
		return userAddressRepository;
	}
}
