package com.monsor.feasthub.jpa.service;

import java.io.Serializable;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.CountriesRepository;

/**
 * @author Amit
 *
 */

@Component
public class CountriesService implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8206505988858649959L;
	private final CountriesRepository countriesRepository;

	@Autowired
	CountriesService(@Nonnull final CountriesRepository countriesRepository) {
		this.countriesRepository = countriesRepository;
	}

	@Nonnull
	public CountriesRepository getCountriesRepository() {
		return countriesRepository;
	}
}
