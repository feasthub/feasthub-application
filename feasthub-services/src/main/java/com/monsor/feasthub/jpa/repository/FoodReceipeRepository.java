package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodMenuReceipe;

@Repository
public interface FoodReceipeRepository
		extends CrudRepository<FoodMenuReceipe, BigInteger>, JpaSpecificationExecutor<FoodMenuReceipe> {

	@Nonnull
	public List<FoodMenuReceipe> searchFoodMenuReceipesIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public FoodMenuReceipe findByFoodCategoryId(BigInteger foodCategoryId);
}
