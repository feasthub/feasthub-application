package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.events.EventsCrossSalesEventItem;

@Repository
public interface EventsCrossSalesEventItemRepository extends CrudRepository<EventsCrossSalesEventItem, BigInteger>,
		JpaSpecificationExecutor<EventsCrossSalesEventItem> {

	@Nullable
	public List<EventsCrossSalesEventItem> findByMainEventId(BigInteger mainEventId);

}
