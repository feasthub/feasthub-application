package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.IngredientsCategoryRepository;

/**
 * @author Amit
 *
 */

@Component
public class IngredientsCategoryService {
	private final IngredientsCategoryRepository ingredientscategoryRepository;

	@Autowired
	IngredientsCategoryService(@Nonnull final IngredientsCategoryRepository ingredientscategoryRepository) {
		this.ingredientscategoryRepository = ingredientscategoryRepository;
	}

	@Nonnull
	public IngredientsCategoryRepository getIngredientsCategoryRepository() {
		return ingredientscategoryRepository;
	}
}
