/**
 * 
 */
package com.monsor.feasthub.jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.UserRepository;

/**
 * @author Amit
 *
 */

@Component
public class UserRegistrationService {
	private final UserRepository userRepository;

	@Autowired
	UserRegistrationService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}
}
