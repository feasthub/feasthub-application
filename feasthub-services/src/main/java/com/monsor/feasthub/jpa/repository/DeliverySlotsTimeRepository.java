package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.delivery.DeliverySlotsTime;

@Repository
public interface DeliverySlotsTimeRepository
		extends CrudRepository<DeliverySlotsTime, BigInteger>, JpaSpecificationExecutor<DeliverySlotsTime> {

}
