package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nonnull;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.FrontPageStaticContent;
import com.monsor.feasthub.jpa.model.enums.StaticContent;

@Repository
public interface StaticContentRepository
		extends CrudRepository<FrontPageStaticContent, BigInteger>, JpaSpecificationExecutor<FrontPageStaticContent> {
	public FrontPageStaticContent findOneByPageId(@Nonnull StaticContent pageId);
}
