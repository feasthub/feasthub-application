package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.view.FhCrossEventView;

@Repository
public interface FhCrossEventViewRepository extends CrudRepository<FhCrossEventView, BigInteger> {

}
