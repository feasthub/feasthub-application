package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.PackagedFoodItemToAlternativeOptionItemListRepository;

@Component
public class PackagedFoodItemToAlternativeOptionItemListService {
	private final PackagedFoodItemToAlternativeOptionItemListRepository packagedFoodItemToAltOptItemListRepository;

	@Autowired
	PackagedFoodItemToAlternativeOptionItemListService(
			@Nonnull final PackagedFoodItemToAlternativeOptionItemListRepository packagedFoodItemToAltOptItemListRepository) {
		this.packagedFoodItemToAltOptItemListRepository = packagedFoodItemToAltOptItemListRepository;
	}

	@Nonnull
	public PackagedFoodItemToAlternativeOptionItemListRepository getPackagedFoodItemToAlternativeOptionItemListRepository() {
		return packagedFoodItemToAltOptItemListRepository;
	}
}
