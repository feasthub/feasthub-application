package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodIngredientRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodIngredientService {
	private final FoodIngredientRepository foodingredientRepository;

	@Autowired
	FoodIngredientService(@Nonnull final FoodIngredientRepository foodingredientRepository) {
		this.foodingredientRepository = foodingredientRepository;
	}

	@Nonnull
	public FoodIngredientRepository getFoodIngredientRepository() {
		return foodingredientRepository;
	}
}
