package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.UserAdditionalFeesInRepository;

/**
 * @author Amit
 *
 */

@Component
public class UserAdditionalFeesInService {
	private final UserAdditionalFeesInRepository useradditionalfeesinRepository;

	@Autowired
	UserAdditionalFeesInService(@Nonnull final UserAdditionalFeesInRepository useradditionalfeesinRepository) {
		this.useradditionalfeesinRepository = useradditionalfeesinRepository;
	}

	@Nonnull
	public UserAdditionalFeesInRepository getUserAdditionalFeesInRepository() {
		return useradditionalfeesinRepository;
	}
}
