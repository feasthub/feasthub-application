package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.PackagedFoodMenuItemisedPricingRepository;

@Component
public class PackagedFoodMenuItemisedPricingService {
	private final PackagedFoodMenuItemisedPricingRepository packagedFoodMenuItemisedPricingRepository;

	@Autowired
	PackagedFoodMenuItemisedPricingService(
			@Nonnull final PackagedFoodMenuItemisedPricingRepository packagedFoodMenuItemisedPricingRepository) {
		this.packagedFoodMenuItemisedPricingRepository = packagedFoodMenuItemisedPricingRepository;
	}

	@Nonnull
	public PackagedFoodMenuItemisedPricingRepository getPackagedFoodMenuItemisedPricingRepository() {
		return packagedFoodMenuItemisedPricingRepository;
	}
}
