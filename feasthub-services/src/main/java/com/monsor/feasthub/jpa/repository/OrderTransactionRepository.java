package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.order.OrderTransaction;

@Repository
public interface OrderTransactionRepository extends JpaRepository<OrderTransaction, BigInteger>,
	JpaSpecificationExecutor<OrderTransaction> {
    public OrderTransaction findOneByOrderId(String orderId);

    public Page<OrderTransaction> findAllByPurchaserUserId(@Nonnull BigInteger purchaserUserId,
	    Pageable pageable);

    public List<OrderTransaction> findAllByOrderDate(@Nonnull Date orderDate);

}
