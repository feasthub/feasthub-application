package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.PaymentGatewayRepository;

/**
 * @author Amit
 *
 */

@Component
public class PaymentGatewayService {
	private final PaymentGatewayRepository paymentGatewayRepository;

	@Autowired
	PaymentGatewayService(@Nonnull final PaymentGatewayRepository paymentGatewayRepository) {
		this.paymentGatewayRepository = paymentGatewayRepository;
	}

	@Nonnull
	public PaymentGatewayRepository getPaymentGatewayRepository() {
		return paymentGatewayRepository;
	}
}
