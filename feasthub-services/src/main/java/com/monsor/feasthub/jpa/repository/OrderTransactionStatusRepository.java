package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.transaction.OrderTransactionStatus;

@Repository
public interface OrderTransactionStatusRepository
		extends CrudRepository<OrderTransactionStatus, BigInteger>, JpaSpecificationExecutor<OrderTransactionStatus> {

	OrderTransactionStatus findOneByOrderId(String orderId);

	OrderTransactionStatus findOneByTransactionId(BigInteger transactionId);

	OrderTransactionStatus findOneByOrderTransactionId(BigInteger orderTransactionId);
}
