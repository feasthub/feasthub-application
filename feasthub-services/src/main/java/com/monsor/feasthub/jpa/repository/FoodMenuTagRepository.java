package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodMenuTag;

@Repository
public interface FoodMenuTagRepository
		extends CrudRepository<FoodMenuTag, BigInteger>, JpaSpecificationExecutor<FoodMenuTag> {

}
