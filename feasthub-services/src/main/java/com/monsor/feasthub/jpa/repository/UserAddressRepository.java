package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.user.UserAddress;

@Repository
public interface UserAddressRepository
		extends CrudRepository<UserAddress, BigInteger>, JpaSpecificationExecutor<UserAddress> {

	@Nullable
	public List<UserAddress> findByUserId(BigInteger userId);

	@Nullable
	public UserAddress findOneByUserIdAndId(BigInteger userId, BigInteger id);
}
