package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodMenuItemViewRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodMenuItemViewService {
	private final FoodMenuItemViewRepository foodmenuitemRepository;

	@Autowired
	FoodMenuItemViewService(@Nonnull final FoodMenuItemViewRepository foodmenuitemRepository) {
		this.foodmenuitemRepository = foodmenuitemRepository;
	}

	@Nonnull
	public FoodMenuItemViewRepository getFoodMenuItemViewRepository() {
		return foodmenuitemRepository;
	}
}
