package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OffererHistoryFavouriteItemRepository;

/**
 * @author Amit
 *
 */

@Component
public class OffererHistoryFavouriteItemService {
	private final OffererHistoryFavouriteItemRepository offererhistoryfavouriteitemRepository;

	@Autowired
	OffererHistoryFavouriteItemService(
			@Nonnull final OffererHistoryFavouriteItemRepository offererhistoryfavouriteitemRepository) {
		this.offererhistoryfavouriteitemRepository = offererhistoryfavouriteitemRepository;
	}

	@Nonnull
	public OffererHistoryFavouriteItemRepository getOffererHistoryFavouriteItemRepository() {
		return offererhistoryfavouriteitemRepository;
	}
}
