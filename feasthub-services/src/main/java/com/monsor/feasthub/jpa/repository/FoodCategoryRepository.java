package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodCategory;

@Repository
public interface FoodCategoryRepository
		extends CrudRepository<FoodCategory, BigInteger>, JpaSpecificationExecutor<FoodCategory> {

	@Nonnull
	public List<FoodCategory> searchFoodCategorysIgnoreCaseContaining(@Nonnull final String searchString);

	@Nullable
	public FoodCategory findByFoodCategoryNameOrKeyId(@Nullable final java.lang.String foodCategoryName,
			@Nullable final String keyId);

	@Nullable
	public FoodCategory findByKeyId(@Nullable final String keyId);

	@Nullable
	public FoodCategory findFirstByParentId(@Nullable final java.lang.String parentId);

	@Nonnull
	public List<FoodCategory> findAllByParentId(@Nullable final java.lang.String parentId);

	@Nullable
	public FoodCategory findByFoodCategoryCode(@Nullable final java.lang.String foodCategoryCode);
	
	@Nullable
	public List<FoodCategory> findAllByParentNodeFlagTrue();
}
