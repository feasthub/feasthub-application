package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderTransactionRepository;

@Component
public class OrderTransactionService {
	private final OrderTransactionRepository orderTransactionRepository;

	@Autowired
	OrderTransactionService(@Nonnull final OrderTransactionRepository orderTxnTransactionRepository) {
		this.orderTransactionRepository = orderTxnTransactionRepository;
	}

	@Nonnull
	public OrderTransactionRepository getOrderTransactionRepository() {
		return orderTransactionRepository;
	}
}
