package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FhCrossEventViewRepository;

@Component
public class FhCrossEventViewService {
	private final FhCrossEventViewRepository fhCrossEventViewRepository;

	@Autowired
	FhCrossEventViewService(@Nonnull final FhCrossEventViewRepository fhCrossEventViewRepository) {
		this.fhCrossEventViewRepository = fhCrossEventViewRepository;
	}

	@Nonnull
	public FhCrossEventViewRepository getFhCrossEventViewRepository() {
		return fhCrossEventViewRepository;
	}
}
