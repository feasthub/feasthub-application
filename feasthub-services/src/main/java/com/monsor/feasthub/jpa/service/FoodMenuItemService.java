package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodMenuItemRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodMenuItemService {
	private final FoodMenuItemRepository foodmenuitemRepository;

	@Autowired
	FoodMenuItemService(@Nonnull final FoodMenuItemRepository foodmenuitemRepository) {
		this.foodmenuitemRepository = foodmenuitemRepository;
	}

	@Nonnull
	public FoodMenuItemRepository getFoodMenuItemRepository() {
		return foodmenuitemRepository;
	}
}
