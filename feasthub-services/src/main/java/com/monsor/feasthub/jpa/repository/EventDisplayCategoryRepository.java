package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.events.EventDisplayCategory;

@Repository
public interface EventDisplayCategoryRepository extends CrudRepository<EventDisplayCategory, BigInteger> {

}
