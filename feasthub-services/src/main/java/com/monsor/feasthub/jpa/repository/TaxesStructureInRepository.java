package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.taxs.TaxesStructureIn;

@Repository
public interface TaxesStructureInRepository
		extends CrudRepository<TaxesStructureIn, BigInteger>, JpaSpecificationExecutor<TaxesStructureIn> {

	@Nonnull
	public List<TaxesStructureIn> searchTaxesStructureInsIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public TaxesStructureIn findDuplicate(
			@PathParam(value = "businesstypes") @Nullable final java.lang.String businesstypes,

			@PathParam(value = "countrycode") @Nullable final java.lang.String countrycode,

			@PathParam(value = "servicesofferingtypes") @Nullable final java.lang.String servicesofferingtypes,

			@PathParam(value = "statecode") @Nullable final java.lang.String statecode

	);
}
