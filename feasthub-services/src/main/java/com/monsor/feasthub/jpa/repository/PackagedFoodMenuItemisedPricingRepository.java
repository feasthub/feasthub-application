package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.PackagedFoodMenuItemisedPricing;

@Repository
public interface PackagedFoodMenuItemisedPricingRepository
		extends JpaRepository<PackagedFoodMenuItemisedPricing, BigInteger>,
		JpaSpecificationExecutor<PackagedFoodMenuItemisedPricing> {

}
