package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FunctionalParameterRepository;

/**
 * @author Amit
 *
 */

@Component
public class FunctionalParameterService {
	private final FunctionalParameterRepository functionalparameterRepository;

	@Autowired
	FunctionalParameterService(@Nonnull final FunctionalParameterRepository functionalparameterRepository) {
		this.functionalparameterRepository = functionalparameterRepository;
	}

	@Nonnull
	public FunctionalParameterRepository getFunctionalParameterRepository() {
		return functionalparameterRepository;
	}
}
