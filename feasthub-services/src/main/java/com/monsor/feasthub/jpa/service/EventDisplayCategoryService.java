package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.EventDisplayCategoryRepository;

@Component
public class EventDisplayCategoryService {
	private final EventDisplayCategoryRepository eventDisplayCategoryRepository;

	@Autowired
	EventDisplayCategoryService(@Nonnull final EventDisplayCategoryRepository eventDisplayCategoryRepository) {
		this.eventDisplayCategoryRepository = eventDisplayCategoryRepository;
	}

	@Nonnull
	public EventDisplayCategoryRepository getEventDisplayCategoryRepository() {
		return eventDisplayCategoryRepository;
	}
}
