package com.monsor.feasthub.jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodMenuItemRepository;

@Component
public class FeastSearchService {

	private final FoodMenuItemRepository foodMenuItemRepository;

	@Autowired
	public FeastSearchService(FoodMenuItemRepository foodMenuItemRepository) {
		this.foodMenuItemRepository = foodMenuItemRepository;
	}

	public FoodMenuItemRepository getFoodMenuItemRepository() {
		return foodMenuItemRepository;
	}

}
