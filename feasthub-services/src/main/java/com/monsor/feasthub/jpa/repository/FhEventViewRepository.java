package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.enums.BusinessTypes;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.events.FhEventView;

@Repository
public interface FhEventViewRepository
	extends CrudRepository<FhEventView, BigInteger>, JpaSpecificationExecutor<FhEventView> {

    @Nonnull
    public List<FhEventView> searchFhEventsIgnoreCase(@Nonnull final String searchString);

    @Nullable
    public FhEventView findByBusinessType(BusinessTypes businessType);

    @Nullable
    public List<FhEventView> findByServiceOfferingType(ServiceOfferingTypes serviceType);

    @Nullable
    public FhEventView findOneByEventNameIgnoreCase(String eventName);

}
