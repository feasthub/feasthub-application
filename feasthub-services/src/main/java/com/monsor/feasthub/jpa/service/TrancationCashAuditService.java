package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.TransactionCashAuditRepository;

@Component
public class TrancationCashAuditService {
	private final TransactionCashAuditRepository cashAuditRepository;

	@Autowired
	TrancationCashAuditService(@Nonnull final TransactionCashAuditRepository cashAuditRepository) {
		this.cashAuditRepository = cashAuditRepository;
	}

	@Nonnull
	public TransactionCashAuditRepository getTrancationCashAuditRepository() {
		return cashAuditRepository;
	}
}
