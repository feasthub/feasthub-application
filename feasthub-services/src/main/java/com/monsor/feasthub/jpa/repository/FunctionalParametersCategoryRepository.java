package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.fuctional.FunctionalParametersCategory;

@Repository
public interface FunctionalParametersCategoryRepository
		extends CrudRepository<FunctionalParametersCategory, BigInteger>,
		JpaSpecificationExecutor<FunctionalParametersCategory> {

	@Nullable
	public FunctionalParametersCategory findByCategoryName(String categoryName);
}
