package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.app.Country;
import com.monsor.feasthub.repository.ReadOnlyJPARepository;

@Repository
public interface CountriesRepository
		extends ReadOnlyJPARepository<Country, BigInteger>, JpaSpecificationExecutor<Country> {

	Country findOneByCountryCode(String countryCode);
}
