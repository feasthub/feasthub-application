package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.EventsCrossSalesEventItemRepository;

@Component
public class EventsCrossSalesEventItemService {
	private final EventsCrossSalesEventItemRepository eventsCrossSalesEventItemRepository;

	@Autowired
	EventsCrossSalesEventItemService(
			@Nonnull final EventsCrossSalesEventItemRepository eventsCrossSalesEventItemRepository) {
		this.eventsCrossSalesEventItemRepository = eventsCrossSalesEventItemRepository;
	}

	@Nonnull
	public EventsCrossSalesEventItemRepository getEventsCrossSalesEventItemRepository() {
		return eventsCrossSalesEventItemRepository;
	}
}
