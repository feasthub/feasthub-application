package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderIdSequenceRepository;

@Component
public class OrderIdSequenceService {
	private final OrderIdSequenceRepository orderIdSequenceRepository;

	@Autowired
	OrderIdSequenceService(@Nonnull final OrderIdSequenceRepository orderIdSequenceRepository) {
		this.orderIdSequenceRepository = orderIdSequenceRepository;
	}

	@Nonnull
	public OrderIdSequenceRepository getOrderIdSequenceService() {
		return orderIdSequenceRepository;
	}
}
