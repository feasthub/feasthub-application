package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.delivery.DeliverySlot;

@Repository
public interface DeliverySlotRepository
		extends CrudRepository<DeliverySlot, BigInteger>, JpaSpecificationExecutor<DeliverySlot> {

	@Nullable
	public DeliverySlot findDuplicate(
			@PathParam(value = "businesstypes") @Nullable final java.lang.String businesstypes,

			@PathParam(value = "partofday") @Nullable final java.lang.String partofday,

			@PathParam(value = "servicesofferingtypes") @Nullable final java.lang.String servicesofferingtypes

	);

}
