package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FunctionalParametersCategoryRepository;

/**
 * @author Amit
 *
 */

@Component
public class FunctionalParametersCategoryService {
	private final FunctionalParametersCategoryRepository functionalparameterscategoryRepository;

	@Autowired
	FunctionalParametersCategoryService(
			@Nonnull final FunctionalParametersCategoryRepository functionalparameterscategoryRepository) {
		this.functionalparameterscategoryRepository = functionalparameterscategoryRepository;
	}

	@Nonnull
	public FunctionalParametersCategoryRepository getFunctionalParametersCategoryRepository() {
		return functionalparameterscategoryRepository;
	}
}
