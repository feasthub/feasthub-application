package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodMenuServingCategory;

@Repository
public interface FoodServingCategoryRepository
		extends CrudRepository<FoodMenuServingCategory, BigInteger>, JpaSpecificationExecutor<FoodMenuServingCategory> {

}
