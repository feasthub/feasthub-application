package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.user.UserFeastPointDetail;

@Repository
public interface UserFeastPointDetailRepository
		extends CrudRepository<UserFeastPointDetail, BigInteger>, JpaSpecificationExecutor<UserFeastPointDetail> {

	@Nullable
	public UserFeastPointDetail findDuplicate(
			@PathParam(value = "couponcode") @Nullable final java.lang.String couponcode

	);
}
