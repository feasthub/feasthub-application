package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.UserHistoryFavouriteItemRepository;

@Component
public class UserHistoryFavouriteItemService {

	private final UserHistoryFavouriteItemRepository userHistoryFavItemRepo;

	@Autowired
	UserHistoryFavouriteItemService(@Nonnull final UserHistoryFavouriteItemRepository userHistoryFavItemRepo) {
		this.userHistoryFavItemRepo = userHistoryFavItemRepo;
	}

	@Nonnull
	public UserHistoryFavouriteItemRepository getUserHistoryFavItemRepo() {
		return userHistoryFavItemRepo;
	}

}
