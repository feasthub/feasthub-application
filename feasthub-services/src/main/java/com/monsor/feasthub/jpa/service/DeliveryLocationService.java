package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.DeliveryLocationRepository;

/**
 * @author Amit
 *
 */

@Component
public class DeliveryLocationService {
	private final DeliveryLocationRepository deliverylocationRepository;

	@Autowired
	DeliveryLocationService(@Nonnull final DeliveryLocationRepository deliverylocationRepository) {
		this.deliverylocationRepository = deliverylocationRepository;
	}

	@Nonnull
	public DeliveryLocationRepository getDeliveryLocationRepository() {
		return deliverylocationRepository;
	}
}
