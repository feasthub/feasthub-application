package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.delivery.DeliveryLocation;

@Repository
public interface DeliveryLocationRepository
		extends CrudRepository<DeliveryLocation, BigInteger>, JpaSpecificationExecutor<DeliveryLocation> {

	@Nullable
	public DeliveryLocation findDuplicate(@Nullable final java.lang.String areaname,
			@Nullable final java.lang.String citycode, @Nullable final java.lang.String countrycode,
			@Nullable final java.lang.String postalcode, @Nullable final java.lang.String statecode

	);

	@Nullable
	public DeliveryLocation findByLattitudeAndLongitude(@Nullable final Double lattitude,
			@Nullable final Double longitude);

}
