package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderTransactionStatusRepository;

@Component
public class OrderTransactionStatusService {
	private final OrderTransactionStatusRepository orderTxnDeliveryRepo;

	@Autowired
	OrderTransactionStatusService(@Nonnull final OrderTransactionStatusRepository orderTxnDeliveryRepo) {
		this.orderTxnDeliveryRepo = orderTxnDeliveryRepo;
	}

	@Nonnull
	public OrderTransactionStatusRepository getOrderTransactionDeliveryRepository() {
		return orderTxnDeliveryRepo;
	}
}
