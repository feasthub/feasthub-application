package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderTransactionViewRepository;

@Component
public class OrderTransactionViewService {
	private final OrderTransactionViewRepository orderTransactionRepository;

	@Autowired
	OrderTransactionViewService(@Nonnull final OrderTransactionViewRepository orderTxnTransactionRepository) {
		this.orderTransactionRepository = orderTxnTransactionRepository;
	}

	@Nonnull
	public OrderTransactionViewRepository getOrderTransactionViewRepository() {
		return orderTransactionRepository;
	}
}
