package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.TransactionOnlineAuditRepository;

@Component
public class TransactionOnlineAuditService {

	private final TransactionOnlineAuditRepository cashAuditRepository;

	@Autowired
	TransactionOnlineAuditService(@Nonnull final TransactionOnlineAuditRepository cashAuditRepository) {
		this.cashAuditRepository = cashAuditRepository;
	}

	@Nonnull
	public TransactionOnlineAuditRepository getTrancationOnlineAuditRepository() {
		return cashAuditRepository;
	}

}
