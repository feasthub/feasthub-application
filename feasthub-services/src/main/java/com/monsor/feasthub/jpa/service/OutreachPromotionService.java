package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OutreachPromotionRepository;

/**
 * @author Amit
 *
 */

@Component
public class OutreachPromotionService {
	private final OutreachPromotionRepository currentpromotionRepository;

	@Autowired
	OutreachPromotionService(@Nonnull final OutreachPromotionRepository currentpromotionRepository) {
		this.currentpromotionRepository = currentpromotionRepository;
	}

	@Nonnull
	public OutreachPromotionRepository getCurrentPromotionRepository() {
		return currentpromotionRepository;
	}
}
