package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;
import com.monsor.feasthub.repository.ReadOnlyJPARepository;

@Repository
public interface FoodMenuItemViewRepository
		extends ReadOnlyJPARepository<FoodMenuItemView, BigInteger>, JpaSpecificationExecutor<FoodMenuItemView> {
}
