package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.app.OutreachPromotion;

@Repository
public interface OutreachPromotionRepository
		extends CrudRepository<OutreachPromotion, BigInteger>, JpaSpecificationExecutor<OutreachPromotion> {

	@Nonnull
	public List<OutreachPromotion> searchOutreachPromotionsIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public OutreachPromotion findDuplicate(@PathParam(value = "promocode") @Nullable final java.lang.String promocode

	);
}
