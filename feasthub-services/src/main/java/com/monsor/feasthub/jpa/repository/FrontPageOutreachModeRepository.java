package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.app.FrontPageOutReachMode;

@Repository
public interface FrontPageOutreachModeRepository
		extends CrudRepository<FrontPageOutReachMode, BigInteger>, JpaSpecificationExecutor<FrontPageOutReachMode> {
	public FrontPageOutReachMode findOneByOutreachTypeIgnoreCase(String outreachType);
}
