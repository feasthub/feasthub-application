package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FhEventRepository;

/**
 * @author Amit
 *
 */

@Component
public class FhEventService {
	private final FhEventRepository fheventRepository;

	@Autowired
	FhEventService(@Nonnull final FhEventRepository fheventRepository) {
		this.fheventRepository = fheventRepository;
	}

	@Nonnull
	public FhEventRepository getFhEventRepository() {
		return fheventRepository;
	}
}
