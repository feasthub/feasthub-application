package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.review.FeedbackAndReview;

@Repository
public interface FeedbackAndReviewRepository extends JpaRepository<FeedbackAndReview, BigInteger> {
	@Nullable
	public Long countByEventId(BigInteger eventId);

	@Nullable
	public Long sumOfRatings(BigInteger eventId);

	@Nullable
	public Page<FeedbackAndReview> findByEventId(BigInteger eventId, Pageable pageable);

}
