package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FeedbackAndReviewRepository;

@Component
public class FeedBackReviewService {
	private final FeedbackAndReviewRepository feedbackAndReviewRepository;

	@Autowired
	FeedBackReviewService(@Nonnull final FeedbackAndReviewRepository feedbackAndReviewRepository) {
		this.feedbackAndReviewRepository = feedbackAndReviewRepository;
	}

	@Nonnull
	public FeedbackAndReviewRepository getFeedbackAndReviewRepository() {
		return feedbackAndReviewRepository;
	}
}
