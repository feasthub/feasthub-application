package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.app.AppsSecret;

@Repository
public interface AppsSecretRepository
		extends CrudRepository<AppsSecret, BigInteger>, JpaSpecificationExecutor<AppsSecret> {

	@Nullable
	public AppsSecret findDuplicate(@PathParam(value = "appsname") @Nullable final java.lang.String appsname,

			@PathParam(value = "clientid") @Nullable final java.lang.String clientid

	);
}
