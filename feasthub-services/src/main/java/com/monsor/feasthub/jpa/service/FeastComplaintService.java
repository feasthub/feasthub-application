package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FeastComplaintRepository;

@Component
public class FeastComplaintService {
	private final FeastComplaintRepository feastComplaintRepository;

	@Autowired
	FeastComplaintService(@Nonnull final FeastComplaintRepository feastComplaintRepository) {
		this.feastComplaintRepository = feastComplaintRepository;
	}

	@Nonnull
	public FeastComplaintRepository getFeastComplaintRepository() {
		return feastComplaintRepository;
	}
}
