package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.StaticContentRepository;

/**
 * @author Amit
 *
 */

@Component
public class StaticContentService {
	private final StaticContentRepository staticContentRepository;

	@Autowired
	StaticContentService(@Nonnull final StaticContentRepository staticContentRepository) {
		this.staticContentRepository = staticContentRepository;
	}

	@Nonnull
	public StaticContentRepository getStaticContentRepository() {
		return staticContentRepository;
	}
}
