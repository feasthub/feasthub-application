package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.ApplicableStateTaxesInRepository;

/**
 * @author Amit
 *
 */

@Component
public class ApplicableStateTaxesInService {
	private final ApplicableStateTaxesInRepository applicablestatetaxesinRepository;

	@Autowired
	ApplicableStateTaxesInService(@Nonnull final ApplicableStateTaxesInRepository applicablestatetaxesinRepository) {
		this.applicablestatetaxesinRepository = applicablestatetaxesinRepository;
	}

	@Nonnull
	public ApplicableStateTaxesInRepository getApplicableStateTaxesInRepository() {
		return applicablestatetaxesinRepository;
	}
}
