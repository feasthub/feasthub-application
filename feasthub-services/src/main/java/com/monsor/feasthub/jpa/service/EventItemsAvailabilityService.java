package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.EventItemsAvailabilityRepository;

/**
 * @author Amit
 *
 */

@Component
public class EventItemsAvailabilityService {
	private final EventItemsAvailabilityRepository eventitemsavailabilityRepository;

	@Autowired
	EventItemsAvailabilityService(@Nonnull final EventItemsAvailabilityRepository eventitemsavailabilityRepository) {
		this.eventitemsavailabilityRepository = eventitemsavailabilityRepository;
	}

	@Nonnull
	public EventItemsAvailabilityRepository getEventItemsAvailabilityRepository() {
		return eventitemsavailabilityRepository;
	}
}
