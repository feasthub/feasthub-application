package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.AppsSecretRepository;

/**
 * @author Amit
 *
 */

@Component
public class AppsSecretService {
	private final AppsSecretRepository appssecretRepository;

	@Autowired
	AppsSecretService(@Nonnull final AppsSecretRepository appssecretRepository) {
		this.appssecretRepository = appssecretRepository;
	}

	@Nonnull
	public AppsSecretRepository getAppsSecretRepository() {
		return appssecretRepository;
	}
}
