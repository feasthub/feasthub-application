package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodMenuTagRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodMenuTagService {
	private final FoodMenuTagRepository foodmenutagRepository;

	@Autowired
	FoodMenuTagService(@Nonnull final FoodMenuTagRepository foodmenutagRepository) {
		this.foodmenutagRepository = foodmenutagRepository;
	}

	@Nonnull
	public FoodMenuTagRepository getFoodMenuTagRepository() {
		return foodmenutagRepository;
	}
}
