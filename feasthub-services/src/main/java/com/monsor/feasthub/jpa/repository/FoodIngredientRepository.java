package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodIngredient;

@Repository
public interface FoodIngredientRepository
		extends CrudRepository<FoodIngredient, BigInteger>, JpaSpecificationExecutor<FoodIngredient> {
	@Nonnull
	public List<FoodIngredient> searchFoodIngredientsIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public FoodIngredient findByFoodReceipeId(BigInteger foodReceipeId);
}
