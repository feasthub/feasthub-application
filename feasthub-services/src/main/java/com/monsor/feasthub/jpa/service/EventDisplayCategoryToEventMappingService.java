package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.EventDisplayCategoryToEventMappingRepository;

@Component
public class EventDisplayCategoryToEventMappingService {
	private final EventDisplayCategoryToEventMappingRepository eventDisplayCategoryToEventMappingRepository;

	@Autowired
	EventDisplayCategoryToEventMappingService(
			@Nonnull final EventDisplayCategoryToEventMappingRepository eventDisplayCategoryToEventMappingRepository) {
		this.eventDisplayCategoryToEventMappingRepository = eventDisplayCategoryToEventMappingRepository;
	}

	@Nonnull
	public EventDisplayCategoryToEventMappingRepository getEventDisplayCategoryToEventMappingRepository() {
		return eventDisplayCategoryToEventMappingRepository;
	}
}
