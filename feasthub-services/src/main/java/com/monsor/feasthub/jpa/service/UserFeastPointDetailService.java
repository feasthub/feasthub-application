package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.UserFeastPointDetailRepository;

/**
 * @author Amit
 *
 */

@Component
public class UserFeastPointDetailService {
	private final UserFeastPointDetailRepository userfeastpointdetailRepository;

	@Autowired
	UserFeastPointDetailService(@Nonnull final UserFeastPointDetailRepository userfeastpointdetailRepository) {
		this.userfeastpointdetailRepository = userfeastpointdetailRepository;
	}

	@Nonnull
	public UserFeastPointDetailRepository getUserFeastPointDetailRepository() {
		return userfeastpointdetailRepository;
	}
}
