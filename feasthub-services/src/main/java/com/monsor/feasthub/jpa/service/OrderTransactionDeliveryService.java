package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.OrderTransactionDeliveryRepository;

@Component
public class OrderTransactionDeliveryService {
	private final OrderTransactionDeliveryRepository orderTxnDeliveryRepo;

	@Autowired
	OrderTransactionDeliveryService(@Nonnull final OrderTransactionDeliveryRepository orderTxnDeliveryRepo) {
		this.orderTxnDeliveryRepo = orderTxnDeliveryRepo;
	}

	@Nonnull
	public OrderTransactionDeliveryRepository getOrderTransactionDeliveryRepository() {
		return orderTxnDeliveryRepo;
	}
}
