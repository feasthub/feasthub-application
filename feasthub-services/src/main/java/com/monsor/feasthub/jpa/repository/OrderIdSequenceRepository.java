/**
 * 
 */
package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.monsor.feasthub.jpa.model.util.OrderIdSequence;

/**
 * @author amit
 *
 */
public interface OrderIdSequenceRepository
		extends CrudRepository<OrderIdSequence, BigInteger>, JpaSpecificationExecutor<OrderIdSequence> {

	OrderIdSequence findByOrderIdPrefix(String orderIdPrefix);
}
