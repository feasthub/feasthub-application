package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.PathParam;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.user.FeastPointGeneralRule;

@Repository
public interface FeastPointGeneralRuleRepository
		extends CrudRepository<FeastPointGeneralRule, BigInteger>, JpaSpecificationExecutor<FeastPointGeneralRule> {

	@Nonnull
	public List<FeastPointGeneralRule> searchFeastPointGeneralRulesIgnoreCase(@Nonnull final String searchString);

	@Nullable
	public FeastPointGeneralRule findDuplicate(
			@PathParam(value = "couponcode") @Nullable final java.lang.String couponcode

	);
}
