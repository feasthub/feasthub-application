package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import javax.annotation.Nonnull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.order.OrderTransactionView;
import com.monsor.feasthub.repository.ReadOnlyJPARepository;

@Repository
public interface OrderTransactionViewRepository
	extends ReadOnlyJPARepository<OrderTransactionView, BigInteger> {
    public OrderTransactionView findOneByOrderId(String orderId);

    public Page<OrderTransactionView> findAllByPurchaserUserIdOrderByOrderDateDesc(
	    @Nonnull BigInteger purchaserUserId, Pageable pageable);
}
