/**
 * 
 */
package com.monsor.feasthub.jpa.events;

import org.springframework.context.ApplicationEvent;


/**
 * @author AS47321
 *
 */
public class PaymentStatusUpdateEvent extends ApplicationEvent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public PaymentStatusUpdateEvent (Object source) {
	super(source);
	// TODO Auto-generated constructor stub
    }

}
