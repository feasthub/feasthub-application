package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.ExternalResourceEndpointRepository;

/**
 * @author Amit
 *
 */

@Component
public class ExternalResourceEndpointService {
	private final ExternalResourceEndpointRepository externalresourceendpointRepository;

	@Autowired
	ExternalResourceEndpointService(
			@Nonnull final ExternalResourceEndpointRepository externalresourceendpointRepository) {
		this.externalresourceendpointRepository = externalresourceendpointRepository;
	}

	@Nonnull
	public ExternalResourceEndpointRepository getExternalResourceEndpointRepository() {
		return externalresourceendpointRepository;
	}
}
