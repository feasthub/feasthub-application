package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FhEventViewRepository;

/**
 * @author Amit
 *
 */

@Component
public class FhEventViewService {
    private final FhEventViewRepository fheventRepository;

    @Autowired
    FhEventViewService(@Nonnull final FhEventViewRepository fheventRepository) {
	this.fheventRepository = fheventRepository;
    }

    @Nonnull
    public FhEventViewRepository getFhEventViewRepository() {
	return fheventRepository;
    }
}
