package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FeastPointGeneralRuleRepository;

/**
 * @author Amit
 *
 */

@Component
public class FeastPointGeneralRuleService {
	private final FeastPointGeneralRuleRepository feastpointgeneralruleRepository;

	@Autowired
	FeastPointGeneralRuleService(@Nonnull final FeastPointGeneralRuleRepository feastpointgeneralruleRepository) {
		this.feastpointgeneralruleRepository = feastpointgeneralruleRepository;
	}

	@Nonnull
	public FeastPointGeneralRuleRepository getFeastPointGeneralRuleRepository() {
		return feastpointgeneralruleRepository;
	}
}
