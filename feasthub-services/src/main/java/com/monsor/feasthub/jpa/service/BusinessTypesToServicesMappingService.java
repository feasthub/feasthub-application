package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.BusinessTypesToServicesMappingRepository;

/**
 * @author Amit
 *
 */

@Component
public class BusinessTypesToServicesMappingService {
	private final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository;

	@Autowired
	BusinessTypesToServicesMappingService(
			@Nonnull final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository) {
		this.businesstypestoservicesmappingRepository = businesstypestoservicesmappingRepository;
	}

	@Nonnull
	public BusinessTypesToServicesMappingRepository getBusinessTypesToServicesMappingRepository() {
		return businesstypestoservicesmappingRepository;
	}
}
