package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.CashBalanceRepository;

/**
 * @author Amit
 *
 */

@Component
public class CashBalanceService {
	private final CashBalanceRepository cashbalanceRepository;

	@Autowired
	CashBalanceService(@Nonnull final CashBalanceRepository cashbalanceRepository) {
		this.cashbalanceRepository = cashbalanceRepository;
	}

	@Nonnull
	public CashBalanceRepository getCashBalanceRepository() {
		return cashbalanceRepository;
	}
}
