package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodServingCategoryRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodServingCategoryService {
	private final FoodServingCategoryRepository foodservingcategoryRepository;

	@Autowired
	FoodServingCategoryService(@Nonnull final FoodServingCategoryRepository foodservingcategoryRepository) {
		this.foodservingcategoryRepository = foodservingcategoryRepository;
	}

	@Nonnull
	public FoodServingCategoryRepository getFoodServingCategoryRepository() {
		return foodservingcategoryRepository;
	}
}
