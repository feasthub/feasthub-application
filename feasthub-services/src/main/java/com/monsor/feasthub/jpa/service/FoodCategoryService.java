package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodCategoryRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodCategoryService {
	private final FoodCategoryRepository foodcategoryRepository;

	@Autowired
	FoodCategoryService(@Nonnull final FoodCategoryRepository foodcategoryRepository) {
		this.foodcategoryRepository = foodcategoryRepository;
	}

	@Nonnull
	public FoodCategoryRepository getFoodCategoryRepository() {
		return foodcategoryRepository;
	}
}
