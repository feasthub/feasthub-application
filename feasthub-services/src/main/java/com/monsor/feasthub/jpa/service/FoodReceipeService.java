package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FoodReceipeRepository;

/**
 * @author Amit
 *
 */

@Component
public class FoodReceipeService {
	private final FoodReceipeRepository foodreceipeRepository;

	@Autowired
	FoodReceipeService(@Nonnull final FoodReceipeRepository foodreceipeRepository) {
		this.foodreceipeRepository = foodreceipeRepository;
	}

	@Nonnull
	public FoodReceipeRepository getFoodReceipeRepository() {
		return foodreceipeRepository;
	}
}
