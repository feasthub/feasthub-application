package com.monsor.feasthub.jpa.events.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.appservice.mobile.UserProfileAppService;
import com.monsor.feasthub.appservice.mobile.orderupdate.OrderTransactionAppService;
import com.monsor.feasthub.appservice.util.NotificationAppService;
import com.monsor.feasthub.constant.Notifications;
import com.monsor.feasthub.jpa.model.enums.OrderStatus;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;
import com.monsor.feasthub.jpa.model.transaction.OrderTransactionStatus;
import com.monsor.feasthub.jpa.model.user.FhUser;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;

@Component
@Configurable
public class OrderTransactionEventHandler {

    @Autowired
    private NotificationAppService notificationAppService;

    @Autowired
    private OrderTransactionAppService orderTransactionAppService;

    @Autowired
    private UserProfileAppService userProfileAppService;

    @Async
    @EventListener
    public void sentOrderNotification(OrderTransaction orderTransaction) {

	FhUser user = null;
	OrderStatus status = null;
	String messageTitle = null;
	String messageBody = null;

	try {

	    checkNotNull(orderTransaction, "Order cannot be null");

	    status = orderTransaction.getOrderStatus();
	    user = userProfileAppService.findById(orderTransaction.getPurchaserUserId());

	    checkNotNull(user, "User cannot be null");
	    switch (status) {
	    case CREATED:
		messageTitle = String.format(Notifications.ORDER_CREATE_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_CREATE_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    case CONFIRMED_BY_COMPANY:
		messageTitle = String.format(Notifications.ORDER_RECEIVED_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_RECEIVED_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    case OUT_FOR_DELIVERY:
		messageTitle = String.format(Notifications.ORDER_OUT_FOR_DELIVERY_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_OUT_FOR_DELIVERY_TITLE;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    case DELIVERED:
	    case DELIVERED_TO_NEIGHBOUR:
		messageTitle = String.format(Notifications.ORDER_COMPLETE_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_COMPLETE_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    case PACKING_IN_PROGRESS:
		messageTitle = String.format(Notifications.ORDER_PREPARE_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_PREPARE_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    default:
		break;
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    @Async
    @EventListener
    public void sentPaymentNotification(OrderTransactionStatus orderTransactionStatus) {

	FhUser user = null;
	String messageTitle = null;
	String messageBody = null;
	PaymentStatus status = null;
	try {
	    OrderTransaction orderTransaction = orderTransactionAppService.searchByOrderId(orderTransactionStatus.getOrderId());
	    checkNotNull(orderTransaction, "Order cannot be null");
	    status = orderTransactionStatus.getPaymentStatus();
	    user = userProfileAppService.findById(orderTransaction.getPurchaserUserId());

	    checkNotNull(user, "User cannot be null");
	    switch (status) {
	    case CONFIRM:
		messageTitle = String.format(Notifications.ORDER_PAYMENT_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_PAYMENT_CONFIRM_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	    case DECLINED:
		messageTitle = String.format(Notifications.ORDER_PAYMENT_TITLE, orderTransaction.getOrderId());
		messageBody = Notifications.ORDER_PAYMENT_FAILED_BODY;
		notificationAppService.sendTransactionalNotification(user.getDeviceId(), orderTransaction.getOrderId(),
			messageTitle, messageBody);

		break;
	       default:
		break;
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

}
