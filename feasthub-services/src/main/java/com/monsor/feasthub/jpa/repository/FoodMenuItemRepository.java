package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.food.FoodMenuItem;

@Repository
public interface FoodMenuItemRepository
		extends JpaRepository<FoodMenuItem, BigInteger>, JpaSpecificationExecutor<FoodMenuItem> {
	@Nonnull
	public List<FoodMenuItem> searchByFoodMenuItemsIgnoreCaseContaining(@Nonnull final String searchString);

	@Nullable
	public FoodMenuItem findByFoodName(@Nonnull final String foodName);
}
