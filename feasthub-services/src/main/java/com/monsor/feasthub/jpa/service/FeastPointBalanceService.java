package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FeastPointBalanceRepository;

/**
 * @author Amit
 *
 */

@Component
public class FeastPointBalanceService {
	private final FeastPointBalanceRepository feastpointbalanceRepository;

	@Autowired
	FeastPointBalanceService(@Nonnull final FeastPointBalanceRepository feastpointbalanceRepository) {
		this.feastpointbalanceRepository = feastpointbalanceRepository;
	}

	@Nonnull
	public FeastPointBalanceRepository getFeastPointBalanceRepository() {
		return feastpointbalanceRepository;
	}
}
