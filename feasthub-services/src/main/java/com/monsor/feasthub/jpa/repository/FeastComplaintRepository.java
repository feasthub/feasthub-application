package com.monsor.feasthub.jpa.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.monsor.feasthub.jpa.model.review.FeastComplaint;

@Repository
public interface FeastComplaintRepository extends CrudRepository<FeastComplaint, BigInteger> {

}
