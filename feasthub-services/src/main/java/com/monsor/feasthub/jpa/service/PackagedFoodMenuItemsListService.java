package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.PackagedFoodMenuItemsListRepository;

@Component
public class PackagedFoodMenuItemsListService {
	private final PackagedFoodMenuItemsListRepository packagedFoodMenuItemsListRepository;

	@Autowired
	PackagedFoodMenuItemsListService(
			@Nonnull final PackagedFoodMenuItemsListRepository packagedFoodMenuItemsListRepository) {
		this.packagedFoodMenuItemsListRepository = packagedFoodMenuItemsListRepository;
	}

	@Nonnull
	public PackagedFoodMenuItemsListRepository getPackagedFoodMenuItemsListRepository() {
		return packagedFoodMenuItemsListRepository;
	}
}
