package com.monsor.feasthub.jpa.service;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.monsor.feasthub.jpa.repository.FrontPageOutreachModeRepository;

/**
 * @author Amit
 *
 */

@Component
public class FrontPageOutreachModeService {
	private final FrontPageOutreachModeRepository frontPageOutReachMode;

	@Autowired
	FrontPageOutreachModeService(@Nonnull final FrontPageOutreachModeRepository frontPageOutReachMode) {
		this.frontPageOutReachMode = frontPageOutReachMode;
	}

	@Nonnull
	public FrontPageOutreachModeRepository getFrontPageOutReachModeRepository() {
		return frontPageOutReachMode;
	}
}
