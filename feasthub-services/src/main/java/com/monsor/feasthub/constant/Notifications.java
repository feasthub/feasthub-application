package com.monsor.feasthub.constant;

public interface Notifications {

    public static final String ORDER_CREATE_TITLE = "Order %s created";
    
    public static final String ORDER_CREATE_BODY = "We have received your order, Please proceed with payment";

    public static final String ORDER_RECEIVED_TITLE = "Order %s receved";

    public static final String ORDER_RECEIVED_BODY = "We have received your order, Rest assured it will be cooked with love and best ingrediants ";

    public static final String ORDER_PREPARE_TITLE = "Your food is being prepared";

    public static final String ORDER_PREPARE_BODY = "We thought to keep you occupied when you are hungry, your food is being prepared now.";

    public static final String ORDER_OUT_FOR_DELIVERY_TITLE = "Order %s is out for delivery";

    public static final String ORDER_OUT_FOR_DELIVERY_BODY = "We won't keep you waited any more, your food has just left our kitchen";

    public static final String ORDER_COMPLETE_TITLE = "Your order %s is complete";

    public static final String ORDER_COMPLETE_BODY = "Enjoy your food, Happy fooding.";
    
    public static final String ORDER_PAYMENT_TITLE = "Payment Status for %s";
    
    public static final String ORDER_PAYMENT_CONFIRM_BODY = "We have successfuly received payment for %s.";

    public static final String ORDER_PAYMENT_FAILED_BODY = "We are sorry to inform you. We are unable to confirm order as payment is declined. Please try again.";

   
    public static final String ORDER_PAYMENT_CASH_BODY = "You have opted for cash on delivery.";

}
