package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodCategory;
import com.monsor.feasthub.jpa.repository.FoodCategoryRepository;
import com.monsor.feasthub.jpa.service.FoodCategoryService;
/**
 * @author Amit
 *
 */

@Service
// @Path("/v1.0/feasthub/foodcategories")
// @Group(name = "/foodcategories", title = "foodcategories")
// @HttpCode("500>Internal Server Error,200>Success Response")
public class FoodCategoryAppService implements BaseService<FoodCategory> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodCategoryAppService.class);
	@Autowired
	private FoodCategoryService foodcategoryService;

	 

	public FoodCategory create(@Nonnull final FoodCategory foodcategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: " + foodcategory);
		}
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		if (!StringUtils.isEmpty(foodcategory.getParentId())) {
			checkState(foodcategoryRepository.findByKeyId(foodcategory.getParentId()) != null,
					"Please provide a valid parent category Id");
		}

		checkState(foodcategoryRepository.findByFoodCategoryCode(foodcategory.getFoodCategoryCode()) == null,
				"This foodCategoryCode is already exists, Please enter different code");

		checkState(isFoodCategoryExists(foodcategory.getFoodCategoryName(), foodcategory.getKeyId()) == null,
				"This record is already exits, Please provide different combination of data");

		return foodcategoryRepository.save(foodcategory);
	}

	 

	public FoodCategory update(@Nonnull final FoodCategory foodcategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: " + foodcategory);
		}

		checkNotNull(foodcategory.getKeyId(), "This key must not be null");
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		checkState(foodcategoryRepository.findFirstByParentId(foodcategory.getParentId()) != null,
				"Please provide a valid parent category Id");
		return foodcategoryRepository.save(foodcategory);
	}

	 
	public void delete(@Nonnull final BigInteger foodcategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: " + foodcategoryId);
		}
		checkNotNull(foodcategoryId, "This foodcategoryId must not be null");
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		final FoodCategory foodcategory = foodcategoryRepository.findOne(foodcategoryId);
		foodcategoryRepository.delete(foodcategory);
	}

	 
	public List<FoodCategory> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: ");
		}
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		return (List<FoodCategory>) foodcategoryRepository.findAll();
	}

	 
	public List<FoodCategory> findAllByParentId(@Nonnull String parentId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: ");
		}
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		return (List<FoodCategory>) foodcategoryRepository.findAllByParentId(parentId);
	}

	 

	public FoodCategory findById(@Nonnull final BigInteger foodcategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: " + foodcategoryId);
		}
		checkNotNull(foodcategoryId, "This foodcategoryId must not be null");
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		final FoodCategory foodcategory = foodcategoryRepository.findOne(foodcategoryId);
		return foodcategory;
	}

	// @Path("/search")
	 

	public List<FoodCategory> searchFoodCategorys(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodcategory :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();
		return (List<FoodCategory>) foodcategoryRepository.searchFoodCategorysIgnoreCaseContaining(search);
	}

	// @Path("/validatefoodcategorys/{foodcategoryname}")

	public FoodCategory isFoodCategoryExists(@Nullable final String foodcategoryname, @Nullable final String keyId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFoodCategoryExists");
		}
		if (foodcategoryname == null) {
			throw new BadRequestException("Please provide either foodcategoryname");
		}
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();

		final FoodCategory foodCategory = foodcategoryRepository.findByFoodCategoryNameOrKeyId(foodcategoryname, keyId);

		return foodCategory;
	}

	public FoodCategory isFoodCategoryExists(@Nullable final String foodcategorycode) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFoodCategoryExists");
		}
		if (foodcategorycode == null) {
			throw new BadRequestException("Please provide either foodcategoryname");
		}
		final FoodCategoryRepository foodcategoryRepository = foodcategoryService.getFoodCategoryRepository();

		final FoodCategory foodCategory = foodcategoryRepository.findByFoodCategoryCode(foodcategorycode);

		return foodCategory;
	}
	
	public List<FoodCategory> getAllParentCategory(){
		
		return foodcategoryService.getFoodCategoryRepository().findAllByParentNodeFlagTrue();
	}
}
