package com.monsor.feasthub.appservice.event;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.events.FhEventView;
import com.monsor.feasthub.jpa.repository.FhEventViewRepository;
import com.monsor.feasthub.jpa.service.FhEventViewService;
/**
 * @author Amit
 *
 */

@Service
public class FhEventViewAppService implements BaseService<FhEventView> {

    private static final Logger LOG = LoggerFactory.getLogger(FhEventViewAppService.class);

    @Autowired
    private FhEventViewService fheventService;

    @Override
    public FhEventView create(@Nonnull final FhEventView fhevent) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register fhevent :: " + fhevent);
	}
	checkNotNull(fhevent.getBusinessType(), "This businessType must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();

	/*
	 * checkState(isFhEventExists(fhevent.getBusinessType()) != null,
	 * "This record is already exits, Please provide different combination of data"
	 * );
	 */
	return fheventRepository.save(fhevent);
    }

    @Override
    public FhEventView update(@Nonnull final FhEventView fhevent) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register fhevent :: " + fhevent);
	}
	checkNotNull(fhevent.getBusinessType(), "This businessType must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	return fheventRepository.save(fhevent);
    }

    @Override
    public void delete(@Nonnull final BigInteger fheventId) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register fhevent :: " + fheventId);
	}
	checkNotNull(fheventId, "This fheventId must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	final FhEventView fhevent = fheventRepository.findOne(fheventId);
	fheventRepository.delete(fhevent);
    }

    @Override
    public List<FhEventView> findAll() {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register fhevent :: ");
	}
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	return (List<FhEventView>) fheventRepository.findAll();
    }

    @Override
    public FhEventView findById(@Nonnull final BigInteger fheventId) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside findById fhevent :: " + fheventId);
	}
	checkNotNull(fheventId, "This fheventId must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	final FhEventView fhevent = fheventRepository.findOne(fheventId);
	return fhevent;
    }

    public List<FhEventView> findByServiceOfferingType(
	    @Nonnull final ServiceOfferingTypes serviceType) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside findByServiceOfferingType:: " + serviceType);
	}
	checkNotNull(serviceType, "This fheventId must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	final List<FhEventView> feastFactoryItems = fheventRepository
		.findByServiceOfferingType(serviceType);
	return feastFactoryItems;
    }

    public List<FhEventView> searchFhEvents(@Nonnull final String search) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register fhevent :: " + search);
	}
	checkNotNull(search, "This search must not be null");
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();
	return fheventRepository.searchFhEventsIgnoreCase(search);
    }

    // @Path("/validatefhevents/{fheventname}")

    public FhEventView isFhEventExists(
	    @PathParam(value = "fheventname") @Nullable final String fheventname) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside isFhEventExists");
	}
	if (fheventname == null) {
	    throw new BadRequestException("Please provide either fheventname");
	}
	final FhEventViewRepository fheventRepository = fheventService.getFhEventViewRepository();

	final FhEventView fhEvent = fheventRepository.findOneByEventNameIgnoreCase(fheventname);

	return fhEvent;
    }

    public List<FhEventView> getEvents(final String lat, String lng) {
	// User currentUser = (User)a.getPrincipal();
	BigInteger probableDeliveryLocationId = null;
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside feast search searchResult :: ");
	}
	// LOG.info("Logged in user is: " + a.getPrincipal());

	LOG.debug(
		"Inside feast search probableDeliveryLocationId :: " + probableDeliveryLocationId);
	List<FhEventView> eventList = findAll();
	// return filtereventListByLocationId(eventList,
	// probableDeliveryLocationId);
	return eventList;
    }

    /*
     * for bulk order
     */

}
