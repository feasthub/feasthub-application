package com.monsor.feasthub.appservice.functioanl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.fuctional.FunctionalParametersCategory;
import com.monsor.feasthub.jpa.repository.FunctionalParametersCategoryRepository;
import com.monsor.feasthub.jpa.service.FunctionalParametersCategoryService;
/**
 * @author Amit
 *
 */

@Service
public class FunctionalParametersCategoryAppService implements BaseService<FunctionalParametersCategory> {

	private static final Logger LOG = LoggerFactory.getLogger(FunctionalParametersCategoryAppService.class);
	@Autowired
	private FunctionalParametersCategoryService functionalparameterscategoryService;

	public FunctionalParametersCategory create(
			@Nonnull final FunctionalParametersCategory functionalparameterscategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategory);
		}
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		return functionalparameterscategoryRepository.save(functionalparameterscategory);
	}

	public FunctionalParametersCategory update(
			@Nonnull final FunctionalParametersCategory functionalparameterscategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategory);
		}
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		return functionalparameterscategoryRepository.save(functionalparameterscategory);
	}

	public void delete(@Nonnull final BigInteger functionalparameterscategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategoryId);
		}
		checkNotNull(functionalparameterscategoryId, "This functionalparameterscategoryId must not be null");
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		final FunctionalParametersCategory functionalparameterscategory = functionalparameterscategoryRepository
				.findOne(functionalparameterscategoryId);
		functionalparameterscategoryRepository.delete(functionalparameterscategory);
	}

	public List<FunctionalParametersCategory> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameterscategory :: ");
		}
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		return (List<FunctionalParametersCategory>) functionalparameterscategoryRepository.findAll();
	}

	public FunctionalParametersCategory findById(@Nonnull final BigInteger functionalparameterscategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameterscategory :: " + functionalparameterscategoryId);
		}
		checkNotNull(functionalparameterscategoryId, "This functionalparameterscategoryId must not be null");
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		final FunctionalParametersCategory functionalparameterscategory = functionalparameterscategoryRepository
				.findOne(functionalparameterscategoryId);
		return functionalparameterscategory;
	}

	public FunctionalParametersCategory getFunctionalparamByName(String categoryName) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register getFunctionalparamByName :: " + categoryName);
		}
		checkNotNull(categoryName, "This categoryName must not be null");
		final FunctionalParametersCategoryRepository functionalparameterscategoryRepository = functionalparameterscategoryService
				.getFunctionalParametersCategoryRepository();
		final FunctionalParametersCategory functionalparameterscategory = functionalparameterscategoryRepository
				.findByCategoryName(categoryName);
		return functionalparameterscategory;
	}

}
