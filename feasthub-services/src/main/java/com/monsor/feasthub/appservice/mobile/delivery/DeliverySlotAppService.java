package com.monsor.feasthub.appservice.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlot;
import com.monsor.feasthub.jpa.repository.DeliverySlotRepository;
import com.monsor.feasthub.jpa.service.DeliverySlotService;
/**
 * @author Amit
 *
 */

@Service
public class DeliverySlotAppService implements BaseService<DeliverySlot> {

	private static final Logger LOG = LoggerFactory.getLogger(DeliverySlotAppService.class);
	@Autowired
	private DeliverySlotService deliveryslotService;

	 

	public DeliverySlot create(@Nonnull final DeliverySlot deliveryslot) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslot :: " + deliveryslot);
		}
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();

		checkState(isDeliverySlotExists(deliveryslot.getId()) != null,
				"This record is already exits, Please provide different combination of data");

		return deliveryslotRepository.save(deliveryslot);
	}

	 

	public DeliverySlot update(@Nonnull final DeliverySlot deliveryslot) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslot :: " + deliveryslot);
		}
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();
		return deliveryslotRepository.save(deliveryslot);
	}

	 

	public void delete(@Nonnull final BigInteger deliveryslotId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslot :: " + deliveryslotId);
		}
		checkNotNull(deliveryslotId, "This deliveryslotId must not be null");
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();
		final DeliverySlot deliveryslot = deliveryslotRepository.findOne(deliveryslotId);
		deliveryslotRepository.delete(deliveryslot);
	}

	 

	public List<DeliverySlot> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslot :: ");
		}
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();
		return (List<DeliverySlot>) deliveryslotRepository.findAll();
	}

	 

	public DeliverySlot findById(@Nonnull final BigInteger deliveryslotId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslot :: " + deliveryslotId);
		}
		checkNotNull(deliveryslotId, "This deliveryslotId must not be null");
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();
		final DeliverySlot deliveryslot = deliveryslotRepository.findOne(deliveryslotId);
		return deliveryslot;
	}

	// @Path("/validatedeliveryslots/{deliveryslotname}")

	public DeliverySlot isDeliverySlotExists(@Nullable final BigInteger deliveryslotId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isDeliverySlotExists");
		}
		if (deliveryslotId == null) {
			throw new BadRequestException("Please provide either deliveryslotname");
		}
		final DeliverySlotRepository deliveryslotRepository = deliveryslotService.getDeliverySlotRepository();

		final DeliverySlot deliverySlot = deliveryslotRepository.findOne(deliveryslotId);

		return deliverySlot;
	}
}
