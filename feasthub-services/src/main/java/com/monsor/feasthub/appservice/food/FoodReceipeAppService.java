package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodMenuReceipe;
import com.monsor.feasthub.jpa.repository.FoodCategoryRepository;
import com.monsor.feasthub.jpa.repository.FoodReceipeRepository;
import com.monsor.feasthub.jpa.service.FoodReceipeService;
/**
 * @author Amit
 *
 */

@Service
public class FoodReceipeAppService implements BaseService<FoodMenuReceipe> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodReceipeAppService.class);
	@Autowired
	private FoodReceipeService foodreceipeService;

	@Autowired
	private FoodCategoryRepository foodCategoryRepository;

	 

	public FoodMenuReceipe create(@Nonnull final FoodMenuReceipe foodreceipe) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: " + foodreceipe);
		}
		checkNotNull(foodreceipe.getFoodCategoryId(), "This foodCategoryId must not be null");
		checkState(foodCategoryRepository.findOne(foodreceipe.getFoodCategoryId()) != null,
				"Please provide a valid Food Category.");
		checkNotNull(foodreceipe.getPrimaryIngredientsList(), "This primaryIngredientsList must not be null");
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		return foodreceipeRepository.save(foodreceipe);
	}

	 

	public FoodMenuReceipe update(@Nonnull final FoodMenuReceipe foodreceipe) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: " + foodreceipe);
		}

		checkNotNull(foodreceipe.getFoodCategoryId(), "This foodCategoryId must not be null");
		checkNotNull(foodreceipe.getPrimaryIngredientsList(), "This primaryIngredientsList must not be null");
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		return foodreceipeRepository.save(foodreceipe);
	}

	 

	public void delete(@Nonnull final BigInteger foodreceipeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: " + foodreceipeId);
		}
		checkNotNull(foodreceipeId, "This foodreceipeId must not be null");
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		final FoodMenuReceipe foodreceipe = foodreceipeRepository.findOne(foodreceipeId);
		foodreceipeRepository.delete(foodreceipe);
	}

	 

	public List<FoodMenuReceipe> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: ");
		}
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		return (List<FoodMenuReceipe>) foodreceipeRepository.findAll();
	}

	 

	public FoodMenuReceipe findById(@Nonnull final BigInteger foodreceipeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: " + foodreceipeId);
		}
		checkNotNull(foodreceipeId, "This foodreceipeId must not be null");
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		final FoodMenuReceipe foodreceipe = foodreceipeRepository.findOne(foodreceipeId);
		return foodreceipe;
	}

	// @Path("/search")
	 

	public List<FoodMenuReceipe> search(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodreceipe :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();
		return (List<FoodMenuReceipe>) foodreceipeRepository.searchFoodMenuReceipesIgnoreCase(search);
	}

	// @Path("/validatefoodreceipes/{foodreceipename}")

	public FoodMenuReceipe isFoodMenuReceipeExists(@Nullable final BigInteger foodcategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFoodReceipeExists");
		}
		if (foodcategoryId == null) {
			throw new BadRequestException("Please provide either foodreceipename");
		}
		final FoodReceipeRepository foodreceipeRepository = foodreceipeService.getFoodReceipeRepository();

		final FoodMenuReceipe FoodMenuReceipe = foodreceipeRepository.findByFoodCategoryId(foodcategoryId);

		return FoodMenuReceipe;
	}
}
