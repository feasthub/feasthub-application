package com.monsor.feasthub.appservice.event;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.delivery.DeliveryLocation;
import com.monsor.feasthub.jpa.model.enums.ServiceOfferingTypes;
import com.monsor.feasthub.jpa.model.events.EventsCrossSalesEventItem;
import com.monsor.feasthub.jpa.model.events.FhEvent;
import com.monsor.feasthub.jpa.model.food.FoodMenuCategoryMapping;
import com.monsor.feasthub.jpa.repository.DeliveryLocationRepository;
import com.monsor.feasthub.jpa.repository.EventsCrossSalesEventItemRepository;
import com.monsor.feasthub.jpa.repository.FhEventRepository;
import com.monsor.feasthub.jpa.service.DeliveryLocationService;
import com.monsor.feasthub.jpa.service.EventsCrossSalesEventItemService;
import com.monsor.feasthub.jpa.service.FhEventService;
import com.monsor.feasthub.util.DistanceCalculator;

/**
 * @author Amit
 *
 */

@Service
public class FhEventAppService implements BaseService<FhEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(FhEventAppService.class);

    @Autowired
    private FhEventService fheventService;

    @Autowired
    private DeliveryLocationService deliverylocationService;

    @Autowired
    private EventsCrossSalesEventItemService eventsCrossSalesEventItemService;

    public FhEvent create(@Nonnull final FhEvent fhevent) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: " + fhevent);
	}
	checkNotNull(fhevent.getBusinessType(), "This businessType must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();

	/*
	 * checkState(isFhEventExists(fhevent.getBusinessType()) != null,
	 * "This record is already exits, Please provide different combination of data"
	 * );
	 */
	return fheventRepository.save(fhevent);
    }

    public FhEvent update(@Nonnull final FhEvent fhevent) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: " + fhevent);
	}
	checkNotNull(fhevent.getBusinessType(), "This businessType must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	return fheventRepository.save(fhevent);
    }

    public void delete(@Nonnull final BigInteger fheventId) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: " + fheventId);
	}
	checkNotNull(fheventId, "This fheventId must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	final FhEvent fhevent = fheventRepository.findOne(fheventId);
	fheventRepository.delete(fhevent);
    }

    public List<FhEvent> findAll() {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: ");
	}
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();

	return (List<FhEvent>) fheventRepository.findAll();
    }

    public List<FhEvent> findAll(String foodPrefrence) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: ");
	}
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();

	List<FhEvent> resultList = (List<FhEvent>) fheventRepository.findAll();
	if(StringUtils.isNotBlank(foodPrefrence)){
	    resultList = resultList.stream().filter(item -> findItem(item, foodPrefrence)).collect(Collectors.toList());
	}
	return resultList;
    }

    private boolean findItem(FhEvent event, String foodPrefrence) {

	List<FoodMenuCategoryMapping> mapping = event.getEventCuisine().getFoodMenuItem().getFoodMenuCategoryMappings();
	FoodMenuCategoryMapping item = mapping.stream().filter(item1 -> item1.getFoodCategory().getFoodCategoryCode()
		.equals(foodPrefrence)).findFirst().orElse(null);
	if ( item != null ) {
	    return true;
	}
	return false;
    }

    public FhEvent findById(@Nonnull final BigInteger fheventId) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside findById fhevent :: " + fheventId);
	}
	checkNotNull(fheventId, "This fheventId must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	final FhEvent fhevent = fheventRepository.findOne(fheventId);
	return fhevent;
    }

    public List<FhEvent> findByServiceOfferingType(@Nonnull final ServiceOfferingTypes serviceType) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside findByServiceOfferingType:: " + serviceType);
	}
	checkNotNull(serviceType, "This fheventId must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	final List<FhEvent> feastFactoryItems = fheventRepository.findByServiceOfferingType(serviceType);
	return feastFactoryItems;
    }

    public List<FhEvent> searchFhEvents(@Nonnull final String search) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register fhevent :: " + search);
	}
	checkNotNull(search, "This search must not be null");
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	return (List<FhEvent>) fheventRepository.searchFhEventsIgnoreCase(search);
    }

    // @Path("/validatefhevents/{fheventname}")

    public FhEvent isFhEventExists(@PathParam(value = "fheventname") @Nullable final String fheventname) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside isFhEventExists");
	}
	if ( fheventname == null ) {
	    throw new BadRequestException("Please provide either fheventname");
	}
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();

	final FhEvent fhEvent = fheventRepository.findOneByEventNameIgnoreCase(fheventname);

	return fhEvent;
    }

    public List<FhEvent> getEvents(final String lat, String lng, final String foodPreference) {

	// User currentUser = (User)a.getPrincipal();
	BigInteger probableDeliveryLocationId = null;
	Double minDistance = 0.0;
	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside feast search searchResult :: ");
	}
	// LOG.info("Logged in user is: " + a.getPrincipal());
	List<DeliveryLocation> allDeliveryLocations = findAllDeliveryLocations();
	for ( DeliveryLocation location : allDeliveryLocations ) {
	    Double dis = DistanceCalculator.distance(new Double(lat), new Double(lng), location.getLattitude(), location
		    .getLongitude(), "K");
	    if ( dis != null ) {
		if ( dis < location.getDistanceUpperRangeOfCoverageArea() ) {
		    if ( minDistance == 0.0 ) {
			minDistance = dis;
			probableDeliveryLocationId = location.getId();
		    } else if ( minDistance > dis ) {
			minDistance = dis;
			probableDeliveryLocationId = location.getId();
		    }
		}
	    }
	}
	LOG.debug("Inside feast search probableDeliveryLocationId :: " + probableDeliveryLocationId);
	List<FhEvent> eventList = findAll(foodPreference);

	// return filtereventListByLocationId(eventList,
	// probableDeliveryLocationId);
	return eventList;
    }

    /*
     * for bulk order
     */

    public List<FhEvent> searchfeastFactory(@Nonnull final String lat, @QueryParam(value = "lng") String lng) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside searchfeastFactory :: ");
	}
	final FhEventRepository fheventRepository = fheventService.getFhEventRepository();
	return (List<FhEvent>) fheventRepository.findByServiceOfferingType(ServiceOfferingTypes.BULK_COOKING);

    }

    @SuppressWarnings("unused")
    private List<FhEvent> filtereventListByLocationId(List<FhEvent> eventList, BigInteger probableDeliveryLocationId) {

	LOG.info("Inside filtereventListByLocationId eventList size original is:  :: " + eventList.size());
	for ( Iterator<FhEvent> iterator = eventList.iterator(); iterator.hasNext(); ) {
	    FhEvent event = iterator.next();
	    if ( event.getDeliveryLocationId() != probableDeliveryLocationId ) {
		// Remove the current element from the iterator and the list.
		iterator.remove();
	    }
	}
	LOG.info("Inside filtereventListByLocationId eventList size new is:  :: " + eventList.size());
	return eventList;
    }

    private List<DeliveryLocation> findAllDeliveryLocations() {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside register deliverylocation :: ");
	}
	final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
		.getDeliveryLocationRepository();
	return (List<DeliveryLocation>) deliverylocationRepository.findAll();
    }

    public List<EventsCrossSalesEventItem> findCrossSalesEvent(BigInteger eventId) {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside findCrossSalesEvent FhEvent :: " + eventId);
	}
	checkNotNull(eventId, "This eventId must not be null");
	final EventsCrossSalesEventItemRepository eventsCrossSalesEventItemRepo = eventsCrossSalesEventItemService
		.getEventsCrossSalesEventItemRepository();
	final List<EventsCrossSalesEventItem> crossEventList = eventsCrossSalesEventItemRepo.findByMainEventId(eventId);
	return crossEventList;
    }
}
