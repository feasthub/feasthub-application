package com.monsor.feasthub.appservice.functioanl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.fuctional.FunctionalParameter;
import com.monsor.feasthub.jpa.repository.FunctionalParameterRepository;
import com.monsor.feasthub.jpa.service.FunctionalParameterService;
/**
 * @author Amit
 *
 */

@Service
public class FunctionalParameterAppService implements BaseService<FunctionalParameter> {

	private static final Logger LOG = LoggerFactory.getLogger(FunctionalParameterAppService.class);
	@Autowired
	private FunctionalParameterService functionalparameterService;

	public FunctionalParameter create(@Nonnull final FunctionalParameter functionalparameter) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameter :: " + functionalparameter);
		}
		checkNotNull(functionalparameter.getCategoryId(), "This categoryId must not be null");
		checkNotNull(functionalparameter.getFunctionalParametersKey(), "This functionalParametersKey must not be null");
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();
		checkState(isResourceExists(functionalparameter.getFunctionalParametersKey()) == null,
				"This record is already exits, Please provide different combination of data");
		return functionalparameterRepository.save(functionalparameter);
	}

	public FunctionalParameter update(@Nonnull final FunctionalParameter functionalparameter) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameter :: " + functionalparameter);
		}
		checkNotNull(functionalparameter.getCategoryId(), "This categoryId must not be null");
		checkNotNull(functionalparameter.getFunctionalParametersKey(), "This functionalParametersKey must not be null");
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();
		return functionalparameterRepository.save(functionalparameter);
	}

	public void delete(@Nonnull final BigInteger functionalparameterId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameter :: " + functionalparameterId);
		}
		checkNotNull(functionalparameterId, "This functionalparameterId must not be null");
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();
		final FunctionalParameter functionalparameter = functionalparameterRepository.findOne(functionalparameterId);
		functionalparameterRepository.delete(functionalparameter);
	}

	public List<FunctionalParameter> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameter :: ");
		}
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();
		return (List<FunctionalParameter>) functionalparameterRepository.findAll();
	}

	public FunctionalParameter findById(@Nonnull final BigInteger functionalparameterId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register functionalparameter :: " + functionalparameterId);
		}
		checkNotNull(functionalparameterId, "This functionalparameterId must not be null");
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();
		final FunctionalParameter functionalparameter = functionalparameterRepository.findOne(functionalparameterId);
		return functionalparameter;
	}

	public FunctionalParameter isResourceExists(@Nullable final java.lang.String functionalParametersKey) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFunctionalParameterExists");
		}
		checkNotNull(functionalParametersKey, "This functionalParametersKey must not be null");
		final FunctionalParameterRepository functionalparameterRepository = functionalparameterService
				.getFunctionalParameterRepository();

		final FunctionalParameter functionalParameter = functionalparameterRepository
				.findByFunctionalParametersKey(functionalParametersKey);

		return functionalParameter;
	}
}
