/**
 * 
 */
package com.monsor.feasthub.appservice.util;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.jpa.model.util.OrderIdSequence;
import com.monsor.feasthub.jpa.service.OrderIdSequenceService;
/**
 * @author amit
 *
 */

@Service
public class OrderIdGeneratorAppService {

	//private static final Logger LOG = LoggerFactory.getLogger(OrderIdGeneratorAppService.class);
	@Autowired
	private OrderIdSequenceService orderIdSequenceService;

	public String getNextOrderId(BigInteger deliveryLocationId) {
		String nextOrderId = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MMdd");
		Date date = new Date();
		String orderIdPrefix = sdf.format(date);
		OrderIdSequence orderIdSequence = orderIdSequenceService.getOrderIdSequenceService()
				.findByOrderIdPrefix(orderIdPrefix);
		if (orderIdSequence == null) {
			orderIdSequence = orderIdSequenceService.getOrderIdSequenceService()
					.save(new OrderIdSequence(orderIdPrefix, new BigInteger("0")));
		}
		orderIdSequence.setSequenceNumber(orderIdSequence.getSequenceNumber().add(new BigInteger("1")));
		Random rand = new Random();
		int pick = rand.nextInt(900) + 100;
		nextOrderId = String.format("%s-%d-%d-%d", orderIdPrefix,deliveryLocationId, pick, orderIdSequence.getSequenceNumber());
		orderIdSequenceService.getOrderIdSequenceService().save(orderIdSequence);

		return nextOrderId;
	}
}
