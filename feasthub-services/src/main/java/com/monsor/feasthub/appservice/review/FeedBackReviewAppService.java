package com.monsor.feasthub.appservice.review;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.review.FeedbackAndReview;
import com.monsor.feasthub.jpa.repository.FeedbackAndReviewRepository;
import com.monsor.feasthub.jpa.service.FeedBackReviewService;

@Service
public class FeedBackReviewAppService implements BaseService<FeedbackAndReview> {

	
	private static final Logger LOG = LoggerFactory.getLogger(FeedBackReviewAppService.class);
	@Autowired
	private FeedBackReviewService feedBackReviewService;

	@Override
	public FeedbackAndReview findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById FeedbackAndReview :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		final FeedbackAndReview feedbackAndReview = feedbackAndReviewRepository.findOne(id);
		return feedbackAndReview;
	}

	@Override
	public List<FeedbackAndReview> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findAll FeedbackAndReview :: ");
		}
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		return (List<FeedbackAndReview>) feedbackAndReviewRepository.findAll();
	}

	@Override
	public FeedbackAndReview create(FeedbackAndReview entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create FeedbackAndReview :: " + entity);
		}
		checkNotNull(entity, "This UserAddress must not be null");
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		return feedbackAndReviewRepository.save(entity);
	}

	@Override
	public FeedbackAndReview update(FeedbackAndReview entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update FeedbackAndReview :: " + entity);
		}
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		return feedbackAndReviewRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside delete FeastComplaint :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		final FeedbackAndReview feedback = feedbackAndReviewRepository.findOne(id);
		feedbackAndReviewRepository.delete(feedback);

	}

	public Long countByEventId(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById FeedbackAndReview :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		final Long count = feedbackAndReviewRepository.countByEventId(id);
		return count;
	}

	public Long getSumOfRatings(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById FeedbackAndReview :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		final Long count = feedbackAndReviewRepository.sumOfRatings(id);
		return count;
	}

	/*
	 * public List<FeedbackAndReview> findTop5Review() { if
	 * (LOG.isDebugEnabled()) { LOG.debug("Inside findAll FeedbackAndReview :: "
	 * ); } final FeedbackAndReviewRepository feedbackAndReviewRepository =
	 * feedBackReviewService.getFeedbackAndReviewRepository(); return
	 * (List<FeedbackAndReview>)
	 * feedbackAndReviewRepository.findTop5OrderByReviewDateDesc(); }
	 */

	public Page<FeedbackAndReview> getPageableReview(BigInteger eventId, Pageable pageable) {
		final FeedbackAndReviewRepository feedbackAndReviewRepository = feedBackReviewService
				.getFeedbackAndReviewRepository();
		return feedbackAndReviewRepository.findByEventId(eventId, pageable);
	}

}
