/**
 * 
 */
package com.monsor.feasthub.appservice.mobile.orderupdate;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.transaction.OrderTransactionStatus;
import com.monsor.feasthub.jpa.repository.OrderTransactionStatusRepository;
import com.monsor.feasthub.jpa.service.OrderTransactionStatusService;

/**
 * @author Amit
 *
 */

@Service
public class OrderTransactionStatusAppService
	implements Serializable, BaseService<OrderTransactionStatus> {

    /**
    * 
    */
    private static final long serialVersionUID = 1960539231223199241L;

    private static final Logger LOG = LoggerFactory
	    .getLogger(OrderTransactionStatusAppService.class);

    @Autowired
    private OrderTransactionStatusService orderTransactionStatusService;

    public OrderTransactionStatus create(@Nonnull final OrderTransactionStatus fhUser) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register user :: " + fhUser);
	}
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	return orderTransactionStatusRepository.save(fhUser);
    }

    public OrderTransactionStatus update(@Nonnull final OrderTransactionStatus fhUser) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register user :: " + fhUser);
	}
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	return orderTransactionStatusRepository.save(fhUser);
    }

    public OrderTransactionStatus findByOrderId(String orderId) {
	checkNotNull(orderId, "orderId can not be null");
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	return orderTransactionStatusRepository.findOneByOrderId(orderId);
    }

    @Override
    public OrderTransactionStatus findById(BigInteger id) {
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	return orderTransactionStatusRepository.findOne(id);
    }

    @Override
    public List<OrderTransactionStatus> findAll() {
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	return (List<OrderTransactionStatus>) orderTransactionStatusRepository.findAll();
    }

    @Override
    public void delete(BigInteger id) {
	final OrderTransactionStatusRepository orderTransactionStatusRepository = orderTransactionStatusService
		.getOrderTransactionDeliveryRepository();
	orderTransactionStatusRepository.delete(id);
    }

}
