/**
 * 
 */
package com.monsor.feasthub.appservice.util;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.monsor.feasthub.util.fcm.FCMHelper;

/**
 * @author Amit
 *
 */
@Service
public class NotificationAppService {

    
   
    
    public void sendTransactionalNotification(final String deviceId, final String orderID, final String messageTitle, final String messageBody) throws IOException{
    	
    	
    	FCMHelper fcm = FCMHelper.getInstance();
    	
    	JsonObject notificationObject = new JsonObject();
    	notificationObject.addProperty("title", messageTitle); 
    	notificationObject.addProperty("body", messageBody);
    	notificationObject.addProperty("time_to_live", 7200);
    	
    	 
    	
    	JsonObject dataObject = new JsonObject();
    	dataObject.addProperty("title",messageTitle );  
    	dataObject.addProperty("body",messageBody );
    	dataObject.addProperty("message_type","TRANSACTIONAL" );
    	dataObject.addProperty("orderID",orderID );  
    	   	
    	fcm.sendNotifictaionAndData(FCMHelper.TYPE_TO, deviceId, notificationObject, dataObject);
    }
    
    public void sendPromotionalNotification(final String topics, String promoID, int validityInDays, final String messageTitle, final String messageBody) throws IOException{
    	
    	
    	FCMHelper fcm = FCMHelper.getInstance();
    	
    	JsonObject notificationObject = new JsonObject();
    	notificationObject.addProperty("title", messageTitle); 
    	notificationObject.addProperty("body", messageBody);
    	notificationObject.addProperty("time_to_live", validityInDays*60*60);
    	
    	 
    	
    	JsonObject dataObject = new JsonObject();
    	dataObject.addProperty("title",messageTitle );  
    	dataObject.addProperty("body",messageBody );
    	dataObject.addProperty("message_type","PROMOTIONAL" );
    	dataObject.addProperty("promotion_ID", promoID );  
    	   	
    	fcm.sendTopicNotificationAndData(topics, notificationObject, dataObject);
    }
    
    
    
    
}
