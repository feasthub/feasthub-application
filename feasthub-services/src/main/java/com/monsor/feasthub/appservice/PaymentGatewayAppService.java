package com.monsor.feasthub.appservice;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.payment.PaymentGateway;
import com.monsor.feasthub.jpa.service.PaymentGatewayService;
/**
 * @author Amit
 *
 */

@Service
public class PaymentGatewayAppService implements BaseService<PaymentGateway> {

	private static final Logger LOG = LoggerFactory.getLogger(PaymentGatewayAppService.class);
	@Autowired
	private PaymentGatewayService paymentGatewayService;
	@Override
	public PaymentGateway findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById PaymentGatewayAppService :: "+id );
		}
		checkNotNull(id, "This payment id must not be null");
		// TODO Auto-generated method stub
		return paymentGatewayService.getPaymentGatewayRepository().findOne(id);
	}
	@Override
	public List<PaymentGateway> findAll() {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findAll PaymentGatewayAppService :: " );
		}
		return (List<PaymentGateway> ) paymentGatewayService.getPaymentGatewayRepository().findAll();
	}
	@Override
	public PaymentGateway create(PaymentGateway entity) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update PaymentGatewayAppService :: "+entity.toString() );
		}
		checkNotNull(entity, "This paymentGateway detail must not be null");
		return  paymentGatewayService.getPaymentGatewayRepository().save(entity);
	}
	@Override
	public PaymentGateway update(PaymentGateway entity) {
		// TODO Auto-generated method stub
		checkNotNull(entity, "This paymentGateway detail must not be null");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update PaymentGatewayAppService :: " +entity.toString());
		}
		return  paymentGatewayService.getPaymentGatewayRepository().save(entity);
	}
	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub
		checkNotNull(id, "This id must not be null");
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside delete PaymentGatewayAppService :: "+id );
		}
		 paymentGatewayService.getPaymentGatewayRepository().delete(id);
	}
	
	public PaymentGateway findByDeliveryLocationId(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById PaymentGatewayAppService :: "+id );
		}
		checkNotNull(id, "This delivery Location id must not be null");
		// TODO Auto-generated method stub
		return paymentGatewayService.getPaymentGatewayRepository().findOneByDeliveryLocationId(id);
	}

	
}
