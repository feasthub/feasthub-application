package com.monsor.feasthub.appservice.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.delivery.DeliveryCharge;
import com.monsor.feasthub.jpa.repository.DeliveryChargeRepository;
import com.monsor.feasthub.jpa.service.DeliveryChargeService;
/**
 * @author Amit
 *
 */

@Service
// @Path("/v1.0/feasthub/deliverycharges")
// @Group(name = "/deliverycharges", title = "deliverycharges")
// @HttpCode("500>Internal Server Error,200>Success Response")
public class DeliveryChargeAppService implements BaseService<DeliveryCharge> {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryChargeAppService.class);
	@Autowired
	private DeliveryChargeService deliverychargeService;

	 

	public DeliveryCharge create(@Nonnull final DeliveryCharge deliverycharge) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverycharge :: " + deliverycharge);
		}
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();

		return deliverychargeRepository.save(deliverycharge);
	}

	 

	public DeliveryCharge update(@Nonnull final DeliveryCharge deliverycharge) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverycharge :: " + deliverycharge);
		}
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();
		return deliverychargeRepository.save(deliverycharge);
	}

	 

	public void delete(@Nonnull final BigInteger deliverychargeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverycharge :: " + deliverychargeId);
		}
		checkNotNull(deliverychargeId, "This deliverychargeId must not be null");
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();
		final DeliveryCharge deliverycharge = deliverychargeRepository.findOne(deliverychargeId);
		deliverychargeRepository.delete(deliverycharge);
	}

	 

	public List<DeliveryCharge> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverycharge :: ");
		}
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();
		return (List<DeliveryCharge>) deliverychargeRepository.findAll();
	}

	 

	public DeliveryCharge findById(@Nonnull final BigInteger deliverychargeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverycharge :: " + deliverychargeId);
		}
		checkNotNull(deliverychargeId, "This deliverychargeId must not be null");
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();
		final DeliveryCharge deliverycharge = deliverychargeRepository.findOne(deliverychargeId);
		return deliverycharge;
	}

	// @Path("/validatedeliverycharges/{deliverychargename}")

	public DeliveryCharge isDeliveryChargeExists(
			@PathParam(value = "deliverychargename") @Nullable final BigInteger deliverychargename) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isDeliveryChargeExists");
		}
		if (deliverychargename == null) {
			throw new BadRequestException("Please provide either deliverychargename");
		}
		final DeliveryChargeRepository deliverychargeRepository = deliverychargeService.getDeliveryChargeRepository();

		final DeliveryCharge deliveryCharge = deliverychargeRepository.findOne(deliverychargename);

		return deliveryCharge;
	}
}
