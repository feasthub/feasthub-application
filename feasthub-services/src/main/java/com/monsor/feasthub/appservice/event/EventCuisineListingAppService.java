package com.monsor.feasthub.appservice.event;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.events.EventCuisineListing;
import com.monsor.feasthub.jpa.repository.EventCuisineListingRepository;
import com.monsor.feasthub.jpa.service.EventCuisineListingService;
/**
 * @author Amit
 *
 */

@Service
public class EventCuisineListingAppService implements BaseService<EventCuisineListing> {

	private static final Logger LOG = LoggerFactory.getLogger(EventCuisineListingAppService.class);
	@Autowired
	private EventCuisineListingService eventcuisinelistingService;

	 

	public EventCuisineListing create(@Nonnull final EventCuisineListing eventcuisinelisting) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelisting);
		}
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();

		checkState(isEventCuisineListingExists(eventcuisinelisting.getId()) != null,
				"This record is already exits, Please provide different combination of data");

		return eventcuisinelistingRepository.save(eventcuisinelisting);
	}

	 

	public EventCuisineListing update(@Nonnull final EventCuisineListing eventcuisinelisting) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelisting);
		}
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();
		return eventcuisinelistingRepository.save(eventcuisinelisting);
	}

	 

	public void delete(@Nonnull final BigInteger eventcuisinelistingId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelistingId);
		}
		checkNotNull(eventcuisinelistingId, "This eventcuisinelistingId must not be null");
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();
		final EventCuisineListing eventcuisinelisting = eventcuisinelistingRepository.findOne(eventcuisinelistingId);
		eventcuisinelistingRepository.delete(eventcuisinelisting);
	}

	 

	public List<EventCuisineListing> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventcuisinelisting :: ");
		}
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();
		return (List<EventCuisineListing>) eventcuisinelistingRepository.findAll();
	}

	 

	public EventCuisineListing findById(@Nonnull final BigInteger eventcuisinelistingId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventcuisinelisting :: " + eventcuisinelistingId);
		}
		checkNotNull(eventcuisinelistingId, "This eventcuisinelistingId must not be null");
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();
		final EventCuisineListing eventcuisinelisting = eventcuisinelistingRepository.findOne(eventcuisinelistingId);
		return eventcuisinelisting;
	}

	public EventCuisineListing findByEventId(@Nonnull final BigInteger eventId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside  findByEventId :: " + eventId);
		}
		checkNotNull(eventId, "This eventId must not be null");
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();
		EventCuisineListing eventcuisinelist = (EventCuisineListing) eventcuisinelistingRepository
				.findByEventId(eventId);
		return eventcuisinelist;
	}

	// @Path("/validateeventcuisinelistings/{eventcuisinelistingname}")

	public EventCuisineListing isEventCuisineListingExists(@Nullable final BigInteger eventcuisinelistingname) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isEventCuisineListingExists");
		}
		if (eventcuisinelistingname == null) {
			throw new BadRequestException("Please provide either eventcuisinelistingname");
		}
		final EventCuisineListingRepository eventcuisinelistingRepository = eventcuisinelistingService
				.getEventCuisineListingRepository();

		final EventCuisineListing eventCuisineListing = eventcuisinelistingRepository.findOne(eventcuisinelistingname);

		return eventCuisineListing;
	}

}
