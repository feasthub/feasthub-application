package com.monsor.feasthub.appservice.review;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.review.FeastComplaint;
import com.monsor.feasthub.jpa.repository.FeastComplaintRepository;
import com.monsor.feasthub.jpa.service.FeastComplaintService;

@Service
public class FeastComplaintAppService implements BaseService<FeastComplaint> {

	private static final Logger LOG = LoggerFactory.getLogger(FeastComplaintAppService.class);
	@Autowired
	private FeastComplaintService feastComplaintService;

	@Override
	public FeastComplaint findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById FeastComplaint :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeastComplaintRepository feastComplaintRepository = feastComplaintService.getFeastComplaintRepository();
		final FeastComplaint complaint = feastComplaintRepository.findOne(id);
		return complaint;
	}

	@Override
	public List<FeastComplaint> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findAll FeastComplaint :: ");
		}
		final FeastComplaintRepository feastComplaintRepository = feastComplaintService.getFeastComplaintRepository();
		return (List<FeastComplaint>) feastComplaintRepository.findAll();
	}

	@Override
	public FeastComplaint create(FeastComplaint entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FeastComplaint update(FeastComplaint entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update FeastComplaint :: " + entity);
		}
		final FeastComplaintRepository feastComplaintRepository = feastComplaintService.getFeastComplaintRepository();
		return feastComplaintRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside delete FeastComplaint :: " + id);
		}
		checkNotNull(id, "This id must not be null");
		final FeastComplaintRepository feastComplaintRepository = feastComplaintService.getFeastComplaintRepository();
		final FeastComplaint complaint = feastComplaintRepository.findOne(id);
		feastComplaintRepository.delete(complaint);

	}

}
