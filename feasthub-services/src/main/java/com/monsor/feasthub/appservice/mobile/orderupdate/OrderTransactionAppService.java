package com.monsor.feasthub.appservice.mobile.orderupdate;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.appservice.PaymentGatewayAppService;
import com.monsor.feasthub.appservice.event.EventItemsAvailabilityAppService;
import com.monsor.feasthub.appservice.event.FhEventViewAppService;
import com.monsor.feasthub.appservice.transaction.audit.TransactionCashAuditAppService;
import com.monsor.feasthub.appservice.transaction.audit.TransactionFeastAuditAppService;
import com.monsor.feasthub.appservice.transaction.audit.TransactionOnlineAuditAppService;
import com.monsor.feasthub.appservice.user.UserAddressAppService;
import com.monsor.feasthub.appservice.util.OrderIdGeneratorAppService;
import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.dto.OrderPaymentDTO;
import com.monsor.feasthub.jpa.model.audit.TransactionCashAudit;
import com.monsor.feasthub.jpa.model.audit.TransactionFeastAudit;
import com.monsor.feasthub.jpa.model.audit.TransactionOnlineAudit;
import com.monsor.feasthub.jpa.model.enums.DeliveryStatus;
import com.monsor.feasthub.jpa.model.enums.OrderStatus;
import com.monsor.feasthub.jpa.model.enums.PaymentMode;
import com.monsor.feasthub.jpa.model.enums.PaymentStatus;
import com.monsor.feasthub.jpa.model.enums.TransactionMode;
import com.monsor.feasthub.jpa.model.events.EventItemsAvailability;
import com.monsor.feasthub.jpa.model.events.FhEventView;
import com.monsor.feasthub.jpa.model.food.PackagedFoodItemToAlternativeOptionItemList;
import com.monsor.feasthub.jpa.model.food.PackagedFoodMenuItemsList;
import com.monsor.feasthub.jpa.model.order.OrderCustomisedCusine;
import com.monsor.feasthub.jpa.model.order.OrderDelivery;
import com.monsor.feasthub.jpa.model.order.OrderEvent;
import com.monsor.feasthub.jpa.model.order.OrderTransaction;
import com.monsor.feasthub.jpa.model.order.OrderTransactionView;
import com.monsor.feasthub.jpa.model.payment.PaymentGateway;
import com.monsor.feasthub.jpa.model.transaction.OrderTransactionStatus;
import com.monsor.feasthub.jpa.model.user.UserAddress;
import com.monsor.feasthub.jpa.repository.OrderTransactionRepository;
import com.monsor.feasthub.jpa.repository.OrderTransactionViewRepository;
import com.monsor.feasthub.jpa.service.OrderTransactionService;
import com.monsor.feasthub.jpa.service.OrderTransactionViewService;

@Service
public class OrderTransactionAppService implements BaseService<OrderTransaction> {

    private static final Logger LOG = LoggerFactory.getLogger(OrderTransactionAppService.class);

    @Autowired
    private OrderTransactionService orderTxnTransactionService;

    @Autowired
    private OrderTransactionViewService orderTreansactionViewService;

    @Autowired
    private UserAddressAppService userAddressAppService;

    @Autowired
    private FhEventViewAppService fhEventAppService;

    @Autowired
    private OrderIdGeneratorAppService orderIdGeneratorAppService;

    @Autowired
    private EventItemsAvailabilityAppService eventItemsAvailabilityAppService;

    @Autowired
    private TransactionCashAuditAppService transactionCashAuditAppService;

    @Autowired
    private TransactionFeastAuditAppService transactionFeastAuditAppService;

    @Autowired
    private TransactionOnlineAuditAppService transactionOnlineAuditAppService;

    @Autowired
    private OrderTransactionStatusAppService orderTransactionStatusAppService;

    @Autowired
    private ApplicationEventPublisher  publisher;

    @Autowired
    private PaymentGatewayAppService paymentGatewayAppService;

    
    
   

    @Override
    public OrderTransaction findById(BigInteger id) {

	checkNotNull(id, "This id must not be null");
	final OrderTransactionRepository orderTxnEventRepo = orderTxnTransactionService.getOrderTransactionRepository();

	return orderTxnEventRepo.findOne(id);
    }

    @Override
    public List<OrderTransaction> findAll() {

	if ( LOG.isDebugEnabled() ) {
	    LOG.debug("Inside findAll OrderTransactionEvent :: ");
	}
	final OrderTransactionRepository orderTxnEventRepo = orderTxnTransactionService.getOrderTransactionRepository();

	return orderTxnEventRepo.findAll();
    }

    public OrderTransaction create(BigInteger userId, OrderTransaction order) {

	final OrderTransactionRepository orderTxnEventRepo = orderTxnTransactionService.getOrderTransactionRepository();
	double totalAmount = 0;
	int deliveryDays;
	Date today = new Date();
	String orderId = null;
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
	BigInteger deliveryLocationId = null;
	Date currentDate = null;
	/*
	 * Check all precondition needed for creating order
	 */

	checkNotNull(order.getPurchaserUserId(), "Purchaser User Id can not be nulll");
	checkState(userId.equals(order.getPurchaserUserId()), "No user found for this purchaser");

	checkState(order.getOrderEvents() != null, "Order must contains atleast 1 item");
	checkState(order.getOrderEvents().size() > 0, "Order must contains atleast 1 item");

	order.setOrderStatus(OrderStatus.CREATED);
	order.setPaymentStatus(PaymentStatus.INITIATED);
	order.setTotalAmount(0.0d);
	order.setTotalTax(0.0d);
	order.setPurchaserUserId(userId);
	order.setOrderDate(new Date());

	UserAddress address = userAddressAppService.findByUserIdAndId(userId, order.getAddressId());
	order.setDeliveryAddressId(address);
	try {
	    currentDate = sdf.parse(sdf.format(today));
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	checkNotNull(currentDate, "Error processing order");
	for ( OrderEvent orderItem : order.getOrderEvents() ) {

	    BigInteger eventId = orderItem.getEventId();
	    FhEventView event = fhEventAppService.findById(eventId);
	    orderItem.setOrderTransactionId(order);
	    orderItem.setEventDetails(event);
	    deliveryLocationId = event.getDeliveryLocationId();
	    checkState(event != null, "Please select valid event for proceeding to place order");
	    checkState(event.getEventListingEndDate().compareTo(currentDate) > 0,
		    "Order contains items which are expired");

	    EventItemsAvailability eventItem = eventItemsAvailabilityAppService.findByEventIdAndListingDate(orderItem
		    .getEventId(), new Date());
	    if ( eventItem == null ) {
		int offeredQuantity = event.getEventCuisine().getOfferedQuantity();
		eventItem = new EventItemsAvailability(orderItem.getEventId(), offeredQuantity);
		eventItemsAvailabilityAppService.create(eventItem);
	    }
	    eventItem = eventItemsAvailabilityAppService.findByEventIdAndListingDate(orderItem.getEventId(),
		    new Date());
	    totalAmount = totalAmount + (orderItem.getOrderedQuantity() * event.getFinalOfferedPrice());
	    order.setTotalTax(order.getTotalTax() + (orderItem.getOrderedQuantity() * event.getAddTaxes()));

	    // Check if food menu item is package menu item or not
	    if ( event.getEventCuisine().getFoodMenuItem().getPackagedItemFlag() ) {
		for ( PackagedFoodMenuItemsList packageFoodMenuItemList : event.getEventCuisine().getFoodMenuItem()
			.getPackagedFoodMenuItemsList() ) {
		    // Check if alternative list item are available or not
		    if ( packageFoodMenuItemList.getAlternativeOptionsAvailabilityFlag() ) {
			// Retrive the orderCousine for customisable food menu
			// item.
			OrderCustomisedCusine orderCousine = orderItem.getOrderCustomizeCusine().stream().filter(
				item -> item.getDefaultFoodMenuId().equals(packageFoodMenuItemList
					.getAdditionalFoodMenuId().getId())).findFirst().orElse(null);

			if ( orderCousine != null && packageFoodMenuItemList.getAlternativeOptionsAvailabilityFlag()
				&& packageFoodMenuItemList.getAdditionalFoodMenuId().getId().equals(orderCousine
					.getDefaultFoodMenuId()) ) {

			    PackagedFoodItemToAlternativeOptionItemList alternativeItem = packageFoodMenuItemList
				    .getOptionalFoodMenuItemList().stream().filter(item -> item
					    .getAlternativeOptionalFoodMenuId().getId().equals(orderCousine
						    .getCustomisedFoodMenuId())).findFirst().orElse(null);
			    orderCousine.setDefaultFoodMenuName(packageFoodMenuItemList.getAdditionalFoodMenuId()
				    .getFoodName());

			    orderCousine.setCustomisedFoodMenuName(alternativeItem.getAlternativeOptionalFoodMenuId()
				    .getFoodName());

			    orderCousine.setAdditinalCost(alternativeItem.getPackagedFoodMenuItemisedPricingId()
				    .getTopUpPrice());

			    totalAmount = totalAmount + orderItem.getOrderedQuantity() * alternativeItem
				    .getPackagedFoodMenuItemisedPricingId().getTopUpPrice();
			}
		    }
		}
	    }

	    if ( order.getOrderDelivery() != null ) {
		for ( OrderDelivery delivery : order.getOrderDelivery() ) {
		    delivery.setDeliveryStatus(DeliveryStatus.YET_TO_DELIVERED);
		    delivery.setOrderTransactionId(order);
		}

	    }
	    if ( orderItem.getOrderCustomizeCusine() != null ) {
		orderItem.getOrderCustomizeCusine().forEach(item -> item.setOrderEventId(orderItem));
	    }
	    orderItem.setOrderTransactionId(order);
	    order.setTotalAmount(order.getTotalAmount() + (orderItem.getOrderedQuantity() * event
		    .getFinalOfferedPrice()));
	    order.setTotalTax(order.getTotalTax() + (orderItem.getOrderedQuantity() * event.getAddTaxes()));
	}
	deliveryDays = order.getOrderDelivery().size();
	order.setTotalAmount(deliveryDays * totalAmount);
	order.setTotalTax(deliveryDays * order.getTotalTax());
	orderId = orderIdGeneratorAppService.getNextOrderId(deliveryLocationId);
	order.setOrderId(orderId);
	orderTxnEventRepo.save(order);
	order = orderTxnEventRepo.findOne(order.getId());
	PaymentGateway payment = paymentGatewayAppService.findByDeliveryLocationId(deliveryLocationId);
	if ( payment != null ) {
	    order.setPaymentGateway(payment);
	}
	publisher.publishEvent(order);
	//publisher.sendOrderNotification(order);
	return order;
    }

    @Override
    public OrderTransaction update(OrderTransaction order) {

	final OrderTransactionRepository orderTxnEventRepo = orderTxnTransactionService.getOrderTransactionRepository();
	
	order = orderTxnEventRepo.save(order);
	publisher.publishEvent(order);
	return order;
    }

    @Override
    public void delete(BigInteger id) {
	// TODO Auto-generated method stub

    }

    public OrderTransaction searchByOrderId(String orderId) {

	checkNotNull(orderId, "Order id must not be null");
	return orderTxnTransactionService.getOrderTransactionRepository().findOneByOrderId(orderId);
    }

    public OrderTransaction confirmPayment(OrderTransaction order, OrderPaymentDTO orderPaymentDetails) {

	// TODO Auto-generated method stub
	final OrderTransactionRepository orderTxnEventRepo = orderTxnTransactionService.getOrderTransactionRepository();
	checkState(!(order.getOrderStatus() == OrderStatus.CONFIRMED_BY_COMPANY || order
		.getOrderStatus() == OrderStatus.CANCELLED), "This order is already processed");
	for ( OrderEvent orderItem : order.getOrderEvents() ) {
	    EventItemsAvailability eventItem = eventItemsAvailabilityAppService.findByEventIdAndListingDate(orderItem
		    .getEventDetails().getId(), new Date());
	    eventItem.setOfferedQuantity(eventItem.getOfferedQuantity() - (orderItem.getOrderedQuantity() * orderItem
		    .getOrderedQuantity()));
	    eventItem.setTotalSoldQuantity(eventItem.getTotalSoldQuantity() + (orderItem.getOrderedQuantity()
		    * orderItem.getOrderedQuantity()));
	    eventItemsAvailabilityAppService.update(eventItem);

	}
	order.setPaymentMode(PaymentMode.PAID_IN);
	switch (orderPaymentDetails.getTransactionMode()) {
	case CASH:
	case CHEQUE:
	    TransactionCashAudit cashAudit = new TransactionCashAudit();
	    cashAudit.setAmount(order.getTotalAmount());
	    cashAudit.setOrderTransactionId(order.getId());
	    cashAudit.setPaymentMode(PaymentMode.PAID_IN);
	    if ( orderPaymentDetails.getTransactionMode() == TransactionMode.CHEQUE ) {
		cashAudit.setPaymentStatus(PaymentStatus.INITIATED);
		cashAudit.setTransactionType(TransactionMode.CHEQUE);
		cashAudit.setChequeNumber(orderPaymentDetails.getChequeNumebr());
	    } else {
		cashAudit.setPaymentStatus(PaymentStatus.CASH_ON_DELIVERY);
		cashAudit.setTransactionType(TransactionMode.CASH);
	    }
	    cashAudit.setTransactionDate(new Date());
	    cashAudit.setCurrency("INR");
	    cashAudit.setTransactionTime(new Time(cashAudit.getTransactionDate().getTime()));
	    cashAudit.setTransactionPurpose(orderPaymentDetails.getTransactionPurpose());

	    transactionCashAuditAppService.create(cashAudit);
	    order.setOrderStatus(OrderStatus.CONFIRMED_BY_COMPANY);
	    break;
	case FEAST_POINT:
	    TransactionFeastAudit feastAudit = new TransactionFeastAudit();
	    feastAudit.setFpPoints(Math.round(order.getTotalAmount()));
	    feastAudit.setOrderTransactionId(order.getId());
	    feastAudit.setFpCreditUserId(order.getPurchaserUserId());
	    feastAudit.setFpDebitUserId(order.getPurchaserUserId());
	    feastAudit.setTransactionDate(new Date());
	    feastAudit.setTransactionType(TransactionMode.FEAST_POINT);
	    feastAudit.setTransactionTime(new Time(feastAudit.getTransactionDate().getTime()));
	    feastAudit.setTransactionPurpose(orderPaymentDetails.getTransactionPurpose());
	    transactionFeastAuditAppService.create(feastAudit);
	    order.setPaymentStatus(PaymentStatus.CONFIRM);
	    order.setOrderStatus(OrderStatus.CONFIRMED_BY_COMPANY);
	    break;

	case E_WALLET:
	case SEND_PAYMENT_LINK:
	case RTGS:
	case NEFT:
	case MOBILE_PAYMENT:
	    if ( isPaymentConfirmFromGateway(order) ) {
		TransactionOnlineAudit transactionOnline = new TransactionOnlineAudit();
		transactionOnline.setOrderTransactionId(order.getId());
		transactionOnline.setTransactionDate(new Date());
		transactionOnline.setTransactionMode(orderPaymentDetails.getTransactionMode());
		transactionOnline.setTransactionPurpose(orderPaymentDetails.getTransactionPurpose());
		transactionOnline.setCurrency("INR");
		transactionOnlineAuditAppService.create(transactionOnline);
		order.setPaymentStatus(PaymentStatus.CONFIRM);
		order.setOrderStatus(OrderStatus.CONFIRMED_BY_COMPANY);
	    } else {
		order.setPaymentStatus(PaymentStatus.DECLINED);
		order.setOrderStatus(OrderStatus.CANCELLED);
	    }
	    break;
	default:
	    order.setPaymentStatus(PaymentStatus.DECLINED);
	    order.setOrderStatus(OrderStatus.CANCELLED);
	    break;
	}
	orderTxnEventRepo.save(order);
	publisher.publishEvent(order);
	return orderTxnEventRepo.findOne(order.getId());
    }

    @Deprecated
    @Override
    public OrderTransaction create(OrderTransaction entity) {

	// TODO Auto-generated method stub
	return null;
    }

    private Boolean isPaymentConfirmFromGateway(OrderTransaction order) {

	OrderTransactionStatus orderTransactionStatus = orderTransactionStatusAppService.findByOrderId(order
		.getOrderId());
	if ( orderTransactionStatus != null && PaymentStatus.CONFIRM.equals(orderTransactionStatus
		.getPaymentStatus()) ) {
	    return Boolean.TRUE;
	}

	return Boolean.TRUE;
    }

    public void updatePaymentStatus(String orderId, Double amount, int statusCode, String paymentStatus) {

	OrderTransactionStatus orderTransactionStatus = new OrderTransactionStatus();
	orderTransactionStatus.setOrderId(orderId);
	if ( statusCode == 0 ) {
	    orderTransactionStatus.setPaymentStatus(PaymentStatus.CONFIRM);
	} else {
	    orderTransactionStatus.setPaymentStatus(PaymentStatus.DECLINED);
	}
	orderTransactionStatus.setStatusMessage(paymentStatus);
	publisher.publishEvent(orderTransactionStatus);
    }

    public Page<OrderTransactionView> findAllOrderByUserId(@Nonnull BigInteger userId, PageRequest pageRequest) {

	checkNotNull(userId, "UserId must not be null");
	OrderTransactionViewRepository orderTxnTransactionRepository = orderTreansactionViewService
		.getOrderTransactionViewRepository();
	return orderTxnTransactionRepository.findAllByPurchaserUserIdOrderByOrderDateDesc(userId, pageRequest);
    }

    public List<OrderTransaction> listAllOrder(Date orderDate) {

	OrderTransactionRepository orderTxnTransactionRepository = orderTxnTransactionService
		.getOrderTransactionRepository();
	return orderTxnTransactionRepository.findAllByOrderDate(orderDate);
    }
    
   

}
