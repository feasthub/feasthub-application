package com.monsor.feasthub.appservice.transaction.audit;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.appservice.event.EventCuisineListingAppService;
import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.audit.TransactionCashAudit;
import com.monsor.feasthub.jpa.repository.TransactionCashAuditRepository;
import com.monsor.feasthub.jpa.service.TrancationCashAuditService;

@Service
public class TransactionCashAuditAppService implements BaseService<TransactionCashAudit> {

	private static final Logger LOG = LoggerFactory.getLogger(EventCuisineListingAppService.class);
	@Autowired
	private TrancationCashAuditService cashAuditService;

	@Override
	public TransactionCashAudit findById(BigInteger id) {
		final TransactionCashAuditRepository cashAuditRepository = cashAuditService.getTrancationCashAuditRepository();
		return cashAuditRepository.findOne(id);
	}

	@Override
	public List<TransactionCashAudit> findAll() {
		final TransactionCashAuditRepository cashAuditRepository = cashAuditService.getTrancationCashAuditRepository();
		return (List<TransactionCashAudit>) cashAuditRepository.findAll();
	}

	@Override
	public TransactionCashAudit create(TransactionCashAudit entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create CashAudit :: " + entity);
		}
		final TransactionCashAuditRepository cashAuditRepository = cashAuditService.getTrancationCashAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public TransactionCashAudit update(TransactionCashAudit entity) {
		final TransactionCashAuditRepository cashAuditRepository = cashAuditService.getTrancationCashAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		final TransactionCashAuditRepository cashAuditRepository = cashAuditService.getTrancationCashAuditRepository();
		cashAuditRepository.delete(id);

	}

}
