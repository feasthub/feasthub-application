package com.monsor.feasthub.appservice;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.FrontPageStaticContent;
import com.monsor.feasthub.jpa.model.enums.StaticContent;
import com.monsor.feasthub.jpa.service.StaticContentService;
/**
 * @author Amit
 *
 */

@Service
public class StaticContentAppService implements BaseService<FrontPageStaticContent> {

	private static final Logger LOG = LoggerFactory.getLogger(StaticContentAppService.class);
	@Autowired
	private StaticContentService staticContentService;
	@Override
	public FrontPageStaticContent findById(BigInteger id) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById StaticContentAppService :: "+id );
		}
		checkNotNull(id, "This static content id must not be null");
		return staticContentService.getStaticContentRepository().findOne(id);
	}
	
	public FrontPageStaticContent findContentByType(StaticContent staticContent) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById StaticContentAppService :: "+staticContent );
		}
		checkNotNull(staticContent, "This static content  must not be null");
		return staticContentService.getStaticContentRepository().findOneByPageId(staticContent);
	}
	
	@Override
	public List<FrontPageStaticContent> findAll() {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findall StaticContentAppService :: " );
		}
		return (List<FrontPageStaticContent>)staticContentService.getStaticContentRepository().findAll();
	}
	@Override
	public FrontPageStaticContent create(FrontPageStaticContent entity) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create StaticContentAppService :: "+entity.toString() );
		}
		checkNotNull(entity, "This static content must not be null");
		return staticContentService.getStaticContentRepository().save(entity);
	}
	@Override
	public FrontPageStaticContent update(FrontPageStaticContent entity) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update StaticContentAppService :: "+entity.toString() );
		}
		checkNotNull(entity, "This static content must not be null");
		return staticContentService.getStaticContentRepository().save(entity);
	}
	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside delete StaticContentAppService :: "+id );
		}
		checkNotNull(id, "This payment id must not be null");
		staticContentService.getStaticContentRepository().delete(id);;
	}

	
}
