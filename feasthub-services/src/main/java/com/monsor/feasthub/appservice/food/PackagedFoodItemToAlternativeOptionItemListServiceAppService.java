package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.PackagedFoodItemToAlternativeOptionItemList;
import com.monsor.feasthub.jpa.repository.PackagedFoodItemToAlternativeOptionItemListRepository;
import com.monsor.feasthub.jpa.service.PackagedFoodItemToAlternativeOptionItemListService;

@Service
public class PackagedFoodItemToAlternativeOptionItemListServiceAppService
		implements BaseService<PackagedFoodItemToAlternativeOptionItemList> {

	private static final Logger LOG = LoggerFactory
			.getLogger(PackagedFoodItemToAlternativeOptionItemListServiceAppService.class);
	@Autowired
	private PackagedFoodItemToAlternativeOptionItemListService packagedFoodItemToAltOptionItemListService;

	@Override
	public PackagedFoodItemToAlternativeOptionItemList findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register PackagedFoodItemToAlternativeOptionItemList :: " + id);
		}
		checkNotNull(id, "This foodmenuitemId must not be null");
		final PackagedFoodItemToAlternativeOptionItemListRepository packagedFoodItemToAltOptionItemListRepo = packagedFoodItemToAltOptionItemListService
				.getPackagedFoodItemToAlternativeOptionItemListRepository();
		final PackagedFoodItemToAlternativeOptionItemList packagedFoodItemToAltOptionItemList = packagedFoodItemToAltOptionItemListRepo
				.findOne(id);
		return packagedFoodItemToAltOptionItemList;
	}

	@Override
	public List<PackagedFoodItemToAlternativeOptionItemList> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register PackagedFoodItemToAlternativeOptionItemList findAll :: ");
		}
		final PackagedFoodItemToAlternativeOptionItemListRepository packagedFoodItemToAltOptionItemListRepo = packagedFoodItemToAltOptionItemListService
				.getPackagedFoodItemToAlternativeOptionItemListRepository();
		return (List<PackagedFoodItemToAlternativeOptionItemList>) packagedFoodItemToAltOptionItemListRepo.findAll();
	}

	@Override
	public PackagedFoodItemToAlternativeOptionItemList create(PackagedFoodItemToAlternativeOptionItemList entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackagedFoodItemToAlternativeOptionItemList update(PackagedFoodItemToAlternativeOptionItemList entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}

}
