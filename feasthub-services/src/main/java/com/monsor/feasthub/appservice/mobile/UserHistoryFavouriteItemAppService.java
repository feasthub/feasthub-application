package com.monsor.feasthub.appservice.mobile;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.user.UserHistoryFavouriteItem;
import com.monsor.feasthub.jpa.repository.UserHistoryFavouriteItemRepository;
import com.monsor.feasthub.jpa.service.UserHistoryFavouriteItemService;

@Service
public class UserHistoryFavouriteItemAppService implements BaseService<UserHistoryFavouriteItem> {

	private static final Logger LOG = LoggerFactory.getLogger(UserHistoryFavouriteItemAppService.class);
	@Autowired
	private UserHistoryFavouriteItemService userHistoryFavItemService;

	 

	public List<UserHistoryFavouriteItem> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findAll UserHistoryFavouriteItem :: ");
		}
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		return (List<UserHistoryFavouriteItem>) userhistoryFavItemRepo.findAll();
	}

	/*
	 * to get history for a particular user
	 */

	// @Path("/histories/{id}")

	public List<UserHistoryFavouriteItem> findHistoryyByUserId(@Nonnull final BigInteger userId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById UserHistoryFavouriteItem :: " + userId);
		}
		checkNotNull(userId, "This userHisFavId must not be null");
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		final List<UserHistoryFavouriteItem> userHistoryItems = userhistoryFavItemRepo.findHistoryyByUserId(userId);
		return userHistoryItems;
	}

	/*
	 * to get fav. items for a particular user
	 */

	// @Path("/favs/{userid}")

	public List<UserHistoryFavouriteItem> findFavByUserId(@PathParam("userid") @Nonnull final BigInteger userId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findFavByUserId UserHistoryFavouriteItem :: " + userId);
		}
		checkNotNull(userId, "This userHisFavId must not be null");
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		final List<UserHistoryFavouriteItem> userFavItems = userhistoryFavItemRepo
				.findFavByUserIdAndFavoriteFlagTrue(userId);
		return userFavItems;
	}

	/*
	 * 
	 * 
	 * //@Path("/histories")
	 * 
	 *  
	 * 
	 * @RequestMapping(consumes = { MediaType.APPLICATION_JSON,
	 * MediaType.APPLICATION_XML }) public void createUserHistoryFavouriteItem(
	 * 
	 * @Nonnull final UserHistoryFavouriteItem userHistoryFavItem) { if
	 * (LOG.isDebugEnabled()) { LOG.debug(
	 * "Inside  createUserHistoryFavouriteItem :: " + userHistoryFavItem); }
	 * checkNotNull(userHistoryFavItem.getUserId(),
	 * "This userId must not be null");
	 * checkNotNull(userHistoryFavItem.getFoodMenuId(),
	 * "This FoodMenuId must not be null"); final
	 * UserHistoryFavouriteItemRepository userhistoryFavItemRepo =
	 * userHistoryFavItemService .getUserHistoryFavItemRepo();
	 * userhistoryFavItemRepo.save(userHistoryFavItem); }
	 */

	@Override
	public UserHistoryFavouriteItem findById(BigInteger id) {
		// TODO Auto-generated method stub
		checkNotNull(id, "This userHisFavId must not be null");
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		return userhistoryFavItemRepo.findOne(id);
	}

	@Override
	public UserHistoryFavouriteItem create(UserHistoryFavouriteItem entity) {
		// TODO Auto-generated method stub
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		return userhistoryFavItemRepo.save(entity);
	}

	@Override
	public UserHistoryFavouriteItem update(UserHistoryFavouriteItem entity) {
		// TODO Auto-generated method stub
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();
		return userhistoryFavItemRepo.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub
		final UserHistoryFavouriteItemRepository userhistoryFavItemRepo = userHistoryFavItemService
				.getUserHistoryFavItemRepo();

		userhistoryFavItemRepo.delete(id);
	}

}
