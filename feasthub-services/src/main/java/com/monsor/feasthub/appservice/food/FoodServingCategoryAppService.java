package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodMenuServingCategory;
import com.monsor.feasthub.jpa.repository.FoodServingCategoryRepository;
import com.monsor.feasthub.jpa.service.FoodServingCategoryService;
/**
 * @author Amit
 *
 */

@Service
public class FoodServingCategoryAppService implements BaseService<FoodMenuServingCategory> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodServingCategoryAppService.class);
	@Autowired
	private FoodServingCategoryService foodservingcategoryService;

	public FoodMenuServingCategory create(@Nonnull final FoodMenuServingCategory foodservingcategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodservingcategory :: " + foodservingcategory);
		}
		final FoodServingCategoryRepository foodservingcategoryRepository = foodservingcategoryService
				.getFoodServingCategoryRepository();
		return foodservingcategoryRepository.save(foodservingcategory);
	}

	public FoodMenuServingCategory update(@Nonnull final FoodMenuServingCategory foodservingcategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodservingcategory :: " + foodservingcategory);
		}
		final FoodServingCategoryRepository foodservingcategoryRepository = foodservingcategoryService
				.getFoodServingCategoryRepository();
		return foodservingcategoryRepository.save(foodservingcategory);
	}

	public void delete(@Nonnull final BigInteger foodservingcategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodservingcategory :: " + foodservingcategoryId);
		}
		checkNotNull(foodservingcategoryId, "This foodservingcategoryId must not be null");
		final FoodServingCategoryRepository foodservingcategoryRepository = foodservingcategoryService
				.getFoodServingCategoryRepository();
		final FoodMenuServingCategory foodservingcategory = foodservingcategoryRepository
				.findOne(foodservingcategoryId);
		foodservingcategoryRepository.delete(foodservingcategory);
	}

	public List<FoodMenuServingCategory> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodservingcategory :: ");
		}
		final FoodServingCategoryRepository foodservingcategoryRepository = foodservingcategoryService
				.getFoodServingCategoryRepository();
		return (List<FoodMenuServingCategory>) foodservingcategoryRepository.findAll();
	}

	public FoodMenuServingCategory findById(@Nonnull final BigInteger foodservingcategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodservingcategory :: " + foodservingcategoryId);
		}
		checkNotNull(foodservingcategoryId, "This foodservingcategoryId must not be null");
		final FoodServingCategoryRepository foodservingcategoryRepository = foodservingcategoryService
				.getFoodServingCategoryRepository();
		final FoodMenuServingCategory foodservingcategory = foodservingcategoryRepository
				.findOne(foodservingcategoryId);
		return foodservingcategory;
	}

}
