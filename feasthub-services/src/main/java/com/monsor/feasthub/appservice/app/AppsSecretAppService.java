package com.monsor.feasthub.appservice.app;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.app.AppsSecret;
import com.monsor.feasthub.jpa.repository.AppsSecretRepository;
import com.monsor.feasthub.jpa.service.AppsSecretService;
/**
 * @author Amit
 *
 */

@Service
public class AppsSecretAppService implements BaseService<AppsSecret> {

	private static final Logger LOG = LoggerFactory.getLogger(AppsSecretAppService.class);
	@Autowired
	private AppsSecretService appssecretService;

	public AppsSecret create(@Nonnull final AppsSecret appssecret) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecret);
		}
		checkNotNull(appssecret.getAppsName(), "This appsName must not be null");
		checkNotNull(appssecret.getClientId(), "This clientId must not be null");
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();
		checkState(isResourceExists(appssecret.getAppsName(), appssecret.getClientId()) == null,
				"This record is already exits, Please provide different combination of data");
		return appssecretRepository.save(appssecret);
	}

	public AppsSecret update(@Nonnull final AppsSecret appssecret) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecret);
		}
		checkNotNull(appssecret.getAppsName(), "This appsName must not be null");
		checkNotNull(appssecret.getClientId(), "This clientId must not be null");
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();
		return appssecretRepository.save(appssecret);
	}

	public void delete(@Nonnull final BigInteger appssecretId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecretId);
		}
		checkNotNull(appssecretId, "This appssecretId must not be null");
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();
		final AppsSecret appssecret = appssecretRepository.findOne(appssecretId);
		appssecretRepository.delete(appssecret);
	}

	public List<AppsSecret> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: ");
		}
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();
		return (List<AppsSecret>) appssecretRepository.findAll();
	}

	public AppsSecret findById(@Nonnull final BigInteger appssecretId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecretId);
		}
		checkNotNull(appssecretId, "This appssecretId must not be null");
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();
		final AppsSecret appssecret = appssecretRepository.findOne(appssecretId);
		return appssecret;
	}

	public AppsSecret isResourceExists(@Nullable final java.lang.String appsName,
			@Nullable final java.lang.String clientId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isAppsSecretExists");
		}
		checkNotNull(appsName, "This appsName must not be null");
		checkNotNull(clientId, "This clientId must not be null");
		final AppsSecretRepository appssecretRepository = appssecretService.getAppsSecretRepository();

		final AppsSecret AppsSecret = appssecretRepository.findDuplicate(appsName, clientId);

		return AppsSecret;
	}
}
