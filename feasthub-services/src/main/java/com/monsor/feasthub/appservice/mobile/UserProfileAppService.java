/**
 * 
 */
package com.monsor.feasthub.appservice.mobile;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.user.FhUser;
import com.monsor.feasthub.jpa.repository.UserRepository;
import com.monsor.feasthub.jpa.service.UserRegistrationService;

/**
 * @author Amit
 *
 */

@Service
// @Path("/v1.0/feasthub/users")
// @Group(name = "/registrations", title = "registrations")
// @HttpCode("500>Internal Server Error,200>Success Response")
public class UserProfileAppService implements Serializable, BaseService<FhUser> {

    /**
    * 
    */
    private static final long serialVersionUID = -2058096338957797820L;

    private static final Logger LOG = LoggerFactory.getLogger(UserProfileAppService.class);

    @Autowired
    private UserRegistrationService userRegistrationService;

    // @Path("/updateuser")

    public FhUser update(@Nonnull final FhUser user) {
	checkNotNull(user, "This user must not be null");
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return userRepository.save(user);
    }

    public FhUser performLogin(@Nonnull final String username, @Nonnull final String password) {

	checkNotNull(username, "This username must not be null");
	checkNotNull(password, "This password must not be null");
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	final FhUser user = userRepository.findByUserNameAndPassword(username, password);
	return user;
    }

    // @Path("/changepassword")

    public FhUser changePassword(@Nonnull final String userid, @Nonnull final String password,
	    @Nonnull final String newPassword) {

	checkNotNull(userid, "This username must not be null");
	checkNotNull(password, "This password must not be null");
	checkNotNull(newPassword, "This newPassword must not be null");
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	final FhUser user = userRepository.findByUserNameAndPassword(userid, password);
	checkNotNull(user, "no matching user found");
	user.setPassword(newPassword);
	return userRepository.save(user);
    }

    public FhUser resetPassword(@Nonnull final String userid, @Nonnull final String newPassword) {

	checkNotNull(userid, "This username must not be null");
	checkNotNull(newPassword, "This newPassword must not be null");
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	final FhUser user = userRepository.findByUserName(userid);
	checkNotNull(user, "no matching user found");
	user.setPassword(newPassword);
	return userRepository.save(user);
    }

    public FhUser create(@Nonnull final FhUser fhUser) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside register user :: " + fhUser);
	}
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return userRepository.save(fhUser);
    }

    // @Path("/validateuser")

    public List<FhUser> isUserExists(@Nullable final String username,
	    @Nullable final String emailId, @Nullable final String mobileNumber) {
	if (LOG.isDebugEnabled()) {
	    LOG.debug("Inside isUserExists");
	}
	if (username == null && emailId == null && mobileNumber == null) {
	    throw new BadRequestException("Please provide either username or email id");
	}
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	final List<FhUser> fhUser = userRepository
		.findAllByUserNameOrDefaultEmailIdOrDefaultMobileNumber(username, emailId,
			mobileNumber);
	return fhUser;
    }

    public FhUser findByUsername(@Nonnull final String username) {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return userRepository.findByUserName(username);
    }

    @Override
    public FhUser findById(BigInteger id) {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return userRepository.findOne(id);
    }

    @Override
    public List<FhUser> findAll() {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return (List<FhUser>) userRepository.findAll();
    }

    @Override
    public void delete(BigInteger id) {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	userRepository.delete(id);
    }

    public FhUser findByDefaultMobileNumber(String mobileNumber) {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	return userRepository.findByDefaultMobileNumber(mobileNumber);
    }

    public FhUser updateProfilePic(@Nonnull BigInteger userId, @Nonnull final String profileImage) {
	final UserRepository userRepository = userRegistrationService.getUserRepository();
	FhUser user = userRepository.findOne(userId);
	user.setProfilePhoto(profileImage);
	return userRepository.save(user);
    }
}
