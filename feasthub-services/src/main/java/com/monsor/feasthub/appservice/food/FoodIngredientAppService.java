package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodIngredient;
import com.monsor.feasthub.jpa.repository.FoodIngredientRepository;
import com.monsor.feasthub.jpa.service.FoodIngredientService;
/**
 * @author Amit
 *
 */

@Service
public class FoodIngredientAppService implements BaseService<FoodIngredient> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodIngredientAppService.class);
	@Autowired
	private FoodIngredientService foodingredientService;

	 

	public FoodIngredient create(@Nonnull final FoodIngredient foodingredient) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: " + foodingredient);
		}
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();

		checkState(!isFoodIngredientExists(foodingredient.getFoodReceipeId()),
				"This record is already exits, Please provide different combination of data");

		return foodingredientRepository.save(foodingredient);
	}

	 

	public FoodIngredient update(@Nonnull final FoodIngredient foodingredient) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: " + foodingredient);
		}
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();
		return foodingredientRepository.save(foodingredient);
	}

	 

	public void delete(@Nonnull final BigInteger foodingredientId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: " + foodingredientId);
		}
		checkNotNull(foodingredientId, "This foodingredientId must not be null");
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();
		final FoodIngredient foodingredient = foodingredientRepository.findOne(foodingredientId);
		foodingredientRepository.delete(foodingredient);
	}

	 

	public List<FoodIngredient> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: ");
		}
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();
		return (List<FoodIngredient>) foodingredientRepository.findAll();
	}

	 

	public FoodIngredient findById(@Nonnull final BigInteger foodingredientId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: " + foodingredientId);
		}
		checkNotNull(foodingredientId, "This foodingredientId must not be null");
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();
		final FoodIngredient foodingredient = foodingredientRepository.findOne(foodingredientId);
		return foodingredient;
	}

	// @Path("/search")
	 

	public List<FoodIngredient> search(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodingredient :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();
		return (List<FoodIngredient>) foodingredientRepository.searchFoodIngredientsIgnoreCase(search);
	}

	// @Path("/validatefoodingredients/{foodingredientname}")

	public Boolean isFoodIngredientExists(@Nullable final BigInteger foodingredientname) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFoodIngredientExists");
		}
		if (foodingredientname == null) {
			throw new BadRequestException("Please provide either foodingredientname");
		}
		final FoodIngredientRepository foodingredientRepository = foodingredientService.getFoodIngredientRepository();

		final FoodIngredient FoodIngredient = foodingredientRepository.findByFoodReceipeId(foodingredientname);
		if (FoodIngredient != null) {
			return true;
		}
		return false;
	}
}
