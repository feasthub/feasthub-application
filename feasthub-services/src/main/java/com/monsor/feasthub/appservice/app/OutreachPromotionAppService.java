package com.monsor.feasthub.appservice.app;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.app.OutreachPromotion;
import com.monsor.feasthub.jpa.repository.OutreachPromotionRepository;
import com.monsor.feasthub.jpa.service.OutreachPromotionService;
/**
 * @author Amit
 *
 */

@Service
public class OutreachPromotionAppService implements BaseService<OutreachPromotion> {

	private static final Logger LOG = LoggerFactory.getLogger(OutreachPromotionAppService.class);
	@Autowired
	private OutreachPromotionService currentpromotionService;

	 
	public OutreachPromotion create(@Nonnull final OutreachPromotion currentpromotion) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: " + currentpromotion);
		}
		checkNotNull(currentpromotion.getPromoCode(), "This promoCode must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		checkState(isResourceExists(currentpromotion.getPromoCode()) == null,
				"This record is already exits, Please provide different combination of data");
		return currentpromotionRepository.save(currentpromotion);
	}

	 
	public OutreachPromotion update(@Nonnull final OutreachPromotion currentpromotion) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: " + currentpromotion);
		}
		checkNotNull(currentpromotion.getPromoCode(), "This promoCode must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		return currentpromotionRepository.save(currentpromotion);
	}

	 
	public void delete(@Nonnull final BigInteger currentpromotionId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: " + currentpromotionId);
		}
		checkNotNull(currentpromotionId, "This currentpromotionId must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		final OutreachPromotion currentpromotion = currentpromotionRepository.findOne(currentpromotionId);
		currentpromotionRepository.delete(currentpromotion);
	}

	 
	public List<OutreachPromotion> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: ");
		}
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		return (List<OutreachPromotion>) currentpromotionRepository.findAll();
	}

	 
	public OutreachPromotion findById(@Nonnull final BigInteger currentpromotionId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: " + currentpromotionId);
		}
		checkNotNull(currentpromotionId, "This currentpromotionId must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		final OutreachPromotion currentpromotion = currentpromotionRepository.findOne(currentpromotionId);
		return currentpromotion;
	}

	 
	public List<OutreachPromotion> searchCurrentPromotions(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register currentpromotion :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();
		return (List<OutreachPromotion>) currentpromotionRepository.searchOutreachPromotionsIgnoreCase(search);
	}

	// 0
	public OutreachPromotion isResourceExists(@Nullable final java.lang.String promoCode) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isCurrentPromotionExists");
		}
		checkNotNull(promoCode, "This promoCode must not be null");
		final OutreachPromotionRepository currentpromotionRepository = currentpromotionService
				.getCurrentPromotionRepository();

		final OutreachPromotion currentPromotion = currentpromotionRepository.findDuplicate(promoCode);
		return currentPromotion;
	}
}
