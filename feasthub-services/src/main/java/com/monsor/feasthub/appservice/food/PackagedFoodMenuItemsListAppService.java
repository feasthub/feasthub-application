package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.PackagedFoodMenuItemsList;
import com.monsor.feasthub.jpa.repository.PackagedFoodMenuItemsListRepository;
import com.monsor.feasthub.jpa.service.PackagedFoodMenuItemsListService;

@Service

public class PackagedFoodMenuItemsListAppService implements BaseService<PackagedFoodMenuItemsList> {

	private static final Logger LOG = LoggerFactory.getLogger(PackagedFoodMenuItemsListAppService.class);
	@Autowired
	private PackagedFoodMenuItemsListService packagedFoodMenuItemsListService;

	@Override
	public PackagedFoodMenuItemsList findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register PackagedFoodItemToAlternativeOptionItemList :: " + id);
		}
		checkNotNull(id, "This foodmenuitemId must not be null");
		final PackagedFoodMenuItemsListRepository packagedFoodMenuItemsListRepository = packagedFoodMenuItemsListService
				.getPackagedFoodMenuItemsListRepository();
		final PackagedFoodMenuItemsList packagedFoodMenuItemsList = packagedFoodMenuItemsListRepository.findOne(id);
		return packagedFoodMenuItemsList;
	}

	@Override
	public List<PackagedFoodMenuItemsList> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: ");
		}
		final PackagedFoodMenuItemsListRepository packagedFoodMenuItemsListRepository = packagedFoodMenuItemsListService
				.getPackagedFoodMenuItemsListRepository();
		return (List<PackagedFoodMenuItemsList>) packagedFoodMenuItemsListRepository.findAll();
	}

	@Override
	public PackagedFoodMenuItemsList create(PackagedFoodMenuItemsList entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackagedFoodMenuItemsList update(PackagedFoodMenuItemsList entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}

}
