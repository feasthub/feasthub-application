package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodMenuTag;
import com.monsor.feasthub.jpa.repository.FoodMenuTagRepository;
import com.monsor.feasthub.jpa.service.FoodCategoryService;
import com.monsor.feasthub.jpa.service.FoodMenuItemService;
import com.monsor.feasthub.jpa.service.FoodMenuTagService;
/**
 * @author Amit
 *
 */

@Service
public class FoodMenuTagAppService implements BaseService<FoodMenuTag> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodMenuTagAppService.class);
	@Autowired
	private FoodMenuTagService foodmenutagService;

	@Autowired
	private FoodMenuItemService foodmenuitemService;

	@Autowired
	private FoodCategoryService foodCategoryService;

	public FoodMenuTag create(@Nonnull final FoodMenuTag foodmenutag) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenutag :: " + foodmenutag);
		}
		checkState(foodmenuitemService.getFoodMenuItemRepository().findOne(foodmenutag.getFoodMenuId()) != null,
				"Please provide a valid food menu item");
		checkState(foodCategoryService.getFoodCategoryRepository().findOne(foodmenutag.getFoodCategoryKey()) != null,
				"Please provide a valid food category");
		final FoodMenuTagRepository foodmenutagRepository = foodmenutagService.getFoodMenuTagRepository();
		return foodmenutagRepository.save(foodmenutag);
	}

	public FoodMenuTag update(@Nonnull final FoodMenuTag foodmenutag) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenutag :: " + foodmenutag);
		}
		checkState(foodmenuitemService.getFoodMenuItemRepository().findOne(foodmenutag.getFoodMenuId()) != null,
				"Please provide a valid food menu item");
		checkState(foodCategoryService.getFoodCategoryRepository().findOne(foodmenutag.getFoodCategoryKey()) != null,
				"Please provide a valid food category");
		final FoodMenuTagRepository foodmenutagRepository = foodmenutagService.getFoodMenuTagRepository();
		return foodmenutagRepository.save(foodmenutag);
	}

	public void delete(@Nonnull final BigInteger foodmenutagId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenutag :: " + foodmenutagId);
		}
		checkNotNull(foodmenutagId, "This foodmenutagId must not be null");
		final FoodMenuTagRepository foodmenutagRepository = foodmenutagService.getFoodMenuTagRepository();
		final FoodMenuTag foodmenutag = foodmenutagRepository.findOne(foodmenutagId);
		foodmenutagRepository.delete(foodmenutag);
	}

	public List<FoodMenuTag> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenutag :: ");
		}
		final FoodMenuTagRepository foodmenutagRepository = foodmenutagService.getFoodMenuTagRepository();
		return (List<FoodMenuTag>) foodmenutagRepository.findAll();
	}

	public FoodMenuTag findById(@Nonnull final BigInteger foodmenutagId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenutag :: " + foodmenutagId);
		}
		checkNotNull(foodmenutagId, "This foodmenutagId must not be null");
		final FoodMenuTagRepository foodmenutagRepository = foodmenutagService.getFoodMenuTagRepository();
		final FoodMenuTag foodmenutag = foodmenutagRepository.findOne(foodmenutagId);
		return foodmenutag;
	}

}
