package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.PackagedFoodMenuItemisedPricing;
import com.monsor.feasthub.jpa.repository.PackagedFoodMenuItemisedPricingRepository;
import com.monsor.feasthub.jpa.service.PackagedFoodMenuItemisedPricingService;

@Service
public class PackagedFoodMenuItemisedPricingAppService implements BaseService<PackagedFoodMenuItemisedPricing> {

	private static final Logger LOG = LoggerFactory.getLogger(PackagedFoodMenuItemisedPricingAppService.class);
	@Autowired
	private PackagedFoodMenuItemisedPricingService packagedFoodMenuItemisedPricingService;

	@Override
	public PackagedFoodMenuItemisedPricing findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register PackagedFoodMenuItemisedPricing :: " + id);
		}
		checkNotNull(id, "This foodmenuitemId must not be null");
		final PackagedFoodMenuItemisedPricingRepository packagedFoodMenuItemisedPricingRepo = packagedFoodMenuItemisedPricingService
				.getPackagedFoodMenuItemisedPricingRepository();
		final PackagedFoodMenuItemisedPricing packagedFoodMenuItemisedPricing = packagedFoodMenuItemisedPricingRepo
				.findOne(id);
		return packagedFoodMenuItemisedPricing;
	}

	@Override
	public List<PackagedFoodMenuItemisedPricing> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register PackagedFoodMenuItemisedPricing :: ");
		}
		final PackagedFoodMenuItemisedPricingRepository packagedFoodMenuItemisedPricingRepo = packagedFoodMenuItemisedPricingService
				.getPackagedFoodMenuItemisedPricingRepository();
		return (List<PackagedFoodMenuItemisedPricing>) packagedFoodMenuItemisedPricingRepo.findAll();
	}

	@Override
	public PackagedFoodMenuItemisedPricing create(PackagedFoodMenuItemisedPricing entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackagedFoodMenuItemisedPricing update(PackagedFoodMenuItemisedPricing entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}

}
