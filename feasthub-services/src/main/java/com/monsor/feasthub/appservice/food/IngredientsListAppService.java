package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.MasterGroceryIngredientsList;
import com.monsor.feasthub.jpa.repository.IngredientsListRepository;
import com.monsor.feasthub.jpa.service.IngredientsListService;
/**
 * @author Amit
 *
 */

@Service
public class IngredientsListAppService implements BaseService<MasterGroceryIngredientsList> {

	private static final Logger LOG = LoggerFactory.getLogger(IngredientsListAppService.class);
	@Autowired
	private IngredientsListService ingredientslistService;

	public MasterGroceryIngredientsList create(@Nonnull final MasterGroceryIngredientsList ingredientslist) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientslist :: " + ingredientslist);
		}
		final IngredientsListRepository ingredientslistRepository = ingredientslistService
				.getIngredientsListRepository();
		return ingredientslistRepository.save(ingredientslist);
	}

	public MasterGroceryIngredientsList update(@Nonnull final MasterGroceryIngredientsList ingredientslist) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientslist :: " + ingredientslist);
		}
		final IngredientsListRepository ingredientslistRepository = ingredientslistService
				.getIngredientsListRepository();
		return ingredientslistRepository.save(ingredientslist);
	}

	public void delete(@Nonnull final BigInteger ingredientslistId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientslist :: " + ingredientslistId);
		}
		checkNotNull(ingredientslistId, "This ingredientslistId must not be null");
		final IngredientsListRepository ingredientslistRepository = ingredientslistService
				.getIngredientsListRepository();
		final MasterGroceryIngredientsList ingredientslist = ingredientslistRepository.findOne(ingredientslistId);
		ingredientslistRepository.delete(ingredientslist);
	}

	public List<MasterGroceryIngredientsList> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientslist :: ");
		}
		final IngredientsListRepository ingredientslistRepository = ingredientslistService
				.getIngredientsListRepository();
		return (List<MasterGroceryIngredientsList>) ingredientslistRepository.findAll();
	}

	public MasterGroceryIngredientsList findById(@Nonnull final BigInteger ingredientslistId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientslist :: " + ingredientslistId);
		}
		checkNotNull(ingredientslistId, "This ingredientslistId must not be null");
		final IngredientsListRepository ingredientslistRepository = ingredientslistService
				.getIngredientsListRepository();
		final MasterGroceryIngredientsList ingredientslist = ingredientslistRepository.findOne(ingredientslistId);
		return ingredientslist;
	}

}
