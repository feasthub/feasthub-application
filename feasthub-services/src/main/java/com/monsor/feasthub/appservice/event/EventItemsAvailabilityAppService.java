package com.monsor.feasthub.appservice.event;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.events.EventItemsAvailability;
import com.monsor.feasthub.jpa.repository.EventItemsAvailabilityRepository;
import com.monsor.feasthub.jpa.service.EventItemsAvailabilityService;
/**
 * @author Amit
 *
 */

@Service
public class EventItemsAvailabilityAppService implements BaseService<EventItemsAvailability> {

	private static final Logger LOG = LoggerFactory.getLogger(EventItemsAvailabilityAppService.class);
	@Autowired
	private EventItemsAvailabilityService eventitemsavailabilityService;

	 

	public EventItemsAvailability create(@Nonnull final EventItemsAvailability eventitemsavailability) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailability);
		}
		checkNotNull(eventitemsavailability.getEventId(), "This eventId must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return eventitemsavailabilityRepository.save(eventitemsavailability);
	}

	 

	public EventItemsAvailability update(@Nonnull final EventItemsAvailability eventitemsavailability) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailability);
		}
		checkNotNull(eventitemsavailability.getEventId(), "This eventId must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return eventitemsavailabilityRepository.save(eventitemsavailability);
	}

	 

	public void delete(@Nonnull final BigInteger eventitemsavailabilityId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailabilityId);
		}
		checkNotNull(eventitemsavailabilityId, "This eventitemsavailabilityId must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		final EventItemsAvailability eventitemsavailability = eventitemsavailabilityRepository
				.findOne(eventitemsavailabilityId);
		eventitemsavailabilityRepository.delete(eventitemsavailability);
	}

	 

	public List<EventItemsAvailability> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: ");
		}
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return (List<EventItemsAvailability>) eventitemsavailabilityRepository.findAll();
	}

	 
	public EventItemsAvailability findById(@Nonnull final BigInteger eventitemsavailabilityId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventitemsavailabilityId);
		}
		checkNotNull(eventitemsavailabilityId, "This eventitemsavailabilityId must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		final EventItemsAvailability eventitemsavailability = eventitemsavailabilityRepository
				.findOne(eventitemsavailabilityId);
		return eventitemsavailability;
	}

	// @Path("/search")
	 

	public List<EventItemsAvailability> searchEventItemsAvailabilitys(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return (List<EventItemsAvailability>) eventitemsavailabilityRepository
				.searchEventItemsAvailabilitysIgnoreCase(search);
	}

	@SuppressWarnings("unchecked")
	public List<EventItemsAvailability> isResourceExists(@Nonnull final BigInteger eventid,
			@Nonnull final BigInteger foodid) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventid + " :: " + foodid);
		}
		checkNotNull(eventid, "This eventid must not be null");
		checkNotNull(foodid, "This foodid must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return (List<EventItemsAvailability>) eventitemsavailabilityRepository.findDuplicate(eventid, foodid);
	}

	 
	@SuppressWarnings("unchecked")
	public List<EventItemsAvailability> findByEventId(@Nonnull final BigInteger eventid) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventitemsavailability :: " + eventid);
		}
		checkNotNull(eventid, "This eventid must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		return (List<EventItemsAvailability>) eventitemsavailabilityRepository.findByEventId(eventid);
	}

	 
	public EventItemsAvailability findByEventIdAndListingDate(@Nonnull final BigInteger eventId,
			@Nonnull final Date listingDate) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register eventId :: " + eventId);
			LOG.debug("Inside register listingDate :: " + listingDate);
		}
		checkNotNull(eventId, "This eventId must not be null");
		checkNotNull(listingDate, "This listingDate must not be null");
		final EventItemsAvailabilityRepository eventitemsavailabilityRepository = eventitemsavailabilityService
				.getEventItemsAvailabilityRepository();
		final EventItemsAvailability eventitemsavailability = eventitemsavailabilityRepository
				.findByEventIdAndListingDate(eventId, listingDate);
		return eventitemsavailability;
	}

}
