package com.monsor.feasthub.appservice.user;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.user.UserAddress;
import com.monsor.feasthub.jpa.repository.UserAddressRepository;
import com.monsor.feasthub.jpa.service.UserAddressService;

@Service
public class UserAddressAppService implements BaseService<UserAddress> {

	private static final Logger LOG = LoggerFactory.getLogger(UserAddressAppService.class);
	@Autowired
	private UserAddressService userAddressService;

	@Override
	public UserAddress findById(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById UserAddress :: " + id);
		}
		checkNotNull(id, "This address id must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		final UserAddress userAddress = userAddressRepository.findOne(id);
		return userAddress;
	}

	public UserAddress findByUserIdAndId(BigInteger userId, BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findById UserAddress :: " + id);
		}
		checkNotNull(userId, "This user id must not be null");
		checkNotNull(id, "This address id must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		final UserAddress userAddress = userAddressRepository.findOneByUserIdAndId(userId, id);
		return userAddress;
	}

	@Override
	public List<UserAddress> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: ");
		}

		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		return (List<UserAddress>) userAddressRepository.findAll();
	}

	@Override
	public UserAddress create(UserAddress entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create UserAddress :: " + entity);
		}
		checkNotNull(entity, "This UserAddress must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		return userAddressRepository.save(entity);
	}

	@Override
	public UserAddress update(UserAddress entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside update UserAddress :: " + entity);
		}
		checkNotNull(entity, "This UserAddress must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		return userAddressRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside delete UserAddress :: " + id);
		}
		checkNotNull(id, "This foodmenuitemId must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		final UserAddress userAddress = userAddressRepository.findOne(id);
		userAddressRepository.delete(userAddress);
	}

	public List<UserAddress> findByUserId(BigInteger userId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findByUserId UserAddress :: ");
		}
		checkNotNull(userId, "This userId must not be null");
		final UserAddressRepository userAddressRepository = userAddressService.getUserAddressRepository();
		return (List<UserAddress>) userAddressRepository.findByUserId(userId);
	}

}
