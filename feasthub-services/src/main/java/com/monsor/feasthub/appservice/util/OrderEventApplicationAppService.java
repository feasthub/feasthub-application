/**
 * 
 */
package com.monsor.feasthub.appservice.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.order.OrderEvent;
import com.monsor.feasthub.jpa.repository.OrderEventRepository;
import com.monsor.feasthub.jpa.service.OrderEventService;

/**
 * @author amit
 *
 */


@Service
public class OrderEventApplicationAppService implements BaseService<OrderEvent> {
	//private static final Logger LOG = LoggerFactory.getLogger(OrderEventApplicationAppService.class);
	@Autowired
	private OrderEventService orderEventService;

	@Override
	public OrderEvent findById(BigInteger id) {
		final OrderEventRepository orderEventRepository = orderEventService.getOrderEventRepository();
		checkNotNull(id, "Order Event Id cannot be null");
		return orderEventRepository.findOne(id);
	}

	@Override
	public List<OrderEvent> findAll() {
		final OrderEventRepository orderEventRepository = orderEventService.getOrderEventRepository();
		return (List<OrderEvent>) orderEventRepository.findAll();
	}

	@Override
	public OrderEvent create(OrderEvent entity) {
		final OrderEventRepository orderEventRepository = orderEventService.getOrderEventRepository();
		checkNotNull(entity, "Order Event cannot be null");
		return orderEventRepository.save(entity);
	}

	@Override
	public OrderEvent update(OrderEvent entity) {
		final OrderEventRepository orderEventRepository = orderEventService.getOrderEventRepository();
		checkNotNull(entity, "Order Event cannot be null");
		checkNotNull(entity.getId(), "Order Event Id cannot be null");
		return orderEventRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		final OrderEventRepository orderEventRepository = orderEventService.getOrderEventRepository();
		checkNotNull(id, "Order Event Id cannot be null");
		orderEventRepository.delete(id);
	}

}
