package com.monsor.feasthub.appservice.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.delivery.DeliverySlotsTime;
import com.monsor.feasthub.jpa.repository.DeliverySlotsTimeRepository;
import com.monsor.feasthub.jpa.service.DeliverySlotsTimeService;
/**
 * @author Amit
 *
 */

@Service
public class DeliverySlotsTimeAppService implements BaseService<DeliverySlotsTime> {

	private static final Logger LOG = LoggerFactory.getLogger(DeliverySlotsTimeAppService.class);
	@Autowired
	private DeliverySlotsTimeService deliveryslotstimeService;

	 

	public DeliverySlotsTime create(@Nonnull final DeliverySlotsTime deliveryslotstime) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslotstime :: " + deliveryslotstime);
		}
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();

		checkState(isDeliverySlotsTimeExists(deliveryslotstime.getId()) != null,
				"This record is already exits, Please provide different combination of data");

		return deliveryslotstimeRepository.save(deliveryslotstime);
	}

	 

	public DeliverySlotsTime update(@Nonnull final DeliverySlotsTime deliveryslotstime) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslotstime :: " + deliveryslotstime);
		}
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();
		return deliveryslotstimeRepository.save(deliveryslotstime);
	}

	 

	public void delete(@Nonnull final BigInteger deliveryslotstimeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslotstime :: " + deliveryslotstimeId);
		}
		checkNotNull(deliveryslotstimeId, "This deliveryslotstimeId must not be null");
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();
		final DeliverySlotsTime deliveryslotstime = deliveryslotstimeRepository.findOne(deliveryslotstimeId);
		deliveryslotstimeRepository.delete(deliveryslotstime);
	}

	 

	public List<DeliverySlotsTime> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslotstime :: ");
		}
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();
		return (List<DeliverySlotsTime>) deliveryslotstimeRepository.findAll();
	}

	 

	public DeliverySlotsTime findById(@Nonnull final BigInteger deliveryslotstimeId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliveryslotstime :: " + deliveryslotstimeId);
		}
		checkNotNull(deliveryslotstimeId, "This deliveryslotstimeId must not be null");
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();
		final DeliverySlotsTime deliveryslotstime = deliveryslotstimeRepository.findOne(deliveryslotstimeId);
		return deliveryslotstime;
	}

	// @Path("/validatedeliveryslotstimes/{deliveryslotstimename}")

	public DeliverySlotsTime isDeliverySlotsTimeExists(@Nullable final BigInteger deliveryslotstimename) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isDeliverySlotsTimeExists");
		}
		if (deliveryslotstimename == null) {
			throw new BadRequestException("Please provide either deliveryslotstimename");
		}
		final DeliverySlotsTimeRepository deliveryslotstimeRepository = deliveryslotstimeService
				.getDeliverySlotsTimeRepository();

		final DeliverySlotsTime deliverySlotsTime = deliveryslotstimeRepository.findOne(deliveryslotstimename);

		return deliverySlotsTime;
	}
}
