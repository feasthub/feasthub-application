package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.FoodMenuItem;
import com.monsor.feasthub.jpa.model.view.FoodMenuItemView;
import com.monsor.feasthub.jpa.repository.FoodMenuItemRepository;
import com.monsor.feasthub.jpa.repository.FoodMenuItemViewRepository;
import com.monsor.feasthub.jpa.service.FoodMenuItemService;
import com.monsor.feasthub.jpa.service.FoodMenuItemViewService;
/**
 * @author Amit
 *
 */

@Service
public class FoodMenuItemAppService implements BaseService<FoodMenuItem> {

	private static final Logger LOG = LoggerFactory.getLogger(FoodMenuItemAppService.class);
	@Autowired
	private FoodMenuItemService foodmenuitemService;

	@Autowired
	private FoodMenuItemViewService foodmenuitemViewService;

	public FoodMenuItem create(@Nonnull FoodMenuItem foodmenuitem) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + foodmenuitem);
		}
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();

		checkState(isFoodMenuItemExists(foodmenuitem.getFoodName()) == null,
				"This record is already exits, Please provide different combination of data");

		/*
		 * for (CustomFoodCategoriesList categorList :
		 * foodmenuitem.getCustomFoodCategoriesList()) {
		 * categorList.setFoodMenuId(foodmenuitem); for
		 * (CustomFoodCategoriesItems items :
		 * categorList.getCustomFoodCategoriesItems()) {
		 * items.setCustomFoodCategoriesListId(categorList); for
		 * (CustomFoodCategoriesItemsList itemList :
		 * items.getCustomFoodCategoriesItemsList()) {
		 * itemList.setCustomFoodCategoriesItemsId(items); } } }
		 */
		return foodmenuitemRepository.save(foodmenuitem);

	}

	public FoodMenuItem update(@Nonnull final FoodMenuItem foodmenuitem) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + foodmenuitem);
		}
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		/*
		 * for (CustomFoodCategoriesList categorList :
		 * foodmenuitem.getCustomFoodCategoriesList()) {
		 * categorList.setFoodMenuId(foodmenuitem); for
		 * (CustomFoodCategoriesItems items :
		 * categorList.getCustomFoodCategoriesItems()) {
		 * items.setCustomFoodCategoriesListId(categorList); for
		 * (CustomFoodCategoriesItemsList itemList :
		 * items.getCustomFoodCategoriesItemsList()) {
		 * itemList.setCustomFoodCategoriesItemsId(items); } } }
		 */
		return foodmenuitemRepository.save(foodmenuitem);
	}

	public void delete(@Nonnull final BigInteger foodmenuitemId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + foodmenuitemId);
		}
		checkNotNull(foodmenuitemId, "This foodmenuitemId must not be null");
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		final FoodMenuItem foodmenuitem = foodmenuitemRepository.findOne(foodmenuitemId);
		foodmenuitemRepository.delete(foodmenuitem);
	}

	 
	public List<FoodMenuItem> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: ");
		}
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		return (List<FoodMenuItem>) foodmenuitemRepository.findAll();
	}

	public FoodMenuItem findById(@Nonnull final BigInteger foodmenuitemId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + foodmenuitemId);
		}
		checkNotNull(foodmenuitemId, "This foodmenuitemId must not be null");
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		final FoodMenuItem foodmenuitem = foodmenuitemRepository.findOne(foodmenuitemId);
		return foodmenuitem;
	}

	/**
	 * This method is used to search for Food Menu Item. Given text will be
	 * search in food menu item and food menu description. If giving text is
	 * found then that item will be returned at the calling point.
	 * 
	 * @param search
	 * @return
	 */
	public List<FoodMenuItem> search(@Nonnull final String search) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + search);
		}
		checkNotNull(search, "This search must not be null");
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		return (List<FoodMenuItem>) foodmenuitemRepository.searchByFoodMenuItemsIgnoreCaseContaining(search);
	}

	/**
	 * This method is used to check the availability of food menu item using the
	 * food menu name
	 * 
	 * @param foodmenuitemname
	 * @return
	 */
	public FoodMenuItem isFoodMenuItemExists(@Nullable final String foodmenuitemname) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isFoodMenuItemExists");
		}
		if (foodmenuitemname == null) {
			throw new BadRequestException("Please provide either foodmenuitemname");
		}
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();

		final FoodMenuItem foodMenuItem = foodmenuitemRepository.findByFoodName(foodmenuitemname);

		return foodMenuItem;
	}

	/**
	 * This method is used to change custom food menu item to additional food
	 * menu item which can not be changes in tha package meal.
	 * 
	 * @param complexFoodMenuItemId
	 * @param customFoodMenuId
	 * @return
	 */

	public FoodMenuItem changeToAdditionalItem(final BigInteger complexFoodMenuItemId,
			final BigInteger customFoodMenuId) {
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		FoodMenuItem foodMenuItem = foodmenuitemRepository.findOne(complexFoodMenuItemId);

		/*
		 * CustomFoodCategoriesList customFoodCategoryList =
		 * foodMenuItem.getCustomFoodCategoriesList().stream() .filter(item ->
		 * item.getCustomFoodMenuId().getId().equals(customFoodMenuId)).
		 * findFirst().orElse(null); AdditionalFoodItemsList additionalItem =
		 * new AdditionalFoodItemsList(foodMenuItem,
		 * customFoodCategoryList.getCustomFoodMenuId());
		 * foodMenuItem.getAdditionalFoodItemsList().add(additionalItem);
		 * foodMenuItem.getCustomFoodCategoriesList().remove(
		 * customFoodCategoryList);
		 */
		return foodmenuitemRepository.save(foodMenuItem);

	}

	/**
	 * This method is used to change a additional menu item to custom menu item.
	 * Custom menu item can be replace with the suggested change menu item with
	 * in a particular package meal.
	 * 
	 * @param complexFoodMenuItemId
	 * @param additionalItemListId
	 * @param customFoodCategoriesItems
	 * @return
	 */
	/*
	 * public FoodMenuItem changeToCustomFoodItem(final BigInteger
	 * complexFoodMenuItemId, final BigInteger additionalItemListId,
	 * List<CustomFoodCategoriesItems> customFoodCategoriesItems) { final
	 * FoodMenuItemRepository foodmenuitemRepository =
	 * foodmenuitemService.getFoodMenuItemRepository(); FoodMenuItem
	 * foodMenuItem = foodmenuitemRepository.findOne(complexFoodMenuItemId);
	 * 
	 * AdditionalFoodItemsList additionalItem =
	 * foodMenuItem.getAdditionalFoodItemsList().stream() .filter(item ->
	 * item.getId().equals(additionalItemListId)).findFirst().orElse(null);
	 * 
	 * foodMenuItem.getAdditionalFoodItemsList().remove(additionalItem); final
	 * CustomFoodCategoriesList customFoodCategoryList = new
	 * CustomFoodCategoriesList(foodMenuItem,
	 * additionalItem.getAdditionalFoodMenuId(), customFoodCategoriesItems);
	 * foodMenuItem.getCustomFoodCategoriesList().add(customFoodCategoryList);
	 * return foodmenuitemRepository.save(foodMenuItem);
	 * 
	 * }
	 */

	/**
	 * This method is used to copy the existing item to created a new food menu
	 * item. The object which this method will return will have all data coppied
	 * except the id fields.
	 * 
	 * @param foodMenuItem
	 * @return
	 */
	public FoodMenuItem copyItem(BigInteger foodMenuItemId) {
		final FoodMenuItemRepository foodmenuitemRepository = foodmenuitemService.getFoodMenuItemRepository();
		FoodMenuItem foodMenuItem = foodmenuitemRepository.findOne(foodMenuItemId);
		FoodMenuItem newFoodMenuItem = new FoodMenuItem();

		BeanUtils.copyProperties(foodMenuItem, newFoodMenuItem);
		newFoodMenuItem.setId(null);
		/*
		 * for (AdditionalFoodItemsList additionalItem :
		 * newFoodMenuItem.getAdditionalFoodItemsList()) {
		 * additionalItem.setId(null); additionalItem.setFoodMenuId(null); } for
		 * (CustomFoodCategoriesList categorList :
		 * newFoodMenuItem.getCustomFoodCategoriesList()) {
		 * categorList.setId(null); categorList.setFoodMenuId(null); for
		 * (CustomFoodCategoriesItems items :
		 * categorList.getCustomFoodCategoriesItems()) { items.setId(null);
		 * 
		 * for (CustomFoodCategoriesItemsList itemList :
		 * items.getCustomFoodCategoriesItemsList()) { itemList.setId(null); } }
		 * }
		 */

		return newFoodMenuItem;
	}

	public FoodMenuItemView findFoodMenuItemViewById(@Nonnull final BigInteger foodmenuitemId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register foodmenuitem :: " + foodmenuitemId);
		}
		checkNotNull(foodmenuitemId, "This foodmenuitemId must not be null");
		final FoodMenuItemViewRepository foodmenuitemRepository = foodmenuitemViewService
				.getFoodMenuItemViewRepository();
		final FoodMenuItemView foodmenuitem = foodmenuitemRepository.findOne(foodmenuitemId);
		return foodmenuitem;
	}

}
