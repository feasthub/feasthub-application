package com.monsor.feasthub.appservice.transaction.audit;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.appservice.event.EventCuisineListingAppService;
import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.audit.TransactionFeastAudit;
import com.monsor.feasthub.jpa.repository.TransactionFeastAuditRepository;
import com.monsor.feasthub.jpa.service.TrancationFeastAuditService;

@Service
public class TransactionFeastAuditAppService implements BaseService<TransactionFeastAudit> {

	private static final Logger LOG = LoggerFactory.getLogger(EventCuisineListingAppService.class);
	@Autowired
	private TrancationFeastAuditService cashAuditService;

	@Override
	public TransactionFeastAudit findById(BigInteger id) {
		final TransactionFeastAuditRepository cashAuditRepository = cashAuditService
				.getTrancationFeastAuditRepository();
		return cashAuditRepository.findOne(id);
	}

	@Override
	public List<TransactionFeastAudit> findAll() {
		final TransactionFeastAuditRepository cashAuditRepository = cashAuditService
				.getTrancationFeastAuditRepository();
		return (List<TransactionFeastAudit>) cashAuditRepository.findAll();
	}

	@Override
	public TransactionFeastAudit create(TransactionFeastAudit entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create CashAudit :: " + entity);
		}
		final TransactionFeastAuditRepository cashAuditRepository = cashAuditService
				.getTrancationFeastAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public TransactionFeastAudit update(TransactionFeastAudit entity) {
		final TransactionFeastAuditRepository cashAuditRepository = cashAuditService
				.getTrancationFeastAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		final TransactionFeastAuditRepository cashAuditRepository = cashAuditService
				.getTrancationFeastAuditRepository();
		cashAuditRepository.delete(id);

	}

}
