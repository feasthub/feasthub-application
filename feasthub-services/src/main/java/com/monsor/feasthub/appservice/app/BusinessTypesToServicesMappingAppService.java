package com.monsor.feasthub.appservice.app;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.app.BusinessTypesToServicesMapping;
import com.monsor.feasthub.jpa.repository.BusinessTypesToServicesMappingRepository;
import com.monsor.feasthub.jpa.service.BusinessTypesToServicesMappingService;
/**
 * @author Amit
 *
 */

@Service
public class BusinessTypesToServicesMappingAppService implements BaseService<BusinessTypesToServicesMapping> {

	private static final Logger LOG = LoggerFactory.getLogger(BusinessTypesToServicesMappingAppService.class);
	@Autowired
	private BusinessTypesToServicesMappingService businesstypestoservicesmappingService;

	public BusinessTypesToServicesMapping create(
			@Nonnull final BusinessTypesToServicesMapping businesstypestoservicesmapping) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmapping);
		}
		checkNotNull(businesstypestoservicesmapping.getAdditionalServiceOffering(),
				"This additionalServiceOffering must not be null");
		checkNotNull(businesstypestoservicesmapping.getServicesOfferingTypes(),
				"This servicesOfferingTypes must not be null");
		final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository = businesstypestoservicesmappingService
				.getBusinessTypesToServicesMappingRepository();
		return businesstypestoservicesmappingRepository.save(businesstypestoservicesmapping);
	}

	public BusinessTypesToServicesMapping update(
			@Nonnull final BusinessTypesToServicesMapping businesstypestoservicesmapping) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmapping);
		}
		checkNotNull(businesstypestoservicesmapping.getAdditionalServiceOffering(),
				"This additionalServiceOffering must not be null");
		checkNotNull(businesstypestoservicesmapping.getServicesOfferingTypes(),
				"This servicesOfferingTypes must not be null");
		final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository = businesstypestoservicesmappingService
				.getBusinessTypesToServicesMappingRepository();
		return businesstypestoservicesmappingRepository.save(businesstypestoservicesmapping);
	}

	public void delete(@Nonnull final BigInteger businesstypestoservicesmappingId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmappingId);
		}
		checkNotNull(businesstypestoservicesmappingId, "This businesstypestoservicesmappingId must not be null");
		final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository = businesstypestoservicesmappingService
				.getBusinessTypesToServicesMappingRepository();
		final BusinessTypesToServicesMapping businesstypestoservicesmapping = businesstypestoservicesmappingRepository
				.findOne(businesstypestoservicesmappingId);
		businesstypestoservicesmappingRepository.delete(businesstypestoservicesmapping);
	}

	public List<BusinessTypesToServicesMapping> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register businesstypestoservicesmapping :: ");
		}
		final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository = businesstypestoservicesmappingService
				.getBusinessTypesToServicesMappingRepository();
		return (List<BusinessTypesToServicesMapping>) businesstypestoservicesmappingRepository.findAll();
	}

	public BusinessTypesToServicesMapping findById(@Nonnull final BigInteger businesstypestoservicesmappingId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register businesstypestoservicesmapping :: " + businesstypestoservicesmappingId);
		}
		checkNotNull(businesstypestoservicesmappingId, "This businesstypestoservicesmappingId must not be null");
		final BusinessTypesToServicesMappingRepository businesstypestoservicesmappingRepository = businesstypestoservicesmappingService
				.getBusinessTypesToServicesMappingRepository();
		final BusinessTypesToServicesMapping businesstypestoservicesmapping = businesstypestoservicesmappingRepository
				.findOne(businesstypestoservicesmappingId);
		return businesstypestoservicesmapping;
	}

}
