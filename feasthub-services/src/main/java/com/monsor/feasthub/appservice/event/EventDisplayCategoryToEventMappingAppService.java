package com.monsor.feasthub.appservice.event;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.events.EventDisplayCategoryToEventMapping;
import com.monsor.feasthub.jpa.repository.EventDisplayCategoryToEventMappingRepository;
import com.monsor.feasthub.jpa.service.EventDisplayCategoryToEventMappingService;

@Service
public class EventDisplayCategoryToEventMappingAppService implements BaseService<EventDisplayCategoryToEventMapping> {
	private static final Logger LOG = LoggerFactory.getLogger(EventDisplayCategoryToEventMappingAppService.class);
	@Autowired
	private EventDisplayCategoryToEventMappingService eventDisplayCategoryToEventMappingService;

	@Override
	public EventDisplayCategoryToEventMapping findById(BigInteger id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EventDisplayCategoryToEventMapping> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register EventDisplayCategory :: ");
		}
		final EventDisplayCategoryToEventMappingRepository eventDisplayCategoryToEventMappingRepository = eventDisplayCategoryToEventMappingService
				.getEventDisplayCategoryToEventMappingRepository();
		return (List<EventDisplayCategoryToEventMapping>) eventDisplayCategoryToEventMappingRepository.findAll();
	}

	@Override
	public EventDisplayCategoryToEventMapping create(EventDisplayCategoryToEventMapping entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventDisplayCategoryToEventMapping update(EventDisplayCategoryToEventMapping entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}

}
