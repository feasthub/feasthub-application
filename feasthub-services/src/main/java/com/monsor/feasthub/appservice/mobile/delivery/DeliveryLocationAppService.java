package com.monsor.feasthub.appservice.mobile.delivery;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.delivery.DeliveryLocation;
import com.monsor.feasthub.jpa.repository.DeliveryLocationRepository;
import com.monsor.feasthub.jpa.service.DeliveryLocationService;
/**
 * @author Amit
 *
 */

@Service
public class DeliveryLocationAppService implements BaseService<DeliveryLocation> {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryLocationAppService.class);
	@Autowired
	private DeliveryLocationService deliverylocationService;

	 

	public DeliveryLocation create(@Nonnull final DeliveryLocation deliverylocation) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverylocation :: " + deliverylocation);
		}
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();

		return deliverylocationRepository.save(deliverylocation);
	}

	 

	public DeliveryLocation update(@Nonnull final DeliveryLocation deliverylocation) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverylocation :: " + deliverylocation);
		}
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();
		return deliverylocationRepository.save(deliverylocation);
	}

	 

	public void delete(@Nonnull final BigInteger deliverylocationId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverylocation :: " + deliverylocationId);
		}
		checkNotNull(deliverylocationId, "This deliverylocationId must not be null");
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();
		final DeliveryLocation deliverylocation = deliverylocationRepository.findOne(deliverylocationId);
		deliverylocationRepository.delete(deliverylocation);
	}

	 

	public List<DeliveryLocation> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverylocation :: ");
		}
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();
		return (List<DeliveryLocation>) deliverylocationRepository.findAll();
	}

	 

	public DeliveryLocation findById(@Nonnull final BigInteger deliverylocationId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register deliverylocation :: " + deliverylocationId);
		}
		checkNotNull(deliverylocationId, "This deliverylocationId must not be null");
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();
		final DeliveryLocation deliverylocation = deliverylocationRepository.findOne(deliverylocationId);
		return deliverylocation;
	}

	// @Path("/validatedeliverylocations/{deliverylocationname}")

	public DeliveryLocation isDeliveryLocationExists(@Nullable final BigInteger deliverylocationname) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isDeliveryLocationExists");
		}
		if (deliverylocationname == null) {
			throw new BadRequestException("Please provide either deliverylocationname");
		}
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();

		final DeliveryLocation deliveryLocation = deliverylocationRepository.findOne(deliverylocationname);

		return deliveryLocation;
	}

	// @Path("/findByLatLng/{latitude}/{longitude}")

	public DeliveryLocation findByLattitudeAndLongitude(@Nullable final Double latitude,
			@Nullable final Double longitude) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findByLatLng");
		}
		if ((latitude == null) || (longitude == null)) {
			throw new BadRequestException("Please provide either deliverylocationname");
		}
		final DeliveryLocationRepository deliverylocationRepository = deliverylocationService
				.getDeliveryLocationRepository();

		final DeliveryLocation deliveryLocation = deliverylocationRepository.findByLattitudeAndLongitude(latitude,
				longitude);

		return deliveryLocation;
	}
}
