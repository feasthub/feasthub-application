package com.monsor.feasthub.appservice.food;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.food.IngredientsCategory;
import com.monsor.feasthub.jpa.repository.IngredientsCategoryRepository;
import com.monsor.feasthub.jpa.service.IngredientsCategoryService;
/**
 * @author Amit
 *
 */

@Service
public class IngredientsCategoryAppService implements BaseService<IngredientsCategory> {

	private static final Logger LOG = LoggerFactory.getLogger(IngredientsCategoryAppService.class);
	@Autowired
	private IngredientsCategoryService ingredientscategoryService;

	public IngredientsCategory create(@Nonnull final IngredientsCategory ingredientscategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientscategory :: " + ingredientscategory);
		}
		final IngredientsCategoryRepository ingredientscategoryRepository = ingredientscategoryService
				.getIngredientsCategoryRepository();
		return ingredientscategoryRepository.save(ingredientscategory);
	}

	public IngredientsCategory update(@Nonnull final IngredientsCategory ingredientscategory) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientscategory :: " + ingredientscategory);
		}
		final IngredientsCategoryRepository ingredientscategoryRepository = ingredientscategoryService
				.getIngredientsCategoryRepository();
		return ingredientscategoryRepository.save(ingredientscategory);
	}

	public void delete(@Nonnull final BigInteger ingredientscategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientscategory :: " + ingredientscategoryId);
		}
		checkNotNull(ingredientscategoryId, "This ingredientscategoryId must not be null");
		final IngredientsCategoryRepository ingredientscategoryRepository = ingredientscategoryService
				.getIngredientsCategoryRepository();
		final IngredientsCategory ingredientscategory = ingredientscategoryRepository.findOne(ingredientscategoryId);
		ingredientscategoryRepository.delete(ingredientscategory);
	}

	public List<IngredientsCategory> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientscategory :: ");
		}
		final IngredientsCategoryRepository ingredientscategoryRepository = ingredientscategoryService
				.getIngredientsCategoryRepository();
		return (List<IngredientsCategory>) ingredientscategoryRepository.findAll();
	}

	public IngredientsCategory findById(@Nonnull final BigInteger ingredientscategoryId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register ingredientscategory :: " + ingredientscategoryId);
		}
		checkNotNull(ingredientscategoryId, "This ingredientscategoryId must not be null");
		final IngredientsCategoryRepository ingredientscategoryRepository = ingredientscategoryService
				.getIngredientsCategoryRepository();
		final IngredientsCategory ingredientscategory = ingredientscategoryRepository.findOne(ingredientscategoryId);
		return ingredientscategory;
	}

}
