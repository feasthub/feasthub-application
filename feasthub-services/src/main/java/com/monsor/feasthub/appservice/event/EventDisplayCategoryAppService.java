package com.monsor.feasthub.appservice.event;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.events.EventDisplayCategory;
import com.monsor.feasthub.jpa.repository.EventDisplayCategoryRepository;
import com.monsor.feasthub.jpa.service.EventDisplayCategoryService;

@Service
public class EventDisplayCategoryAppService implements BaseService<EventDisplayCategory> {
	private static final Logger LOG = LoggerFactory.getLogger(EventDisplayCategoryAppService.class);
	@Autowired
	private EventDisplayCategoryService eventDisplayCategoryService;

	@Override
	public EventDisplayCategory findById(BigInteger id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EventDisplayCategory> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register EventDisplayCategory :: ");
		}
		final EventDisplayCategoryRepository eventDisplayCategoryRepository = eventDisplayCategoryService
				.getEventDisplayCategoryRepository();
		return (List<EventDisplayCategory>) eventDisplayCategoryRepository.findAll();
	}

	@Override
	public EventDisplayCategory create(EventDisplayCategory entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventDisplayCategory update(EventDisplayCategory entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}
}
