package com.monsor.feasthub.appservice.app;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.app.FrontPageOutReachMode;
import com.monsor.feasthub.jpa.repository.FrontPageOutreachModeRepository;
import com.monsor.feasthub.jpa.service.FrontPageOutreachModeService;
/**
 * @author Amit
 *
 */

@Service
public class FrontPageOutreachModeAppService implements BaseService<FrontPageOutReachMode> {

	private static final Logger LOG = LoggerFactory.getLogger(FrontPageOutreachModeAppService.class);
	@Autowired
	private FrontPageOutreachModeService frontPageOutReachModeService;

	 
	public FrontPageOutReachMode create(@Nonnull final FrontPageOutReachMode appssecret) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecret);
		}
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		return appssecretRepository.save(appssecret);
	}

	 
	public FrontPageOutReachMode update(@Nonnull final FrontPageOutReachMode appssecret) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecret);
		}
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		return appssecretRepository.save(appssecret);
	}

	 
	public void delete(@Nonnull final BigInteger appssecretId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecretId);
		}
		checkNotNull(appssecretId, "This appssecretId must not be null");
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		final FrontPageOutReachMode appssecret = appssecretRepository.findOne(appssecretId);
		appssecretRepository.delete(appssecret);
	}

	 
	public List<FrontPageOutReachMode> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: ");
		}
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		return (List<FrontPageOutReachMode>) appssecretRepository.findAll();
	}

	 
	public FrontPageOutReachMode findById(@Nonnull final BigInteger appssecretId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + appssecretId);
		}
		checkNotNull(appssecretId, "This appssecretId must not be null");
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		final FrontPageOutReachMode appssecret = appssecretRepository.findOne(appssecretId);
		return appssecret;
	}

	 
	public FrontPageOutReachMode findByOutreachType(@Nonnull final String outreachType) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register appssecret :: " + outreachType);
		}
		checkNotNull(outreachType, "This outreachType must not be null");
		final FrontPageOutreachModeRepository appssecretRepository = frontPageOutReachModeService.getFrontPageOutReachModeRepository();
		final FrontPageOutReachMode appssecret = appssecretRepository.findOneByOutreachTypeIgnoreCase(outreachType);
		return appssecret;
	}
	
}
