package com.monsor.feasthub.appservice.mobile.orderupdate;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.order.OrderDelivery;
import com.monsor.feasthub.jpa.repository.OrderTransactionDeliveryRepository;
import com.monsor.feasthub.jpa.service.OrderTransactionDeliveryService;

@Service
public class OrderTransactionDeliveryAppService implements BaseService<OrderDelivery> {

	private static final Logger LOG = LoggerFactory.getLogger(OrderTransactionDeliveryAppService.class);
	@Autowired
	private OrderTransactionDeliveryService orderTxnDeliveryService;

	@Override
	public OrderDelivery findById(BigInteger id) {
		checkNotNull(id, "This id must not be null");
		return findById(id);
	}

	@Override
	public List<OrderDelivery> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside findAll OrderTransactionEvent :: ");
		}
		final OrderTransactionDeliveryRepository orderTxnDeliveryRepo = orderTxnDeliveryService
				.getOrderTransactionDeliveryRepository();

		return (List<OrderDelivery>) orderTxnDeliveryRepo.findAll();
	}

	@Override
	public OrderDelivery create(OrderDelivery entity) {
		final OrderTransactionDeliveryRepository orderTxnDeliveryRepo = orderTxnDeliveryService
				.getOrderTransactionDeliveryRepository();

		return orderTxnDeliveryRepo.save(entity);
	}

	@Override
	public OrderDelivery update(OrderDelivery entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(BigInteger id) {
		// TODO Auto-generated method stub

	}

}
