package com.monsor.feasthub.appservice;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.ExternalResourceEndpoint;
import com.monsor.feasthub.jpa.repository.ExternalResourceEndpointRepository;
import com.monsor.feasthub.jpa.service.ExternalResourceEndpointService;
/**
 * @author Amit
 *
 */

@Service
public class ExternalResourceEndpointAppService implements BaseService<ExternalResourceEndpoint> {

	private static final Logger LOG = LoggerFactory.getLogger(ExternalResourceEndpointAppService.class);
	@Autowired
	private ExternalResourceEndpointService externalresourceendpointService;

	public ExternalResourceEndpoint create(@Nonnull final ExternalResourceEndpoint externalresourceendpoint) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register externalresourceendpoint :: " + externalresourceendpoint);
		}
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();

		return externalresourceendpointRepository.save(externalresourceendpoint);
	}

	public ExternalResourceEndpoint update(@Nonnull final ExternalResourceEndpoint externalresourceendpoint) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register externalresourceendpoint :: " + externalresourceendpoint);
		}
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();
		return externalresourceendpointRepository.save(externalresourceendpoint);
	}

	public void delete(@Nonnull final BigInteger externalresourceendpointId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register externalresourceendpoint :: " + externalresourceendpointId);
		}
		checkNotNull(externalresourceendpointId, "This externalresourceendpointId must not be null");
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();
		final ExternalResourceEndpoint externalresourceendpoint = externalresourceendpointRepository
				.findOne(externalresourceendpointId);
		externalresourceendpointRepository.delete(externalresourceendpoint);
	}

	public List<ExternalResourceEndpoint> findAll() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register externalresourceendpoint :: ");
		}
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();
		return (List<ExternalResourceEndpoint>) externalresourceendpointRepository.findAll();
	}

	public ExternalResourceEndpoint findById(@Nonnull final BigInteger externalresourceendpointId) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside register externalresourceendpoint :: " + externalresourceendpointId);
		}
		checkNotNull(externalresourceendpointId, "This externalresourceendpointId must not be null");
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();
		final ExternalResourceEndpoint externalresourceendpoint = externalresourceendpointRepository
				.findOne(externalresourceendpointId);
		return externalresourceendpoint;
	}

	public Boolean isExternalResourceEndpointExists(@Nullable final BigInteger externalresourceendpointname) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside isExternalResourceEndpointExists");
		}
		if (externalresourceendpointname == null) {
			throw new BadRequestException("Please provide either externalresourceendpointname");
		}
		final ExternalResourceEndpointRepository externalresourceendpointRepository = externalresourceendpointService
				.getExternalResourceEndpointRepository();

		final ExternalResourceEndpoint ExternalResourceEndpoint = externalresourceendpointRepository
				.findOne(externalresourceendpointname);
		if (ExternalResourceEndpoint != null) {
			return true;
		}
		return false;
	}
}
