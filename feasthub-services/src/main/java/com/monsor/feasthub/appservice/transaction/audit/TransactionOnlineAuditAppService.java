package com.monsor.feasthub.appservice.transaction.audit;

import java.math.BigInteger;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monsor.feasthub.appservice.event.EventCuisineListingAppService;
import com.monsor.feasthub.common.BaseService;
import com.monsor.feasthub.jpa.model.audit.TransactionOnlineAudit;
import com.monsor.feasthub.jpa.repository.TransactionOnlineAuditRepository;
import com.monsor.feasthub.jpa.service.TransactionOnlineAuditService;

@Service
public class TransactionOnlineAuditAppService implements BaseService<TransactionOnlineAudit> {

	private static final Logger LOG = LoggerFactory.getLogger(EventCuisineListingAppService.class);
	@Autowired
	private TransactionOnlineAuditService onlineAuditService;

	@Override
	public TransactionOnlineAudit findById(BigInteger id) {
		final TransactionOnlineAuditRepository cashAuditRepository = onlineAuditService
				.getTrancationOnlineAuditRepository();
		return cashAuditRepository.findOne(id);
	}

	@Override
	public List<TransactionOnlineAudit> findAll() {
		final TransactionOnlineAuditRepository cashAuditRepository = onlineAuditService
				.getTrancationOnlineAuditRepository();
		return (List<TransactionOnlineAudit>) cashAuditRepository.findAll();
	}

	@Override
	public TransactionOnlineAudit create(TransactionOnlineAudit entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inside create CashAudit :: " + entity);
		}
		final TransactionOnlineAuditRepository cashAuditRepository = onlineAuditService
				.getTrancationOnlineAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public TransactionOnlineAudit update(TransactionOnlineAudit entity) {
		final TransactionOnlineAuditRepository cashAuditRepository = onlineAuditService
				.getTrancationOnlineAuditRepository();
		return cashAuditRepository.save(entity);
	}

	@Override
	public void delete(BigInteger id) {
		final TransactionOnlineAuditRepository cashAuditRepository = onlineAuditService
				.getTrancationOnlineAuditRepository();
		cashAuditRepository.delete(id);

	}

}
